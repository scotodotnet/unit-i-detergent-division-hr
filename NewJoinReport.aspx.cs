﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;

public partial class NewJoinReport : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Date = "";
    string SSQL;
    //string mvarUserType = "IF User";
    string mvarUserType;
    DataTable mDataSet = new DataTable();
    DataTable dt = new DataTable();
    BALDataAccess objdata = new BALDataAccess();
    DataTable AutoDataTable = new DataTable();
    string iEmpDet = "";
    //char[] delimiters = new char[] { "-",">" };

    string[] delimiters = { "-", ">" };

    string mUnitName = "";
    string strPhotoPath = "";
    FileStream fs;
    BinaryReader br;
    DataRow drow;
    string Empcode;
    DataSet ds = new DataSet();
    string Division;
    string FInancialYear;
    string FromDate;
    string ToDate;

    MailMessage Mail = new MailMessage("SCOTO <nldvnaga.hr@gmail.com>", "sambamurthyp@nagamills.com,prabhup@nagamills.com");
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-New Join Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("SalaryProcess"));
                //li.Attributes.Add("class", "droplink active open");
            }

            Date = Convert.ToDateTime(DateTime.Now.AddDays(-1)).ToString();
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            Division = Request.QueryString["Division"].ToString();
            FInancialYear = DateTime.Now.Year.ToString();
            FInancialYear = FInancialYear + "-01" + "-01";

            if (SessionUserType == "2")
            {
                GetEmployeeJoinTable();
                
            }
            else
            {
                GetEmployeeJoinTable();
              
            }
           

            ds.Tables.Add(AutoDataTable);
            if (AutoDataTable.Rows.Count > 0)
            {
                ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("crystal/New_Joiners_Report.rpt"));

                report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                //if (Division != "-Select-")
                //{
                //    report.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division:" + "'" + "'" + Division + "'";
                //}
                //else
                //{
                //    report.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division:" + "'";
                //}
                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = report;

                //string Server_Path = Server.MapPath("~");
                //string AttachfileName_Miss = "";
                ////  string Invoice_No_Str = WitSalesOrdNo.Replace("/", "_");
                //AttachfileName_Miss = Server_Path + "/Daily_Report/Daily_Employee_New_Joiners " + Convert.ToDateTime(Date).ToString("dd_MM_yyyy_HHmmss") + ".pdf";

                //if (File.Exists(AttachfileName_Miss))
                //{
                //    File.Delete(AttachfileName_Miss);
                //}

                //report.ExportToDisk(ExportFormatType.PortableDocFormat, AttachfileName_Miss);
                //report.Close();
                //report.Dispose();
            }
            else
            {
                Response.Write("<script>alert('No Records Found!!!')</Script>");
            }
        }
        //MailReport();
    }
    public void GetEmployeeJoinTable()
    {
        AutoDataTable.Columns.Add("Sno");
        AutoDataTable.Columns.Add("CompanyName");
        AutoDataTable.Columns.Add("LocationName");
        AutoDataTable.Columns.Add("EmpNo");
        AutoDataTable.Columns.Add("DeptName");
        AutoDataTable.Columns.Add("Designation");
        AutoDataTable.Columns.Add("Contractor");
        AutoDataTable.Columns.Add("Age");
        AutoDataTable.Columns.Add("EmpName");
        AutoDataTable.Columns.Add("Company");
        AutoDataTable.Columns.Add("Address");
        AutoDataTable.Columns.Add("Date");
        AutoDataTable.Columns.Add("ToDate");



        SSQL = "SELECT * FROM employee_mst WHERE DATE_FORMAT(DOJ, '%Y/%m/%d') between DATE_FORMAT('" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "','%Y/%m/%d') and DATE_FORMAT('" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "','%Y/%m/%d') and compCode='" + SessionCcode.ToString() + "' and loccode='" + SessionLcode.ToString() + "'";

        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
        SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        string name = dt.Rows[0]["CompName"].ToString();
        string Addres = dt.Rows[0]["Add1"].ToString();
        int sno = 1;
        if (mDataSet.Rows.Count > 0)
        {

            for (int iRow = 0; iRow < mDataSet.Rows.Count; iRow++)
            {
              
                AutoDataTable.NewRow();
                AutoDataTable.Rows.Add();


                AutoDataTable.Rows[iRow]["Sno"] = sno;
                AutoDataTable.Rows[iRow]["CompanyName"] = SessionCcode;
                AutoDataTable.Rows[iRow]["LocationName"] = SessionLcode;
                AutoDataTable.Rows[iRow]["EmpNo"] = mDataSet.Rows[iRow]["EmpNo"].ToString();
                AutoDataTable.Rows[iRow]["DeptName"] = mDataSet.Rows[iRow]["DeptName"].ToString();
                AutoDataTable.Rows[iRow]["Designation"] = mDataSet.Rows[iRow]["Designation"].ToString();
                AutoDataTable.Rows[iRow]["Contractor"] = mDataSet.Rows[iRow]["Contract"].ToString();
                AutoDataTable.Rows[iRow]["Age"] = mDataSet.Rows[iRow]["Age"].ToString();
                AutoDataTable.Rows[iRow]["Company"] = name.ToString();
                AutoDataTable.Rows[iRow]["Address"] = Addres.ToString();
                AutoDataTable.Rows[iRow]["EmpName"] = mDataSet.Rows[iRow]["FirstName"].ToString();
                AutoDataTable.Rows[iRow]["Date"] = DateTime.Parse(FromDate).ToString("dd/MM/yyyy");
                AutoDataTable.Rows[iRow]["ToDate"] = DateTime.Parse(ToDate).ToString("dd/MM/yyyy");

                sno += 1;

            }
        }
        //SSQL = "SELECT * FROM company_mst Where CompCode='" + SessionCcode + "'";

        //mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
        //if(mDataSet.Rows.Count>0)
        //{ 
        
        //}


    }
    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }
    private void MailReport()
    {
        //Mail.CC = "padmanaban";
        Mail.Subject = "Daily Employee New Joiners Report";
        Mail.Body = "Dear Sir, Please find the Attachement for (" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + ") New Joiners Report";
        // Mail.CC=new 
        // string FileName = Path.GetFileName(fuAttachment.PostedFile.FileName);
        string Server_Path = Server.MapPath("~");
        string AttachfileName_Miss = "";
        //  string Invoice_No_Str = WitSalesOrdNo.Replace("/", "_");
        AttachfileName_Miss = Server_Path + "/Daily_Report/Daily_Employee_New_Joiners " + Convert.ToDateTime(Date).ToString("dd_MM_yyyy_HHmmss") + ".pdf";
        if (File.Exists(AttachfileName_Miss))
        {
            Mail.Attachments.Add(new Attachment(AttachfileName_Miss));
        }
       
        Mail.IsBodyHtml = false;
        SmtpClient smtp = new SmtpClient();
        smtp.Host = "smtp.gmail.com";
        smtp.EnableSsl = true;
        NetworkCredential NetworkCred = new NetworkCredential("nldvnaga.hr@gmail.com", "Altius2005");
        smtp.UseDefaultCredentials = false;
        smtp.Credentials = NetworkCred;
        smtp.Port = 587;
        smtp.Send(Mail);
        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('Email sent.');", true);
        //File.Delete(Server_Path + "/Daily_Report/Daily_Attendance_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy") + ".pdf");
        //File.Delete(Server_Path + "/Daily_Report/Daily_Attendance_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy") + ".pdf");
        //File.Delete(Server_Path + "/Daily_Report/SalaryConsolidate_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy") + ".pdf");
        //File.Delete(Server_Path + "/Daily_Report/Daily_Improper_Punch_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy") + ".pdf");
    }
}