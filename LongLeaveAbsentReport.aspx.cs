﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;

public partial class LongLeaveAbsentReport : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Date = "";
    string SSQL;
    //string mvarUserType = "IF User";
    string mvarUserType;
    DataTable mDataSet = new DataTable();
    DataTable dt = new DataTable();
    BALDataAccess objdata = new BALDataAccess();
    DataTable AutoDataTable = new DataTable();
    string iEmpDet = "";
    //char[] delimiters = new char[] { "-",">" };

    string[] delimiters = { "-", ">" };

    string mUnitName = "";
    string strPhotoPath = "";
    FileStream fs;
    BinaryReader br;
    DataRow drow;
    string Empcode;
    DataSet ds = new DataSet();
    string Division;
    string FInancialYear;
    string FromDate = "";
    string ToDate = "";

    MailMessage Mail = new MailMessage("SCOTO <nldvnaga.hr@gmail.com>", "prabhup@nagamills.com,sasikumar@nagamills.com");
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report- Employee Long Leave Absent Report";

            }

            Date = Convert.ToDateTime(DateTime.Now.AddDays(-1)).ToString();
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();

            SessionUserType = Session["Isadmin"].ToString();

            Division = Request.QueryString["Division"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            if (SessionUserType == "2")
            {
                GetLongLeaveAbsentTable();

            }
            else
            {
                GetLongLeaveAbsentTable();

            }


            ds.Tables.Add(AutoDataTable);
            if (AutoDataTable.Rows.Count > 0)
            {
                ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("crystal/LongLeave_AbsentReport.rpt"));

                report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                //if (Division != "-Select-")
                //{
                //    report.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division:" + "'" + "'" + Division + "'";
                //}
                //else
                //{
                report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate + "'";
                report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate + "'";
                //}
                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = report;
                //   report.Dispose();
            }
            else
            {
                Response.Write("<script>alert('No Records Found!!!')</Script>");
            }
        }
        // MailReport();
    }
    public void GetLongLeaveAbsentTable()
    {
        AutoDataTable.Columns.Add("Sno");
        AutoDataTable.Columns.Add("CompanyName");
        AutoDataTable.Columns.Add("LocationName");
        AutoDataTable.Columns.Add("TokenNo");
        AutoDataTable.Columns.Add("EmpName");
        AutoDataTable.Columns.Add("DeptName");
        AutoDataTable.Columns.Add("Designation");
        AutoDataTable.Columns.Add("Contractor");
        AutoDataTable.Columns.Add("MobileNo");
        AutoDataTable.Columns.Add("Company");
        AutoDataTable.Columns.Add("Address");
        AutoDataTable.Columns.Add("Date");

        SSQL = "";
        SSQL = "SELECT LD.ExistingCode,LD.FirstName,EM.EmployeeMobile,EM.Deptname,EM.Designation,EM.Contract FROM logtime_days LD INNER JOIN employee_mst EM ON EM.MachineID=LD.MachineID WHERE LD.Present='0.0'";
        SSQL = SSQL + " AND LD.Attn_Date BETWEEN DATE_FORMAT('" + Convert.ToDateTime(FromDate).ToString("yyyy-MM-dd") + "','%Y-%m-%d') and";
        SSQL = SSQL + " DATE_FORMAT('" + Convert.ToDateTime(ToDate).ToString("yyyy-MM-dd") + "','%Y-%m-%d') AND EM.Deptname!='SECURITY' GROUP BY LD.ExistingCode,LD.FirstName HAVING COUNT(LD.Present) > 2";

        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
        SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        string name = dt.Rows[0]["CompName"].ToString();
        string Addres = dt.Rows[0]["Add1"].ToString();
        int sno = 1;
        if (mDataSet.Rows.Count > 0)
        {

            for (int iRow = 0; iRow < mDataSet.Rows.Count; iRow++)
            {

                AutoDataTable.NewRow();
                AutoDataTable.Rows.Add();


                AutoDataTable.Rows[iRow]["Sno"] = sno;
                AutoDataTable.Rows[iRow]["CompanyName"] = SessionCcode;
                AutoDataTable.Rows[iRow]["LocationName"] = SessionLcode;
                AutoDataTable.Rows[iRow]["TokenNo"] = mDataSet.Rows[iRow]["ExistingCode"].ToString();
                AutoDataTable.Rows[iRow]["EmpName"] = mDataSet.Rows[iRow]["FirstName"].ToString();
                AutoDataTable.Rows[iRow]["DeptName"] = mDataSet.Rows[iRow]["DeptName"].ToString();
                AutoDataTable.Rows[iRow]["Designation"] = mDataSet.Rows[iRow]["Designation"].ToString();
                AutoDataTable.Rows[iRow]["Contractor"] = mDataSet.Rows[iRow]["Contract"].ToString();
                AutoDataTable.Rows[iRow]["MobileNo"] = mDataSet.Rows[iRow]["EmployeeMobile"].ToString();
                AutoDataTable.Rows[iRow]["Company"] = name.ToString();
                AutoDataTable.Rows[iRow]["Address"] = Addres.ToString();
                AutoDataTable.Rows[iRow]["Date"] = DateTime.Parse(FromDate).ToString("dd/MM/yyyy");

                sno += 1;

            }
        }

    }
    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }
    private void MailReport()
    {
        //Mail.CC = "padmanaban";
        Mail.Subject = "Daily Employee Long Leave Absent Report";
        Mail.Body = "Dear Sir, Please find the Attachement for (" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + ") Employee Long Leave Absent Report";

        string Server_Path = Server.MapPath("~");
        string AttachfileName_Miss = "";
        //  string Invoice_No_Str = WitSalesOrdNo.Replace("/", "_");
        AttachfileName_Miss = Server_Path + "/Daily_Report/Daily_Employee_Long_Leave " + Convert.ToDateTime(Date).ToString("dd_MM_yyyy_HHmmss") + ".pdf";
        if (File.Exists(AttachfileName_Miss))
        {
            Mail.Attachments.Add(new Attachment(AttachfileName_Miss));
        }

        Mail.IsBodyHtml = false;
        SmtpClient smtp = new SmtpClient();
        smtp.Host = "smtp.gmail.com";
        smtp.EnableSsl = true;
        NetworkCredential NetworkCred = new NetworkCredential("nldvnaga.hr@gmail.com", "Altius2005");
        smtp.UseDefaultCredentials = false;
        smtp.Credentials = NetworkCred;
        smtp.Port = 587;
        smtp.Send(Mail);
        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('Email sent.');", true);
        //File.Delete(Server_Path + "/Daily_Report/Daily_Attendance_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy") + ".pdf");
        //File.Delete(Server_Path + "/Daily_Report/Daily_Attendance_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy") + ".pdf");
        //File.Delete(Server_Path + "/Daily_Report/SalaryConsolidate_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy") + ".pdf");
        //File.Delete(Server_Path + "/Daily_Report/Daily_Improper_Punch_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy") + ".pdf");
    }
}