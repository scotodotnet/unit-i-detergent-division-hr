﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;


public partial class DailyMDMail : System.Web.UI.Page
{
    string ShiftType1 = "";
    string Date = "";
    string Status = "";

    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    // string SessionRights;
    string SessionUserType;
    string ss = "";
    ReportDocument report = new ReportDocument();
    ReportDocument report2 = new ReportDocument();
    ReportDocument report3 = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();



    string SSQL = "";
    string Adolescent_Shift;
    DataSet ds = new DataSet();

    string Division = "";
    DataTable AutoDTable = new DataTable();
    DataTable DataCell = new DataTable();
    DataTable dt = new DataTable();


    string Datestr = "";
    string Datestr1 = "";
    string ShiftType = "";
    DataTable mDataSet = new DataTable();

    DataTable mLocalDS_INTAB = new DataTable();
    DataTable mLocalDS_OUTTAB = new DataTable();
    string Date_Value_Str;
    string Time_IN_Str = "";
    string Time_Out_Str = "";
    int time_Check_dbl = 0;
    string Total_Time_get = "";

    DataRow dtRow;
    //DataTable dt = new DataTable();
    DataTable dt1 = new DataTable();
    DataTable dt2 = new DataTable();
    DataTable dt3 = new DataTable();
    // BALDataAccess objdata = new BALDataAccess();
    DataTable AutoDataTable = new DataTable();
    DataTable AutoDataTable1 = new DataTable();
    DataTable AutoDataTable2 = new DataTable();
    DataTable MLocal_Day = new DataTable();
    DataTable Payroll_DS = new DataTable();

    string Shift_Start_Time;
    string Shift_End_Time;
    string Employee_Time = "";
    DateTime ShiftdateStartIN = new DateTime();
    DateTime ShiftdateEndIN = new DateTime();
    DateTime EmpdateIN = new DateTime();
    string ColumnName = "";
    double Totol_Hours_Check = 0;
    double Totol_Hours_Check_1 = 8.2;
    string[] Time_Split_Str;
    string Time_IN_Get;
    string Time_Out_Update = "";
    long Random_No_Get;
    string[] Time_Minus_Value_Check;
    DataTable DataCells = new DataTable();

    MailMessage Mail = new MailMessage("SCOTO <nldvnaga.hr@gmail.com>", "jayalalitha@nagamills.com,prabhup@nagamills.com,sasikumar@nagamills.com,munusamyk@nagamills.com,thirumalaisamy@nagamills.com,renganathanm@nagamills.com,padmanabhanscoto@gmail.com");
   

    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Session["Isadmin"] == null)
        //{
        //    Response.Redirect("Default.aspx");
        //    Response.Write("Your session expired");
        //}
        //else
        //{
        if (!IsPostBack)
        {

            Page.Title = "Spay Module | MD Mailing Report";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
            //li.Attributes.Add("class", "droplink active open");
        }

        Date = Convert.ToDateTime(DateTime.Now.AddDays(-1)).ToString();
        //Date = Convert.ToDateTime("02/07/2021").ToString();
        SessionCcode = "NAGA";
        SessionLcode = "UNIT I";
        //SessionAdmin = Session["Isadmin"].ToString();
        //SessionRights = Session["Rights"].ToString();
        //SessionUserType = Session["Isadmin"].ToString();

        GetAttdDayWise_Change();

        GetImproperPunch();

        SalaryConsolidate();

        GetShiftManPowerTable1("logtime_bar_plant");
        GetShiftManPowerTable2("logtime_powder_plant");
        GetShiftManPowerTable3("logtime_tsp_plant");

        SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        string name = dt.Rows[0]["CompName"].ToString();
        string Addres = dt.Rows[0]["Add1"].ToString();

        for (int i = AutoDataTable.Rows.Count - 1; i >= 0; i--)
        {
            if (AutoDataTable.Rows[i]["CompanyName"] == DBNull.Value && AutoDataTable.Rows[i]["LocationName"] == DBNull.Value)
            {
                AutoDataTable.Rows[i].Delete();
            }
        }
        AutoDataTable.AcceptChanges();
        ds.Tables.Add(AutoDataTable);
        for (int i = AutoDataTable1.Rows.Count - 1; i >= 0; i--)
        {
            if (AutoDataTable1.Rows[i]["CompanyName"] == DBNull.Value && AutoDataTable1.Rows[i]["LocationName"] == DBNull.Value)
            {
                AutoDataTable1.Rows[i].Delete();
            }
        }
        AutoDataTable1.AcceptChanges();
        ds.Tables.Add(AutoDataTable1);
        for (int i = AutoDataTable2.Rows.Count - 1; i >= 0; i--)
        {
            if (AutoDataTable2.Rows[i]["CompanyName"] == DBNull.Value && AutoDataTable2.Rows[i]["LocationName"] == DBNull.Value)
            {
                AutoDataTable2.Rows[i].Delete();
            }
        }
        AutoDataTable2.AcceptChanges();
        ds.Tables.Add(AutoDataTable2);

        //ds.Tables.Add(AutoDataTable);
        //ds.Tables.Add(AutoDataTable1);
        //ds.Tables.Add(AutoDataTable2);
        AutoDataTable.Merge(AutoDataTable1);
        AutoDataTable.Merge(AutoDataTable2);
        if (AutoDataTable.Rows.Count > 0)
        {
           
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/PlantWiseContractorStrength.rpt"));

            report.DataDefinition.FormulaFields["Company"].Text = "'" + name + "'";
            report.DataDefinition.FormulaFields["Address"].Text = "'" + Addres + "'";
            report.DataDefinition.FormulaFields["LocCode"].Text = "'" + SessionLcode + "'";
            report.DataDefinition.FormulaFields["FromDate"].Text = "'" + DateTime.Parse(Date).ToString("dd/MM/yyyy") + "'";

            report.Database.Tables[0].SetDataSource(AutoDataTable);

            string Server_Path = Server.MapPath("~");
            string AttachfileName_Miss = "";
            //  string Invoice_No_Str = WitSalesOrdNo.Replace("/", "_");
            AttachfileName_Miss = Server_Path + "/Daily_Report/PlantWiseContractorStrength_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy_HHmmss") + ".pdf";

            if (File.Exists(AttachfileName_Miss))
            {
                File.Delete(AttachfileName_Miss);
            }

            report.ExportToDisk(ExportFormatType.PortableDocFormat, AttachfileName_Miss);
            report.Close();

        }
        else
        {


        }

       MailReport();

        // }
    }
    //protected void Page_Unload(object sender, EventArgs e)
    //{
    //    CrystalReportViewer1.Dispose();
    //    CrystalReportViewer2.Dispose();
    //    CrystalReportViewer3.Dispose();
    //}

    private void MailReport()
    {
        //Mail.CC = "padmanaban";
        Mail.Subject = "Daily Attendance Report";
        Mail.Body = "Dear Sir, Please find the Attachement for (" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + ") Attendance Report";
        // Mail.CC=new 
        // string FileName = Path.GetFileName(fuAttachment.PostedFile.FileName);
        string Server_Path = Server.MapPath("~");
        string AttachfileName_Miss = "";
        //  string Invoice_No_Str = WitSalesOrdNo.Replace("/", "_");
        AttachfileName_Miss = Server_Path + "/Daily_Report/Daily_Attendance_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy_HHmmss") + ".pdf";
        if (File.Exists(AttachfileName_Miss))
        {
            Mail.Attachments.Add(new Attachment(AttachfileName_Miss));
        }
        AttachfileName_Miss = Server_Path + "/Daily_Report/SalaryConsolidate_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy_HHmmss") + ".pdf";
        if (File.Exists(AttachfileName_Miss))
        {
            Mail.Attachments.Add(new Attachment(AttachfileName_Miss));
        }
        AttachfileName_Miss = Server_Path + "/Daily_Report/Daily_Improper_Punch_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy_HHmmss") + ".pdf";
        if (File.Exists(AttachfileName_Miss))
        {
            Mail.Attachments.Add(new Attachment(AttachfileName_Miss));
        }
        AttachfileName_Miss = Server_Path + "/Daily_Report/PlantWiseContractorStrength_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy_HHmmss") + ".pdf";
        if (File.Exists(AttachfileName_Miss))
        {
            Mail.Attachments.Add(new Attachment(AttachfileName_Miss));
        }
        Mail.IsBodyHtml = false;
        SmtpClient smtp = new SmtpClient();
        smtp.Host = "smtp.gmail.com";
        smtp.EnableSsl = true;
        NetworkCredential NetworkCred = new NetworkCredential("nldvnaga.hr@gmail.com", "Altius2005");
        smtp.UseDefaultCredentials = false;
        smtp.Credentials = NetworkCred;
        smtp.Port = 587;
        smtp.Send(Mail);
        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('Email sent.');", true);
        //File.Delete(Server_Path + "/Daily_Report/Daily_Attendance_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy") + ".pdf");
        //File.Delete(Server_Path + "/Daily_Report/Daily_Attendance_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy") + ".pdf");
        //File.Delete(Server_Path + "/Daily_Report/SalaryConsolidate_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy") + ".pdf");
        //File.Delete(Server_Path + "/Daily_Report/Daily_Improper_Punch_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy") + ".pdf");
    }

    private void SalaryConsolidate()
    {
        string TableName = "";

        string GrandTotal = "";
        DataTable AnutoDT = new DataTable();
        string GrandTotalWages = "";

        int generalcount = 0;
        int shift1count = 0;
        int shift2count = 0;
        int shift3count = 0;
        int shift4count = 0;
        int shift5count = 0;
        int shift6count = 0;
        int noshiftcount = 0;

        int general = 0;
        int shift1 = 0;
        int shift2 = 0;
        int shift3 = 0;
        int shift4 = 0;
        int shift5 = 0;
        int shift6 = 0;
        int noshift = 0;

        if (Status == "Pending")
        {
            TableName = "Employee_Mst_New_Emp";
        }

        else
        {
            TableName = "Employee_Mst";
        }

        decimal rounded = 0;
        decimal roundedsalary = 0;
        decimal roundedhalfdaysalary = 0;
        decimal staffonedaysalary;
        // AnutoDT.Reset();

        AnutoDT.Columns.Add("ExCode");
        AnutoDT.Columns.Add("Dept");
        AnutoDT.Columns.Add("Name");
        AnutoDT.Columns.Add("TimeIN");
        AnutoDT.Columns.Add("TimeOUT");
        AnutoDT.Columns.Add("DayWages");
        AnutoDT.Columns.Add("TotalMIN");
        AnutoDT.Columns.Add("Wages");
        AnutoDT.Columns.Add("Shift");
        AnutoDT.Columns.Add("SubCatName");
        AnutoDT.Columns.Add("GradeName");
        AnutoDT.Columns.Add("OTSal");
        AnutoDT.Columns.Add("Total");

        AnutoDT.Columns.Add("GradeTotalCount");
        AnutoDT.Columns.Add("GeneralCount");
        AnutoDT.Columns.Add("Shift1Count");
        AnutoDT.Columns.Add("Shift2Count");
        AnutoDT.Columns.Add("Shift3Count");

        AnutoDT.Columns.Add("ShiftWages");

        AnutoDT.Columns.Add("OTHours");

        DataTable Shift_DS = new DataTable();

        SSQL = "select EM.MachineID,EM.Wages,DEP.DeptName,EM.Shift,COALESCE(EM.FirstName,'') as FirstName,EM.TimeIN,EM.TimeOUT,";
        SSQL = SSQL + "DEP.SubCatName,EM.Total_Hrs,EM.Total_Hrs1,DEP.CatName,DEP.BaseSalary,DEP.StdWrkHrs,DEP.OTEligible,DEP.IsActive from LogTime_Days EM  inner join Employee_Mst DEP on  EM.MachineID= DEP.MachineID ";
        SSQL = SSQL + " where EM.CompCode='" + SessionCcode + "' ANd EM.LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And DEP.CompCode='" + SessionCcode + "' ANd DEP.LocCode='" + SessionLcode + "'";

        SSQL = SSQL + " And EM.Shift!='Leave'";
        SSQL = SSQL + " And DEP.DeptName!='CONTRACT'";

        if (Date != "")
        {
            SSQL = SSQL + " And DATE_FORMAT(EM.Attn_Date,'%Y-%m-%d')= DATE_FORMAT('" + Convert.ToDateTime(Date).ToString("yyyy-MM-dd") + "','%Y-%m-%d')";
        }
        SSQL = SSQL + " And  (EM.Present_Absent='Present' or EM.Present_Absent='Half Day') ";

        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "";
        SSQL = "Select Count(*) from LogTime_Days where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        if (Date != "")
        {
            SSQL = SSQL + " And Attn_Date= DATE_FORMAT('" + Convert.ToDateTime(Date).ToString("yyyy-MM-dd") + "','%Y-%m-%d')";
        }
        SSQL = SSQL + " and   (TypeName='Improper' OR TypeName='Absent') AND Division = '" + Division + "'";

        DataTable imp_dt = new DataTable();
        imp_dt = objdata.RptEmployeeMultipleDetails(SSQL);
        int impcount = 0;

        if (imp_dt.Rows.Count > 0)
        {
            impcount = Convert.ToInt32(imp_dt.Rows[0][0].ToString());
        }

        string staffsalary = "";
        string staffwages = "";

        string TempWorkHours = "";

        string[] TempWorkHourSlip;
        string Machineid = "";
        string Deptname = "";
        string workinghours = "";
        string stdwrkhrs = "";
        decimal staffonehoursalary;
        decimal labourhalfDaysalary;
        decimal Totalwages;
        staffonedaysalary = 0;
        labourhalfDaysalary = 0;
        string OThours = ""; //Edit by Narmatha
        string OTSal = "0"; //Edit by Narmatha

        string OTEligible = "";//By Selva
        string IsActive = "";//By Selva

        DataTable dt1 = new DataTable();

        if (dt.Rows.Count > 0)
        {
            for (int k = 0; k < dt.Rows.Count; k++)
            {
                Machineid = dt.Rows[k]["MachineID"].ToString();
                staffwages = dt.Rows[k]["CatName"].ToString();
                Deptname = dt.Rows[k]["DeptName"].ToString();
                stdwrkhrs = dt.Rows[k]["StdWrkHrs"].ToString();

                OTEligible = dt.Rows[k]["OTEligible"].ToString();//By Selva
                IsActive = dt.Rows[k]["IsActive"].ToString();//By Selva

                OTSal = "0";//Edit by Narmatha

                if (stdwrkhrs == "" || stdwrkhrs == "0")
                {
                    stdwrkhrs = "8";
                }

                //Check Above 8 hours conditions
                TempWorkHours = dt.Rows[k]["Total_Hrs1"].ToString();
                TempWorkHourSlip = TempWorkHours.Split(':');

                workinghours = dt.Rows[k]["Total_Hrs"].ToString();
                OThours = "0"; //Edit by Narmatha


               
                if (Machineid == "920016")
                {
                    Machineid = "920016";
                }
                if (staffwages == "STAFF" || Deptname == "FITTER & ELECTRICIANS" || Deptname == "SECURITY" || Deptname == "DRIVERS")
                {
                    if (dt.Rows[k]["OTEligible"].ToString().Trim() == "1")
                    {
                        staffsalary = dt.Rows[k]["BaseSalary"].ToString();
                        staffonedaysalary = Convert.ToDecimal(staffsalary.ToString()) / 26;
                        roundedsalary = (Math.Round(staffonedaysalary, 0, MidpointRounding.AwayFromZero));

                        //OT salary for Labour by Narmatha
                        string workinghours_New = (Convert.ToDecimal(workinghours) - Convert.ToDecimal(OThours)).ToString();

                        decimal check = 0;
                        check = Convert.ToDecimal(workinghours.ToString());
                        check = (Math.Round(check, 0, MidpointRounding.AwayFromZero));

                        if (Convert.ToDecimal(check) >= Convert.ToDecimal(stdwrkhrs))
                        {
                            rounded = (Convert.ToDecimal(roundedsalary) / Convert.ToDecimal(8));
                            rounded = (Convert.ToDecimal(check) * Convert.ToDecimal(rounded));
                            rounded = (Math.Round(rounded, 0, MidpointRounding.AwayFromZero));

                            OThours = (Convert.ToDecimal(workinghours_New) - Convert.ToDecimal(stdwrkhrs)).ToString();
                        }
                        else if (check >= 4 || check < Convert.ToDecimal(stdwrkhrs))
                        {
                            staffonehoursalary = staffonedaysalary / 2;
                            //Totalwages = staffonehoursalary * Convert.ToDecimal(workinghours.ToString());
                            rounded = (Math.Round(staffonehoursalary, 0, MidpointRounding.AwayFromZero));
                        }
                        else if (check <= 4)
                        {
                            rounded = 0;
                        }
                    }
                    else
                    {
                        staffsalary = dt.Rows[k]["BaseSalary"].ToString();
                        staffonedaysalary = Convert.ToDecimal(staffsalary.ToString()) / 26;
                        roundedsalary = (Math.Round(staffonedaysalary, 0, MidpointRounding.AwayFromZero));


                        //staffsalary = dt.Rows[k]["BaseSalary"].ToString();
                        //staffonedaysalary = Convert.ToDecimal(staffsalary.ToString()) / 26;
                        //roundedsalary = (Math.Round(staffonedaysalary, 0, MidpointRounding.AwayFromZero));
                        //labourhalfDaysalary = Convert.ToDecimal(staffonedaysalary.ToString()) / 2;
                        //roundedhalfdaysalary = (Math.Round(labourhalfDaysalary, 0, MidpointRounding.AwayFromZero));

                        //SSQL = "select (Total_Hrs1-8) as OTHrs,Total_Hrs1, EM.MachineID,EM.Wages,DEP.DeptName,EM.Shift,COALESCE(EM.FirstName,'') as FirstName,EM.TimeIN,EM.TimeOUT,";
                        //SSQL = SSQL + "DEP.SubCatName,EM.Total_Hrs,EM.Total_Hrs1,DEP.CatName,DEP.BaseSalary,DEP.StdWrkHrs,DEP.OTEligible,DEP.IsActive from LogTime_Days EM  inner join Employee_Mst DEP on  EM.MachineID= DEP.MachineID ";
                        //SSQL = SSQL + " where EM.CompCode='" + SessionCcode + "' ANd EM.LocCode='" + SessionLcode + "'";
                        //SSQL = SSQL + " And DEP.CompCode='" + SessionCcode + "' ANd DEP.LocCode='" + SessionLcode + "'";

                        //SSQL = SSQL + " And EM.MachineID='" + dt.Rows[k]["MachineID"] + "' And EM.Shift!='Leave'";
                        //SSQL = SSQL + " And DEP.DeptName!='CONTRACT'";

                        //if (Date != "")
                        //{
                        //    SSQL = SSQL + " And DATE_FORMAT(EM.Attn_Date,'%Y-%m-%d')= DATE_FORMAT('" + Convert.ToDateTime(Date).ToString("yyyy-MM-dd") + "','%Y-%m-%d')";
                        //}
                        //SSQL = SSQL + " And  (EM.Present_Absent='Present' or EM.Present_Absent='Half Day') And Total_Hrs1 > '11:56' AND Total_Hrs1 < '15:00' ORDER BY Total_Hrs1 ASC";

                        //dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

                        //if (dt.Rows.Count != 0)
                        //{
                        //    OThours = dt.Rows[k]["OTHrs"].ToString();
                        //    OTSal = (Convert.ToDecimal(roundedsalary) + Convert.ToDecimal(roundedhalfdaysalary)).ToString();

                        //}

                        //OT salary for Labour by Narmatha
                        string workinghours_New = (Convert.ToDecimal(workinghours)).ToString();

                        decimal check = 0;
                        check = Convert.ToDecimal(workinghours.ToString());
                        check = (Math.Round(check, 0, MidpointRounding.AwayFromZero));

                        if (Convert.ToDecimal(check) >= Convert.ToDecimal(stdwrkhrs))
                        {
                            rounded = (Convert.ToDecimal(roundedsalary) / Convert.ToDecimal(8));
                            rounded = (Convert.ToDecimal(8) * Convert.ToDecimal(rounded));
                            rounded = (Math.Round(rounded, 0, MidpointRounding.AwayFromZero));

                            OThours = "0";
                        }
                        else if (check >= 4 || check < Convert.ToDecimal(stdwrkhrs))
                        {
                            staffonehoursalary = staffonedaysalary / 8;
                            Totalwages = staffonehoursalary * Convert.ToDecimal(workinghours.ToString());
                            rounded = (Math.Round(Totalwages, 0, MidpointRounding.AwayFromZero));
                        }
                        else if (check <= 4)
                        {
                            rounded = 0;
                        }
                    }
                }
                else
                {
                    if (dt.Rows[k]["MachineID"].ToString() == "631")
                    {
                        string pass = "";
                    }
                    DataTable da_Days = new DataTable();
                   

                    //Check Above 8 hours conditions
                    TempWorkHours = dt.Rows[k]["Total_Hrs1"].ToString();
                    string WHours = TempWorkHours.Replace(':', '.');


                    if ((OTEligible == "Yes" && IsActive == "Yes" || OTEligible == "YES"))
                    {
                        if (Convert.ToDecimal(WHours) >= 11.55M && Convert.ToDecimal(WHours) <= 15.55M)
                        {
                            //OThours = (Convert.ToDecimal(workinghours_New) - Convert.ToDecimal(stdwrkhrs)).ToString();
                            //workinghours = (Convert.ToDecimal(workinghours_New) - Convert.ToDecimal(OThours)).ToString();
                            OThours = "4";

                            staffsalary = dt.Rows[k]["BaseSalary"].ToString();
                            staffonedaysalary = Convert.ToDecimal(staffsalary.ToString());
                            roundedsalary = (Math.Round(staffonedaysalary, 0, MidpointRounding.AwayFromZero));

                            labourhalfDaysalary = Convert.ToDecimal(staffsalary.ToString()) / 2;
                            roundedhalfdaysalary = (Math.Round(labourhalfDaysalary, 0, MidpointRounding.AwayFromZero));

                            //staffonehoursalary = staffonedaysalary / Convert.ToDecimal(7);
                            Totalwages = Convert.ToDecimal(roundedsalary.ToString());

                            OTSal = Convert.ToDecimal(labourhalfDaysalary.ToString()).ToString();
                            OTSal = Math.Round(Convert.ToDecimal(OTSal), 0, MidpointRounding.AwayFromZero).ToString();

                            //Stdworking hrs Salary plus OT Sal = TotalWages by Narmatha
                            Totalwages = Totalwages + Convert.ToDecimal(OTSal.ToString());

                            rounded = (Math.Round(Totalwages, 0, MidpointRounding.AwayFromZero));
                        }
                        else if (Convert.ToDecimal(WHours) >= 15.55M && Convert.ToDecimal(WHours) <= 20.00M)
                        {
                            OThours = "8";
                            staffsalary = dt.Rows[k]["BaseSalary"].ToString();
                            staffonedaysalary = Convert.ToDecimal(staffsalary.ToString());
                            roundedsalary = (Math.Round(staffonedaysalary, 0, MidpointRounding.AwayFromZero));

                            labourhalfDaysalary = Convert.ToDecimal(staffsalary.ToString()) / 2;
                            roundedhalfdaysalary = (Math.Round(labourhalfDaysalary, 0, MidpointRounding.AwayFromZero));

                            //staffonehoursalary = staffonedaysalary / Convert.ToDecimal(7);
                            Totalwages = Convert.ToDecimal(roundedsalary.ToString());

                            OTSal = Convert.ToDecimal(staffonedaysalary.ToString()).ToString();
                            OTSal = Math.Round(Convert.ToDecimal(OTSal), 0, MidpointRounding.AwayFromZero).ToString();

                            //Stdworking hrs Salary plus OT Sal = TotalWages by Narmatha
                            Totalwages = Totalwages + Convert.ToDecimal(OTSal.ToString());

                            rounded = (Math.Round(Totalwages, 0, MidpointRounding.AwayFromZero));
                        }

                        else
                        {
                            OThours = "0";
                            staffsalary = dt.Rows[k]["BaseSalary"].ToString();
                            staffonedaysalary = Convert.ToDecimal(staffsalary.ToString());
                            roundedsalary = (Math.Round(staffonedaysalary, 0, MidpointRounding.AwayFromZero));

                            labourhalfDaysalary = Convert.ToDecimal(staffsalary.ToString()) / 2;
                            roundedhalfdaysalary = (Math.Round(labourhalfDaysalary, 0, MidpointRounding.AwayFromZero));

                            //staffonehoursalary = staffonedaysalary / Convert.ToDecimal(7);
                            Totalwages = Convert.ToDecimal(roundedsalary.ToString());

                            OTSal = "0";
                            OTSal = "0";

                            //Stdworking hrs Salary plus OT Sal = TotalWages by Narmatha
                            Totalwages = Totalwages + Convert.ToDecimal(OTSal.ToString());

                            rounded = (Math.Round(Totalwages, 0, MidpointRounding.AwayFromZero));
                        }
                    }
                        //By Selva ^

                        //if (dt.Rows[k]["Shift"].ToString().Trim().ToUpper() == "SHIFT3")
                        //{
                        //    staffonehoursalary = staffonedaysalary / Convert.ToDecimal(7);
                        //    Totalwages = staffonehoursalary * Convert.ToDecimal(workinghours.ToString());

                        //    OTSal = (Convert.ToDecimal(staffonehoursalary) * Convert.ToDecimal(OThours.ToString())).ToString();
                        //    OTSal = Math.Round(Convert.ToDecimal(OTSal), 0, MidpointRounding.AwayFromZero).ToString();

                        //    //Stdworking hrs Salary plus OT Sal = TotalWages by Narmatha
                        //    Totalwages = Totalwages + Convert.ToDecimal(OTSal.ToString());

                        //    rounded = (Math.Round(Totalwages, 0, MidpointRounding.AwayFromZero));
                        //}
                        //else
                        //{
                        //    staffonehoursalary = staffonedaysalary / Convert.ToDecimal(stdwrkhrs.ToString());
                        //    Totalwages = staffonehoursalary * Convert.ToDecimal(workinghours.ToString());

                        //    OTSal = (Convert.ToDecimal(staffonehoursalary) * Convert.ToDecimal(OThours.ToString())).ToString();
                        //    OTSal = Math.Round(Convert.ToDecimal(OTSal), 0, MidpointRounding.AwayFromZero).ToString();

                        //    //Stdworking hrs Salary plus OT Sal = TotalWages by Narmatha
                        //    Totalwages = Totalwages + Convert.ToDecimal(OTSal.ToString());

                        //    rounded = (Math.Round(Totalwages, 0, MidpointRounding.AwayFromZero));
                        //}
                   
                }


                AnutoDT.NewRow();
                AnutoDT.Rows.Add();
                AnutoDT.Rows[k]["ExCode"] = dt.Rows[k]["MachineID"].ToString();
                AnutoDT.Rows[k]["Dept"] = dt.Rows[k]["DeptName"].ToString();
                AnutoDT.Rows[k]["Name"] = dt.Rows[k]["FirstName"].ToString();
                AnutoDT.Rows[k]["TimeIN"] = dt.Rows[k]["TimeIN"].ToString();
                AnutoDT.Rows[k]["TimeOUT"] = dt.Rows[k]["TimeOUT"].ToString();
                AnutoDT.Rows[k]["DayWages"] = rounded;

                AnutoDT.Rows[k]["TotalMIN"] = workinghours;// dt.Rows[k]["Total_Hrs"].ToString();
                AnutoDT.Rows[k]["Wages"] = roundedsalary.ToString();
                AnutoDT.Rows[k]["Shift"] = dt.Rows[k]["Shift"].ToString();
                AnutoDT.Rows[k]["SubCatName"] = dt.Rows[k]["SubCatName"].ToString();
                AnutoDT.Rows[k]["GradeName"] = "A+";
                AnutoDT.Rows[k]["OTHours"] = OThours;
                AnutoDT.Rows[k]["OTSal"] = OTSal;


                if (dt.Rows[k]["Shift"].ToString() == "GENERAL")
                {
                    generalcount = generalcount + 1;
                    general = Convert.ToInt32(Convert.ToDecimal(general) + Convert.ToDecimal(rounded));
                }
                else if (dt.Rows[k]["Shift"].ToString() == "SHIFT1")
                {
                    shift1count = shift1count + 1;
                    shift1 = Convert.ToInt32(Convert.ToDecimal(shift1) + Convert.ToDecimal(rounded));
                }
                else if (dt.Rows[k]["Shift"].ToString() == "SHIFT2")
                {
                    shift2count = shift2count + 1;
                    shift2 = Convert.ToInt32(Convert.ToDecimal(shift2) + Convert.ToDecimal(rounded));
                }
                else if (dt.Rows[k]["Shift"].ToString() == "SHIFT3")
                {
                    shift3count = shift3count + 1;
                    shift3 = Convert.ToInt32(Convert.ToDecimal(shift3) + Convert.ToDecimal(rounded));
                }
                else if (dt.Rows[k]["Shift"].ToString() == "SHIFT4")
                {
                    shift4count = shift4count + 1;
                    shift4 = Convert.ToInt32(Convert.ToDecimal(shift4) + Convert.ToDecimal(rounded));
                }
                else if (dt.Rows[k]["Shift"].ToString() == "SHIFT5")
                {
                    shift5count = shift5count + 1;
                    shift5 = Convert.ToInt32(Convert.ToDecimal(shift5) + Convert.ToDecimal(rounded));
                }
                else if (dt.Rows[k]["Shift"].ToString() == "SHIFT6")
                {
                    shift6count = shift6count + 1;
                    shift6 = Convert.ToInt32(Convert.ToDecimal(shift6) + Convert.ToDecimal(rounded));
                }
                else if (dt.Rows[k]["Shift"].ToString() == "No Shift")
                {
                    noshiftcount = noshiftcount + 1;
                    noshift = Convert.ToInt32(Convert.ToDecimal(noshift) + Convert.ToDecimal(rounded));
                }

                GrandTotal = (Convert.ToDecimal(generalcount) + Convert.ToDecimal(shift1count) + Convert.ToDecimal(shift2count) + Convert.ToDecimal(shift3count) + Convert.ToDecimal(shift4count) + Convert.ToDecimal(shift5count) + Convert.ToDecimal(shift6count) + Convert.ToDecimal(noshiftcount)).ToString();

                GrandTotalWages = (Convert.ToDecimal(general) + Convert.ToDecimal(shift1) + Convert.ToDecimal(shift2) + Convert.ToDecimal(shift3) + Convert.ToDecimal(shift4) + Convert.ToDecimal(shift5) + Convert.ToDecimal(shift6) + Convert.ToDecimal(noshift)).ToString();

            }
        }

        SSQL = "select ShiftDesc from Shift_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        Shift_DS = objdata.RptEmployeeMultipleDetails(SSQL);

        DataTable Depart = new DataTable();

        SSQL = "select distinct  DeptName from Department_Mst ";
        SSQL = SSQL + " where DeptName!='CONTRACT' order by DeptName Asc"; //Edit By Narmatha
        Depart = objdata.RptEmployeeMultipleDetails(SSQL);

        DataTable SubCat = new DataTable();

        SSQL = "select SubCatName from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' group by SubCatName ";
        SubCat = objdata.RptEmployeeMultipleDetails(SSQL);

        string CName = "";
        string LocName = "";
        SSQL = "";
        SSQL = "Select * from Naga_rights.AdminRights where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        DataTable Company_Dt = new DataTable();
        Company_Dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Company_Dt.Rows.Count > 0)
        {
            CName = Company_Dt.Rows[0]["Cname"].ToString().ToUpper();
            LocName = Company_Dt.Rows[0]["Lcode"].ToString().ToUpper() + " - " + Company_Dt.Rows[0]["Location"].ToString().ToUpper();
        }

        ds.Tables.Add(AnutoDT);
        //ReportDocument report = new ReportDocument();
        report3.Load(Server.MapPath("crystal/SalaryConsolidateNew.rpt"));
        report3.DataDefinition.FormulaFields["GradeTotalCount"].Text = "'" + GrandTotal + "'";

        report3.DataDefinition.FormulaFields["ShiftDate"].Text = "'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "'";
        report3.DataDefinition.FormulaFields["GeneralCount"].Text = "'" + generalcount + "'";
        report3.DataDefinition.FormulaFields["Shift1Count"].Text = "'" + shift1count + "'";
        report3.DataDefinition.FormulaFields["Shift2Count"].Text = "'" + shift2count + "'";
        report3.DataDefinition.FormulaFields["Shift3Count"].Text = "'" + shift3count + "'";
        report3.DataDefinition.FormulaFields["Shift4Count"].Text = "'" + shift4count + "'";
        report3.DataDefinition.FormulaFields["Shift5Count"].Text = "'" + shift5count + "'";
        report3.DataDefinition.FormulaFields["Shift6Count"].Text = "'" + shift6count + "'";
        report3.DataDefinition.FormulaFields["NoShiftCount"].Text = "'" + noshiftcount + "'";

        report3.DataDefinition.FormulaFields["Cname"].Text = "'" + CName + "'";
        report3.DataDefinition.FormulaFields["Lname"].Text = "'" + LocName + "'";

        //Get All Salary
        report3.DataDefinition.FormulaFields["GrandTotalWages"].Text = "'" + GrandTotalWages + "'";
        report3.DataDefinition.FormulaFields["GenWages"].Text = "'" + general + "'";
        report3.DataDefinition.FormulaFields["S1Wages"].Text = "'" + shift1 + "'";
        report3.DataDefinition.FormulaFields["S2Wages"].Text = "'" + shift2 + "'";
        report3.DataDefinition.FormulaFields["S3Wages"].Text = "'" + shift3 + "'";
        report3.DataDefinition.FormulaFields["S4Wages"].Text = "'" + shift4 + "'";
        report3.DataDefinition.FormulaFields["S5Wages"].Text = "'" + shift5 + "'";
        report3.DataDefinition.FormulaFields["S6Wages"].Text = "'" + shift6 + "'";
        report3.DataDefinition.FormulaFields["NoShiftWages"].Text = "'" + noshift + "'";
        report3.DataDefinition.FormulaFields["impcount"].Text = "'" + impcount + "'";

        report3.Database.Tables[0].SetDataSource(AnutoDT);
        // report2.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

        //CrystalReportViewer2.ReportSource = report2;
        string Server_Path = Server.MapPath("~");
        string AttachfileName_Miss = "";
        //  string Invoice_No_Str = WitSalesOrdNo.Replace("/", "_");
        AttachfileName_Miss = Server_Path + "/Daily_Report/SalaryConsolidate_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy_HHmmss") + ".pdf";

        if (File.Exists(AttachfileName_Miss))
        {
            File.Delete(AttachfileName_Miss);
        }

        report3.ExportToDisk(ExportFormatType.PortableDocFormat, AttachfileName_Miss);
        report3.Close();
    }

    public void GetImproperPunch()
    {
        DataSet ds = new DataSet();

        DataCells.Columns.Add("CompanyName");
        DataCells.Columns.Add("LocationName");
        DataCells.Columns.Add("ShiftDate");

        DataCells.Columns.Add("SNo");
        DataCells.Columns.Add("Dept");
        DataCells.Columns.Add("Type");
        DataCells.Columns.Add("Shift");
        DataCells.Columns.Add("Category");
        DataCells.Columns.Add("SubCategory");
        DataCells.Columns.Add("EmpCode");
        DataCells.Columns.Add("ExCode");
        DataCells.Columns.Add("Name");
        DataCells.Columns.Add("TimeIN");
        DataCells.Columns.Add("TimeOUT");
        DataCells.Columns.Add("MachineID");
        DataCells.Columns.Add("PrepBy");
        DataCells.Columns.Add("PrepDate");
        DataCells.Columns.Add("TotalMIN");
        DataCells.Columns.Add("GrandTOT");
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        DataTable dt3 = new DataTable();
        double Count = 0;
        double Count1;

        DateTime dayy = Convert.ToDateTime(Date);



        string SSQL = "";

        DataTable dt2 = new DataTable();
        SSQL = "select MachineID,COALESCE(DeptName,'') As DeptName,Shift,COALESCE(FirstName,'') as FirstName,TimeIN,TimeOUT,";
        SSQL = SSQL + "Total_Hrs from LogTime_Days where CompCode='" + SessionCcode + "' ANd LocCode='" + SessionLcode.ToString() + "'";

        if (Date != "")
        {
            SSQL = SSQL + " And  Attn_Date= DATE_FORMAT('" + Convert.ToDateTime(Date).ToString("yyyy-MM-dd") + "','%Y-%m-%d')";
        }
        //if (Division != "-Select-")
        //{
        //    SSQL = SSQL + " And Division = '" + Division + "'";
        //}

        // SSQL = SSQL + "  And Shift !='No Shift'  And TimeIN!=''";
        SSQL = SSQL + "  And (Present_Absent='Present' OR Present_Absent='Half Day') ";
        dt2 = objdata.RptEmployeeMultipleDetails(SSQL);



        SSQL = "";

        SSQL = "select MachineID,DeptName,ExistingCode,Shift,COALESCE(FirstName,'') + '.'+ COALESCE(LastName,'') as FirstName,TimeIN,TimeOUT";
        SSQL = SSQL + " from LogTime_Days where CompCode='" + SessionCcode + "' ANd LocCode='" + SessionLcode.ToString() + "'";



        if (Date != "")
        {
            SSQL = SSQL + " And  Attn_Date= DATE_FORMAT('" + Convert.ToDateTime(Date).ToString("yyyy-MM-dd") + "','%Y-%m-%d')";
        }
        if (Division != "-Select-")
        {
            SSQL = SSQL + " And Division = '" + Division + "'";
        }


        SSQL = SSQL + " And (TypeName='Improper' or TypeName='Absent') ";

        SSQL = SSQL + "  And Shift !='No Shift'  And TimeIN!=''";

        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "select MachineID,DeptName,ExistingCode,Shift,COALESCE(FirstName,'') as FirstName,TypeName,TimeIN,TimeOUT";
        SSQL = SSQL + " from LogTime_Days   ";
        SSQL = SSQL + " where CompCode='" + SessionCcode.ToString() + "' ANd LocCode='" + SessionLcode.ToString() + "'";

        if (Date != "")
        {
            SSQL = SSQL + " And  Attn_Date= DATE_FORMAT('" + Convert.ToDateTime(Date).ToString("yyyy-MM-dd") + "','%Y-%m-%d')";
        }
        if (Division != "-Select-")
        {
            SSQL = SSQL + " And Division = '" + Division + "'";
        }


        SSQL = SSQL + " And TypeName='Improper'";

        dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
        SSQL = "Select * from Company_Mst";
        dt3 = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt2.Rows.Count != 0)
        {
            if (dt1.Rows.Count != 0)
            {
                int sno = 1;
                DataTable Emp_dt = new DataTable();
                string category;
                string subCat;
                for (int i = 0; i < dt1.Rows.Count; i++)
                {

                    SSQL = "Select * from ";

                    SSQL = SSQL + " Employee_Mst";

                    SSQL = SSQL + " where MachineID='" + dt1.Rows[i]["MachineID"].ToString() + "' ";
                    Emp_dt = objdata.RptEmployeeMultipleDetails(SSQL);

                    category = "";
                    subCat = "";
                    if (Emp_dt.Rows.Count != 0)
                    {
                        category = Emp_dt.Rows[0]["CatName"].ToString();
                        subCat = Emp_dt.Rows[0]["SubCatName"].ToString();
                    }


                    DataCells.NewRow();
                    DataCells.Rows.Add();

                    DataCells.Rows[i]["CompanyName"] = dt3.Rows[0]["CompName"].ToString();
                    DataCells.Rows[i]["LocationName"] = SessionLcode;
                    DataCells.Rows[i]["ShiftDate"] = Convert.ToDateTime(Date).ToString("dd/MM/yyyy");
                    DataCells.Rows[i]["SNo"] = sno;


                    DataCells.Rows[i]["Dept"] = dt1.Rows[i]["DeptName"].ToString();
                    DataCells.Rows[i]["Type"] = dt1.Rows[i]["TypeName"].ToString();
                    DataCells.Rows[i]["Shift"] = dt1.Rows[i]["Shift"].ToString();
                    DataCells.Rows[i]["Category"] = category;

                    DataCells.Rows[i]["SubCategory"] = subCat;
                    DataCells.Rows[i]["EmpCode"] = dt1.Rows[i]["MachineID"].ToString();
                    DataCells.Rows[i]["ExCode"] = dt1.Rows[i]["ExistingCode"].ToString();
                    DataCells.Rows[i]["Name"] = dt1.Rows[i]["FirstName"].ToString();

                    DataCells.Rows[i]["TimeIN"] = dt1.Rows[i]["TimeIN"].ToString();
                    DataCells.Rows[i]["TimeOUT"] = dt1.Rows[i]["TimeOUT"].ToString();
                    DataCells.Rows[i]["MachineID"] = dt1.Rows[i]["MachineID"].ToString();

                    DataCells.Rows[i]["PrepBy"] = dt2.Rows.Count;
                    DataCells.Rows[i]["PrepDate"] = dt.Rows.Count;
                    DataCells.Rows[i]["TotalMIN"] = "";
                    DataCells.Rows[i]["TotalMIN"] = "";


                    sno = sno + 1;

                }
            }
        }

        if (DataCells.Rows.Count > 0)
        {
            ds.Tables.Add(DataCells);

            //ReportDocument report = new ReportDocument();
            report2.Load(Server.MapPath("crystal/PresentAbstract.rpt"));

            report2.Database.Tables[0].SetDataSource(ds.Tables[0]);

            //string SSQL1 = "";
            DataTable dt21 = new DataTable();

            SSQL = "";
            SSQL = "select LD.MachineID,LD.ExistingCode,COALESCE(LD.DeptName,'') As DeptName,LD.Shift,LD.TypeName,COALESCE(LD.FirstName,'') as FirstName,";
            SSQL = SSQL + " LD.TimeIN,LD.TimeOUT,LD.Total_Hrs1 as Total_Hrs,EM.CatName as CatName,EM.SubCatName as SubCatName from LogTime_Days LD";
            SSQL = SSQL + " inner join ";

            SSQL = SSQL + " Employee_Mst";

            SSQL = SSQL + " EM on EM.MachineID = LD.MachineID";

            SSQL = SSQL + " where LD.CompCode='" + SessionCcode.ToString() + "' ANd LD.LocCode='" + SessionCcode.ToString() + "'";
            SSQL = SSQL + " And EM.CompCode='" + SessionLcode.ToString() + "' ANd EM.LocCode='" + SessionLcode.ToString() + "'";

            if (Division != "-Select-")
            {
                SSQL = SSQL + " And EM.Division = '" + Division + "'";
            }
            if (ShiftType1 != "ALL")
            {
                SSQL = SSQL + " And Shift='" + ShiftType1 + "'";
            }
            if (Date != "")
            {
                SSQL = SSQL + " And Attn_Date= DATE_FORMAT('" + Convert.ToDateTime(Date).ToString("yyyy-MM-dd") + "','%Y-%m-%d')";
            }

            SSQL = SSQL + "  And Shift !='No Shift' And TimeIN!=''  ";
            dt21 = objdata.RptEmployeeMultipleDetails(SSQL);
            report2.DataDefinition.FormulaFields["New_Formula"].Text = "'" + dt2.Rows.Count.ToString() + "'";


            // report2.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

            //CrystalReportViewer2.ReportSource = report2;
            string Server_Path = Server.MapPath("~");
            string AttachfileName_Miss = "";
            //  string Invoice_No_Str = WitSalesOrdNo.Replace("/", "_");
            AttachfileName_Miss = Server_Path + "/Daily_Report/Daily_Improper_Punch_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy_HHmmss") + ".pdf";

            if (File.Exists(AttachfileName_Miss))
            {
                File.Delete(AttachfileName_Miss);
            }

            report2.ExportToDisk(ExportFormatType.PortableDocFormat, AttachfileName_Miss);
            report2.Close();
        }
        else
        {
            //CrystalReportViewer2.Dispose();
        }
    }

    private void GetAttdDayWise_Change()
    {
        string TableName = "";

        // Date = (Convert.ToDateTime(DateTime.Now.AddDays(-1)).ToString("dd/MM/yyyy")).ToString();

        TableName = "Employee_Mst";
        DataCell.Columns.Add("SNo");
        DataCell.Columns.Add("Dept");
        DataCell.Columns.Add("Type");
        DataCell.Columns.Add("Shift");


        DataCell.Columns.Add("EmpCode");
        DataCell.Columns.Add("ExCode");
        DataCell.Columns.Add("Name");
        DataCell.Columns.Add("TimeIN");
        DataCell.Columns.Add("TimeOUT");
        DataCell.Columns.Add("MachineID");
        DataCell.Columns.Add("Category");
        DataCell.Columns.Add("SubCategory");
        DataCell.Columns.Add("TotalMIN");
        DataCell.Columns.Add("GrandTOT");
        DataCell.Columns.Add("ShiftDate");
        DataCell.Columns.Add("CompanyName");
        DataCell.Columns.Add("LocationName");

        SSQL = "";
        SSQL = "select LD.MachineID,LD.ExistingCode,COALESCE(LD.DeptName,'') As DeptName,LD.Shift,LD.TypeName,COALESCE(LD.FirstName,'') as FirstName,";
        SSQL = SSQL + " LD.TimeIN,LD.TimeOUT,LD.Total_Hrs1 as Total_Hrs,EM.CatName as CatName,EM.SubCatName as SubCatName from LogTime_Days LD";
        SSQL = SSQL + " inner join " + TableName + " EM on EM.MachineID = LD.MachineID";
        SSQL = SSQL + " where LD.CompCode='" + SessionCcode.ToString() + "' ANd LD.LocCode='" + SessionLcode.ToString() + "'";
        SSQL = SSQL + " And EM.CompCode='" + SessionCcode.ToString() + "' ANd EM.LocCode='" + SessionLcode.ToString() + "'";
        SSQL = SSQL + " And Attn_Date= DATE_FORMAT('" + Convert.ToDateTime(Date).ToString("yyyy-MM-dd") + "','%Y-%m-%d')";

        // SSQL = SSQL + "  And Shift !='No Shift' And TimeIN!=''  ";
        SSQL = SSQL + "  AND  (LD.Present_Absent='Present' OR LD.Present_Absent='Half Day') ";

        AutoDTable = objdata.RptEmployeeMultipleDetails(SSQL);


        SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        string name = dt.Rows[0]["CompName"].ToString();
        if (AutoDTable.Rows.Count != 0)
        {
            int sno = 1;
            for (int i = 0; i < AutoDTable.Rows.Count; i++)
            {
                DataCell.NewRow();
                DataCell.Rows.Add();


                DataCell.Rows[i]["SNo"] = sno;
                DataCell.Rows[i]["Dept"] = AutoDTable.Rows[i]["DeptName"].ToString();
                DataCell.Rows[i]["Type"] = AutoDTable.Rows[i]["TypeName"].ToString();

                DataCell.Rows[i]["Shift"] = AutoDTable.Rows[i]["Shift"].ToString();

                DataCell.Rows[i]["EmpCode"] = AutoDTable.Rows[i]["MachineID"].ToString();
                DataCell.Rows[i]["ExCode"] = AutoDTable.Rows[i]["ExistingCode"].ToString();
                DataCell.Rows[i]["Name"] = AutoDTable.Rows[i]["FirstName"].ToString();
                DataCell.Rows[i]["TimeIN"] = AutoDTable.Rows[i]["TimeIN"].ToString();
                DataCell.Rows[i]["TimeOUT"] = AutoDTable.Rows[i]["TimeOUT"].ToString();
                DataCell.Rows[i]["MachineID"] = AutoDTable.Rows[i]["MachineID"].ToString();
                DataCell.Rows[i]["Category"] = AutoDTable.Rows[i]["CatName"].ToString();
                DataCell.Rows[i]["SubCategory"] = AutoDTable.Rows[i]["SubCatName"].ToString();
                DataCell.Rows[i]["TotalMIN"] = AutoDTable.Rows[i]["Total_Hrs"].ToString();
                DataCell.Rows[i]["GrandTOT"] = AutoDTable.Rows[i]["Total_Hrs"].ToString();
                DataCell.Rows[i]["ShiftDate"] = Convert.ToDateTime(Date).ToString("dd/MM/yyyy");
                DataCell.Rows[i]["CompanyName"] = name.ToString();
                DataCell.Rows[i]["LocationName"] = SessionLcode;

                sno += 1;
            }

            ds.Tables.Add(DataCell);
            //ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/Attendance.rpt"));

            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            //CrystalReportViewer1.ReportSource = report;
            //Mail.Attachments.Add(new Attachment(CrystalReportViewer1.PrintMode, "Day Attendance Day Wise"));
            string Server_Path = Server.MapPath("~");
            string AttachfileName_Miss = "";
            //  string Invoice_No_Str = WitSalesOrdNo.Replace("/", "_");
            AttachfileName_Miss = Server_Path + "/Daily_Report/Daily_Attendance_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy_HHmmss") + ".pdf";

            if (File.Exists(AttachfileName_Miss))
            {
                File.Delete(AttachfileName_Miss);
            }

            report.ExportToDisk(ExportFormatType.PortableDocFormat, AttachfileName_Miss);
            report.Close();
            report.Dispose();
        }
        else
        {
            // CrystalReportViewer1.Dispose();
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
        }
    }

    public void GetShiftManPowerTable1(String Plant)
    {
        string Male = "";
        string Female = "";

        AutoDataTable.Columns.Add("Sno");
        AutoDataTable.Columns.Add("CompanyName");
        AutoDataTable.Columns.Add("LocationName");
        AutoDataTable.Columns.Add("Shift");
        AutoDataTable.Columns.Add("Plant");
        AutoDataTable.Columns.Add("Contractor");
        AutoDataTable.Columns.Add("Male");
        AutoDataTable.Columns.Add("Female");
        AutoDataTable.Columns.Add("Company");
        AutoDataTable.Columns.Add("Address");
        AutoDataTable.Columns.Add("FromDate");

        DataTable Cnt = new DataTable();
        DataTable Shift = new DataTable();

        SSQL = "SELECT * FROM mstcontract where compCode='" + SessionCcode.ToString() + "' and loccode='" + SessionLcode.ToString() + "'";
        Cnt = objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "SELECT * FROM Shift_mst where compCode='" + SessionCcode.ToString() + "' and loccode='" + SessionLcode.ToString() + "'";
        Shift = objdata.RptEmployeeMultipleDetails(SSQL);



        int sno = 1;
        int Mcount = 0;
        if (Cnt.Rows.Count > 0)
        {
            for (int iRow = 0; iRow < Shift.Rows.Count; iRow++)
            {

                SSQL = "SELECT CASE WHEN EM.Gender='Male' THEN COALESCE(COUNT(EM.Gender),0) END AS Male,CASE WHEN EM.Gender='Female' THEN COALESCE(COUNT(EM.Gender),0) END AS Female, ";
                SSQL = SSQL + " em.shift_name,'Bar Plant' AS Plant,Con.Contractor as Contractor FROM employee_mst em  INNER JOIN logtime_bar_plant lp ON lp.machineid=em.machineID_Encrypt INNER JOIN MstContract Con ON Con.Contractor=Em.Contract ";
                SSQL = SSQL + " WHERE DATE_FORMAT(lp.TIME,'%Y-%m-%d')='" + Convert.ToDateTime(Date).ToString("yyyy-MM-dd") + "' and em.compCode='" + SessionCcode.ToString() + "' and em.loccode='" + SessionLcode.ToString() + "' AND EM.Shift_Name='" + Shift.Rows[iRow]["ShiftDesc"].ToString() + "' GROUP BY con.contractor";
                dt3 = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt3.Rows.Count > 0)
                {
                    for (int k = 0; k < dt3.Rows.Count; k++)
                    {


                        AutoDataTable.NewRow();
                        AutoDataTable.Rows.Add();

                        if (dt3.Rows[0]["shift_name"].ToString() == "")
                        {

                            //AutoDataTable.Rows[k]["Sno"] = sno;
                            AutoDataTable.Rows[Mcount]["CompanyName"] = SessionCcode;
                            AutoDataTable.Rows[Mcount]["LocationName"] = SessionLcode;
                            AutoDataTable.Rows[Mcount]["Shift"] = Shift.Rows[iRow]["ShiftDesc"].ToString();
                            AutoDataTable.Rows[Mcount]["Plant"] = "BAR";
                            AutoDataTable.Rows[Mcount]["Contractor"] = Cnt.Rows[iRow]["Contractor"].ToString();
                            AutoDataTable.Rows[Mcount]["Male"] = "0";
                            AutoDataTable.Rows[Mcount]["Female"] = "0";
                            AutoDataTable.Rows[Mcount]["FromDate"] = DateTime.Parse(Date).ToString("dd/MM/yyyy");

                            //sno += 1;
                        }
                        else
                        {
                            AutoDataTable.Rows[Mcount]["Sno"] = sno;
                            AutoDataTable.Rows[Mcount]["CompanyName"] = SessionCcode;
                            AutoDataTable.Rows[Mcount]["LocationName"] = SessionLcode;
                            AutoDataTable.Rows[Mcount]["Shift"] = dt3.Rows[0]["shift_name"].ToString();
                            AutoDataTable.Rows[Mcount]["Plant"] = dt3.Rows[0]["Plant"].ToString();
                            AutoDataTable.Rows[Mcount]["Contractor"] = dt3.Rows[k]["Contractor"].ToString();
                            //AutoDataTable.Rows[k]["Address"] = Addres.ToString();
                            //AutoDataTable.Rows[k]["Company"] = name.ToString();
                            Male = dt3.Rows[k]["Male"].ToString();
                            if (Male == "")
                            {
                                AutoDataTable.Rows[Mcount]["Male"] = "0";
                            }
                            else
                            {
                                AutoDataTable.Rows[Mcount]["Male"] = dt3.Rows[k]["Male"].ToString();
                            }
                            Female = dt3.Rows[k]["Female"].ToString();
                            if (Female == "")
                            {
                                AutoDataTable.Rows[Mcount]["Female"] = "0";
                            }
                            else
                            {
                                AutoDataTable.Rows[Mcount]["Female"] = dt3.Rows[k]["Female"].ToString();
                            }
                            AutoDataTable.Rows[Mcount]["FromDate"] = DateTime.Parse(Date).ToString("dd/MM/yyyy");

                            sno += 1;
                        }
                        Mcount += 1;
                    }
                }
            }
        }

    }
    public void GetShiftManPowerTable2(String Plant)
    {
        string Male = "";
        string Female = "";
        AutoDataTable1.Columns.Add("Sno");
        AutoDataTable1.Columns.Add("CompanyName");
        AutoDataTable1.Columns.Add("LocationName");
        AutoDataTable1.Columns.Add("Shift");
        AutoDataTable1.Columns.Add("Plant");
        AutoDataTable1.Columns.Add("Contractor");
        AutoDataTable1.Columns.Add("Male");
        AutoDataTable1.Columns.Add("Female");
        AutoDataTable1.Columns.Add("Company");
        AutoDataTable1.Columns.Add("Address");
        AutoDataTable1.Columns.Add("FromDate");

        DataTable Cnt = new DataTable();
        DataTable Shift = new DataTable();

        SSQL = "SELECT * FROM mstcontract where compCode='" + SessionCcode.ToString() + "' and loccode='" + SessionLcode.ToString() + "'";
        Cnt = objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "SELECT * FROM Shift_mst where compCode='" + SessionCcode.ToString() + "' and loccode='" + SessionLcode.ToString() + "'";
        Shift = objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        string name = dt.Rows[0]["CompName"].ToString();
        string Addres = dt.Rows[0]["Add1"].ToString();
        int sno = 1;
        int Mcount = 0;
        if (Cnt.Rows.Count > 0)
        {
            for (int iRow = 0; iRow < Shift.Rows.Count; iRow++)
            {

                SSQL = "SELECT CASE WHEN EM.Gender='Male' THEN COALESCE(COUNT(EM.Gender),0) END AS Male,CASE WHEN EM.Gender='Female' THEN COALESCE(COUNT(EM.Gender),0) END AS Female, ";
                SSQL = SSQL + " em.shift_name,'Powder Plant' AS Plant,Con.Contractor as Contractor FROM employee_mst em  INNER JOIN logtime_Powder_plant lp ON lp.machineid=em.machineID_Encrypt INNER JOIN MstContract Con ON Con.Contractor=Em.Contract ";
                SSQL = SSQL + " WHERE DATE_FORMAT(lp.TIME,'%Y-%m-%d')='" + Convert.ToDateTime(Date).ToString("yyyy-MM-dd") + "' and em.compCode='" + SessionCcode.ToString() + "' and em.loccode='" + SessionLcode.ToString() + "' AND EM.Shift_Name='" + Shift.Rows[iRow]["ShiftDesc"].ToString() + "' GROUP BY con.contractor";
                dt3 = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt3.Rows.Count > 0)
                {
                    //Mcount = Convert.ToInt32(dt3.Rows.Count) + Convert.ToInt32(AutoDataTable1.Rows.Count);
                    for (int k = 0; k < dt3.Rows.Count; k++)
                    {
                        AutoDataTable1.NewRow();
                        AutoDataTable1.Rows.Add();

                        if (dt3.Rows[0]["shift_name"].ToString() == "")
                        {

                            //AutoDataTable1.Rows[k]["Sno"] = sno;
                            AutoDataTable1.Rows[Mcount]["CompanyName"] = SessionCcode;
                            AutoDataTable1.Rows[Mcount]["LocationName"] = SessionLcode;
                            AutoDataTable1.Rows[Mcount]["Shift"] = Shift.Rows[iRow]["ShiftDesc"].ToString();
                            AutoDataTable1.Rows[Mcount]["Plant"] = "BAR";
                            AutoDataTable1.Rows[Mcount]["Contractor"] = Cnt.Rows[iRow]["Contractor"].ToString();
                            AutoDataTable1.Rows[Mcount]["Address"] = Addres.ToString();
                            AutoDataTable1.Rows[Mcount]["Company"] = name.ToString();
                            AutoDataTable1.Rows[Mcount]["Male"] = "0";
                            AutoDataTable1.Rows[Mcount]["Female"] = "0";
                            AutoDataTable1.Rows[Mcount]["FromDate"] = DateTime.Parse(Date).ToString("dd/MM/yyyy");

                            // sno += 1;
                        }
                        else
                        {
                            AutoDataTable1.Rows[Mcount]["Sno"] = sno;
                            AutoDataTable1.Rows[Mcount]["CompanyName"] = SessionCcode;
                            AutoDataTable1.Rows[Mcount]["LocationName"] = SessionLcode;
                            AutoDataTable1.Rows[Mcount]["Shift"] = dt3.Rows[0]["shift_name"].ToString();
                            AutoDataTable1.Rows[Mcount]["Plant"] = dt3.Rows[0]["Plant"].ToString();
                            AutoDataTable1.Rows[Mcount]["Contractor"] = dt3.Rows[k]["Contractor"].ToString();
                            AutoDataTable1.Rows[Mcount]["Address"] = Addres.ToString();
                            AutoDataTable1.Rows[Mcount]["Company"] = name.ToString();
                            Male = dt3.Rows[k]["Male"].ToString();
                            if (Male == "")
                            {
                                AutoDataTable1.Rows[Mcount]["Male"] = "0";
                            }
                            else
                            {
                                AutoDataTable1.Rows[Mcount]["Male"] = dt3.Rows[k]["Male"].ToString();
                            }
                            Female = dt3.Rows[k]["Female"].ToString();
                            if (Female == "")
                            {
                                AutoDataTable1.Rows[Mcount]["Female"] = "0";
                            }
                            else
                            {
                                AutoDataTable1.Rows[Mcount]["Female"] = dt3.Rows[k]["Female"].ToString();
                            }
                            AutoDataTable1.Rows[Mcount]["FromDate"] = DateTime.Parse(Date).ToString("dd/MM/yyyy");

                            sno += 1;
                        }
                        Mcount += 1;
                    }

                }

            }
        }

    }
    public void GetShiftManPowerTable3(String Plant)
    {
        string Male = "";
        string Female = "";
        AutoDataTable2.Columns.Add("Sno");
        AutoDataTable2.Columns.Add("CompanyName");
        AutoDataTable2.Columns.Add("LocationName");
        AutoDataTable2.Columns.Add("Shift");
        AutoDataTable2.Columns.Add("Plant");
        AutoDataTable2.Columns.Add("Contractor");
        AutoDataTable2.Columns.Add("Male");
        AutoDataTable2.Columns.Add("Female");
        AutoDataTable2.Columns.Add("Company");
        AutoDataTable2.Columns.Add("Address");
        AutoDataTable2.Columns.Add("FromDate");

        DataTable Cnt = new DataTable();
        DataTable Shift = new DataTable();

        SSQL = "SELECT * FROM mstcontract where compCode='" + SessionCcode.ToString() + "' and loccode='" + SessionLcode.ToString() + "'";
        Cnt = objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "SELECT * FROM Shift_mst where compCode='" + SessionCcode.ToString() + "' and loccode='" + SessionLcode.ToString() + "'";
        Shift = objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        string name = dt.Rows[0]["CompName"].ToString();
        string Addres = dt.Rows[0]["Add1"].ToString();
        int sno = 1;
        int Mcount = 0;
        if (Cnt.Rows.Count > 0)
        {
            for (int iRow = 0; iRow < Shift.Rows.Count; iRow++)
            {

                SSQL = "SELECT CASE WHEN EM.Gender='Male' THEN COALESCE(COUNT(EM.Gender),0) END AS Male,CASE WHEN EM.Gender='Female' THEN COALESCE(COUNT(EM.Gender),0) END AS Female, ";
                SSQL = SSQL + " em.shift_name,'TSP Plant' AS Plant,Con.Contractor as Contractor FROM employee_mst em  INNER JOIN logtime_TSP_plant lp ON lp.machineid=em.machineID_Encrypt INNER JOIN MstContract Con ON Con.Contractor=Em.Contract ";
                SSQL = SSQL + " WHERE DATE_FORMAT(lp.TIME,'%Y-%m-%d')='" + Convert.ToDateTime(Date).ToString("yyyy-MM-dd") + "' and em.compCode='" + SessionCcode.ToString() + "' and em.loccode='" + SessionLcode.ToString() + "' AND EM.Shift_Name='" + Shift.Rows[iRow]["ShiftDesc"].ToString() + "' GROUP BY con.contractor";
                dt3 = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt3.Rows.Count > 0)
                {
                    for (int k = 0; k < dt3.Rows.Count; k++)
                    {


                        AutoDataTable2.NewRow();
                        AutoDataTable2.Rows.Add();

                        if (dt3.Rows[0]["shift_name"].ToString() == "")
                        {

                            AutoDataTable2.Rows[Mcount]["Sno"] = sno;
                            AutoDataTable2.Rows[Mcount]["CompanyName"] = SessionCcode;
                            AutoDataTable2.Rows[Mcount]["LocationName"] = SessionLcode;
                            AutoDataTable2.Rows[Mcount]["Shift"] = Shift.Rows[iRow]["ShiftDesc"].ToString();
                            AutoDataTable2.Rows[Mcount]["Plant"] = "BAR";
                            AutoDataTable2.Rows[Mcount]["Contractor"] = Cnt.Rows[iRow]["Contractor"].ToString();
                            AutoDataTable2.Rows[Mcount]["Address"] = Addres.ToString();
                            AutoDataTable2.Rows[Mcount]["Company"] = name.ToString();
                            AutoDataTable2.Rows[Mcount]["Male"] = "0";
                            AutoDataTable2.Rows[Mcount]["Female"] = "0";
                            AutoDataTable2.Rows[Mcount]["FromDate"] = DateTime.Parse(Date).ToString("dd/MM/yyyy");

                            sno += 1;
                        }
                        else
                        {
                            AutoDataTable2.Rows[Mcount]["Sno"] = sno;
                            AutoDataTable2.Rows[Mcount]["CompanyName"] = SessionCcode;
                            AutoDataTable2.Rows[Mcount]["LocationName"] = SessionLcode;
                            AutoDataTable2.Rows[Mcount]["Shift"] = dt3.Rows[0]["shift_name"].ToString();
                            AutoDataTable2.Rows[Mcount]["Plant"] = dt3.Rows[0]["Plant"].ToString();
                            AutoDataTable2.Rows[Mcount]["Contractor"] = dt3.Rows[k]["Contractor"].ToString();
                            AutoDataTable2.Rows[Mcount]["Address"] = Addres.ToString();
                            AutoDataTable2.Rows[Mcount]["Company"] = name.ToString();
                            Male = dt3.Rows[k]["Male"].ToString();
                            if (Male == "")
                            {
                                AutoDataTable2.Rows[Mcount]["Male"] = "0";
                            }
                            else
                            {
                                AutoDataTable2.Rows[Mcount]["Male"] = dt3.Rows[k]["Male"].ToString();
                            }
                            Female = dt3.Rows[k]["Female"].ToString();
                            if (Female == "")
                            {
                                AutoDataTable2.Rows[Mcount]["Female"] = "0";
                            }
                            else
                            {
                                AutoDataTable2.Rows[Mcount]["Female"] = dt3.Rows[k]["Female"].ToString();
                            }
                            AutoDataTable2.Rows[Mcount]["FromDate"] = DateTime.Parse(Date).ToString("dd/MM/yyyy");

                            sno += 1;
                        }
                        Mcount += 1;
                    }
                }
            }
        }

    }
}