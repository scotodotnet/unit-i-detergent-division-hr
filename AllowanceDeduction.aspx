<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="AllowanceDeduction.aspx.cs" Inherits="AllowanceDeduction" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
 <script>
     $(document).ready(function() {
     $('#example').dataTable();
     $('.datepicker').datepicker({
         format: "dd/mm/yyyy",
         autoclose: true
     });
     });
 </script>


<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
                $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
            }
        });
    };
</script>
<asp:UpdatePanel ID="UpdatePanel5" runat="server">
 <ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Salary Process</a></li>
				<li class="active">Allowance & Deduction</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Allowance & Deduction</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Allowance & Deduction</h4>
                        </div>
                        <div class="panel-body">
                        <div class="form-group">
                        <!-- begin row -->
                          <div class="row">
                            <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Month</label>
								 <asp:DropDownList runat="server" ID="ddlMonth" class="form-control select2" 
                                        style="width:100%;" AutoPostBack="true" 
                                        onselectedindexchanged="ddlMonth_SelectedIndexChanged">
								 <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
								 <asp:ListItem Value="January">January</asp:ListItem>
								 <asp:ListItem Value="February">February</asp:ListItem>
								 <asp:ListItem Value="March">March</asp:ListItem>
								 <asp:ListItem Value="April">April</asp:ListItem>
								 <asp:ListItem Value="May">May</asp:ListItem>
								 <asp:ListItem Value="June">June</asp:ListItem>
								 <asp:ListItem Value="July">July</asp:ListItem>
								 <asp:ListItem Value="August">August</asp:ListItem>
								 <asp:ListItem Value="September">September</asp:ListItem>
								 <asp:ListItem Value="October">October</asp:ListItem>
								 <asp:ListItem Value="November">November</asp:ListItem>
								 <asp:ListItem Value="December">December</asp:ListItem>
							 	 </asp:DropDownList>
							 	 <asp:RequiredFieldValidator ControlToValidate="ddlMonth" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                 </asp:RequiredFieldValidator>
								</div>
                               </div>
                           <!-- end col-4 -->
                             <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Fin. Year</label>
								 <asp:DropDownList runat="server" ID="ddlFinYear" class="form-control select2" 
                                        style="width:100%;" AutoPostBack="true" 
                                        onselectedindexchanged="ddlFinYear_SelectedIndexChanged">
							 	 </asp:DropDownList>
							 	 <asp:RequiredFieldValidator ControlToValidate="ddlFinYear" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                 </asp:RequiredFieldValidator>
								</div>
                               </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Wages Type</label>
								 <asp:DropDownList runat="server" ID="ddlWages" class="form-control select2" 
                                        AutoPostBack="true" style="width:100%;" 
                                        onselectedindexchanged="ddlWages_SelectedIndexChanged">
							 	 </asp:DropDownList>
							 	 <asp:RequiredFieldValidator ControlToValidate="ddlFinYear" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                 </asp:RequiredFieldValidator>
								</div>
                               </div>
                           <!-- end col-4 -->
                         </div>
                        <!-- end row -->
                       <!-- begin row -->
                          <div class="row">
                          <!-- begin col-4 -->
                            <div class="col-md-4">
							   <div class="form-group">
								<label>Token No</label>
								<asp:TextBox runat="server" ID="txtTokenNo" class="form-control" 
                                       AutoPostBack="true" ontextchanged="txtTokenNo_TextChanged" ></asp:TextBox>
								</div>
                             </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                           <div class="col-md-4">
                           <div class="form-group">
                           <label>Machine ID</label>
                           <asp:DropDownList runat="server" ID="txtMachineID" class="form-control select2" 
                                 AutoPostBack="true" style="width:100%;" onselectedindexchanged="txtMachineID_SelectedIndexChanged">
                           </asp:DropDownList>
                           <asp:RequiredFieldValidator ControlToValidate="txtMachineID" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                 </asp:RequiredFieldValidator>
                            </div>
                           </div>
                           <!-- end col-4 -->
                          
                          <!-- begin col-4 -->
                            <div class="col-md-4">
							   <div class="form-group">
								<label>Employee Name</label>
								<asp:Label runat="server" ID="txtEmpName" class="form-control" BackColor="#F3F3F3"></asp:Label>
								</div>
                             </div>
                           <!-- end col-4 -->
                            
                          
                           </div>
                        <!-- end row -->
                        <!-- begin row -->
                          <div class="row">
                          <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>From Date</label>
										<asp:TextBox runat="server" ID="txtFromDate" class="form-control datepicker" 
                                            placeholder="dd/MM/YYYY" AutoPostBack="true" 
                                            ontextchanged="txtFromDate_TextChanged"></asp:TextBox>
										<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                          TargetControlID="txtFromDate" ValidChars="0123456789/">
                                        </cc1:FilteredTextBoxExtender>
									</div>
                                  </div>
                               <!-- end col-4 -->
                               <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>To Date</label>
										<asp:TextBox runat="server" ID="txtToDate" class="form-control datepicker" 
                                            placeholder="dd/MM/YYYY" AutoPostBack="true" 
                                            ontextchanged="txtToDate_TextChanged"></asp:TextBox>
										<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                          TargetControlID="txtToDate" ValidChars="0123456789/">
                                        </cc1:FilteredTextBoxExtender>
									</div>
                                  </div>
                               <!-- end col-4 -->
                                <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
									<asp:Label ID="lblAllow3" runat="server" Text="Allow.Others1" style="font-weight: 600; color: #242a30; margin-bottom: 5px;max-width: 100%;line-height: 1.42857143;"></asp:Label>
										<asp:TextBox runat="server" ID="txtAllow1" class="form-control"></asp:TextBox>
										<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                          TargetControlID="txtAllow1" ValidChars="0123456789.">
                                        </cc1:FilteredTextBoxExtender>
									</div>
                                  </div>
                               <!-- end col-4 -->
                          </div>
                        <!-- end row -->
                         <!-- begin row -->
                          <div class="row">
                          <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
									<asp:Label ID="lblAllow4" runat="server" Text="Allow.Others2" style="font-weight: 600; color: #242a30; margin-bottom: 5px;max-width: 100%;line-height: 1.42857143;"></asp:Label>
										<%--<label runat="server" id="lblAllow4">Allow Others2</label>--%>
										<asp:TextBox runat="server" ID="txtAllow2" class="form-control"></asp:TextBox>
										<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                          TargetControlID="txtAllow2" ValidChars="0123456789.">
                                        </cc1:FilteredTextBoxExtender>
									</div>
                                  </div>
                               <!-- end col-4 -->
                               <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>Advance-1</label>
										<asp:TextBox runat="server" ID="txtAdvance" class="form-control"></asp:TextBox>
										<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                          TargetControlID="txtAdvance" ValidChars="0123456789.">
                                        </cc1:FilteredTextBoxExtender>
									</div>
                                  </div>
                               <!-- end col-4 -->
                                <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<asp:Label ID="lblDed3" runat="server" Text="Canteen" style="font-weight: 600; color: #242a30; margin-bottom: 5px;max-width: 100%;line-height: 1.42857143;"></asp:Label>
										<asp:TextBox runat="server" ID="txtCanteen" class="form-control"></asp:TextBox>
										<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                          TargetControlID="txtCanteen" ValidChars="0123456789.">
                                        </cc1:FilteredTextBoxExtender>
									</div>
                                  </div>
                               <!-- end col-4 -->
                          </div>
                        <!-- end row -->
                         <!-- begin row -->
                          <div class="row">
                          <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<%--<label>LWF</label>--%>
										<asp:Label ID="lblDed4" runat="server" Text="LWF" style="font-weight: 600; color: #242a30; margin-bottom: 5px;max-width: 100%;line-height: 1.42857143;"></asp:Label>
										<asp:TextBox runat="server" ID="txtLWF" class="form-control"></asp:TextBox>
										<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                          TargetControlID="txtLWF" ValidChars="0123456789.">
                                        </cc1:FilteredTextBoxExtender>
									</div>
                                  </div>
                               <!-- end col-4 -->
                               <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<%--<label>P.Tax</label>--%>
										<asp:Label ID="lblDed5" runat="server" Text="P.Tax" style="font-weight: 600; color: #242a30; margin-bottom: 5px;max-width: 100%;line-height: 1.42857143;"></asp:Label>
										<asp:TextBox runat="server" ID="txtPTax" class="form-control"></asp:TextBox>
										<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                          TargetControlID="txtPTax" ValidChars="0123456789.">
                                        </cc1:FilteredTextBoxExtender>
									</div>
                                  </div>
                               <!-- end col-4 -->
                                <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<%--<label>Ded.Others1</label>--%>
										<asp:Label ID="lblDedOth1" runat="server" Text="Ded. Others1" style="font-weight: 600; color: #242a30; margin-bottom: 5px;max-width: 100%;line-height: 1.42857143;"></asp:Label>
										<asp:TextBox runat="server" ID="txtDedOthers1" class="form-control"></asp:TextBox>
										<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                          TargetControlID="txtDedOthers1" ValidChars="0123456789.">
                                        </cc1:FilteredTextBoxExtender>
									</div>
                                  </div>
                               <!-- end col-4 -->
                          </div>
                        <!-- end row -->
                        <!-- begin row -->
                          <div class="row">
                          <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<%--<label>Ded.Others1</label>--%>
										<asp:Label ID="lblDedOth2" runat="server" Text="Ded. Others2" style="font-weight: 600; color: #242a30; margin-bottom: 5px;max-width: 100%;line-height: 1.42857143;"></asp:Label>
										<asp:TextBox runat="server" ID="txtDedOthers2" class="form-control"></asp:TextBox>
										<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                          TargetControlID="txtDedOthers2" ValidChars="0123456789.">
                                        </cc1:FilteredTextBoxExtender>
									</div>
                                  </div>
                               <!-- end col-4 -->
                               <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>H.Allowed</label>
										<asp:TextBox runat="server" ID="txtHouseAllow" class="form-control"></asp:TextBox>
										<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                          TargetControlID="txtHouseAllow" ValidChars="0123456789.">
                                        </cc1:FilteredTextBoxExtender>
									</div>
                                  </div>
                               <!-- end col-4 -->
                                <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>OT Hours</label>
										<asp:TextBox runat="server" ID="txtOTHrs" class="form-control"></asp:TextBox>
										<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                          TargetControlID="txtOTHrs" ValidChars="0123456789.">
                                        </cc1:FilteredTextBoxExtender>
									</div>
                                  </div>
                               <!-- end col-4 -->
                          </div>
                        <!-- end row -->
                        <!-- begin row -->
                          <div class="row">
                          <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>Canteen Days Minus</label>
										<asp:TextBox runat="server" ID="txtCanteenDays" class="form-control"></asp:TextBox>
										<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                          TargetControlID="txtCanteenDays" ValidChars="0123456789.">
                                        </cc1:FilteredTextBoxExtender>
									</div>
                                  </div>
                               <!-- end col-4 -->
                               <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label></label><%--LOP Days--%>
										<asp:TextBox runat="server" ID="txtLOPDays" Text="0" class="form-control" Visible="false"></asp:TextBox>
										
									</div>
                                  </div>
                               <!-- end col-4 -->
                                
                          </div>
                        <!-- end row -->
                      <!-- begin row -->  
                        <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success"  
                                         ValidationGroup="Validate_Field" onclick="btnSave_Click"/>
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                         onclick="btnClear_Click" />
								 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row --> 
                         <div style ="  
        height:30px; margin:0;padding:0">
        <table cellspacing="0" cellpadding = "0" rules="all" border="1" id="tblHeader" 
         style="font-family:Arial;font-size:10pt;color:#000;
         border-collapse:collapse;height:100%;">
         <thead  style ="background:blue">
            <tr>
               <td style ="width:6.25%;text-align:center">M ID</td>
               <td style ="width:6.25%;text-align:center">Token No</td>
               <td style ="width:10%;text-align:center">E Name</td>
               <td style ="width:4.25%;text-align:center">Allow3</td>
                <td style ="width:4.50%;text-align:center">Allow4</td>
               <td style ="width:6.25%;text-align:center">Advance</td>
               <td style ="width:6.25%;text-align:center">Ded3</td>
               <td style ="width:6.25%;text-align:center">Ded4</td>
               <td style ="width:6.25%;text-align:center">Ded5</td>
                <td style ="width:6.25%;text-align:center">DedOthers1</td>
               <td style ="width:6.25%;text-align:center">DedOthers2</td>
               <td style ="width:6.25%;text-align:center">H.Allowed</td>
               <td style ="width:6.25%;text-align:center">OT Hours</td>
               <td style ="width:6.25%;text-align:center">CanteenDays</td>
                <td style ="width:6.25%;text-align:center">Advance_Check</td>
                <td style ="width:6.25%;text-align:center">Mode</td>
             
            </tr>
         </thead>
         <%--<tbody style ="height:400px; overflow:scroll;">
         
         </tbody>--%>
        </table>
        </div>
        
         <div style ="height:400px; overflow:scroll;">
        <asp:GridView ID="Repeater1" runat="server"
            AutoGenerateColumns = "false" Font-Names = "Arial" ShowHeader = "false" 
            Font-Size = "11pt" AlternatingRowStyle-BackColor = "#ffffff" >
           <Columns>
            <asp:BoundField ItemStyle-Width = "6.25%" DataField = "MachineID" />
            <asp:BoundField ItemStyle-Width = "6.25%" DataField = "ExisistingCode" />
            <asp:BoundField ItemStyle-Width = "8%" DataField = "EmpName" />
            <asp:BoundField ItemStyle-Width = "4.25%" DataField = "allowances3" />
            <asp:BoundField ItemStyle-Width = "4.50%" DataField = "allowances4" />
            <asp:BoundField ItemStyle-Width = "6.25%" DataField = "Advance" />
            <asp:BoundField ItemStyle-Width = "6.25%" DataField = "Deduction3" />
            <asp:BoundField ItemStyle-Width = "6.25%" DataField = "Deduction4" />
              <asp:BoundField ItemStyle-Width = "6.25%" DataField = "Deduction5" />
            <asp:BoundField ItemStyle-Width = "6.25%" DataField = "DedOthers1" />
            <asp:BoundField ItemStyle-Width = "6.25%" DataField = "DedOthers2" />
          
              <asp:BoundField ItemStyle-Width = "6.25%" DataField = "HAllowed" />
            <asp:BoundField ItemStyle-Width = "6.25%" DataField = "OTHours" />
            <asp:BoundField ItemStyle-Width = "6.25%" DataField = "CanteenDaysMinus" />
            <asp:BoundField ItemStyle-Width = "6.25%" DataField = "Advance_Check" />
            <asp:TemplateField >
             <ItemTemplate>
          <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                           Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument='<%#Eval("ExisistingCode")%>' CommandName='<%# Eval("MachineID")%>'
                                           CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Delete this Employee Allowance and Deduction?');">
                                     </asp:LinkButton>
           </ItemTemplate></asp:TemplateField>
          <%-- --%>
           </Columns> 
        </asp:GridView>
        </div>
                        
                    <!-- table start -->
				<%--	<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                
                                                <th>Machine ID</th>
                                                <th>Token No</th>
                                                <th>Emp Name</th>
                                                <th>Allow.Others1</th>
                                                <th>Allow.Others2</th>
                                                <th>Deduction</th>
                                                <th>H.Allowed</th>
                                               
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                    <td><%# Eval("MachineID")%></td>
                                    <td><%# Eval("ExisistingCode")%></td>
                                    <td><%# Eval("EmpName")%></td>
                                    <td><%# Eval("allowances3")%></td>
                                    <td><%# Eval("allowances4")%></td>
                                    <td><%# Eval("DedOthers2")%></td>
                                    <td><%# Eval("HAllowed")%></td>
                                   
                                     <td>
                                     <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                           Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument='<%#Eval("ExisistingCode")%>' CommandName='<%# Eval("MachineID")%>'
                                           CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Delete this Employee Allowance and Deduction?');">
                                     </asp:LinkButton>
                                     
                                    </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>--%>
					<!-- table End -->
                        
                        </div>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->
</ContentTemplate>
</asp:UpdatePanel>




</asp:Content>

