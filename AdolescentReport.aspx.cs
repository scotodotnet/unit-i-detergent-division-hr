﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Runtime.CompilerServices;
using System.IO;

public partial class AdolescentReport : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string WagesType;
    string Division;
    string AdolescentType;
    BALDataAccess objdata = new BALDataAccess();
   // string SSQL;
    DataTable mDataSet = new DataTable();
    
    DataTable AutoDataTable = new DataTable();
    string[] delimiters = { "-", ">" };

 
    string[] Time_Minus_Value_Check;
   
    string Empcode;
    string Date1 = "";
    string Date2 = "";

    DateTime date1 = new DateTime();
    DateTime date2 = new DateTime();

    DataSet ds = new DataSet();
    DataTable DataCell = new DataTable();
    string State;
    string SSQL="";
    DataTable dt1 = new DataTable();

    System.Web.UI.WebControls.DataGrid grid =
                           new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Employee Relieving Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("SalaryProcess"));
                //li.Attributes.Add("class", "droplink active open");
            }


            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
           Division = Request.QueryString["Division"].ToString();
           string TempWages = Request.QueryString["Wages"].ToString();
           WagesType = TempWages.Replace("_", "&");

           AdolescentType = Request.QueryString["AdolescentType"].ToString();
        
            GetDetails();
          
        }
    }


    public void GetDetails()
    {
        DataTable Emp_DS=new DataTable();
         string Certificate_No_Join="";
        string Certificate_Date_Join="";
        string Certificate_Due_Join="";
        string Certificate_Remarks_Join="";
              
 
             DataCell.Columns.Add ("S.No");
             DataCell.Columns.Add("Name of the employee");
             DataCell.Columns.Add("Token No");
             DataCell.Columns.Add("Department");
             DataCell.Columns.Add("Date Of Birth");
             DataCell.Columns.Add("DOJ");
             DataCell.Columns.Add("Certificate No");
             DataCell.Columns.Add("Certificate Date");
             DataCell.Columns.Add("Due Falls");
             DataCell.Columns.Add("Remarks");

       

                
                SSQL = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Wages='" + WagesType + "' And IsActive='Yes'";
          if (Division != "-Select-")
                {
                    SSQL = SSQL + " And Division = '" + Division + "'";
                }
   
                SSQL = SSQL + " And DATEDIFF(hour,BirthDate,CURDATE())/8766 <= 18";
                SSQL = SSQL + " Order by ExistingCode Asc";

                Emp_DS =objdata.RptEmployeeMultipleDetails (SSQL);
        if(Emp_DS.Rows.Count !=0)
        {
            int sno=0;
            int srno = 1;
            
            for (int intRow = 0; intRow < Emp_DS.Rows.Count; intRow++)
            {
                //Adolescent Type Check
                SSQL = "Select * from Adolcent_Emp_Det where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + Emp_DS.Rows[intRow]["MachineID"].ToString() + "' Order by Certificate_Date Asc";
                mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
                if (mDataSet.Rows.Count != 0)
                {
                    SSQL = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Wages='" + WagesType + "' And IsActive='Yes'";
                    DataCell.NewRow();
                    DataCell.Rows.Add();



                    DataCell.Rows[sno]["S.No"] = srno;
                    DataCell.Rows[sno]["Name of the employee"] = Emp_DS.Rows[intRow]["FirstName"].ToString() + " - " + Emp_DS.Rows[intRow]["Permanent_Dist"].ToString();
                    DataCell.Rows[sno]["Token No"] = Emp_DS.Rows[intRow]["ExistingCode"].ToString();
                    DataCell.Rows[sno]["Department"] = Emp_DS.Rows[intRow]["DeptName"].ToString();
                    DataCell.Rows[sno]["Date Of Birth"] = Convert.ToDateTime(Emp_DS.Rows[intRow]["BirthDate"]).ToString("dd/MM/yyyy");
                    DataCell.Rows[sno]["DOJ"] = Convert.ToDateTime(Emp_DS.Rows[intRow]["DOJ"]).ToString("dd/MM/yyyy");

                    //Get Certificate No And Date And Due Falls

                    Certificate_No_Join = ""; Certificate_Date_Join = ""; Certificate_Due_Join = "";
                    Certificate_Remarks_Join = "";
                    for (int i = 0; i < mDataSet.Rows.Count; i++)
                    {
                        if (Certificate_No_Join != "")
                        {
                            Certificate_No_Join = Certificate_No_Join + "/";
                        }
                        if (Certificate_Date_Join != "")
                        {
                            Certificate_Date_Join = Certificate_Date_Join + ",";
                        }
                        if (Certificate_Due_Join != "")
                        {
                            Certificate_Due_Join = Certificate_Due_Join + ",";
                        }
                        string Due_Date_Get;

                        if (mDataSet.Rows[i]["Certificate_Date_Str"].ToString() != "")
                        {
                            Due_Date_Get = Convert.ToDateTime(mDataSet.Rows[i]["Certificate_Date_Str"]).ToString("dd/MM/yyyy");

                        }
                        else
                        {
                            Due_Date_Get = mDataSet.Rows[i]["Next_Due_Date_Str"].ToString();

                        }

                        Certificate_No_Join = Certificate_No_Join + mDataSet.Rows[i]["Certificate_No"].ToString();
                        Certificate_Date_Join = Certificate_Date_Join + mDataSet.Rows[i]["Certificate_Date_Str"].ToString();

                        Certificate_Due_Join = Certificate_Due_Join + Due_Date_Get;

                        //Get Remakrs Value
                        if (Certificate_Remarks_Join != "")
                        {
                            Certificate_Remarks_Join = Certificate_Remarks_Join + ",";
                            Certificate_Remarks_Join = Certificate_Remarks_Join + mDataSet.Rows[i]["Remarks"].ToString();
                        }
                        

                    }

                    

                    DataCell.Rows[sno]["Certificate No"] = Certificate_No_Join;
                    DataCell.Rows[sno]["Certificate Date"] = Certificate_Date_Join;
                    DataCell.Rows[sno]["Due Falls"] = Certificate_Due_Join;
                    if (Certificate_Remarks_Join != "")
                    {
                        DataCell.Rows[sno]["Remarks"] = Certificate_Remarks_Join;
                    }
                    else
                    {
                        DataCell.Rows[sno]["Remarks"] = "";
                    }

                    srno += 1;
                    sno += 1;
                       
                }
              
            }
              SSQL = "Select * from Company_Mst ";
            dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
            string name = dt1.Rows[0]["CompName"].ToString();


            grid.DataSource = DataCell;
            grid.DataBind();
            string attachment = "attachment;filename=Adolescent.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table border='1'>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">" + name +"-"+ SessionLcode + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");


            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">YOUNG PERSON WORKERS LIST </a>");
            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">Type of certificate : Adolescent</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            //Response.Write("<tr Font-Bold='true' align='center'>");
            //Response.Write("<td colspan='10'>");
            //Response.Write("<a style=\"font-weight:bold\"> FROM -" + FromDate + "</a>");
            //Response.Write("&nbsp;&nbsp;&nbsp;");
            //Response.Write("<a style=\"font-weight:bold\"> TO -" + ToDate + "</a>");
            //Response.Write("</td>");
            //Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();

        }
               
    }


    public static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = string.Empty;
        UTF8Encoding encodepwd = new UTF8Encoding();
        Decoder Decode = encodepwd.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        decryptpwd = new String(decoded_char);
        return decryptpwd;
    }
    public static string UTF8Encryption_OLD(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }

    public string UTF8Encryption(string mvarPlanText)
    {
        string cipherText = "";
        try
        {
            string passPhrase = "Altius";
            string saltValue = "info@altius.co.in";
            string hashAlgorithm = "SHA1";
            string initVector = "@1B2c3D4e5F6g7H8";
            int passwordIterations = 2;
            int keySize = 256;
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(mvarPlanText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);
            byte[] keyBytes = password.GetBytes(keySize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            cipherText = Convert.ToBase64String(cipherTextBytes);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

        return cipherText;
    }
 
    



   


    public string Right_Val(string Value, int Length)
    {
        // Recreate a RIGHT function for string manipulation
        int i = 0;
        i = 0;
        if (Value.Length >= Length)
        {
            //i = Value.Length - Length
            return Value.Substring(Value.Length - Length, Length);
        }
        else
        {
            return Value;
        }
    }
    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }

    public int GetRandom(int min, int max)
    {

        Random random = new Random(); return random.Next(min, max);
    }

   
 
}
