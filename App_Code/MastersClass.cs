﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for DistrictClass
/// </summary>
public class MastersClass
{
    private string _StateCd;
    private string _DistrictCd;
    private string _DistrictNm;
    private string _DistrictNmTN;

    private string _TalukCd;
    private string _TalukNm;
    private string _TalukNmTn;

    private string _QualificationCd;
    private string _QualificationNm;

    private string _LeaveCd;
    private string _LeaveType;

    private string _EmpTypeCd;
    private string _EmpType;
    private string _EmpCate;

    private string _BankCd;
    private string _Bankname;

    private string _CompanyCd;
    private string _CompanyNm;

    private string _DepartmentCd;
    private string _DepartmentNm;

    private string _InsuranceCmpcd;
    private string _InsuranceCmpNm;

    private string _ProbationCd;
    private string _Probationmonth;

    private string _EmployeeCd;
    private string _Designation;
    private string _allocatedleave;

    

    public MastersClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public string EmpCategory
    {
        get { return _EmpCate; }
        set { _EmpCate = value; }
    }
    public string ProbationCd
    {
        get { return _ProbationCd; }
        set { _ProbationCd = value; }
    }
    public string ProbationMonth
    {
        get { return _Probationmonth; }
        set { _Probationmonth = value; }
    }
    public string InsuranceCmpCd
    {
        get { return _InsuranceCmpcd; }
        set { _InsuranceCmpcd = value; }
    }
    public string InsuranceCmpNm
    {
        get { return _InsuranceCmpNm; }
        set { _InsuranceCmpNm = value; }
    }

    public string DepartmentCd
    {
        get { return _DepartmentCd; }
        set { _DepartmentCd = value; }
    }
    public string DepartmentNm
    {
        get { return _DepartmentNm; }
        set { _DepartmentNm = value; }
    }
    public string CompanyCd
    {
        get { return _CompanyCd; }
        set { _CompanyCd = value; }
    }
    public string CompanyNm
    {
        get { return _CompanyNm; }
        set { _CompanyNm = value; }
    }
    public string Bankcd
    {
        get { return _BankCd; }
        set { _BankCd = value; }
    }
    public string Bankname
    {
        get { return _Bankname; }
        set { _Bankname = value; }
    }
    public string StateCd
    {
        get { return _StateCd; }
        set { _StateCd = value; }
    }
    public string DistrictCd
    {
        get { return _DistrictCd; }
        set { _DistrictCd = value; }
    }
    public string DistrictNm
    {
        get { return _DistrictNm; }
        set { _DistrictNm = value; }
    }
    public string DistrictNmTN
    {
        get { return _DistrictNmTN; }
        set { _DistrictNmTN = value; }
    }

    public string TalukCd
    {
        get { return _TalukCd; }
        set { _TalukCd = value; }
    }
    public string TalukNm
    {
        get { return _TalukNm; }
        set { _TalukNm = value; }
    }
    public string TalukNmTN
    {
        get { return _TalukNmTn; }
        set { _TalukNmTn = value; }
    }
    public string Qualificationcd
    {
        get { return _QualificationCd; }
        set { _QualificationCd = value; }
    }
    public string QualificationNm
    {
        get { return _QualificationNm; }
        set { _QualificationNm = value; }
    }
    public string LeaveCd
    {
        get { return _LeaveCd; }
        set { _LeaveCd = value; }
    }
    public string LeaveType
    {
        get { return _LeaveType; }
        set { _LeaveType = value; }
    }
    public string EmpTypeCd
    {
        get { return _EmpTypeCd; }
        set { _EmpTypeCd = value; }
    }

    public string EmpType
    {
        get { return _EmpType; }
        set { _EmpType = value; }
    }
    public string EmployeeCd
    {
        get { return _EmployeeCd; }
        set { _EmployeeCd = value; }
    }
    public string Designation
    {
        get { return _Designation; }
        set { _Designation = value; }
    }
    public string allocatedleave
    {
        get { return _allocatedleave; }
        set { _allocatedleave = value; }
    }
}
