﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using MySql.Data.MySqlClient;
/// <summary>
/// Summary description for DALDataAccess
/// </summary>
namespace Altius.DataAccessLayer.DALDataAccess
{
    public class DALDataAccess : BaseDataAccess
    {

        public DataTable CheckUser_Login(string CompanyCode, string LocationCode, string UserName, string usert)
        {
            MySql.Data.MySqlClient.MySqlParameter[] UserRights = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@Ccode", CompanyCode),
                new MySql.Data.MySqlClient.MySqlParameter("@Lcode", LocationCode),
                new MySql.Data.MySqlClient.MySqlParameter("@uname", UserName),
                new MySql.Data.MySqlClient.MySqlParameter("@utype", usert),
                //new MySql.Data.MySqlClient.MySqlParameter("@SNo", ss)
            };
            DataTable dt = SQL.ExecuteDatatable("CheckUserCreation_SP", UserRights);
            return dt;
        }



        public DataTable CheckAdminRights(string SessionCcode, string SessionLcode, string UserName)
        {
            MySql.Data.MySqlClient.MySqlParameter[] AdminRights = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@CompanyCode", SessionCcode),
                new MySql.Data.MySqlClient.MySqlParameter("@LocationCode", SessionLcode),
                new MySql.Data.MySqlClient.MySqlParameter("@UserName", UserName)
            };
            DataTable dt = SQL.ExecuteDatatable("CheckAdminRights_SP", AdminRights);
            return dt;
        }



        public string UserCreationCcodeCounting(string CompanyCode, string LocationCode)
        {
            MySql.Data.MySqlClient.MySqlParameter[] CompanyAndLocation = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@CompanyCode", CompanyCode),
                new MySql.Data.MySqlClient.MySqlParameter("@LocationCode", LocationCode),
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "UserCreationCcodeCounting_SP", CompanyAndLocation);
            return str;

        }

        public DataTable UserCreationRegistration(UserCreationClass objUsercreation)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@UserName",objUsercreation.UserName),
                new MySql.Data.MySqlClient.MySqlParameter("@Password",objUsercreation.Password),
                new MySql.Data.MySqlClient.MySqlParameter("@UserType",objUsercreation.UserCode),
                new MySql.Data.MySqlClient.MySqlParameter("@Ccode",objUsercreation.Ccode),
                new MySql.Data.MySqlClient.MySqlParameter("@Lcode",objUsercreation.Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("UserCreationRegistration_SP", user);
            return dt;
        }

        public DataTable CheckEmpType_AlreadyExist(string Employeetype)
        {
            MySql.Data.MySqlClient.MySqlParameter[] Designchk = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@DesignName", Employeetype)
            };
            DataTable dt = SQL.ExecuteDatatable("Desgination_Check", Designchk);
            return dt;
        }

        public DataTable DesginationRegister(string Employeetype)
        {
            MySql.Data.MySqlClient.MySqlParameter[] Design = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@DesignName", Employeetype)
            };
            DataTable dt = SQL.ExecuteDatatable("Desgination_Register", Design);
            return dt;
        }

        public DataTable EditDesignation(int DesignNo)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@DesignNo",DesignNo),
            };
            DataTable dt = SQL.ExecuteDatatable("DesignationEdit_SP", user);
            return dt;
        }
        public DataTable EditEmployeeType(int TypeID)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@TypeID",TypeID),
            };
            DataTable dt = SQL.ExecuteDatatable("EmployeeTypeEdit_SP", user);
            return dt;
        }
        public DataTable EditWagesType(string WgsTypeNo)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@WagesTypeName",WgsTypeNo),
            };
            DataTable dt = SQL.ExecuteDatatable("WagesTypeEdit_SP", user);
            return dt;
        }

        public DataTable CheckEmployeeDesignation(int DesignNo)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@DesignNo",DesignNo),
            };
            DataTable dt = SQL.ExecuteDatatable("DesignationChechEmp_SP", user);
            return dt;
        }
        
        public DataTable CheckingEmployeeType(string TypeName)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@TypeName",TypeName),
            };
            DataTable dt = SQL.ExecuteDatatable("TypeNameEmployee_SP", user);
            return dt;
        }

        public DataTable CheckingWagesType(string TypeName)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@TypeName",TypeName),
            };
            DataTable dt = SQL.ExecuteDatatable("TypeNameWages_SP", user);
            return dt;
        }

        public DataTable DeleteDesgination(int DesignNo)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@DesignNo",DesignNo),
            };
            DataTable dt = SQL.ExecuteDatatable("DeleteDesignation_SP", user);
            return dt;
        }


        public DataTable DeleteEmployeeeType(int TypeID)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@TypeID",TypeID),
            };
            DataTable dt = SQL.ExecuteDatatable("DeleteEmployeeType_SP", user);
            return dt;
        }


        public DataTable DeleteWagesType(string WgsTypeNo)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@WagesTypeName",WgsTypeNo),
            };
            DataTable dt = SQL.ExecuteDatatable("DeleteWagesType_SP", user);
            return dt;
        }

        public DataTable DesginationDisplay()
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
              
            };
            DataTable dt = SQL.ExecuteDatatable("DesginationDisplay_SP", user);
            return dt;
        }



        public DataTable EmployeeTypeDisplay()
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
              
            };
            DataTable dt = SQL.ExecuteDatatable("EmployeeTypeDisplay_SP", user);
            return dt;
        }

        public DataTable WagesTypeDisplay()
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
              
            };
            DataTable dt = SQL.ExecuteDatatable("WagesTypeDisplay_SP", user);
            return dt;
        }

        public DataTable DesignationUpdate(string DesignName, int ss)
        {
            MySql.Data.MySqlClient.MySqlParameter[] Design = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@DesignName", DesignName),
                new MySql.Data.MySqlClient.MySqlParameter("@DesignNo", ss)
            };
            DataTable dt = SQL.ExecuteDatatable("Desgination_Update", Design);
            return dt;
        }




        public DataTable WagesTypeUpdate(string TypeName, int ss)
        {
            MySql.Data.MySqlClient.MySqlParameter[] Design = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@WagesTypeName", TypeName),
                new MySql.Data.MySqlClient.MySqlParameter("@WagesTypeNo", ss)
            };
            DataTable dt = SQL.ExecuteDatatable("WagesType_Update", Design);
            return dt;
        }

        public DataTable EmployeeDisplay(string Ccode, string Lcode)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
              new MySql.Data.MySqlClient.MySqlParameter("@Ccode",Ccode),
              new MySql.Data.MySqlClient.MySqlParameter("@Lcode",Lcode),
            };
            DataTable dt = SQL.ExecuteDatatable("EmployeeDisplay_SP", user);
            return dt;
        }


        public DataTable EmployeeTypeUpdate(string TypeName, int ss)
        {
            MySql.Data.MySqlClient.MySqlParameter[] Design = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@TypeID", ss),
                new MySql.Data.MySqlClient.MySqlParameter("@TypeName", TypeName)
            };
            DataTable dt = SQL.ExecuteDatatable("EmployeeType_Update", Design);
            return dt;
        }

        public DataTable DepartmmentUpdate(string DeptName, int ss)
        {
            MySql.Data.MySqlClient.MySqlParameter[] Design = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@DeptName", DeptName),
                new MySql.Data.MySqlClient.MySqlParameter("@DeptNo", ss)
            };
            DataTable dt = SQL.ExecuteDatatable("Department_Update", Design);
            return dt;
        }

        public DataTable EmployeeTypeRegister(string Employeetype)
        {
            MySql.Data.MySqlClient.MySqlParameter[] Design = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@TypeName", Employeetype)
            };
            DataTable dt = SQL.ExecuteDatatable("EmployeeType_Register", Design);
            return dt;
        }

        public DataTable EditUserCreation(UserCreationClass objUsercreation)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@UserName",objUsercreation.UserName),
                new MySql.Data.MySqlClient.MySqlParameter("@Password",objUsercreation.Password),
                new MySql.Data.MySqlClient.MySqlParameter("@UserType",objUsercreation.UserCode),
                new MySql.Data.MySqlClient.MySqlParameter("@Ccode",objUsercreation.Ccode),
                new MySql.Data.MySqlClient.MySqlParameter("@Lcode",objUsercreation.Lcode),
                new MySql.Data.MySqlClient.MySqlParameter("@SNo",objUsercreation.UserID)

            };
            DataTable dt = SQL.ExecuteDatatable("EditUserCreation_SP", user);
            return dt;
        }


        public DataTable UserCreationDisplay(string Ccode,string Lcode)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@Ccode",Ccode),
                new MySql.Data.MySqlClient.MySqlParameter("@Lcode",Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("UserCreationDisplay_SP", user);
            return dt;
        }

        

        public DataTable DepartmentIncentiveDisplay(string Ccode, string Lcode)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@Ccode",Ccode),
                new MySql.Data.MySqlClient.MySqlParameter("@Lcode",Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("DepartmentIncenDisplay_SP", user);
            return dt;
        }



        public DataTable DepartmentDisplay()
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
              
            };
            DataTable dt = SQL.ExecuteDatatable("DepartmentDisplay_SP", user);
            return dt;
        }




        public DataTable DeleteUserCreation(int SNo)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@SNo",SNo),
            };
            DataTable dt = SQL.ExecuteDatatable("UserCreationDelete_SP", user);
            return dt;
        }



        public DataTable ChechDepartmentPermisssion(string permtype, string deptname, string tt,string CompanyCode,string LocationCode,string Monthname)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@PermissionType",permtype),
                new MySql.Data.MySqlClient.MySqlParameter("@deptname",deptname),
                new MySql.Data.MySqlClient.MySqlParameter("@finanYear",tt),
                new MySql.Data.MySqlClient.MySqlParameter("@Month", Monthname),
                new MySql.Data.MySqlClient.MySqlParameter("@Ccode", CompanyCode),
                new MySql.Data.MySqlClient.MySqlParameter("@Lcode", LocationCode)
            };
            DataTable dt = SQL.ExecuteDatatable("PermissionTypeCheck_SP", user);
            return dt;
        }



        public DataTable CheckingDesignEmployee(string DesignName)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@Designation",DesignName),
            };
            DataTable dt = SQL.ExecuteDatatable("DesginationNameInEmployee_SP", user);
            return dt;
        }


        public DataTable CheckingEmployee(string DeptName)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@DeptName",DeptName),
            };
            DataTable dt = SQL.ExecuteDatatable("DeptNameInEmployee_SP", user);
            return dt;
        }


        public DataTable DeleteDepartment(int DeptCode)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@DeptCode",DeptCode),
            };
            DataTable dt = SQL.ExecuteDatatable("DeleteDepartment_SP", user);
            return dt;
        }


        public DataTable EditUserCreation(int SNo)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@SNo",SNo),
            };
            DataTable dt = SQL.ExecuteDatatable("UserCreationEdit_SP", user);
            return dt;
        }



        public DataTable EditDepartment(string DeptCode)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@DeptName",DeptCode),
            };
            DataTable dt = SQL.ExecuteDatatable("DepartmentEdit_SP", user);
            return dt;
        }

         public DataTable CheckEmployeeDepartment(int DeptCode)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@DeptCode",DeptCode),
            };
            DataTable dt = SQL.ExecuteDatatable("DepartmentChechEmp_SP", user);
            return dt;
        }



        public DataTable AdministrationRegistration(UserCreationClass objUsercreation)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@Ccode",objUsercreation.Ccode),
                new MySql.Data.MySqlClient.MySqlParameter("@Lcode",objUsercreation.Lcode),
                new MySql.Data.MySqlClient.MySqlParameter("@UserName",objUsercreation.UserCode),
                new MySql.Data.MySqlClient.MySqlParameter("@Data",objUsercreation.Data),
                new MySql.Data.MySqlClient.MySqlParameter("@Attn_Log_Clear",objUsercreation.AttendanceLogClear),
                new MySql.Data.MySqlClient.MySqlParameter("@ManAttnCheck",objUsercreation.ManualattendanceCheck),
                new MySql.Data.MySqlClient.MySqlParameter("@ManAttnUpload",objUsercreation.ManAttnUpload),
                new MySql.Data.MySqlClient.MySqlParameter("@Emp_Approval",objUsercreation.EmpApproval),
                new MySql.Data.MySqlClient.MySqlParameter("@Emp_Status",objUsercreation.EmpStatus),
                new MySql.Data.MySqlClient.MySqlParameter("@Emp_Mst_Data_Per",objUsercreation.Emp_Mst_Data_Perm)
           };
            DataTable dt = SQL.ExecuteDatatable("AdministrationRegistration_SP", user);
            return dt;
        }


        public DataTable dropdown_UserName(string CompanyCode, string LocationCode)
        {
            MySql.Data.MySqlClient.MySqlParameter[] UserRights = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@Ccode", CompanyCode),
                new MySql.Data.MySqlClient.MySqlParameter("@Lcode", LocationCode),
            };
            DataTable dt = SQL.ExecuteDatatable("dropdownUserName_SP", UserRights);
            return dt;
        }

       
         public DataTable  dropdown_WagesType()
        {
            MySql.Data.MySqlClient.MySqlParameter[] UserRights = 
            {
               
            };
            DataTable dt = SQL.ExecuteDatatable("DropDownWagesType_SP", UserRights);
            return dt;
        }


         public DataTable dropdown_LeaveType()
        {
            MySql.Data.MySqlClient.MySqlParameter[] dd_company = 
            {
            };
            DataTable dt = SQL.ExecuteDatatable("LeaveTypeMST_SP", dd_company);
            return dt;
        }





         public DataTable Dropdown_Company()
        {
            MySql.Data.MySqlClient.MySqlParameter[] dd_company = 
            {
            };
            DataTable dt = SQL.ExecuteDatatable("Dropdown_Company", dd_company);
            return dt;
        }


        public DataTable Dropdown_Department()
        {
            MySql.Data.MySqlClient.MySqlParameter[] dd_dept = 
            {
            };
            DataTable dt = SQL.ExecuteDatatable("DeptDisplay_SP", dd_dept);
            return dt;
        }



        public DataTable DropDown_EmpType()
        {
            MySql.Data.MySqlClient.MySqlParameter[] dd_dept = 
            {
            };
            DataTable dt = SQL.ExecuteDatatable("DropDown_EmployeeType_SP", dd_dept);
            return dt;
        }



        public DataTable DropDown_WagesType()
        {
            MySql.Data.MySqlClient.MySqlParameter[] dd_dept = 
            {
            };
            DataTable dt = SQL.ExecuteDatatable("DropDownWagesType_SP", dd_dept);
            return dt;
        }

        public DataTable Dropdown_PayPeriod()
        {
            MySql.Data.MySqlClient.MySqlParameter[] dd_payperiod = 
            {
            };
            DataTable dt = SQL.ExecuteDatatable("DropDown_PayPeriod_SP", dd_payperiod);
            return dt;
        }

     
         public DataTable DropDown_MachineID()
        {
            MySql.Data.MySqlClient.MySqlParameter[] dd_machineID = 
            {
            };
            DataTable dt = SQL.ExecuteDatatable("DropDown_MachineID_SP", dd_machineID);
            return dt;
        }

            
        public DataTable Dropdown_Desgination()
        {
            MySql.Data.MySqlClient.MySqlParameter[] dd_desgination = 
            {
            };
            DataTable dt = SQL.ExecuteDatatable("DesginationDisplay_SP", dd_desgination);
            return dt;
        }



        public DataTable DropDown_LeaveTypeMale()
        {
            MySql.Data.MySqlClient.MySqlParameter[] dd_desgination = 
            {
            };
            DataTable dt = SQL.ExecuteDatatable("DropDownLeaveTypeMale_SP", dd_desgination);
            return dt;
        }



        public string Verification_verify()
        {
            MySql.Data.MySqlClient.MySqlParameter[] Verify_verification = 
            {
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Verification_Get", Verify_verification);
            return str;
        }

        public DataTable AltiusLogin(string uname, string pwd1)
        {
            MySql.Data.MySqlClient.MySqlParameter[] ddloc = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@uname",uname),
                 new MySql.Data.MySqlClient.MySqlParameter("@pwd1", pwd1)
            };
            DataTable dt = SQL.ExecuteDatatable("AltiusLogin", ddloc);
            return dt;
        }



        public DataTable dropdown_ParticularLocation(string Ccode, string Lcode)
        {
            MySql.Data.MySqlClient.MySqlParameter[] ddloc = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@Ccode",Ccode),
                 new MySql.Data.MySqlClient.MySqlParameter("@Lcode",Lcode)
             };
            DataTable dt = SQL.ExecuteDatatable("Dropdown_ParticularLocation", ddloc);
            return dt;
        }



        public DataTable dropdown_location(string Ccode)
        {
            MySql.Data.MySqlClient.MySqlParameter[] ddloc = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@Ccode",Ccode),
             };
            DataTable dt = SQL.ExecuteDatatable("LocationCode_Drop", ddloc);
            return dt;
        }



        public DataTable Value_Verify()
        {
            MySql.Data.MySqlClient.MySqlParameter[] Value_ver = 
            {
            };
            DataTable dt = SQL.ExecuteDatatable("Value_Verify", Value_ver);
            return dt;
        }

        public string ServerDate()
        {
            MySql.Data.MySqlClient.MySqlParameter[] ContractExcessLeave = 
            {
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "ServerDate_SP", ContractExcessLeave);
            return str;
        }
        public DataTable UserLogin(string uname,string pwd)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@uname", uname),
                new MySql.Data.MySqlClient.MySqlParameter("@pwd", pwd),
                //new MySql.Data.MySqlClient.MySqlParameter("@Ccode",objuser.Ccode),
                //new MySql.Data.MySqlClient.MySqlParameter("@Lcode",objuser.Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("UserLogin", user);
            return dt;
        }



        public DataTable CheckUserCreation(string Ccode, string Lcode,string uname, string pwd)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@uname", uname),
                new MySql.Data.MySqlClient.MySqlParameter("@pwd", pwd),
                new MySql.Data.MySqlClient.MySqlParameter("@Ccode", Ccode),
                new MySql.Data.MySqlClient.MySqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("CheckUserLogin_SP", user);
            return dt;
        }


        public DataTable Login_Rights(string ccode, string lcode, string uname)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@Ccode", ccode),
                new MySql.Data.MySqlClient.MySqlParameter("@Lcode", lcode),
                new MySql.Data.MySqlClient.MySqlParameter("@uname", uname)
            };
            DataTable dt = SQL.ExecuteDatatable("CheckUserRights_SP", user);
            return dt;
        }



        public DataTable CheckCompanyMst(string Ccode, string Cname)
        {
            MySql.Data.MySqlClient.MySqlParameter[] ddloc = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@Ccode", Ccode),
                new MySql.Data.MySqlClient.MySqlParameter("@Cname", Cname)
            };
            DataTable dt = SQL.ExecuteDatatable("ChkCompanyMst_SP", ddloc);
            return dt;
        }



        public DataTable CheckLocationMst(string Ccode, string Lcode)
        {
            MySql.Data.MySqlClient.MySqlParameter[] ddloc = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@Ccode", Ccode),
                new MySql.Data.MySqlClient.MySqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("ChkLocationMst_SP", ddloc);
            return dt;
        }



        public DataTable dropdown_loc(string Ccode)
        {
            MySql.Data.MySqlClient.MySqlParameter[] ddloc = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@Ccode", Ccode)
            };
            DataTable dt = SQL.ExecuteDatatable("Dropdown_Location", ddloc);
            return dt;
        }



        public void LocationRegistration(CompanyMasterClass Comobj)
        {
            MySql.Data.MySqlClient.MySqlParameter[] LocationMaster = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@LName",Comobj.LocationName),
                new MySql.Data.MySqlClient.MySqlParameter("@CCode",Comobj.Ccode),
                new MySql.Data.MySqlClient.MySqlParameter("@LCode",Comobj.Lcode),
                new MySql.Data.MySqlClient.MySqlParameter("@Address1",Comobj.AddressOne),
                new MySql.Data.MySqlClient.MySqlParameter("@Address2",Comobj.AddressTwo),
                new MySql.Data.MySqlClient.MySqlParameter("@PinCode",Comobj.PinCode),
                new MySql.Data.MySqlClient.MySqlParameter("@State",Comobj.State),
                new MySql.Data.MySqlClient.MySqlParameter("@MobileNo",Comobj.MobileNumber),
                new MySql.Data.MySqlClient.MySqlParameter("@EmailID",Comobj.EMail),
                new MySql.Data.MySqlClient.MySqlParameter("@EstCode",Comobj.EstablishmentCode),
                new MySql.Data.MySqlClient.MySqlParameter("@UdefI",Comobj.UserDefI),
                new MySql.Data.MySqlClient.MySqlParameter("@UdefII",Comobj.UserDefII),
                new MySql.Data.MySqlClient.MySqlParameter("@UdefIII",Comobj.UserDefIII)
             };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "RegistrationLocationMaster_SP", LocationMaster);
        }


        public void CompanyRegistration(CompanyMasterClass Comobj)
        {
            MySql.Data.MySqlClient.MySqlParameter[] CompanyMaster = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@CName",Comobj.CompanyName),
                new MySql.Data.MySqlClient.MySqlParameter("@CCode",Comobj.Ccode),
                new MySql.Data.MySqlClient.MySqlParameter("@Address1",Comobj.AddressOne),
                new MySql.Data.MySqlClient.MySqlParameter("@Address2",Comobj.AddressTwo),
                new MySql.Data.MySqlClient.MySqlParameter("@PinCode",Comobj.PinCode),
                new MySql.Data.MySqlClient.MySqlParameter("@State",Comobj.State),
                new MySql.Data.MySqlClient.MySqlParameter("@MobileNo",Comobj.MobileNumber),
                new MySql.Data.MySqlClient.MySqlParameter("@EmailID",Comobj.EMail),
                new MySql.Data.MySqlClient.MySqlParameter("@EstCode",Comobj.EstablishmentCode),
                new MySql.Data.MySqlClient.MySqlParameter("@UdefI",Comobj.UserDefI),
                new MySql.Data.MySqlClient.MySqlParameter("@UdefII",Comobj.UserDefII),
                new MySql.Data.MySqlClient.MySqlParameter("@UdefIII",Comobj.UserDefIII)
             };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "RegistrationCompanyMaster_SP", CompanyMaster);
        }




        public void CompanyMaster(CompanyMasterClass Comobj)
        {
            MySql.Data.MySqlClient.MySqlParameter[] CompanyMaster = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@CName",Comobj.CompanyName),
                new MySql.Data.MySqlClient.MySqlParameter("@CCode",Comobj.Ccode),
                new MySql.Data.MySqlClient.MySqlParameter("@Address1",Comobj.AddressOne),
                new MySql.Data.MySqlClient.MySqlParameter("@Address2",Comobj.AddressTwo),
                new MySql.Data.MySqlClient.MySqlParameter("@PinCode",Comobj.PinCode),
                new MySql.Data.MySqlClient.MySqlParameter("@State",Comobj.State),
                
             };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "CompanyMaster_SP", CompanyMaster);
        }



        //public void DeductionAndOTRegistration(DeductionAndOTClass objdedOT)
        //{
        //    MySql.Data.MySqlClient.MySqlParameter[] DeductionOT = 
        //    {
        //        new MySql.Data.MySqlClient.MySqlParameter("@CompanyCode",Comobj.CompanyName),
        //        new MySql.Data.MySqlClient.MySqlParameter("@LocationCode",Comobj.LocationName),
        //        new MySql.Data.MySqlClient.MySqlParameter("@Month",Comobj.Ccode),
        //        new MySql.Data.MySqlClient.MySqlParameter("@FinancialYear",Comobj.Lcode),
        //        new MySql.Data.MySqlClient.MySqlParameter("@Wages",Comobj.AddressOne),
        //        new MySql.Data.MySqlClient.MySqlParameter("@MachineID",Comobj.AddressTwo),
        //        new MySql.Data.MySqlClient.MySqlParameter("@Name",Comobj.PinCode),
        //        new MySql.Data.MySqlClient.MySqlParameter("@TokenNo",Comobj.State),
        //        new MySql.Data.MySqlClient.MySqlParameter("@FromDate",Comobj.MobileNumber),
        //        new MySql.Data.MySqlClient.MySqlParameter("@ToDate",Comobj.EMail),
        //        new MySql.Data.MySqlClient.MySqlParameter("@AllowOtherOne",Comobj.EstablishmentCode),
        //        new MySql.Data.MySqlClient.MySqlParameter("@AllowOtherTwo",Comobj.UserType),
        //        new MySql.Data.MySqlClient.MySqlParameter("@Advance",Comobj.UserName),
        //        new MySql.Data.MySqlClient.MySqlParameter("@Canteen",Comobj.Password),
        //        new MySql.Data.MySqlClient.MySqlParameter("@LWF",Comobj.State),
        //        new MySql.Data.MySqlClient.MySqlParameter("@PTax",Comobj.MobileNumber),
        //        new MySql.Data.MySqlClient.MySqlParameter("@DeductionOtherOne",Comobj.EMail),
        //        new MySql.Data.MySqlClient.MySqlParameter("@DeductionOtherTwo",Comobj.EstablishmentCode),
        //        new MySql.Data.MySqlClient.MySqlParameter("@HAllowed",Comobj.UserType),
        //        new MySql.Data.MySqlClient.MySqlParameter("@OTHours",Comobj.UserName),
        //        new MySql.Data.MySqlClient.MySqlParameter("@CanteenDaysMinus",Comobj.Password),
        //        new MySql.Data.MySqlClient.MySqlParameter("@LOPDays",Comobj.UserType),
        //        new MySql.Data.MySqlClient.MySqlParameter("@Ccode",Comobj.UserName),
        //        new MySql.Data.MySqlClient.MySqlParameter("@Lcode",Comobj.Password),
        //     };
        //    SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "DeductionOT_SP", DeductionOT);
        //}


        public DataTable EmpIDRegistration(EmpIDDetails objEmpID)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@Ccode",objEmpID.Ccode),
                new MySql.Data.MySqlClient.MySqlParameter("@Lcode",objEmpID.Lcode),
                new MySql.Data.MySqlClient.MySqlParameter("@AdharNo",objEmpID.AdharNo),
                new MySql.Data.MySqlClient.MySqlParameter("@VoterID",objEmpID.VoterID),
                new MySql.Data.MySqlClient.MySqlParameter("@DrivingLicence",objEmpID.DrivingLicence),
                new MySql.Data.MySqlClient.MySqlParameter("@PassportNo",objEmpID.PassportNo),
                new MySql.Data.MySqlClient.MySqlParameter("@RationCardNo",objEmpID.RationCardNo),
                new MySql.Data.MySqlClient.MySqlParameter("@panCardNo",objEmpID.panCardNo),
                new MySql.Data.MySqlClient.MySqlParameter("@SmartCardNo",objEmpID.SmartCardNo),
                new MySql.Data.MySqlClient.MySqlParameter("@Other",objEmpID.Other)
            };
            DataTable dt = SQL.ExecuteDatatable("EmpIDdetails_SP", user);
            return dt;
        }


        public DataTable EmployeeDetails(EmployeeDetailsClass objEmpDetails)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@Ccode",objEmpDetails.Ccode),
                new MySql.Data.MySqlClient.MySqlParameter("@Lcode",objEmpDetails.Lcode),
                new MySql.Data.MySqlClient.MySqlParameter("@ShiftType",objEmpDetails.ShiftType),
                new MySql.Data.MySqlClient.MySqlParameter("@TypeName",objEmpDetails.EmpType),
                new MySql.Data.MySqlClient.MySqlParameter("@EmpPrefix",objEmpDetails.EmpPrefix),
                new MySql.Data.MySqlClient.MySqlParameter("@TokenNo",objEmpDetails.TokenNo),
                new MySql.Data.MySqlClient.MySqlParameter("@Category",objEmpDetails.Category),
                new MySql.Data.MySqlClient.MySqlParameter("@SubCategory",objEmpDetails.SubCategory),
                new MySql.Data.MySqlClient.MySqlParameter("@ExistingNo",objEmpDetails.ExistingNo),
                new MySql.Data.MySqlClient.MySqlParameter("@MachineID",objEmpDetails.MachineID),
                new MySql.Data.MySqlClient.MySqlParameter("@FirstName",objEmpDetails.FirstName),
                new MySql.Data.MySqlClient.MySqlParameter("@LastName",objEmpDetails.LastName),
                new MySql.Data.MySqlClient.MySqlParameter("@Initial",objEmpDetails.Initial),
                new MySql.Data.MySqlClient.MySqlParameter("@Gender",objEmpDetails.Gender),
                new MySql.Data.MySqlClient.MySqlParameter("@DOB",objEmpDetails.DOB),
                new MySql.Data.MySqlClient.MySqlParameter("@Age",objEmpDetails.Age),
                new MySql.Data.MySqlClient.MySqlParameter("@MartialStatus",objEmpDetails.MaritalStatus),
                new MySql.Data.MySqlClient.MySqlParameter("@DOJ",objEmpDetails.DOJ),
                new MySql.Data.MySqlClient.MySqlParameter("@Department",objEmpDetails.Department),
                new MySql.Data.MySqlClient.MySqlParameter("@Desgination",objEmpDetails.Desgination),
                new MySql.Data.MySqlClient.MySqlParameter("@PayPeriod_Desc",objEmpDetails.PayPeriod),
                new MySql.Data.MySqlClient.MySqlParameter("@BaseSalary",objEmpDetails.BaseSalary),
                new MySql.Data.MySqlClient.MySqlParameter("@PFNumber",objEmpDetails.PFNumber),
                new MySql.Data.MySqlClient.MySqlParameter("@Nominee",objEmpDetails.Nominee),
                new MySql.Data.MySqlClient.MySqlParameter("@ESINumber",objEmpDetails.ESINumber),
                new MySql.Data.MySqlClient.MySqlParameter("@StdWorkingHrs",objEmpDetails.StdWorkingHrs),
                new MySql.Data.MySqlClient.MySqlParameter("@OTEligible",objEmpDetails.OTEligible),
                new MySql.Data.MySqlClient.MySqlParameter("@Nationality",objEmpDetails.Nationality),
                new MySql.Data.MySqlClient.MySqlParameter("@Qualification",objEmpDetails.Qualification),
                new MySql.Data.MySqlClient.MySqlParameter("@Certificate",objEmpDetails.Certificate),
                new MySql.Data.MySqlClient.MySqlParameter("@FamilyDetails",objEmpDetails.FamilyDetails),
                new MySql.Data.MySqlClient.MySqlParameter("@RecuritThr", objEmpDetails.RecuritThr),
                new MySql.Data.MySqlClient.MySqlParameter("@IdentiMark1",objEmpDetails.IdentiMark1),
                new MySql.Data.MySqlClient.MySqlParameter("@IdentiMark2",objEmpDetails.IdentiMark2),
                new MySql.Data.MySqlClient.MySqlParameter("@BloodGroup",objEmpDetails.BloodGroup),
                new MySql.Data.MySqlClient.MySqlParameter("@Handicapped",objEmpDetails.Handicapped),
                new MySql.Data.MySqlClient.MySqlParameter("@Height",objEmpDetails.Height),
                new MySql.Data.MySqlClient.MySqlParameter("@Weight",objEmpDetails.Weight),
                new MySql.Data.MySqlClient.MySqlParameter("@Address1",objEmpDetails.PermanentAddr),
                new MySql.Data.MySqlClient.MySqlParameter("@Address2",objEmpDetails.TempAddress),
                new MySql.Data.MySqlClient.MySqlParameter("@BankName",objEmpDetails.BankName),
                new MySql.Data.MySqlClient.MySqlParameter("@BranchCode",objEmpDetails.BranchCode),
                new MySql.Data.MySqlClient.MySqlParameter("@AccountNo", objEmpDetails.AccountNo),
                new MySql.Data.MySqlClient.MySqlParameter("@EmpStatus", objEmpDetails.EmpStatus),
                new MySql.Data.MySqlClient.MySqlParameter("@IsActive",objEmpDetails.IsActive),
                new MySql.Data.MySqlClient.MySqlParameter("@Created_By",objEmpDetails.CreateBy),
                new MySql.Data.MySqlClient.MySqlParameter("@Created_Date",objEmpDetails.CreateDate),
                new MySql.Data.MySqlClient.MySqlParameter("@IsNonAdmin",objEmpDetails.IsNonAdmin),
                new MySql.Data.MySqlClient.MySqlParameter("@MachineEnc",objEmpDetails.MachineID_Encry),
                new MySql.Data.MySqlClient.MySqlParameter("@Working_Hours",objEmpDetails.Working_Hours),
                new MySql.Data.MySqlClient.MySqlParameter("@Calculate_Work_Hours",objEmpDetails.Calculate_Work_Hours),
                new MySql.Data.MySqlClient.MySqlParameter("@OTHrs", objEmpDetails.OTHrs),
                new MySql.Data.MySqlClient.MySqlParameter("@WagesType",objEmpDetails.WagesType),
                new MySql.Data.MySqlClient.MySqlParameter("@RecutersMob",objEmpDetails.RecutersMob),
                new MySql.Data.MySqlClient.MySqlParameter("@ParentsMobile1",objEmpDetails.ParentsMobile1),
                new MySql.Data.MySqlClient.MySqlParameter("@ParentsMobile2",objEmpDetails.ParentsMobile2),
                new MySql.Data.MySqlClient.MySqlParameter("@EmpMobile",objEmpDetails.EmpMobile),
                new MySql.Data.MySqlClient.MySqlParameter("@SamepresentAddress",objEmpDetails.SamepresentAddress),
                new MySql.Data.MySqlClient.MySqlParameter("@Religion",objEmpDetails.Religion),
                new MySql.Data.MySqlClient.MySqlParameter("@EmpLeft",objEmpDetails.EmpLeft),
                new MySql.Data.MySqlClient.MySqlParameter("@BusNo",objEmpDetails.BusNo),
                new MySql.Data.MySqlClient.MySqlParameter("@Reference", objEmpDetails.Reference),
                new MySql.Data.MySqlClient.MySqlParameter("@PermanentDst",objEmpDetails.PermanentDst),
                new MySql.Data.MySqlClient.MySqlParameter("@TempDst",objEmpDetails.TempDst),
                new MySql.Data.MySqlClient.MySqlParameter("@WeekOff", objEmpDetails.WeekOff),
                new MySql.Data.MySqlClient.MySqlParameter("@PFdoj", objEmpDetails.PFdoj),
                new MySql.Data.MySqlClient.MySqlParameter("@ESIdoj", objEmpDetails.ESIdoj),
                new MySql.Data.MySqlClient.MySqlParameter("@IFSCCode",objEmpDetails.IFSCCode),
                new MySql.Data.MySqlClient.MySqlParameter("@PFEligible",objEmpDetails.PFEligible),
                new MySql.Data.MySqlClient.MySqlParameter("@ESIEligible",objEmpDetails.ESIEligible),
                new MySql.Data.MySqlClient.MySqlParameter("@BusRoute",objEmpDetails.BusRoute),
                new MySql.Data.MySqlClient.MySqlParameter("@HostelRmNo",objEmpDetails.HostelRmNo),    
                new MySql.Data.MySqlClient.MySqlParameter("@BasicSalary",objEmpDetails.BasicSalary),
                new MySql.Data.MySqlClient.MySqlParameter("@Deduction1amt",objEmpDetails.Deduction1amt),
                new MySql.Data.MySqlClient.MySqlParameter("@Deduction2amt",objEmpDetails.Deduction2amt),
                new MySql.Data.MySqlClient.MySqlParameter("@Allowance1amt",objEmpDetails.Allowance1amt),
                new MySql.Data.MySqlClient.MySqlParameter("@Allowance2amt",objEmpDetails.Allowance2amt),
                new MySql.Data.MySqlClient.MySqlParameter("@PFS",objEmpDetails.PFS),
                new MySql.Data.MySqlClient.MySqlParameter("@SalaryThro",objEmpDetails.SalaryThro),
                new MySql.Data.MySqlClient.MySqlParameter("@Mother",objEmpDetails.Mother),
                new MySql.Data.MySqlClient.MySqlParameter("@Permanent_Taluk",objEmpDetails.Permanent_Taluk),
                new MySql.Data.MySqlClient.MySqlParameter("@Present_Taluk",objEmpDetails.Present_Taluk),
                new MySql.Data.MySqlClient.MySqlParameter("@BrokerName",objEmpDetails.BrokerName),
                new MySql.Data.MySqlClient.MySqlParameter("@BrokerType",objEmpDetails.Types),
                new MySql.Data.MySqlClient.MySqlParameter("@BrokerAgent",objEmpDetails.Types_Name),
                new MySql.Data.MySqlClient.MySqlParameter("@Community",objEmpDetails.Community),
                new MySql.Data.MySqlClient.MySqlParameter("@Caste",objEmpDetails.Caste),
                new MySql.Data.MySqlClient.MySqlParameter("@PhysicalRemarks",objEmpDetails.PhysicalRemarks),
                new MySql.Data.MySqlClient.MySqlParameter("@RejoinDate",objEmpDetails.RejoinDate),
                new MySql.Data.MySqlClient.MySqlParameter("RelieveDate",objEmpDetails.RelieveDate)

               

              };
            DataTable dt = SQL.ExecuteDatatable("EmployeeDetails_SP", user);
            return dt;
        }

        public DataTable EmpAdolescentRegistration(AdolescentClass objAdu)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@Ccode",objAdu.Ccode),
                new MySql.Data.MySqlClient.MySqlParameter("@Lcode",objAdu.Lcode),
                new MySql.Data.MySqlClient.MySqlParameter("@CertificateNo",objAdu.CertificateNumber),
                new MySql.Data.MySqlClient.MySqlParameter("@CertificateDate",objAdu.CertificateDate),
                new MySql.Data.MySqlClient.MySqlParameter("@NextDewDate",objAdu.NextNewDate),
                new MySql.Data.MySqlClient.MySqlParameter("@TypesOfCertificate",objAdu.TypesOfCertificate),
                new MySql.Data.MySqlClient.MySqlParameter("@IsAdolescent",objAdu.ISAdolescent),
                new MySql.Data.MySqlClient.MySqlParameter("@Remarks",objAdu.Remarks)
            };
            DataTable dt = SQL.ExecuteDatatable("EmpAdolescentdetails_SP", user);
            return dt;
        }

        public DataTable ManualAttendanceRegister(ManualAttendanceClass objManual)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                //new MySql.Data.MySqlClient.MySqlParameter("@CompanyName",objManual.CompanyCode),
                //new MySql.Data.MySqlClient.MySqlParameter("@LocationCode",objManual.LocationCode),
                new MySql.Data.MySqlClient.MySqlParameter("@TktNo",objManual.TktNo),
                new MySql.Data.MySqlClient.MySqlParameter("@ExistingCode",objManual.ExistingCode),
                new MySql.Data.MySqlClient.MySqlParameter("@EmpName",objManual.EmpName),
                new MySql.Data.MySqlClient.MySqlParameter("@AttendanceStatus",objManual.AttendanceStatus),
                new MySql.Data.MySqlClient.MySqlParameter("@TimeIn",objManual.TimeIn),
                new MySql.Data.MySqlClient.MySqlParameter("@TimeOut",objManual.TimeOut),
                new MySql.Data.MySqlClient.MySqlParameter("@LogTimeIN",objManual.LogTimeIN),
                new MySql.Data.MySqlClient.MySqlParameter("@LogTimeOUT",objManual.LogTimeOUT),
                new MySql.Data.MySqlClient.MySqlParameter("@CurrentDate",objManual.CurrentDate),
                new MySql.Data.MySqlClient.MySqlParameter("@UserName",objManual.UserName),
                new MySql.Data.MySqlClient.MySqlParameter("@AttendanceDate",objManual.AttendanceDate),
                new MySql.Data.MySqlClient.MySqlParameter("@CompensationDate",objManual.CompensationDate),
                new MySql.Data.MySqlClient.MySqlParameter("@CompAgainstDate",objManual.CoAgainstDate),
                new MySql.Data.MySqlClient.MySqlParameter("@Ccode",objManual.Ccode),
                new MySql.Data.MySqlClient.MySqlParameter("@Lcode",objManual.Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("ManualAttendanceRegister_SP", user);
            return dt;
        }





        public DataTable UpdateEmployeeDetails(EmployeeDetailsClass objEmpDetails)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@Ccode",objEmpDetails.Ccode),
                new MySql.Data.MySqlClient.MySqlParameter("@Lcode",objEmpDetails.Lcode),
                new MySql.Data.MySqlClient.MySqlParameter("@ShiftType",objEmpDetails.ShiftType),
                new MySql.Data.MySqlClient.MySqlParameter("@TypeName",objEmpDetails.EmpType),
                new MySql.Data.MySqlClient.MySqlParameter("@EmpPrefix",objEmpDetails.EmpPrefix),
                new MySql.Data.MySqlClient.MySqlParameter("@TokenNo",objEmpDetails.TokenNo),
                new MySql.Data.MySqlClient.MySqlParameter("@Category",objEmpDetails.Category),
                new MySql.Data.MySqlClient.MySqlParameter("@SubCategory",objEmpDetails.SubCategory),
                new MySql.Data.MySqlClient.MySqlParameter("@ExistingNo",objEmpDetails.ExistingNo),
                new MySql.Data.MySqlClient.MySqlParameter("@MachineID",objEmpDetails.MachineID),
                new MySql.Data.MySqlClient.MySqlParameter("@FirstName",objEmpDetails.FirstName),
                new MySql.Data.MySqlClient.MySqlParameter("@LastName",objEmpDetails.LastName),
                new MySql.Data.MySqlClient.MySqlParameter("@Initial",objEmpDetails.Initial),
                new MySql.Data.MySqlClient.MySqlParameter("@Gender",objEmpDetails.Gender),
                new MySql.Data.MySqlClient.MySqlParameter("@DOB",objEmpDetails.DOB),
                new MySql.Data.MySqlClient.MySqlParameter("@Age",objEmpDetails.Age),
                new MySql.Data.MySqlClient.MySqlParameter("@MartialStatus",objEmpDetails.MaritalStatus),
                new MySql.Data.MySqlClient.MySqlParameter("@DOJ",objEmpDetails.DOJ),
                new MySql.Data.MySqlClient.MySqlParameter("@Department",objEmpDetails.Department),
                new MySql.Data.MySqlClient.MySqlParameter("@Desgination",objEmpDetails.Desgination),
                new MySql.Data.MySqlClient.MySqlParameter("@PayPeriod_Desc",objEmpDetails.PayPeriod),
                new MySql.Data.MySqlClient.MySqlParameter("@BaseSalary",objEmpDetails.BaseSalary),
                new MySql.Data.MySqlClient.MySqlParameter("@PFNumber",objEmpDetails.PFNumber),
                new MySql.Data.MySqlClient.MySqlParameter("@Nominee",objEmpDetails.Nominee),
                new MySql.Data.MySqlClient.MySqlParameter("@ESINumber",objEmpDetails.ESINumber),
                new MySql.Data.MySqlClient.MySqlParameter("@StdWorkingHrs",objEmpDetails.StdWorkingHrs),
                new MySql.Data.MySqlClient.MySqlParameter("@OTEligible",objEmpDetails.OTEligible),
                new MySql.Data.MySqlClient.MySqlParameter("@Nationality",objEmpDetails.Nationality),
                new MySql.Data.MySqlClient.MySqlParameter("@Qualification",objEmpDetails.Qualification),
                new MySql.Data.MySqlClient.MySqlParameter("@Certificate",objEmpDetails.Certificate),
                new MySql.Data.MySqlClient.MySqlParameter("@FamilyDetails",objEmpDetails.FamilyDetails),
                new MySql.Data.MySqlClient.MySqlParameter("@RecuritThr", objEmpDetails.RecuritThr),
                new MySql.Data.MySqlClient.MySqlParameter("@IdentiMark1",objEmpDetails.IdentiMark1),
                new MySql.Data.MySqlClient.MySqlParameter("@IdentiMark2",objEmpDetails.IdentiMark2),
                new MySql.Data.MySqlClient.MySqlParameter("@BloodGroup",objEmpDetails.BloodGroup),
                new MySql.Data.MySqlClient.MySqlParameter("@Handicapped",objEmpDetails.Handicapped),
                new MySql.Data.MySqlClient.MySqlParameter("@Height",objEmpDetails.Height),
                new MySql.Data.MySqlClient.MySqlParameter("@Weight",objEmpDetails.Weight),
                new MySql.Data.MySqlClient.MySqlParameter("@Address1",objEmpDetails.PermanentAddr),
                new MySql.Data.MySqlClient.MySqlParameter("@Address2",objEmpDetails.TempAddress),
                new MySql.Data.MySqlClient.MySqlParameter("@BankName",objEmpDetails.BankName),
                new MySql.Data.MySqlClient.MySqlParameter("@BranchCode",objEmpDetails.BranchCode),
                new MySql.Data.MySqlClient.MySqlParameter("@AccountNo", objEmpDetails.AccountNo),
                new MySql.Data.MySqlClient.MySqlParameter("@EmpStatus", objEmpDetails.EmpStatus),
                new MySql.Data.MySqlClient.MySqlParameter("@IsActive",objEmpDetails.IsActive),
                new MySql.Data.MySqlClient.MySqlParameter("@Created_By",objEmpDetails.CreateBy),
                new MySql.Data.MySqlClient.MySqlParameter("@Created_Date",objEmpDetails.CreateDate),
                new MySql.Data.MySqlClient.MySqlParameter("@IsNonAdmin",objEmpDetails.IsNonAdmin),
                new MySql.Data.MySqlClient.MySqlParameter("@MachineEnc",objEmpDetails.MachineID_Encry),
                new MySql.Data.MySqlClient.MySqlParameter("@Working_Hours",objEmpDetails.Working_Hours),
                new MySql.Data.MySqlClient.MySqlParameter("@Calculate_Work_Hours",objEmpDetails.Calculate_Work_Hours),
                new MySql.Data.MySqlClient.MySqlParameter("@OTHrs", objEmpDetails.OTHrs),
                new MySql.Data.MySqlClient.MySqlParameter("@WagesType",objEmpDetails.WagesType),
                new MySql.Data.MySqlClient.MySqlParameter("@RecutersMob",objEmpDetails.RecutersMob),
                new MySql.Data.MySqlClient.MySqlParameter("@ParentsMobile1",objEmpDetails.ParentsMobile1),
                new MySql.Data.MySqlClient.MySqlParameter("@ParentsMobile2",objEmpDetails.ParentsMobile2),
                new MySql.Data.MySqlClient.MySqlParameter("@EmpMobile",objEmpDetails.EmpMobile),
                new MySql.Data.MySqlClient.MySqlParameter("@SamepresentAddress",objEmpDetails.SamepresentAddress),
                new MySql.Data.MySqlClient.MySqlParameter("@Religion",objEmpDetails.Religion),
                new MySql.Data.MySqlClient.MySqlParameter("@EmpLeft",objEmpDetails.EmpLeft),
                new MySql.Data.MySqlClient.MySqlParameter("@BusNo",objEmpDetails.BusNo),
                new MySql.Data.MySqlClient.MySqlParameter("@Reference", objEmpDetails.Reference),
                new MySql.Data.MySqlClient.MySqlParameter("@PermanentDst",objEmpDetails.PermanentDst),
                new MySql.Data.MySqlClient.MySqlParameter("@TempDst",objEmpDetails.TempDst),
                new MySql.Data.MySqlClient.MySqlParameter("@WeekOff", objEmpDetails.WeekOff),
                new MySql.Data.MySqlClient.MySqlParameter("@PFdoj", objEmpDetails.PFdoj),
                new MySql.Data.MySqlClient.MySqlParameter("@ESIdoj", objEmpDetails.ESIdoj),
                new MySql.Data.MySqlClient.MySqlParameter("@IFSCCode",objEmpDetails.IFSCCode),
                new MySql.Data.MySqlClient.MySqlParameter("@PFEligible",objEmpDetails.PFEligible),
                new MySql.Data.MySqlClient.MySqlParameter("@ESIEligible",objEmpDetails.ESIEligible),
                new MySql.Data.MySqlClient.MySqlParameter("@BusRoute",objEmpDetails.BusRoute),
                new MySql.Data.MySqlClient.MySqlParameter("@HostelRmNo",objEmpDetails.HostelRmNo),    
                new MySql.Data.MySqlClient.MySqlParameter("@BasicSalary",objEmpDetails.BasicSalary),
                new MySql.Data.MySqlClient.MySqlParameter("@Deduction1amt",objEmpDetails.Deduction1amt),
                new MySql.Data.MySqlClient.MySqlParameter("@Deduction2amt",objEmpDetails.Deduction2amt),
                new MySql.Data.MySqlClient.MySqlParameter("@Allowance1amt",objEmpDetails.Allowance1amt),
                new MySql.Data.MySqlClient.MySqlParameter("@Allowance2amt",objEmpDetails.Allowance2amt),
                new MySql.Data.MySqlClient.MySqlParameter("@PFS",objEmpDetails.PFS),
                new MySql.Data.MySqlClient.MySqlParameter("@SalaryThro",objEmpDetails.SalaryThro),
                new MySql.Data.MySqlClient.MySqlParameter("@Mother",objEmpDetails.Mother),
                new MySql.Data.MySqlClient.MySqlParameter("@Permanent_Taluk",objEmpDetails.Permanent_Taluk),
                new MySql.Data.MySqlClient.MySqlParameter("@Present_Taluk",objEmpDetails.Present_Taluk),
                new MySql.Data.MySqlClient.MySqlParameter("@BrokerName",objEmpDetails.BrokerName),
                new MySql.Data.MySqlClient.MySqlParameter("@BrokerType",objEmpDetails.Types),
                new MySql.Data.MySqlClient.MySqlParameter("@BrokerAgent",objEmpDetails.Types_Name),
                new MySql.Data.MySqlClient.MySqlParameter("@Community",objEmpDetails.Community),
                new MySql.Data.MySqlClient.MySqlParameter("@Caste",objEmpDetails.Caste),
                new MySql.Data.MySqlClient.MySqlParameter("@PhysicalRemarks",objEmpDetails.PhysicalRemarks),
                new MySql.Data.MySqlClient.MySqlParameter("@RejoinDate",objEmpDetails.RejoinDate),
                new MySql.Data.MySqlClient.MySqlParameter("RelieveDate",objEmpDetails.RelieveDate)
               

              };
            DataTable dt = SQL.ExecuteDatatable("UpdateEmployeeDetails_SP", user);
            return dt;
        }


        public DataTable UpdateEpayEmployeeDetails(EmployeeDetailsEpayClass objEpayEmpDetails)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@TokenNo",objEpayEmpDetails.TokenNo),
                new MySql.Data.MySqlClient.MySqlParameter("@MachineID",objEpayEmpDetails.MachineID),
                new MySql.Data.MySqlClient.MySqlParameter("@ExistingNo",objEpayEmpDetails.ExistingNo),
                new MySql.Data.MySqlClient.MySqlParameter("@EmpName",objEpayEmpDetails.EmpName),
                new MySql.Data.MySqlClient.MySqlParameter("@FatherName",objEpayEmpDetails.FatherName),
                new MySql.Data.MySqlClient.MySqlParameter("@Gender",objEpayEmpDetails.Gender),
                new MySql.Data.MySqlClient.MySqlParameter("@DOB",objEpayEmpDetails.DOB),
                new MySql.Data.MySqlClient.MySqlParameter("@PermanentAddr",objEpayEmpDetails.PermanentAddr),
                new MySql.Data.MySqlClient.MySqlParameter("@TempAddress",objEpayEmpDetails.TempAddress),
                new MySql.Data.MySqlClient.MySqlParameter("@PermanentTlk",objEpayEmpDetails.PermanentTlk),
                new MySql.Data.MySqlClient.MySqlParameter("@PermanentDst",objEpayEmpDetails.PermanentDst),
                new MySql.Data.MySqlClient.MySqlParameter("@EmpMobile",objEpayEmpDetails.EmpMobile),
                new MySql.Data.MySqlClient.MySqlParameter("@Qualification",objEpayEmpDetails.Qualification),
                new MySql.Data.MySqlClient.MySqlParameter("@Department",objEpayEmpDetails.Department),
                new MySql.Data.MySqlClient.MySqlParameter("@Desgination",objEpayEmpDetails.Desgination),
                new MySql.Data.MySqlClient.MySqlParameter("@Category",objEpayEmpDetails.Category),
                new MySql.Data.MySqlClient.MySqlParameter("@Ccode",objEpayEmpDetails.Ccode),
                new MySql.Data.MySqlClient.MySqlParameter("@Lcode",objEpayEmpDetails.Lcode),
                new MySql.Data.MySqlClient.MySqlParameter("@MaritalStatus",objEpayEmpDetails.MaritalStatus),


                new MySql.Data.MySqlClient.MySqlParameter("@IsActive",objEpayEmpDetails.IsActive),
                new MySql.Data.MySqlClient.MySqlParameter("@CreatedBy",objEpayEmpDetails.CreatedBy),
                new MySql.Data.MySqlClient.MySqlParameter("@CreatedDate",objEpayEmpDetails.CreatedDate),
                new MySql.Data.MySqlClient.MySqlParameter("@EmpType",objEpayEmpDetails.EmployeeType),
                new MySql.Data.MySqlClient.MySqlParameter("@Uneducated",objEpayEmpDetails.UnEducated),

                new MySql.Data.MySqlClient.MySqlParameter("@NonPFGrade",objEpayEmpDetails.NonPFGrade),
                new MySql.Data.MySqlClient.MySqlParameter("@IsValid",objEpayEmpDetails.IsValid),
                new MySql.Data.MySqlClient.MySqlParameter("@IsLeave",objEpayEmpDetails.IsLeave),
                new MySql.Data.MySqlClient.MySqlParameter("@Status",objEpayEmpDetails.Status),
                new MySql.Data.MySqlClient.MySqlParameter("@RoleCode",objEpayEmpDetails.RoleCode),
                new MySql.Data.MySqlClient.MySqlParameter("@Initial",objEpayEmpDetails.Initial),
                
            };
            DataTable dt = SQL.ExecuteDatatable("EPayUpdateEmployeeDetails_SP", user);
            return dt;
        }


        public DataTable UpdateEpayProfileDetails(OfficialProfileEPayClass objEpayProfDetails)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@TokenNo",objEpayProfDetails.TokenNo),
                new MySql.Data.MySqlClient.MySqlParameter("@MachineID",objEpayProfDetails.MachineID),
                new MySql.Data.MySqlClient.MySqlParameter("@DOJ",objEpayProfDetails.DOJ),
                new MySql.Data.MySqlClient.MySqlParameter("@ESINumber",objEpayProfDetails.ESINumber),
                new MySql.Data.MySqlClient.MySqlParameter("@PFNumber",objEpayProfDetails.PFNumber),
                new MySql.Data.MySqlClient.MySqlParameter("@BranchCode",objEpayProfDetails.BranchCode),
                new MySql.Data.MySqlClient.MySqlParameter("@BankName",objEpayProfDetails.BankName),
                new MySql.Data.MySqlClient.MySqlParameter("@AccountNo",objEpayProfDetails.AccountNo),
                new MySql.Data.MySqlClient.MySqlParameter("@WagesType",objEpayProfDetails.WagesType),
                new MySql.Data.MySqlClient.MySqlParameter("@Basic",objEpayProfDetails.Basic),
                new MySql.Data.MySqlClient.MySqlParameter("@ESIEligible",objEpayProfDetails.ESIEligible),
                new MySql.Data.MySqlClient.MySqlParameter("@PFEligible",objEpayProfDetails.PFEligible),
                new MySql.Data.MySqlClient.MySqlParameter("@Ccode",objEpayProfDetails.Ccode),
                new MySql.Data.MySqlClient.MySqlParameter("@Lcode",objEpayProfDetails.Lcode),
                //new MySql.Data.MySqlClient.MySqlParameter("@ESICode",objEpayProfDetails.ESICode),
                new MySql.Data.MySqlClient.MySqlParameter("@PFdoj",objEpayProfDetails.PFdoj),
                new MySql.Data.MySqlClient.MySqlParameter("@ESIdoj",objEpayProfDetails.ESIdoj),
                

            };
            DataTable dt = SQL.ExecuteDatatable("EPayUpdateProfileDetails_SP", user);
            return dt;
        }


        public DataTable EpayUpdateSalaryMasterDetails(SalaryMasterEpayClass objEpaySalaryMst)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@TokenNo",objEpaySalaryMst.TokenNo),
                new MySql.Data.MySqlClient.MySqlParameter("@Basic",objEpaySalaryMst.Basic),
                new MySql.Data.MySqlClient.MySqlParameter("@Allowance1",objEpaySalaryMst.Allowance1),
                new MySql.Data.MySqlClient.MySqlParameter("@Allowance2",objEpaySalaryMst.Allowance2),
                new MySql.Data.MySqlClient.MySqlParameter("@Deduction1",objEpaySalaryMst.Deduction1),
                new MySql.Data.MySqlClient.MySqlParameter("@Deduction2",objEpaySalaryMst.Deduction2),
                new MySql.Data.MySqlClient.MySqlParameter("@Ccode",objEpaySalaryMst.Ccode),
                new MySql.Data.MySqlClient.MySqlParameter("@Lcode",objEpaySalaryMst.Lcode),
                new MySql.Data.MySqlClient.MySqlParameter("@BaseSalary",objEpaySalaryMst.BaseSalary),
                 new MySql.Data.MySqlClient.MySqlParameter("@PFSalary",objEpaySalaryMst.PFSalary),
                
            };
            DataTable dt = SQL.ExecuteDatatable("EPayUpdateSalaryMasterDetails_SP", user);
            return dt;
        }










       
        public DataTable CheckDepartment_AlreadyExist(string DeptName)
        {
            MySql.Data.MySqlClient.MySqlParameter[] Deptchk = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@DeptName", DeptName)
            };
            DataTable dt = SQL.ExecuteDatatable("Department_Check", Deptchk);
            return dt;
        }

        public DataTable CheckEmployeeType_AlreadyExist(string Employeetype)
        {
            MySql.Data.MySqlClient.MySqlParameter[] EmployeeType = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@EmpType", Employeetype)
            };
            DataTable dt = SQL.ExecuteDatatable("EmployeeType_Check", EmployeeType);
            return dt;
        }



        public DataTable CheckWagesType_AlreadyExist(string Wagestype)
        {
            MySql.Data.MySqlClient.MySqlParameter[] WagesType = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@WagesTypeName", Wagestype)
            };
            DataTable dt = SQL.ExecuteDatatable("WagesType_Check", WagesType);
            return dt;
        }



        public DataTable DepartmentRegister(string DeptName)
        {
            MySql.Data.MySqlClient.MySqlParameter[] DepReg = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@DeptName", DeptName)
            };
            DataTable dt = SQL.ExecuteDatatable("Department_Register", DepReg);
            return dt;
        }



        public DataTable WagesTypeRegister(string WagesTypeName)
        {
            MySql.Data.MySqlClient.MySqlParameter[] WagesReg = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@WagesTypeName", WagesTypeName)
            };
            DataTable dt = SQL.ExecuteDatatable("WagesType_Register", WagesReg);
            return dt;
        }



        public DataTable EmployeeRegister(string EmpType)
        {
            MySql.Data.MySqlClient.MySqlParameter[] EmpReg = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@EmpType", EmpType)
            };
            DataTable dt = SQL.ExecuteDatatable("EmployeeType_Register", EmpReg);
            return dt;
        }

        public DataTable Sample_check()
        {
            MySql.Data.MySqlClient.MySqlParameter[] dd_company = 
            {
            };
            DataTable dt = SQL.ExecuteDatatable("Sample_Check", dd_company);
            return dt;
        }


        //public DataTable EmployeeDetails(EmployeeDetailsClass objEmpDetails)
        //{
        //    MySql.Data.MySqlClient.MySqlParameter[] user = 
        //    {
        //        new MySql.Data.MySqlClient.MySqlParameter("@Ccode",objEmpDetails.Ccode),
        //        new MySql.Data.MySqlClient.MySqlParameter("@Lcode",objEmpDetails.Lcode),
        //        new MySql.Data.MySqlClient.MySqlParameter("@TokenNo",objEmpDetails.TokenNo),
        //        new MySql.Data.MySqlClient.MySqlParameter("@ExistingNo",objEmpDetails.ExistingNo),
        //        new MySql.Data.MySqlClient.MySqlParameter("@MachineID",objEmpDetails.MachineID),
        //        new MySql.Data.MySqlClient.MySqlParameter("@ShiftType",objEmpDetails.ShiftType),
        //        new MySql.Data.MySqlClient.MySqlParameter("@Category",objEmpDetails.Category),
        //        new MySql.Data.MySqlClient.MySqlParameter("@SubCategory",objEmpDetails.SubCategory),
        //        new MySql.Data.MySqlClient.MySqlParameter("@FirstName",objEmpDetails.FirstName),
        //        new MySql.Data.MySqlClient.MySqlParameter("@Initial",objEmpDetails.Initial),
        //        new MySql.Data.MySqlClient.MySqlParameter("@Gender",objEmpDetails.Gender),
        //        new MySql.Data.MySqlClient.MySqlParameter("@DOB",objEmpDetails.DOB),
        //        new MySql.Data.MySqlClient.MySqlParameter("@Age",objEmpDetails.Age),
        //        new MySql.Data.MySqlClient.MySqlParameter("@MartialStatus",objEmpDetails.MaritalStatus),
        //        new MySql.Data.MySqlClient.MySqlParameter("@DOJ",objEmpDetails.DOJ),
        //        new MySql.Data.MySqlClient.MySqlParameter("@Department",objEmpDetails.Department),
        //        new MySql.Data.MySqlClient.MySqlParameter("@Desgination",objEmpDetails.Desgination),
        //        new MySql.Data.MySqlClient.MySqlParameter("@BaseSalary",objEmpDetails.BaseSalary),
        //        new MySql.Data.MySqlClient.MySqlParameter("@PFNumber",objEmpDetails.PFNumber),
        //        new MySql.Data.MySqlClient.MySqlParameter("@Nominee",objEmpDetails.Nominee),
        //        new MySql.Data.MySqlClient.MySqlParameter("@ESINumber",objEmpDetails.ESINumber),
        //        new MySql.Data.MySqlClient.MySqlParameter("@StdWorkingHrs",objEmpDetails.StdWorkingHrs),
        //        new MySql.Data.MySqlClient.MySqlParameter("@OTEligible",objEmpDetails.OTEligible),
        //        new MySql.Data.MySqlClient.MySqlParameter("@Nationality",objEmpDetails.Nationality),
        //        new MySql.Data.MySqlClient.MySqlParameter("@Qualification",objEmpDetails.Qualification),
        //        new MySql.Data.MySqlClient.MySqlParameter("@FamilyDetails",objEmpDetails.FamilyDetails),
        //        new MySql.Data.MySqlClient.MySqlParameter("@RecuritThr", objEmpDetails.RecuritThr),
        //        new MySql.Data.MySqlClient.MySqlParameter("@IdentiMark1",objEmpDetails.IdentiMark1),
        //        new MySql.Data.MySqlClient.MySqlParameter("@IdentiMark2",objEmpDetails.IdentiMark2),
        //        new MySql.Data.MySqlClient.MySqlParameter("@BloodGroup",objEmpDetails.BloodGroup),
        //        new MySql.Data.MySqlClient.MySqlParameter("@Handicapped",objEmpDetails.Handicapped),
        //        new MySql.Data.MySqlClient.MySqlParameter("@Height",objEmpDetails.Height),
        //        new MySql.Data.MySqlClient.MySqlParameter("@Weight",objEmpDetails.Weight),
        //        new MySql.Data.MySqlClient.MySqlParameter("@Address1",objEmpDetails.PermanentAddr),
        //        new MySql.Data.MySqlClient.MySqlParameter("@Address2",objEmpDetails.TempAddress),
        //        new MySql.Data.MySqlClient.MySqlParameter("@BankName",objEmpDetails.BankName),
        //        new MySql.Data.MySqlClient.MySqlParameter("@BranchCode",objEmpDetails.BranchCode),
        //        new MySql.Data.MySqlClient.MySqlParameter("@AccountNo", objEmpDetails.AccountNo),
        //        new MySql.Data.MySqlClient.MySqlParameter("@IsActive",objEmpDetails.IsActive),
        //        new MySql.Data.MySqlClient.MySqlParameter("@IsNonAdmin",objEmpDetails.IsNonAdmin),
        //        new MySql.Data.MySqlClient.MySqlParameter("@OTHrs", objEmpDetails.OTHrs),
        //        new MySql.Data.MySqlClient.MySqlParameter("@WagesType",objEmpDetails.WagesType),
        //        new MySql.Data.MySqlClient.MySqlParameter("@ParentsMobile1",objEmpDetails.ParentsMobile1),
        //        new MySql.Data.MySqlClient.MySqlParameter("@ParentsMobile2",objEmpDetails.ParentsMobile2),
        //        new MySql.Data.MySqlClient.MySqlParameter("@EmpMobile",objEmpDetails.EmpMobile),
        //        new MySql.Data.MySqlClient.MySqlParameter("@Religion",objEmpDetails.Religion),
        //        new MySql.Data.MySqlClient.MySqlParameter("@BusNo",objEmpDetails.BusNo),
        //        new MySql.Data.MySqlClient.MySqlParameter("@Reference", objEmpDetails.Reference),
        //        new MySql.Data.MySqlClient.MySqlParameter("@PermanentDst",objEmpDetails.PermanentDst),
        //        new MySql.Data.MySqlClient.MySqlParameter("@TempDst",objEmpDetails.TempDst),
        //        new MySql.Data.MySqlClient.MySqlParameter("@WeekOff", objEmpDetails.WeekOff),
        //        new MySql.Data.MySqlClient.MySqlParameter("@ESICode",objEmpDetails.ESICode),
        //        new MySql.Data.MySqlClient.MySqlParameter("@PFdoj", objEmpDetails.PFdoj),
        //        new MySql.Data.MySqlClient.MySqlParameter("@ESIdoj", objEmpDetails.ESIdoj),
        //        new MySql.Data.MySqlClient.MySqlParameter("@IFSCCode",objEmpDetails.IFSCCode),
        //        new MySql.Data.MySqlClient.MySqlParameter("@PFEligible",objEmpDetails.PFEligible),
        //        new MySql.Data.MySqlClient.MySqlParameter("@ESIEligible",objEmpDetails.ESIEligible),
        //        new MySql.Data.MySqlClient.MySqlParameter("@BusRoute",objEmpDetails.BusRoute),
        //        new MySql.Data.MySqlClient.MySqlParameter("@HostelRmNo",objEmpDetails.HostelRmNo),    
                      
                  

        //        //new MySql.Data.MySqlClient.MySqlParameter("@AdharNo",objEmpDetails.AdharNo),
        //        //new MySql.Data.MySqlClient.MySqlParameter("@VoterID",objEmpDetails.VoterID),
        //        //new MySql.Data.MySqlClient.MySqlParameter("@DrivingLicence",objEmpDetails.DrivingLicence),
        //        //new MySql.Data.MySqlClient.MySqlParameter("@PassportNo",objEmpDetails.PassportNo),
        //        //new MySql.Data.MySqlClient.MySqlParameter("@RationCardNo",objEmpDetails.RationCardNo),
        //        //new MySql.Data.MySqlClient.MySqlParameter("@panCardNo",objEmpDetails.panCardNo),
        //        //new MySql.Data.MySqlClient.MySqlParameter("@SmartCardNo",objEmpDetails.SmartCardNo),
        //        //new MySql.Data.MySqlClient.MySqlParameter("@Other",objEmpDetails.Other),

        //        //new MySql.Data.MySqlClient.MySqlParameter("@CertificateNo",objEmpDetails.CertificateNumber),
        //        //new MySql.Data.MySqlClient.MySqlParameter("@CertificateDate",objEmpDetails.CertificateDate),
        //        //new MySql.Data.MySqlClient.MySqlParameter("@NextDewDate",objEmpDetails.NextNewDate),
        //        //new MySql.Data.MySqlClient.MySqlParameter("@TypesOfCertificate",objEmpDetails.TypesOfCertificate),
        //        //new MySql.Data.MySqlClient.MySqlParameter("@IsAdolescent",objEmpDetails.ISAdolescent),
        //        //new MySql.Data.MySqlClient.MySqlParameter("@Remarks",objEmpDetails.Remarks),

        //        //new MySql.Data.MySqlClient.MySqlParameter("@BankName",objEmpDetails.BankName),
        //        //new MySql.Data.MySqlClient.MySqlParameter("@BranchCode",objEmpDetails.BranchCode),
        //        //new MySql.Data.MySqlClient.MySqlParameter("@AcountNumber",objEmpDetails.AccountNo),
        //        //new MySql.Data.MySqlClient.MySqlParameter("@IFSCNumber",objEmpDetails.IFSCnumber),


        //        //new MySql.Data.MySqlClient.MySqlParameter("@ExpInYear",objEmpDetails.ExpInYear),
        //        //new MySql.Data.MySqlClient.MySqlParameter("@ExpInMonth",objEmpDetails.ExpInMonth),
        //        //new MySql.Data.MySqlClient.MySqlParameter("@CompanyName",objEmpDetails.CompanyName),
        //        //new MySql.Data.MySqlClient.MySqlParameter("@ContactName",objEmpDetails.ContactName),
                
                
        //        //new MySql.Data.MySqlClient.MySqlParameter("@PeriodFrom",objEmpDetails.PeriodFrom),
        //        //new MySql.Data.MySqlClient.MySqlParameter("@periodTo",objEmpDetails.PeriodTo),
        //        //new MySql.Data.MySqlClient.MySqlParameter("@MobileNo1",objEmpDetails.MobileNo1),
        //        //new MySql.Data.MySqlClient.MySqlParameter("@MobileNo2",objEmpDetails.MobileNo2),
        //        //new MySql.Data.MySqlClient.MySqlParameter("@PhoneNo1",objEmpDetails.PhoneNo1),
        //        //new MySql.Data.MySqlClient.MySqlParameter("@PhoneNo2",objEmpDetails.PhoneNo2),


              


               

        //      };
        //    DataTable dt = SQL.ExecuteDatatable("EmployeeDetails_SP", user);
        //    return dt;
        //}

        public DataTable AdolescentDetails(AdolscentDetailsClass objAdlDetails)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@Ccode",objAdlDetails.Ccode),
                new MySql.Data.MySqlClient.MySqlParameter("@Lcode",objAdlDetails.Lcode),
                new MySql.Data.MySqlClient.MySqlParameter("@AdoleMode",objAdlDetails.Mode),
                new MySql.Data.MySqlClient.MySqlParameter("@ExistingNo",objAdlDetails.ExistingNo),
                new MySql.Data.MySqlClient.MySqlParameter("@MachineID",objAdlDetails.MachineID),
                new MySql.Data.MySqlClient.MySqlParameter("@DOB",objAdlDetails.DOB),
                new MySql.Data.MySqlClient.MySqlParameter("@AdlCertificateNo",objAdlDetails.AdlCertificateNo),
                new MySql.Data.MySqlClient.MySqlParameter("@AdlIssuseDate",objAdlDetails.AdlIssuseDate),
                new MySql.Data.MySqlClient.MySqlParameter("@AdlNextDueDate",objAdlDetails.AdlNextDueDate),
                new MySql.Data.MySqlClient.MySqlParameter("@AdlCertificateType",objAdlDetails.AdlCertificateType),
                new MySql.Data.MySqlClient.MySqlParameter("@Remarks",objAdlDetails.AdlRemarks)
            };
            DataTable dt = SQL.ExecuteDatatable("EmpAdolescentdetails_SP", user);
            return dt;
        }

        public DataTable ExperienceDetails(ExperienceDetailsClass objExpDetails)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@Ccode",objExpDetails.Ccode),
                new MySql.Data.MySqlClient.MySqlParameter("@Lcode",objExpDetails.Lcode),
                new MySql.Data.MySqlClient.MySqlParameter("@TokenNo",objExpDetails.TokenNo),
                new MySql.Data.MySqlClient.MySqlParameter("@ExistingNo",objExpDetails.ExistingNo),
                new MySql.Data.MySqlClient.MySqlParameter("@MachineID",objExpDetails.MachineID),
                new MySql.Data.MySqlClient.MySqlParameter("@YearExp",objExpDetails.YearExp),
                new MySql.Data.MySqlClient.MySqlParameter("@CompName",objExpDetails.CompName),
                new MySql.Data.MySqlClient.MySqlParameter("@Designation",objExpDetails.Designation),
                new MySql.Data.MySqlClient.MySqlParameter("@Department",objExpDetails.Department),
                new MySql.Data.MySqlClient.MySqlParameter("@PeriodFrom",objExpDetails.PeriodFrom),
                new MySql.Data.MySqlClient.MySqlParameter("@PeriodTo",objExpDetails.PeriodTo),
                new MySql.Data.MySqlClient.MySqlParameter("@ContactName",objExpDetails.ContactName),
                new MySql.Data.MySqlClient.MySqlParameter("@Mobile",objExpDetails.Mobile)
            };
            DataTable dt = SQL.ExecuteDatatable("EmpExperiencedetails_SP", user);
            return dt;
        }

        public DataTable EpayEmployeeDetails(EmployeeDetailsEpayClass objEpayEmpDetails)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@TokenNo",objEpayEmpDetails.TokenNo),
                new MySql.Data.MySqlClient.MySqlParameter("@MachineID",objEpayEmpDetails.MachineID),
                new MySql.Data.MySqlClient.MySqlParameter("@ExistingNo",objEpayEmpDetails.ExistingNo),
                new MySql.Data.MySqlClient.MySqlParameter("@EmpName",objEpayEmpDetails.EmpName),
                new MySql.Data.MySqlClient.MySqlParameter("@FatherName",objEpayEmpDetails.FatherName),
                new MySql.Data.MySqlClient.MySqlParameter("@Gender",objEpayEmpDetails.Gender),
                new MySql.Data.MySqlClient.MySqlParameter("@DOB",objEpayEmpDetails.DOB),
                new MySql.Data.MySqlClient.MySqlParameter("@PermanentAddr",objEpayEmpDetails.PermanentAddr),
                new MySql.Data.MySqlClient.MySqlParameter("@TempAddress",objEpayEmpDetails.TempAddress),
                new MySql.Data.MySqlClient.MySqlParameter("@PermanentTlk",objEpayEmpDetails.PermanentTlk),
                new MySql.Data.MySqlClient.MySqlParameter("@PermanentDst",objEpayEmpDetails.PermanentDst),
                new MySql.Data.MySqlClient.MySqlParameter("@EmpMobile",objEpayEmpDetails.EmpMobile),
                new MySql.Data.MySqlClient.MySqlParameter("@Qualification",objEpayEmpDetails.Qualification),
                new MySql.Data.MySqlClient.MySqlParameter("@Department",objEpayEmpDetails.Department),
                new MySql.Data.MySqlClient.MySqlParameter("@Desgination",objEpayEmpDetails.Desgination),
                new MySql.Data.MySqlClient.MySqlParameter("@Category",objEpayEmpDetails.Category),
                new MySql.Data.MySqlClient.MySqlParameter("@Ccode",objEpayEmpDetails.Ccode),
                new MySql.Data.MySqlClient.MySqlParameter("@Lcode",objEpayEmpDetails.Lcode),
                new MySql.Data.MySqlClient.MySqlParameter("@MaritalStatus",objEpayEmpDetails.MaritalStatus),


                new MySql.Data.MySqlClient.MySqlParameter("@IsActive",objEpayEmpDetails.IsActive),
                new MySql.Data.MySqlClient.MySqlParameter("@CreatedBy",objEpayEmpDetails.CreatedBy),
                new MySql.Data.MySqlClient.MySqlParameter("@CreatedDate",objEpayEmpDetails.CreatedDate),
                new MySql.Data.MySqlClient.MySqlParameter("@EmpType",objEpayEmpDetails.EmployeeType),
                new MySql.Data.MySqlClient.MySqlParameter("@Uneducated",objEpayEmpDetails.UnEducated),

                new MySql.Data.MySqlClient.MySqlParameter("@NonPFGrade",objEpayEmpDetails.NonPFGrade),
                new MySql.Data.MySqlClient.MySqlParameter("@IsValid",objEpayEmpDetails.IsValid),
                new MySql.Data.MySqlClient.MySqlParameter("@IsLeave",objEpayEmpDetails.IsLeave),
                new MySql.Data.MySqlClient.MySqlParameter("@Status",objEpayEmpDetails.Status),
                new MySql.Data.MySqlClient.MySqlParameter("@RoleCode",objEpayEmpDetails.RoleCode),
                new MySql.Data.MySqlClient.MySqlParameter("@Initial",objEpayEmpDetails.Initial),

                
            };
            DataTable dt = SQL.ExecuteDatatable("EPayEmployeeDetails_SP", user);
            return dt;
        }



        public DataTable EpayProfileDetails(OfficialProfileEPayClass objEpayProfDetails)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@TokenNo",objEpayProfDetails.TokenNo),
                new MySql.Data.MySqlClient.MySqlParameter("@MachineID",objEpayProfDetails.MachineID),
                new MySql.Data.MySqlClient.MySqlParameter("@DOJ",objEpayProfDetails.DOJ),
                new MySql.Data.MySqlClient.MySqlParameter("@ESINumber",objEpayProfDetails.ESINumber),
                new MySql.Data.MySqlClient.MySqlParameter("@PFNumber",objEpayProfDetails.PFNumber),
                new MySql.Data.MySqlClient.MySqlParameter("@BranchCode",objEpayProfDetails.BranchCode),
                new MySql.Data.MySqlClient.MySqlParameter("@BankName",objEpayProfDetails.BankName),
                new MySql.Data.MySqlClient.MySqlParameter("@AccountNo",objEpayProfDetails.AccountNo),
                new MySql.Data.MySqlClient.MySqlParameter("@WagesType",objEpayProfDetails.WagesType),
                new MySql.Data.MySqlClient.MySqlParameter("@Basic",objEpayProfDetails.Basic),
                new MySql.Data.MySqlClient.MySqlParameter("@ESIEligible",objEpayProfDetails.ESIEligible),
                new MySql.Data.MySqlClient.MySqlParameter("@PFEligible",objEpayProfDetails.PFEligible),
                new MySql.Data.MySqlClient.MySqlParameter("@Ccode",objEpayProfDetails.Ccode),
                new MySql.Data.MySqlClient.MySqlParameter("@Lcode",objEpayProfDetails.Lcode),
                //new MySql.Data.MySqlClient.MySqlParameter("@ESICode",objEpayProfDetails.ESICode),
                new MySql.Data.MySqlClient.MySqlParameter("@PFdoj",objEpayProfDetails.PFdoj),
                new MySql.Data.MySqlClient.MySqlParameter("@ESIdoj",objEpayProfDetails.ESIdoj),
                

            };
            DataTable dt = SQL.ExecuteDatatable("EPayProfileDetails_SP", user);
            return dt;
        }


        public DataTable EpaySalaryMasterDetails(SalaryMasterEpayClass objEpaySalaryMst)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@TokenNo",objEpaySalaryMst.TokenNo),
                new MySql.Data.MySqlClient.MySqlParameter("@Basic",objEpaySalaryMst.Basic),
                new MySql.Data.MySqlClient.MySqlParameter("@Allowance1",objEpaySalaryMst.Allowance1),
                new MySql.Data.MySqlClient.MySqlParameter("@Allowance2",objEpaySalaryMst.Allowance2),
                new MySql.Data.MySqlClient.MySqlParameter("@Deduction1",objEpaySalaryMst.Deduction1),
                new MySql.Data.MySqlClient.MySqlParameter("@Deduction2",objEpaySalaryMst.Deduction2),
                new MySql.Data.MySqlClient.MySqlParameter("@Ccode",objEpaySalaryMst.Ccode),
                new MySql.Data.MySqlClient.MySqlParameter("@Lcode",objEpaySalaryMst.Lcode),
                new MySql.Data.MySqlClient.MySqlParameter("@BaseSalary",objEpaySalaryMst.BaseSalary),
                 new MySql.Data.MySqlClient.MySqlParameter("@PFSalary",objEpaySalaryMst.PFSalary),
                
            };
            DataTable dt = SQL.ExecuteDatatable("EPaySalaryMasterDetails_SP", user);
            return dt;
        }



        public DataTable SaveDocumentReg(DocumentClass objdoc)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@EmpNo",objdoc.EmployeeNo),
                new MySql.Data.MySqlClient.MySqlParameter("@EmpName",objdoc.EmployeeName),
                new MySql.Data.MySqlClient.MySqlParameter("@MachineID",objdoc.MachineID),
                new MySql.Data.MySqlClient.MySqlParameter("@DocType",objdoc.DocumentType),
                new MySql.Data.MySqlClient.MySqlParameter("@DocNo",objdoc.DocumentNo),
                new MySql.Data.MySqlClient.MySqlParameter("@DocDesc",objdoc.DocumentDesc),
                new MySql.Data.MySqlClient.MySqlParameter("@pathfile",objdoc.Pathfile),
                new MySql.Data.MySqlClient.MySqlParameter("@Ccode",objdoc.Ccode),
                new MySql.Data.MySqlClient.MySqlParameter("@Lcode",objdoc.Lcode),
                
            };
            DataTable dt = SQL.ExecuteDatatable("DocumentRegister_SP", user);
            return dt;
        }




        public DataTable ExperienceDisplay(string EmpNo)
        {
            MySql.Data.MySqlClient.MySqlParameter[] EmpNo1 = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@EmpNo", EmpNo),
            };
            DataTable dt = SQL.ExecuteDatatable("EmpNoExperience_SP", EmpNo1);
            return dt;
        }

        public DataTable DeleteExperience(string SNo)
        {
            MySql.Data.MySqlClient.MySqlParameter[] EmpNo1 = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@SNo",SNo),
            };
            DataTable dt = SQL.ExecuteDatatable("DeleteExperience_SP", EmpNo1);
            return dt;
        }

        public DataTable CheckExistingNO(string ExstNo)
        {
            MySql.Data.MySqlClient.MySqlParameter[] ExstNo1 = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@ExstNo", ExstNo),
            };
            DataTable dt = SQL.ExecuteDatatable("CheckExistingNo_SP", ExstNo1);
            return dt;
        }

        public DataTable CheckAndReturn(string MchID,string Ccode,string Lcode)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                 new MySql.Data.MySqlClient.MySqlParameter("@MchID", MchID),
                 new MySql.Data.MySqlClient.MySqlParameter("@Ccode", Ccode),
                 new MySql.Data.MySqlClient.MySqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("CheckAndReturnDetails_SP", user);
            return dt;
        }

        public DataTable Manual_Data(string TokenNo)
        {
            MySql.Data.MySqlClient.MySqlParameter[] Manu = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@TokenNo", TokenNo)
            };
            DataTable dt = SQL.ExecuteDatatable("ManualAttend_Fetch", Manu);
            return dt;
        }


        public DataTable CheckStaff(string TokenNo,string SessionCcode,string SessionLcode)
        {
            MySql.Data.MySqlClient.MySqlParameter[] Manu = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@TokenNo", TokenNo),
                new MySql.Data.MySqlClient.MySqlParameter("@Ccode", SessionCcode),
                new MySql.Data.MySqlClient.MySqlParameter("@Lcode", SessionLcode)
            };
            DataTable dt = SQL.ExecuteDatatable("CheckStaff_SP", Manu);
            return dt;
        }




        public DataTable DisplayManualAttend(string Ccode, string Lcode)
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
                 new MySql.Data.MySqlClient.MySqlParameter("@Ccode", Ccode),
                 new MySql.Data.MySqlClient.MySqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("ManualAttendanceDisplay_SP", user);
            return dt;
        }



        public DataTable DisplayTimeDelete()
        {
            MySql.Data.MySqlClient.MySqlParameter[] user = 
            {
              
            };
            DataTable dt = SQL.ExecuteDatatable("TimeDeleteDisplay_SP", user);
            return dt;
        }


        public DataTable IPAddressForAll(string Ccode, string Lcode)
        {
            MySql.Data.MySqlClient.MySqlParameter[] ddloc = 
            {
                 new MySql.Data.MySqlClient.MySqlParameter("@Ccode", Ccode),
                 new MySql.Data.MySqlClient.MySqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("IPAddressForAll_SP", ddloc);
            return dt;
        }

        public DataTable IPAddressALL(string Ccode, string Lcode)
        {
            MySql.Data.MySqlClient.MySqlParameter[] ddIPAddr = 
            {
                new MySql.Data.MySqlClient.MySqlParameter("@Ccode",Ccode),
                 new MySql.Data.MySqlClient.MySqlParameter("@Lcode",Lcode)
             };
            DataTable dt = SQL.ExecuteDatatable("IPAddress_SP", ddIPAddr);
            return dt;
        }


        public DataSet read(string Query_Val)
        {
            MySql.Data.MySqlClient.MySqlParameter[] AllTypeEmployeeDetails =
            {
                new MySql.Data.MySqlClient.MySqlParameter("@Query",Query_Val)
            };
            DataSet dt1 = SQL.ExecuteDataset("RptEmployeeDetailsMultiple_SP", AllTypeEmployeeDetails);
            return dt1;
        }

        public DataTable ReturnMultipleValue(string Query_Val)
        {
            MySql.Data.MySqlClient.MySqlParameter[] AllTypeEmployeeDetails =
            {
                new MySql.Data.MySqlClient.MySqlParameter("@Query",Query_Val)
            };
            DataTable dt = SQL.ExecuteDatatable("ReturnMultipleValue_SP", AllTypeEmployeeDetails);
            return dt;
        }

        public DataTable dropdown_TokenNumber(string Ccode, string Lcode)
        {
            MySql.Data.MySqlClient.MySqlParameter[] ddloc = 
            {
              new MySql.Data.MySqlClient.MySqlParameter("@Ccode",Ccode),
              new MySql.Data.MySqlClient.MySqlParameter("@Lcode",Lcode)
             };
            DataTable dt = SQL.ExecuteDatatable("DropDown_TokenNumber", ddloc);
            return dt;
        }



        public DataTable Dropdown_ShiftType(string Ccode, string Lcode)
        {
            MySql.Data.MySqlClient.MySqlParameter[] ddloc = 
            {
              new MySql.Data.MySqlClient.MySqlParameter("@Ccode",Ccode),
              new MySql.Data.MySqlClient.MySqlParameter("@Lcode",Lcode)
             };
            DataTable dt = SQL.ExecuteDatatable("DropDown_ShiftType", ddloc);
            return dt;
        }

        public DataTable BelowFourHours(string Ccode, string Lcode)
        {
            MySql.Data.MySqlClient.MySqlParameter[] ddloc = 
            {
                 new MySql.Data.MySqlClient.MySqlParameter("@Ccode", Ccode),
                 new MySql.Data.MySqlClient.MySqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("BelowFourHours_SP", ddloc);
            return dt;
        }

        public DataTable ShiftMSt(string Ccode, string Lcode, string shift, string shifttype)
        {
            MySql.Data.MySqlClient.MySqlParameter[] ddloc = 
            {
                 new MySql.Data.MySqlClient.MySqlParameter("@Ccode", Ccode),
                 new MySql.Data.MySqlClient.MySqlParameter("@Lcode", Lcode),
                 new MySql.Data.MySqlClient.MySqlParameter("@shift", shift),
                 new MySql.Data.MySqlClient.MySqlParameter("@shifttype", shifttype)
            };
            DataTable dt = SQL.ExecuteDatatable("ShiftMstReport_SP", ddloc);
            return dt;
        }

        public DataTable ShiftMStForAll(string Ccode, string Lcode, string shifttype)
        {
            MySql.Data.MySqlClient.MySqlParameter[] ddloc = 
            {
                 new MySql.Data.MySqlClient.MySqlParameter("@Ccode", Ccode),
                 new MySql.Data.MySqlClient.MySqlParameter("@Lcode", Lcode),
                 new MySql.Data.MySqlClient.MySqlParameter("@shifttype", shifttype)
            };
            DataTable dt = SQL.ExecuteDatatable("ShiftMstReportforALL_SP", ddloc);
            return dt;
        }

        public DataTable IPAddress(string Ccode, string Lcode)
        {
            MySql.Data.MySqlClient.MySqlParameter[] ddloc = 
            {
                 new MySql.Data.MySqlClient.MySqlParameter("@Ccode", Ccode),
                 new MySql.Data.MySqlClient.MySqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("IPAddress_SP", ddloc);
            return dt;
        }

        public DataTable LateINIPAddress(string Ccode, string Lcode)
        {
            MySql.Data.MySqlClient.MySqlParameter[] ddloc = 
            {
                 new MySql.Data.MySqlClient.MySqlParameter("@Ccode", Ccode),
                 new MySql.Data.MySqlClient.MySqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("LateINIPAddress_SP", ddloc);
            return dt;
        }

        public DataTable RptEmployeeMultipleDetails(string Query_Val)
        {
            MySql.Data.MySqlClient.MySqlParameter[] AllTypeEmployeeDetails =
            {
                new MySql.Data.MySqlClient.MySqlParameter("p_Query",Query_Val)
            };
            string double_Quote=@"";
            string str = "\"";
            string StoredProcedure_Name = "CALL RptEmployeeDetailsMultiple_SP(" + str + Query_Val + str + ")";
            DataTable dt = SQL.ExecuteDatatable(StoredProcedure_Name, AllTypeEmployeeDetails);
            //DataTable dt = SQL.ExecuteDatatable("RptEmployeeDetailsMultiple_SP", AllTypeEmployeeDetails);
            //DataTable dt = SQL.ExecuteDatatable("dss", AllTypeEmployeeDetails);
            
            return dt;
        }

    }


}

