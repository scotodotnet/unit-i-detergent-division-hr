﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;


public partial class ContarctorWiseStrengthReport : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Date = "";
    string SSQL;
    //string mvarUserType = "IF User";
    string mvarUserType;
    DataTable mDataSet = new DataTable();
    DataTable dt = new DataTable();
    DataTable dt1 = new DataTable();
    DataTable dt2 = new DataTable();
    BALDataAccess objdata = new BALDataAccess();
    DataTable AutoDataTable = new DataTable();
    string iEmpDet = "";
    //char[] delimiters = new char[] { "-",">" };

    string[] delimiters = { "-", ">" };

    string mUnitName = "";
    string strPhotoPath = "";
    FileStream fs;
    BinaryReader br;
    DataRow drow;
    string Empcode;
    DataSet ds = new DataSet();
    string Division;
    string FInancialYear;
    string FromDate = "";
    MailMessage Mail = new MailMessage("SCOTO <nldvnaga.hr@gmail.com>", "sambamurthyp@nagamills.com,prabhup@nagamills.com");
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Contractor Wise Strength Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("SalaryProcess"));
                //li.Attributes.Add("class", "droplink active open");
            }

            Date = Convert.ToDateTime(DateTime.Now.AddDays(-1)).ToString();
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

           // Division = Request.QueryString["Division"].ToString();
            Division = "";
            FInancialYear = DateTime.Now.Year.ToString();
            FInancialYear = FInancialYear + "-01" + "-01";

            //FromDate = Request.QueryString["FromDate"].ToString();
            FromDate = DateTime.Now.ToShortDateString();
            if (SessionUserType == "2")
            {
                GetContractorStrenghtTable();

            }
            else
            {
                GetContractorStrenghtTable();

            }


            ds.Tables.Add(AutoDataTable);
            if (AutoDataTable.Rows.Count > 0)
            {
                ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("crystal/Contractor_Strength_Report.rpt"));

                report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                ////if (Division != "-Select-")
                ////{
                ////    report.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division:" + "'" + "'" + Division + "'";
                ////}
                ////else
                ////{
                ////    report.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division:" + "'";
                ////}
                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = report;

                //string Server_Path = Server.MapPath("~");
                //string AttachfileName_Miss = "";
                ////  string Invoice_No_Str = WitSalesOrdNo.Replace("/", "_");
                //AttachfileName_Miss = Server_Path + "/Daily_Report/Daily_Contractor_Strength " + Convert.ToDateTime(Date).ToString("dd_MM_yyyy_HHmmss") + ".pdf";

                //if (File.Exists(AttachfileName_Miss))
                //{
                //    File.Delete(AttachfileName_Miss);
                //}

                //report.ExportToDisk(ExportFormatType.PortableDocFormat, AttachfileName_Miss);
                //report.Close();
                //report.Dispose();
            }
            else
            {
                Response.Write("<script>alert('No Records Found!!!')</Script>");
            }
        }
      //  MailReport();
    }
    public void GetContractorStrenghtTable()
    {
        AutoDataTable.Columns.Add("Sno");
        AutoDataTable.Columns.Add("CompanyName");
        AutoDataTable.Columns.Add("LocationName");
        AutoDataTable.Columns.Add("Contractor");
        AutoDataTable.Columns.Add("TotalCount");
        AutoDataTable.Columns.Add("Male");
        AutoDataTable.Columns.Add("Female");
        AutoDataTable.Columns.Add("Company");
        AutoDataTable.Columns.Add("Address");
        AutoDataTable.Columns.Add("Date");


        SSQL = "SELECT contract,COUNT(*) AS Total FROM employee_mst WHERE compCode='" + SessionCcode.ToString() + "' and loccode='" + SessionLcode.ToString() + "' GROUP BY contract";

        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
        SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        string name = dt.Rows[0]["CompName"].ToString();
        string Addres = dt.Rows[0]["Add1"].ToString();
        int sno = 1;
        if (mDataSet.Rows.Count > 0)
        {

            for (int iRow = 0; iRow < mDataSet.Rows.Count; iRow++)
            {

                AutoDataTable.NewRow();
                AutoDataTable.Rows.Add();


                AutoDataTable.Rows[iRow]["Sno"] = sno;
                AutoDataTable.Rows[iRow]["CompanyName"] = SessionCcode;
                AutoDataTable.Rows[iRow]["LocationName"] = SessionLcode;
                AutoDataTable.Rows[iRow]["Contractor"] = mDataSet.Rows[iRow]["Contract"].ToString();
                AutoDataTable.Rows[iRow]["TotalCount"] = mDataSet.Rows[iRow]["Total"].ToString();
                //Male
                SSQL = "SELECT COUNT(*) as Male FROM employee_mst WHERE contract='"+ mDataSet.Rows[iRow]["Contract"].ToString() + "' AND gender='Male'";
                dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

                //Female
                SSQL = "SELECT COUNT(*) as Female FROM employee_mst WHERE contract='" + mDataSet.Rows[iRow]["Contract"].ToString() + "' AND gender='Female'";
                dt2 = objdata.RptEmployeeMultipleDetails(SSQL);

                AutoDataTable.Rows[iRow]["Male"] = dt1.Rows[0]["Male"].ToString();
                AutoDataTable.Rows[iRow]["Female"] = dt2.Rows[0]["Female"].ToString();
                AutoDataTable.Rows[iRow]["Company"] = name.ToString();
                AutoDataTable.Rows[iRow]["Address"] = Addres.ToString();
                AutoDataTable.Rows[iRow]["Date"] = DateTime.Parse(FromDate).ToString("dd/MM/yyyy");


                sno += 1;

            }
        }
       

    }
    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }
    private void MailReport()
    {
        //Mail.CC = "padmanaban";
        Mail.Subject = "Contractor Strength Report";
        Mail.Body = "Dear Sir, Please find the Attachement for (" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + ") Contractor Strength Report";
        // Mail.CC=new 
        // string FileName = Path.GetFileName(fuAttachment.PostedFile.FileName);
        string Server_Path = Server.MapPath("~");
        string AttachfileName_Miss = "";
        //  string Invoice_No_Str = WitSalesOrdNo.Replace("/", "_");
        AttachfileName_Miss = Server_Path + "/Daily_Report/Daily_Contractor_Strength " + Convert.ToDateTime(Date).ToString("dd_MM_yyyy_HHmmss") + ".pdf";
        if (File.Exists(AttachfileName_Miss))
        {
            Mail.Attachments.Add(new Attachment(AttachfileName_Miss));
        }

        Mail.IsBodyHtml = false;
        SmtpClient smtp = new SmtpClient();
        smtp.Host = "smtp.gmail.com";
        smtp.EnableSsl = true;
        NetworkCredential NetworkCred = new NetworkCredential("nldvnaga.hr@gmail.com", "Altius2005");
        smtp.UseDefaultCredentials = false;
        smtp.Credentials = NetworkCred;
        smtp.Port = 587;
        smtp.Send(Mail);
        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('Email sent.');", true);
        //File.Delete(Server_Path + "/Daily_Report/Daily_Attendance_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy") + ".pdf");
        //File.Delete(Server_Path + "/Daily_Report/Daily_Attendance_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy") + ".pdf");
        //File.Delete(Server_Path + "/Daily_Report/SalaryConsolidate_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy") + ".pdf");
        //File.Delete(Server_Path + "/Daily_Report/Daily_Improper_Punch_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy") + ".pdf");
    }
}