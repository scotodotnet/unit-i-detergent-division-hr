﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="MstStdManhours.aspx.cs" Inherits="MstStdManhours" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
<%--<script src="assets/js/form-wizards.demo.min.js"></script>--%>
 <script type="text/javascript">
     $(document).ready(function() {
         $('#example').dataTable();
      
     });
	</script>
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.select2').select2();
                $('#example').dataTable();
            }
        });
    };
</script>

<asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Master</a></li>
				<li class="active">Standard Manhours</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Standard Manhours</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Standard Manhours</h4>
                        </div>
                        <div class="panel-body">
                        <!-- begin row -->
                          <div class="row">
                           
                              <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								  <label>Shift </label>  
								 <%-- <asp:TextBox runat="server" ID="txtBankName" class="form-control"></asp:TextBox>--%>
								 <asp:DropDownList ID="ddlShift" class="form-control select2" runat="server"></asp:DropDownList>
                                            <asp:HiddenField ID="txtAutoID" runat="server" />
                                            <asp:RequiredFieldValidator ControlToValidate="ddlShift" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>
								</div>
                               </div>
                              <div class="col-md-3">
								<div class="form-group">
								  <label>Plant </label>  
								 <%-- <asp:TextBox runat="server" ID="txtBankName" class="form-control"></asp:TextBox>--%>
								 <asp:DropDownList ID="ddlPlant" class="form-control select2" runat="server">
                                     <asp:ListItem Value="1" >-Select-</asp:ListItem>
                                     <asp:ListItem Value="2" >Bar</asp:ListItem>
                                     <asp:Listitem Value="3" >Powder</asp:Listitem>
                                     <asp:Listitem Value="4" >TSP</asp:Listitem>
								 </asp:DropDownList>
                                           
                                            <asp:RequiredFieldValidator ControlToValidate="ddlPlant" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>
								</div>
                               </div>
                              <!-- end col-4 -->
                              
                             <!-- begin col-4 -->
                              <div class="col-md-2">
								<div class="form-group">
								  <label>Standard</label>
								  <asp:TextBox runat="server" ID="txtStd" class="form-control"></asp:TextBox>
                                    <asp:HiddenField runat="server" ID="txtid"></asp:HiddenField>
								  <asp:RequiredFieldValidator ControlToValidate="txtStd" Display="Dynamic"   ValidationGroup="ValidateDept_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                              <!-- end col-4 -->
                              
                              <!-- begin col-4 -->
                              <div class="col-md-2">
								<div class="form-group">
								  <label>Manhours</label>
								  <asp:TextBox runat="server" ID="txtManhrs" class="form-control"></asp:TextBox>
								  <asp:RequiredFieldValidator ControlToValidate="txtManhrs" Display="Dynamic" ValidationGroup="ValidateDept_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                                <div class="col-md-2">
								<div class="form-group">
								  <label>Date</label>
								  <asp:TextBox runat="server" ID="txtdate" class="form-control datepicker"></asp:TextBox>
								  <asp:RequiredFieldValidator ControlToValidate="txtdate" Display="Dynamic" ValidationGroup="ValidateDept_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                              <!-- end col-4 -->
                             
                              </div>
                        <!-- end row -->
                    
                         <div class="row">
                          <!-- begin col-4 -->
                          <div class="col-md-4" runat="server" visible="false">
                               <div class="form-group">
                                   <div class="col-md-8">
                                        <label class="checkbox-inline">
                                            <asp:CheckBox ID="chkDefault" runat="server" /> Default
                                        </label>
                                       
                                    </div>
                                </div>
                             </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                         ValidationGroup="ValidateDept_Field" onclick="btnSave_Click"  />
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                         onclick="btnClear_Click" />
								 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row -->
                          <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Shift</th>
                                                <th>Plant</th>
                                                <th>Standard</th>
                                                <th>ManHours</th>
                                                <th>Date</th>
                                                 <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Container.ItemIndex + 1 %></td>
                                        <td><%# Eval("Shift")%></td>
                                        <td><%# Eval("Plant")%></td>
                                        <td><%# Eval("Std")%></td>
                                        <td><%# Eval("ManHrs")%></td>
                                         <td><%# Eval("Date")%></td>
                                        <td runat="server" visible="false"><%# Eval("id")%></td>
                                        <td>
                                                    <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                        Text="" OnCommand="GridEditEnquiryClick" CommandName='<%# Eval("id")%>'>
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                        Text="" OnCommand="GridDeleteEnquiryClick" CommandName='<%# Eval("id")%>' 
                                                        CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Details?');">
                                                    </asp:LinkButton>
                                                </td>
                                       
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
                       
                        
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->
</ContentTemplate>
</asp:UpdatePanel>


</asp:Content>
