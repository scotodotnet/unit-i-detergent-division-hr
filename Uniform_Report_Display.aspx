﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Uniform_Report_Display.aspx.cs" Inherits="Uniform_Report_Display" %>

<%@ Register assembly="CrystalDecisions.Web, Version=12.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Uniform Report Display</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true" />
        </div>
        <div>
            <asp:Label ID="lblUploadSuccessfully" runat="server" Font-Bold="True" 
                Font-Size="Larger" ForeColor="Red"></asp:Label>                             
        </div>
    </form>
</body>
</html>
