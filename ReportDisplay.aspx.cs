﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.Data.SqlClient;
using System.IO;
using System.Net.Mail;
using System.Drawing;


public partial class ReportDisplay : System.Web.UI.Page
{
    System.Web.UI.WebControls.DataGrid grid =
                  new System.Web.UI.WebControls.DataGrid();
    String CurrentYear1;
    static int CurrentYear;
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode; string SessionUser;
    string SessionDivision; string SessionRights;
    BALDataAccess objdata = new BALDataAccess();
    //string SSQL = "";
    string ss1 = "";
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    string SessionUserType;
    string Date1_str;
    string Date2_str;
    string Wages_Type;
    bool Errflag = false;
    bool Errflag1 = false;


    string strDept = "";
    string strWages = "";
    string Activemode = "";
    string Status = "";
    string ReportName = "";
    string SessionEpay = "";
    string CmpName = "";
    string Cmpaddress = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            SessionRights = Session["Rights"].ToString();
            SessionUser = Session["Usernmdisplay"].ToString();
            SessionEpay = Session["SessionEpay"].ToString();

            con = new SqlConnection(constr);
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report Display";

                ReportName = Request.QueryString["ReportName"].ToString();

                if (ReportName == "EMPLOYEES MIGRATION REPORT")
                {
                    strDept = Request.QueryString["DeptName"].ToString();
                    strWages = Request.QueryString["Wages"].ToString();
                    Activemode = Request.QueryString["IsActive"].ToString();
                    Status = Request.QueryString["Status"].ToString();

                    Get_EMPLOYEES_MIGRATION_REPORT();
                }
            }
        }
    }

    private void Get_EMPLOYEES_MIGRATION_REPORT()
    {
        bool ErrFlag = false;
        string Employee_Table = "";
        string SSQL = "";
        if (Status == "Pending")
        {
            Employee_Table = "Employee_Mst_New_Emp";
        }
        if (Status == "Approval")
        {
            Employee_Table = "Employee_Mst";
        }

        SSQL = "Select ROW_NUMBER() OVER (ORDER BY ExistingCode) AS SNO,ExistingCode as TokenNo,FirstName as Name,Contract as Contract,Gender as Gender,Address1 as Address,EmployeeMobile as Phone";
        SSQL = SSQL + " from " + Employee_Table + " where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        if (strWages != "-Select-")
        {
            SSQL = SSQL + " and Wages='" + strWages + "'";
        }
        if (strDept != "-Select-")
        {
            SSQL = SSQL + " and Deptname='" + strDept + "'";
        }
        if (Activemode == "Yes")
        {
            SSQL = SSQL + " and isActive='Yes'";
        }
        else
        if (Activemode == "No")
        {
            SSQL = SSQL + " and IsActive='No'";
        }
        DataTable dt_ = new DataTable();
        dt_ = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt_.Rows.Count > 0)
        {
            DataTable dt_Cat = new DataTable();
            SSQL = "";
            SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from " + SessionEpay + ".AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            //dt = objdata.RptEmployeeMultipleDetails(SSQL);
            dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);

            if (dt_Cat.Rows.Count > 0)
            {
                CmpName = dt_Cat.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
            }

            grid.DataSource = dt_;
            grid.DataBind();
            string attachment = "attachment;filename=Migration.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='5'>");
            Response.Write("<a style=\"font-weight:bold\">" + CmpName + "</a>");
            Response.Write("--");
            Response.Write("<a style=\"font-weight:bold\">" + Cmpaddress + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='5'>");
            Response.Write("<a style=\"font-weight:bold\">EMPLOYEES MIGRATION</a>");

            Response.Write("</td>");
            Response.Write("</tr>");
           
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page,this.GetType(),"alert","alert('No Records Found');",true);
        }
    }
    public virtual void VerifyRenderingInServerForm(System.Web.UI.Control control)
    {

    }
}