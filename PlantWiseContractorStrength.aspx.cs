﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
public partial class PlantWiseContractorStrength : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Date = "";
    string SSQL;
    //string mvarUserType = "IF User";
    string mvarUserType;
    DataTable mDataSet = new DataTable();
    DataTable dt = new DataTable();
    DataTable dt1 = new DataTable();
    DataTable dt2 = new DataTable();
    DataTable dt3 = new DataTable();
    BALDataAccess objdata = new BALDataAccess();
    DataTable AutoDataTable = new DataTable();
    DataTable AutoDataTable1 = new DataTable();
    DataTable AutoDataTable2 = new DataTable();
    string iEmpDet = "";
    //char[] delimiters = new char[] { "-",">" };

    string[] delimiters = { "-", ">" };

    string mUnitName = "";
    string strPhotoPath = "";
    FileStream fs;
    BinaryReader br;
    DataRow drow;
    string Empcode;
    DataSet ds = new DataSet();
    string Division;
    string FInancialYear;
    string FromDate = "";

    MailMessage Mail = new MailMessage("SCOTO <nldvnaga.hr@gmail.com>", "sambamurthyp@nagamills.com,prabhup@nagamills.com");
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Plant Wise Contractor Strength Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("SalaryProcess"));
                //li.Attributes.Add("class", "droplink active open");
            }

            Date = Convert.ToDateTime(DateTime.Now.AddDays(-1)).ToString();
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            Division = Request.QueryString["Division"].ToString();
            FInancialYear = DateTime.Now.Year.ToString();
            FInancialYear = FInancialYear + "-01" + "-01";
            FromDate = Request.QueryString["FromDate"].ToString();
            FromDate = DateTime.Parse(FromDate).ToString("yyyy/MM/dd");
            if (SessionUserType == "2")
            {
                GetShiftManPowerTable1("logtime_bar_plant");
                GetShiftManPowerTable2("logtime_powder_plant");
                GetShiftManPowerTable3("logtime_tsp_plant");


            }
            else
            {
                GetShiftManPowerTable1("logtime_bar_plant");
                GetShiftManPowerTable2("logtime_powder_plant");
                GetShiftManPowerTable3("logtime_tsp_plant");

            }

            SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "'";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            string name = dt.Rows[0]["CompName"].ToString();
            string Addres = dt.Rows[0]["Add1"].ToString();

            for (int i = AutoDataTable.Rows.Count - 1; i >= 0; i--)
            {
                if (AutoDataTable.Rows[i]["CompanyName"] == DBNull.Value && AutoDataTable.Rows[i]["LocationName"] == DBNull.Value)
                {
                    AutoDataTable.Rows[i].Delete();
                }
            }
            AutoDataTable.AcceptChanges();
            ds.Tables.Add(AutoDataTable);
            for (int i = AutoDataTable1.Rows.Count - 1; i >= 0; i--)
            {
                if (AutoDataTable1.Rows[i]["CompanyName"] == DBNull.Value && AutoDataTable1.Rows[i]["LocationName"] == DBNull.Value)
                {
                    AutoDataTable1.Rows[i].Delete();
                }
            }
            AutoDataTable1.AcceptChanges();
            ds.Tables.Add(AutoDataTable1);
            for (int i = AutoDataTable2.Rows.Count - 1; i >= 0; i--)
            {
                if (AutoDataTable2.Rows[i]["CompanyName"] == DBNull.Value && AutoDataTable2.Rows[i]["LocationName"] == DBNull.Value)
                {
                    AutoDataTable2.Rows[i].Delete();
                }
            }
            AutoDataTable2.AcceptChanges();
            ds.Tables.Add(AutoDataTable2);

            //ds.Tables.Add(AutoDataTable);
            //ds.Tables.Add(AutoDataTable1);
            //ds.Tables.Add(AutoDataTable2);
            AutoDataTable.Merge(AutoDataTable1);
            AutoDataTable.Merge(AutoDataTable2);

            //if (AutoDataTable.Rows.Count > 0)
            //{
            //    var result= AutoDataTable.AsEnumerable().Sum(x=>Convert.ToInt32(x["Male"]));
            //    //int MaleCount = Convert.ToInt32(AutoDataTable.Compute("SUM(Male)", string.Empty));
            //    //int FemaleCount = Convert.ToInt32(AutoDataTable.Compute("SUM(Female)", string.Empty));
            //}
            //if (AutoDataTable1.Rows.Count > 0)
            //{
            //    int MaleCount = Convert.ToInt32(AutoDataTable.Compute("SUM(Male)", string.Empty));
            //    int FemaleCount = Convert.ToInt32(AutoDataTable.Compute("SUM(Female)", string.Empty));
            //}
            //if (AutoDataTable2.Rows.Count > 0)
            //{
            //    int MaleCount = Convert.ToInt32(AutoDataTable.Compute("SUM(Male)", string.Empty));
            //    int FemaleCount = Convert.ToInt32(AutoDataTable.Compute("SUM(Female)", string.Empty));
            //}
            if (AutoDataTable.Rows.Count > 0)
            {

                ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("crystal/PlantWiseContractorStrength.rpt"));

                report.DataDefinition.FormulaFields["Company"].Text = "'" + name + "'";
                report.DataDefinition.FormulaFields["Address"].Text = "'" + Addres + "'";
                report.DataDefinition.FormulaFields["LocCode"].Text = "'" + SessionLcode + "'";
                report.DataDefinition.FormulaFields["FromDate"].Text = "'" + DateTime.Parse(FromDate).ToString("dd/MM/yyyy") + "'";

                report.Database.Tables[0].SetDataSource(AutoDataTable);

               
                //report.Database.Tables[0].SetDataSource(ds.Tables[1]);
                //report.Database.Tables[0].SetDataSource(ds.Tables[2]);

                //if (Division != "-Select-")
                //{
                //    report.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division:" + "'" + "'" + Division + "'";
                //}
                //else
                //{
                //    report.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division:" + "'";
                //}
                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = report;

            }
            else
            {
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Data Found..!');", true);
                Response.Write("<script>alert('No Records Found!!!')</Script>");

            }
        }
    }
    public void GetShiftManPowerTable1(String Plant)
    {
        string Male = "";
        string Female = "";

        AutoDataTable.Columns.Add("Sno");
        AutoDataTable.Columns.Add("CompanyName");
        AutoDataTable.Columns.Add("LocationName");
        AutoDataTable.Columns.Add("Shift");
        AutoDataTable.Columns.Add("Plant");
        AutoDataTable.Columns.Add("Contractor");
        AutoDataTable.Columns.Add("Male");
        AutoDataTable.Columns.Add("Female");
        AutoDataTable.Columns.Add("Company");
        AutoDataTable.Columns.Add("Address");
        AutoDataTable.Columns.Add("FromDate");
       
        DataTable Cnt = new DataTable();
        DataTable Shift = new DataTable();

        SSQL = "SELECT * FROM mstcontract where compCode='" + SessionCcode.ToString() + "' and loccode='" + SessionLcode.ToString() + "'";
        Cnt = objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "SELECT * FROM Shift_mst where compCode='" + SessionCcode.ToString() + "' and loccode='" + SessionLcode.ToString() + "'";
        Shift = objdata.RptEmployeeMultipleDetails(SSQL);

       
     
        int sno = 1;
        int Mcount = 0;
        if (Cnt.Rows.Count > 0)
        {
            for (int iRow = 0; iRow < Shift.Rows.Count; iRow++)
            {

                SSQL = "SELECT CASE WHEN EM.Gender='Male' THEN COALESCE(COUNT(EM.Gender),0) END AS Male,CASE WHEN EM.Gender='Female' THEN COALESCE(COUNT(EM.Gender),0) END AS Female, ";
                SSQL = SSQL + " em.shift_name,'Bar Plant' AS Plant,Con.Contractor as Contractor FROM employee_mst em  INNER JOIN logtime_bar_plant lp ON lp.machineid=em.machineID_Encrypt INNER JOIN MstContract Con ON Con.Contractor=Em.Contract ";
                SSQL = SSQL + " WHERE DATE_FORMAT(lp.TIME,'%Y-%m-%d')='" + Convert.ToDateTime(FromDate).ToString("yyyy-MM-dd") + "' and em.compCode='" + SessionCcode.ToString() + "' and em.loccode='" + SessionLcode.ToString() + "' AND EM.Shift_Name='" + Shift.Rows[iRow]["ShiftDesc"].ToString() + "' GROUP BY con.contractor,Em.Gender";
                dt3 = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt3.Rows.Count > 0)
                {
                    for (int k = 0; k < dt3.Rows.Count; k++)
                    {


                        AutoDataTable.NewRow();
                        AutoDataTable.Rows.Add();

                        if (dt3.Rows[0]["shift_name"].ToString() == "")
                        {

                            //AutoDataTable.Rows[k]["Sno"] = sno;
                            AutoDataTable.Rows[Mcount]["CompanyName"] = SessionCcode;
                            AutoDataTable.Rows[Mcount]["LocationName"] = SessionLcode;
                            AutoDataTable.Rows[Mcount]["Shift"] = Shift.Rows[iRow]["ShiftDesc"].ToString();
                            AutoDataTable.Rows[Mcount]["Plant"] = "BAR";
                            AutoDataTable.Rows[Mcount]["Contractor"] = Cnt.Rows[iRow]["Contractor"].ToString();
                            AutoDataTable.Rows[Mcount]["Male"] = "0";
                            AutoDataTable.Rows[Mcount]["Female"] = "0";
                            AutoDataTable.Rows[Mcount]["FromDate"] = DateTime.Parse(FromDate).ToString("dd/MM/yyyy");

                            //sno += 1;
                        }
                        else
                        {
                            AutoDataTable.Rows[Mcount]["Sno"] = sno;
                            AutoDataTable.Rows[Mcount]["CompanyName"] = SessionCcode;
                            AutoDataTable.Rows[Mcount]["LocationName"] = SessionLcode;
                            AutoDataTable.Rows[Mcount]["Shift"] = dt3.Rows[0]["shift_name"].ToString();
                            AutoDataTable.Rows[Mcount]["Plant"] = dt3.Rows[0]["Plant"].ToString();
                            AutoDataTable.Rows[Mcount]["Contractor"] = dt3.Rows[k]["Contractor"].ToString();
                            //AutoDataTable.Rows[k]["Address"] = Addres.ToString();
                            //AutoDataTable.Rows[k]["Company"] = name.ToString();
                            Male= dt3.Rows[k]["Male"].ToString();
                            if (Male == "")
                            {
                                AutoDataTable.Rows[Mcount]["Male"] = "0";
                            }
                            else
                            {
                                AutoDataTable.Rows[Mcount]["Male"] = dt3.Rows[k]["Male"].ToString();
                            }
                            Female = dt3.Rows[k]["Female"].ToString();
                            if (Female == "")
                            {
                                AutoDataTable.Rows[Mcount]["Female"] = "0";
                            }
                            else
                            {
                                AutoDataTable.Rows[Mcount]["Female"] = dt3.Rows[k]["Female"].ToString();
                            }
                            AutoDataTable.Rows[Mcount]["FromDate"] = DateTime.Parse(FromDate).ToString("dd/MM/yyyy");

                            sno += 1;
                        }
                        Mcount += 1;
                    }
                }
            }
        }
    
    }
    public void GetShiftManPowerTable2(String Plant)
    {
        string Male = "";
        string Female = "";
        AutoDataTable1.Columns.Add("Sno");
        AutoDataTable1.Columns.Add("CompanyName");
        AutoDataTable1.Columns.Add("LocationName");
        AutoDataTable1.Columns.Add("Shift");
        AutoDataTable1.Columns.Add("Plant");
        AutoDataTable1.Columns.Add("Contractor");
        AutoDataTable1.Columns.Add("Male");
        AutoDataTable1.Columns.Add("Female");
        AutoDataTable1.Columns.Add("Company");
        AutoDataTable1.Columns.Add("Address");
        AutoDataTable1.Columns.Add("FromDate");

        DataTable Cnt = new DataTable();
        DataTable Shift = new DataTable();

        SSQL = "SELECT * FROM mstcontract where compCode='" + SessionCcode.ToString() + "' and loccode='" + SessionLcode.ToString() + "'";
        Cnt = objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "SELECT * FROM Shift_mst where compCode='" + SessionCcode.ToString() + "' and loccode='" + SessionLcode.ToString() + "'";
        Shift = objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        string name = dt.Rows[0]["CompName"].ToString();
        string Addres = dt.Rows[0]["Add1"].ToString();
        int sno = 1;
        int Mcount=0;
        if (Cnt.Rows.Count > 0)
        {
            for (int iRow = 0; iRow < Shift.Rows.Count; iRow++)
            {

                SSQL = "SELECT CASE WHEN EM.Gender='Male' THEN COALESCE(COUNT(EM.Gender),0) END AS Male,CASE WHEN EM.Gender='Female' THEN COALESCE(COUNT(EM.Gender),0) END AS Female, ";
                SSQL = SSQL + " em.shift_name,'Powder Plant' AS Plant,Con.Contractor as Contractor FROM employee_mst em  INNER JOIN logtime_Powder_plant lp ON lp.machineid=em.machineID_Encrypt INNER JOIN MstContract Con ON Con.Contractor=Em.Contract ";
                SSQL = SSQL + " WHERE DATE_FORMAT(lp.TIME,'%Y-%m-%d')='" + Convert.ToDateTime(FromDate).ToString("yyyy-MM-dd") + "' and em.compCode='" + SessionCcode.ToString() + "' and em.loccode='" + SessionLcode.ToString() + "' AND EM.Shift_Name='" + Shift.Rows[iRow]["ShiftDesc"].ToString() + "' GROUP BY con.contractor,Em.Gender";
                dt3 = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt3.Rows.Count > 0)
                {
                    //Mcount = Convert.ToInt32(dt3.Rows.Count) + Convert.ToInt32(AutoDataTable1.Rows.Count);
                    for (int k = 0; k < dt3.Rows.Count; k++)
                    {
                        AutoDataTable1.NewRow();
                        AutoDataTable1.Rows.Add();

                        if (dt3.Rows[0]["shift_name"].ToString() == "")
                        {

                           //AutoDataTable1.Rows[k]["Sno"] = sno;
                            AutoDataTable1.Rows[Mcount]["CompanyName"] = SessionCcode;
                            AutoDataTable1.Rows[Mcount]["LocationName"] = SessionLcode;
                            AutoDataTable1.Rows[Mcount]["Shift"] = Shift.Rows[iRow]["ShiftDesc"].ToString();
                            AutoDataTable1.Rows[Mcount]["Plant"] = "BAR";
                            AutoDataTable1.Rows[Mcount]["Contractor"] = Cnt.Rows[iRow]["Contractor"].ToString();
                            AutoDataTable1.Rows[Mcount]["Address"] = Addres.ToString();
                            AutoDataTable1.Rows[Mcount]["Company"] = name.ToString();
                            AutoDataTable1.Rows[Mcount]["Male"] = "0";
                            AutoDataTable1.Rows[Mcount]["Female"] = "0";
                            AutoDataTable1.Rows[Mcount]["FromDate"] = DateTime.Parse(FromDate).ToString("dd/MM/yyyy");

                           // sno += 1;
                        }
                        else
                        {
                            AutoDataTable1.Rows[Mcount]["Sno"] = sno;
                            AutoDataTable1.Rows[Mcount]["CompanyName"] = SessionCcode;
                            AutoDataTable1.Rows[Mcount]["LocationName"] = SessionLcode;
                            AutoDataTable1.Rows[Mcount]["Shift"] = dt3.Rows[0]["shift_name"].ToString();
                            AutoDataTable1.Rows[Mcount]["Plant"] = dt3.Rows[0]["Plant"].ToString();
                            AutoDataTable1.Rows[Mcount]["Contractor"] = dt3.Rows[k]["Contractor"].ToString();
                            AutoDataTable1.Rows[Mcount]["Address"] = Addres.ToString();
                            AutoDataTable1.Rows[Mcount]["Company"] = name.ToString();
                            Male = dt3.Rows[k]["Male"].ToString();
                            if (Male == "")
                            {
                                AutoDataTable1.Rows[Mcount]["Male"] = "0";
                            }
                            else
                            {
                                AutoDataTable1.Rows[Mcount]["Male"] = dt3.Rows[k]["Male"].ToString();
                            }
                            Female = dt3.Rows[k]["Female"].ToString();
                            if (Female == "")
                            {
                                AutoDataTable1.Rows[Mcount]["Female"] = "0";
                            }
                            else
                            {
                                AutoDataTable1.Rows[Mcount]["Female"] = dt3.Rows[k]["Female"].ToString();
                            }
                            AutoDataTable1.Rows[Mcount]["FromDate"] = DateTime.Parse(FromDate).ToString("dd/MM/yyyy");

                            sno += 1;
                        }
                        Mcount += 1;
                    }

                }
               
            }
        }

    }
    public void GetShiftManPowerTable3(String Plant)
    {
        string Male = "";
        string Female = "";
        AutoDataTable2.Columns.Add("Sno");
        AutoDataTable2.Columns.Add("CompanyName");
        AutoDataTable2.Columns.Add("LocationName");
        AutoDataTable2.Columns.Add("Shift");
        AutoDataTable2.Columns.Add("Plant");
        AutoDataTable2.Columns.Add("Contractor");
        AutoDataTable2.Columns.Add("Male");
        AutoDataTable2.Columns.Add("Female");
        AutoDataTable2.Columns.Add("Company");
        AutoDataTable2.Columns.Add("Address");
        AutoDataTable2.Columns.Add("FromDate");

        DataTable Cnt = new DataTable();
        DataTable Shift = new DataTable();

        SSQL = "SELECT * FROM mstcontract where compCode='" + SessionCcode.ToString() + "' and loccode='" + SessionLcode.ToString() + "'";
        Cnt = objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "SELECT * FROM Shift_mst where compCode='" + SessionCcode.ToString() + "' and loccode='" + SessionLcode.ToString() + "'";
        Shift = objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        string name = dt.Rows[0]["CompName"].ToString();
        string Addres = dt.Rows[0]["Add1"].ToString();
        int sno = 1;
        int Mcount = 0;
        if (Cnt.Rows.Count > 0)
        {
            for (int iRow = 0; iRow < Shift.Rows.Count; iRow++)
            {

                SSQL = "SELECT CASE WHEN EM.Gender='Male' THEN COALESCE(COUNT(EM.Gender),0) END AS Male,CASE WHEN EM.Gender='Female' THEN COALESCE(COUNT(EM.Gender),0) END AS Female, ";
                SSQL = SSQL + " em.shift_name,'TSP Plant' AS Plant,Con.Contractor as Contractor FROM employee_mst em  INNER JOIN logtime_TSP_plant lp ON lp.machineid=em.machineID_Encrypt INNER JOIN MstContract Con ON Con.Contractor=Em.Contract ";
                SSQL = SSQL + " WHERE DATE_FORMAT(lp.TIME,'%Y-%m-%d')='" + Convert.ToDateTime(FromDate).ToString("yyyy-MM-dd") + "' and em.compCode='" + SessionCcode.ToString() + "' and em.loccode='" + SessionLcode.ToString() + "' AND EM.Shift_Name='" + Shift.Rows[iRow]["ShiftDesc"].ToString() + "' GROUP BY con.contractor,Em.Gender";
                dt3 = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt3.Rows.Count > 0)
                {
                    for (int k = 0; k < dt3.Rows.Count; k++)
                    {


                        AutoDataTable2.NewRow();
                        AutoDataTable2.Rows.Add();

                        if (dt3.Rows[0]["shift_name"].ToString() == "")
                        {

                            AutoDataTable2.Rows[Mcount]["Sno"] = sno;
                            AutoDataTable2.Rows[Mcount]["CompanyName"] = SessionCcode;
                            AutoDataTable2.Rows[Mcount]["LocationName"] = SessionLcode;
                            AutoDataTable2.Rows[Mcount]["Shift"] = Shift.Rows[iRow]["ShiftDesc"].ToString();
                            AutoDataTable2.Rows[Mcount]["Plant"] = "BAR";
                            AutoDataTable2.Rows[Mcount]["Contractor"] = Cnt.Rows[iRow]["Contractor"].ToString();
                            AutoDataTable2.Rows[Mcount]["Address"] = Addres.ToString();
                            AutoDataTable2.Rows[Mcount]["Company"] = name.ToString();
                            AutoDataTable2.Rows[Mcount]["Male"] = "0";
                            AutoDataTable2.Rows[Mcount]["Female"] = "0";
                            AutoDataTable2.Rows[Mcount]["FromDate"] = DateTime.Parse(FromDate).ToString("dd/MM/yyyy");

                            sno += 1;
                        }
                        else
                        {
                            AutoDataTable2.Rows[Mcount]["Sno"] = sno;
                            AutoDataTable2.Rows[Mcount]["CompanyName"] = SessionCcode;
                            AutoDataTable2.Rows[Mcount]["LocationName"] = SessionLcode;
                            AutoDataTable2.Rows[Mcount]["Shift"] = dt3.Rows[0]["shift_name"].ToString();
                            AutoDataTable2.Rows[Mcount]["Plant"] = dt3.Rows[0]["Plant"].ToString();
                            AutoDataTable2.Rows[Mcount]["Contractor"] = dt3.Rows[k]["Contractor"].ToString();
                            AutoDataTable2.Rows[Mcount]["Address"] = Addres.ToString();
                            AutoDataTable2.Rows[Mcount]["Company"] = name.ToString();
                            Male = dt3.Rows[k]["Male"].ToString();
                            if (Male == "")
                            {
                                AutoDataTable2.Rows[Mcount]["Male"] = "0";
                            }
                            else
                            {
                                AutoDataTable2.Rows[Mcount]["Male"] = dt3.Rows[k]["Male"].ToString();
                            }
                            Female = dt3.Rows[k]["Female"].ToString();
                            if (Female == "")
                            {
                                AutoDataTable2.Rows[Mcount]["Female"] = "0";
                            }
                            else
                            {
                                AutoDataTable2.Rows[Mcount]["Female"] = dt3.Rows[k]["Female"].ToString();
                            }
                            AutoDataTable2.Rows[Mcount]["FromDate"] = DateTime.Parse(FromDate).ToString("dd/MM/yyyy");

                            sno += 1;
                        }
                        Mcount += 1;
                    }
                }
            }
        }

    }
    //protected void Page_Unload(object sender, EventArgs e)
    //{
    //    CrystalReportViewer1.Dispose();
    //}
    private void MailReport()
    {
        //Mail.CC = "padmanaban";
        Mail.Subject = "Shift Manpower Report";
        Mail.Body = "Dear Sir, Please find the Attachement for (" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + ") Shift Manpower Report";
        // Mail.CC=new 
        // string FileName = Path.GetFileName(fuAttachment.PostedFile.FileName);
        string Server_Path = Server.MapPath("~");
        string AttachfileName_Miss = "";
        //  string Invoice_No_Str = WitSalesOrdNo.Replace("/", "_");
        AttachfileName_Miss = Server_Path + "/Daily_Report/Shift_Manpower " + Convert.ToDateTime(Date).ToString("dd_MM_yyyy_HHmmss") + ".pdf";
        if (File.Exists(AttachfileName_Miss))
        {
            Mail.Attachments.Add(new Attachment(AttachfileName_Miss));
        }

        Mail.IsBodyHtml = false;
        SmtpClient smtp = new SmtpClient();
        smtp.Host = "smtp.gmail.com";
        smtp.EnableSsl = true;
        NetworkCredential NetworkCred = new NetworkCredential("nldvnaga.hr@gmail.com", "Altius2005");
        smtp.UseDefaultCredentials = false;
        smtp.Credentials = NetworkCred;
        smtp.Port = 587;
        smtp.Send(Mail);
        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('Email sent.');", true);
        //File.Delete(Server_Path + "/Daily_Report/Daily_Attendance_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy") + ".pdf");
        //File.Delete(Server_Path + "/Daily_Report/Daily_Attendance_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy") + ".pdf");
        //File.Delete(Server_Path + "/Daily_Report/SalaryConsolidate_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy") + ".pdf");
        //File.Delete(Server_Path + "/Daily_Report/Daily_Improper_Punch_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy") + ".pdf");
    }
}