﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="MstShiftRoster.aspx.cs" Inherits="MstShiftRoster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="Basic" runat="server">
        <ContentTemplate>
            <!-- begin #content -->
            <div id="content" class="content">
                <ol class="breadcrumb pull-right">
                    <li class="active">Master</li>
                </ol>
                <h1 class="page-header">Shift Roster</h1>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-inverse">
                            <div class="panel-heading">
                                <div class="panel-heading-btn">
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                                </div>
                                <h4 class="panel-title">Shift Roster</h4>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Roster Name</label>
                                                <asp:TextBox runat="server" ID="txtRosterName" class="form-control"></asp:TextBox>
                                                <asp:HiddenField ID="hiddenID" runat="server" />
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Shift Name</label>
                                                <asp:DropDownList runat="server" ID="ddlShift" AutoPostBack="true"
                                                    class="form-control select2">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <br />
                                                <asp:Button runat="server" ID="btnSave" Text="Save" class="btn btn-success"
                                                    OnClick="btnSave_Click" />
                                                <asp:Button runat="server" ID="btnClear" Text="Clear" class="btn btn-danger"
                                                    OnClick="btnClear_Click" />
                                            </div>
                                        </div>
                                        <div class="col-md-3"></div>
                                    </div>
                                    <div class="row">
                                        <!-- table start -->
                                        <div class="col-md-12">
                                            <div class="row">
                                                <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                                    <HeaderTemplate>
                                                        <table id="example" class="display table table-bordered">
                                                            <thead>
                                                                <tr>
                                                                    <th>S.No</th>
                                                                    <th>Roster Name</th>
                                                                    <th>Shift Order</th>
                                                                    <th>Mode</th>
                                                                </tr>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td><%# Container.ItemIndex + 1 %></td>
                                                            <td><%# Eval("Roster_Name")%></td>
                                                            <td><%# Eval("Shift_Order")%></td>
                                                            <td>
                                                                <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil" runat="server"
                                                                    Text="" OnCommand="btnEditEnquiry_Grid_Command" CommandArgument="Edit" CommandName='<%# Eval("id")%>'>
                                                                </asp:LinkButton>
                                                                <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                    Text="" OnCommand="btnDeleteEnquiry_Grid_Command" CommandArgument="Delete" CommandName='<%# Eval("id")%>'
                                                                    CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this details?');">
                                                                </asp:LinkButton>
                                                            </td>

                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate></table></FooterTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </div>
                                        <!-- table End -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script src="assets/js/master_list_jquery.min.js"></script>
    <script src="assets/js/master_list_jquery-ui.min.js"></script>

    <%--<script src="assets/js/form-wizards.demo.min.js"></script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').dataTable();
        });
    </script>
    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#example').dataTable();
                    $('.select2').select2();
                }
            });
        };
    </script>
</asp:Content>

