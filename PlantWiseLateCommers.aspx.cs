﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;


public partial class PlantWiseLateCommers : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Date = "";
    string SSQL;
    //string mvarUserType = "IF User";
    string mvarUserType;
    DataTable mDataSet = new DataTable();
    DataTable dt = new DataTable();
    DataTable dt1 = new DataTable();
    DataTable dt2 = new DataTable();
    BALDataAccess objdata = new BALDataAccess();
    DataTable AutoDataTable = new DataTable();
    string iEmpDet = "";
    //char[] delimiters = new char[] { "-",">" };

    string[] delimiters = { "-", ">" };

    string mUnitName = "";
    string strPhotoPath = "";
    FileStream fs;
    BinaryReader br;
    DataRow drow;
    string Empcode;
    DataSet ds = new DataSet();
    string Division;
    string FInancialYear;
    string FromDate = "";


    MailMessage Mail = new MailMessage("SCOTO <nldvnaga.hr@gmail.com>", "suresh.s@scoto.in");
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Plant Wise Late Commers And Early Out Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("SalaryProcess"));
                //li.Attributes.Add("class", "droplink active open");
            }

            Date = Convert.ToDateTime(DateTime.Now.AddDays(-1)).ToString();
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            //FromDate = "2021/06/01";
            // Division = Request.QueryString["Division"].ToString();
            Division = "";
            FInancialYear = DateTime.Now.Year.ToString();
            FInancialYear = FInancialYear + "-01" + "-01";

            if (SessionUserType == "2")
            {
                GetData();
                //GetLateCommersTable("logtime_bar_plant", "Bar Plant");
                //GetLateCommersTable1("logtime_powder_plant", "Powder Plant");
                //GetLateCommersTable2("logtime_tsp_plant", "TSP Plant");

            }
            else
            {
                GetData();
                //GetLateCommersTable("logtime_bar_plant", "Bar Plant");
                //GetLateCommersTable1("logtime_powder_plant", "Powder Plant");
                //GetLateCommersTable2("logtime_tsp_plant", "TSP Plant");

            }
            ds.Tables.Add(AutoDataTable);
            // ds.Tables[0].DefaultView.RowFilter = "Plant='BAR'";
            //// ds.Tables[0].DefaultView.RowFilter = "Plant='POWDER'";
            // DataTable dt = (ds.Tables[0].DefaultView).ToTable();
           if (dt.Rows.Count > 0)
            {
                ReportDocument report = new ReportDocument();
                //report.Load(Server.MapPath("crystal/LateIn_EarlyOut_Report.rpt"));
                report.Load(Server.MapPath("crystal/LateIn_EarlyOut_Report1.rpt"));

                report.Database.Tables[0].SetDataSource(ds.Tables[0]);
                ////if (Division != "-Select-")
                ////{
                ////    report.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division:" + "'" + "'" + Division + "'";
                ////}
                ////else
                ////{
                ////    report.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division:" + "'";
                ////}
                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = report;
                //string Server_Path = Server.MapPath("~");
                //string AttachfileName_Miss = "";
                ////  string Invoice_No_Str = WitSalesOrdNo.Replace("/", "_");
                //AttachfileName_Miss = Server_Path + "/Daily_Report/Daily_LateIn_EarlyOut " + Convert.ToDateTime(Date).ToString("dd_MM_yyyy_HHmmss") + ".pdf";

                //if (File.Exists(AttachfileName_Miss))
                //{
                //    File.Delete(AttachfileName_Miss);
                //}

                //report.ExportToDisk(ExportFormatType.PortableDocFormat, AttachfileName_Miss);
                //report.Close();
                //report.Dispose();
            }
            else
            {
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Data Found..!');", true);
                Response.Write("<script>alert('No Records Found!!!')</Script>");

            }
        }
        //MailReport();
    }
    private static string UTF8Decryption(string MachineId)
    {
        string decryptpwd = string.Empty;
        UTF8Encoding encodepwd = new UTF8Encoding();
        Decoder Decode = encodepwd.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(MachineId);
        int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        decryptpwd = new String(decoded_char);
        return MachineId;
    }
    public void GetData()
    {
        bool Late_IN_Check = false;
        bool Early_OUT_Check = false;


        AutoDataTable.Columns.Add("Sno");
        AutoDataTable.Columns.Add("CompanyName");
        AutoDataTable.Columns.Add("LocationName");
        AutoDataTable.Columns.Add("TokenNo");
        AutoDataTable.Columns.Add("EmpName");
        AutoDataTable.Columns.Add("Plant");
        AutoDataTable.Columns.Add("InTime");
        AutoDataTable.Columns.Add("PlantInTime");
        AutoDataTable.Columns.Add("InTimeDiff");
        AutoDataTable.Columns.Add("PlantOutTime");
        AutoDataTable.Columns.Add("OutTime");
        AutoDataTable.Columns.Add("OutTimeDiff");
        AutoDataTable.Columns.Add("LateIn");
        AutoDataTable.Columns.Add("EarlyOut");
        AutoDataTable.Columns.Add("Company");
        AutoDataTable.Columns.Add("Address");
        AutoDataTable.Columns.Add("FromDate");



        //SSQL = "SELECT * FROM logtime_in WHERE compCode='" + SessionCcode.ToString() + "' and loccode='" + SessionLcode.ToString() + "'";

        //mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
        SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);


        //SSQL = "";
        //SSQL = "Select EM.ExistingCode as TokenNo, EM.FirstName as EmpName,EM.MachineID_Encrypt,LD.TimeIN,LD.TimeOUT,EM.Wages from logtime_Days LD inner join Employee_mst EM on EM.MachineID=LD.MachineID";
        //SSQL = SSQL + " where DATE_FORMAT(LD.Attn_Date,'%Y-%m-%d')=DATE_FORMAT('" + Convert.ToDateTime(FromDate).ToString("yyyy-MM-dd") + "','%Y-%m-%d') and LD.Present!='0.0'";
        SSQL = "";
        SSQL = "Select EM.ExistingCode as TokenNo, EM.FirstName as EmpName,EM.MachineID_Encrypt,LD.TimeIN,LD.TimeOUT,EM.Wages from logtime_Days LD inner join Employee_mst EM on EM.MachineID=LD.MachineID";
        SSQL = SSQL + " where DATE_FORMAT(LD.Attn_Date,'%Y-%m-%d')=DATE_FORMAT('" + Convert.ToDateTime(FromDate).ToString("yyyy-MM-dd") + "','%Y-%m-%d') and LD.Present!='0.0'";
        DataTable dt_LogDays = new DataTable();
        dt_LogDays = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt_LogDays.Rows.Count > 0)
        {

            for (int intRow = 0; intRow < dt_LogDays.Rows.Count; intRow++)
            {

                if (dt_LogDays.Rows[intRow]["TokenNo"].ToString() == "230")
                {
                    string Stop = "";
                }

                Late_IN_Check = false;
                Early_OUT_Check = false;

                string Employee_Shift_Name_DB = "";
                bool Shift_Check_blb_Check1 = false;

                SSQL = "";
                SSQL = "Select * from Shift_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                if (((dt_LogDays.Rows[intRow]["Wages"].ToString().ToUpper() == ("STAFF")) && (dt_LogDays.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL"))) || (dt_LogDays.Rows[intRow]["Wages"].ToString().ToUpper() == ("SECURITY").ToUpper()) || (dt_LogDays.Rows[intRow]["Wages"].ToString().ToUpper() == ("FITTER & ELECTRICIANS").ToUpper()) || (dt_LogDays.Rows[intRow]["Wages"].ToString().ToUpper() == ("DRIVERS").ToUpper()))
                {
                    SSQL = SSQL + " And ShiftDesc = 'GENERAL'";
                }
                else
                {
                    SSQL = SSQL + " And ShiftDesc <> 'GENERAL'";
                }
                SSQL = SSQL + " Order by ShiftDesc Asc";
                DataTable Shift_Ds = new DataTable();
                Shift_Ds = objdata.RptEmployeeMultipleDetails(SSQL);
                if (Shift_Ds.Rows.Count != 0)
                {
                    SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'"; //And ShiftDesc like '%SHIFT%'";
                    if (((dt_LogDays.Rows[intRow]["Wages"].ToString().ToUpper() == ("STAFF").ToUpper()) && (dt_LogDays.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL"))) || (dt_LogDays.Rows[intRow]["Wages"].ToString().ToUpper() == ("SECURITY").ToUpper()) || (dt_LogDays.Rows[intRow]["Wages"].ToString().ToUpper() == ("FITTER & ELECTRICIANS").ToUpper()) || (dt_LogDays.Rows[intRow]["Wages"].ToString().ToUpper() == ("DRIVERS").ToUpper()))
                    {
                        SSQL = SSQL + " And ShiftDesc = 'GENERAL'";
                    }
                    else
                    {
                        SSQL = SSQL + " And ShiftDesc like '%SHIFT%'";
                    }
                    Shift_Ds = objdata.RptEmployeeMultipleDetails(SSQL);
                    //Shift_Check_blb = false;
                    for (int K = 0; K < Shift_Ds.Rows.Count; K++)
                    {
                        string Start_IN = Convert.ToDateTime(DateTime.Now).AddDays(Convert.ToDouble(Shift_Ds.Rows[K]["StartIN_Days"])).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[K]["StartIN"].ToString();
                        string End_In = Convert.ToDateTime(DateTime.Now).AddDays(Convert.ToDouble(Shift_Ds.Rows[K]["EndIN_Days"])).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[K]["EndIN"].ToString();
                        string Start_OUT = Convert.ToDateTime(DateTime.Now).AddDays(Convert.ToDouble(Shift_Ds.Rows[K]["StartOUT_Days"])).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[K]["StartOUT"].ToString();
                        string End_OUT = Convert.ToDateTime(DateTime.Now).AddDays(Convert.ToDouble(Shift_Ds.Rows[K]["EndOUT_Days"])).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[K]["EndOUT"].ToString();

                        DateTime ShiftdateStartIN_Check = Convert.ToDateTime(Start_IN);
                        DateTime ShiftdateEndIN_Check = Convert.ToDateTime(End_In);

                        DateTime ShiftdateStartOUT_Check = Convert.ToDateTime(Start_OUT);
                        DateTime ShiftdateEndOUT_Check = Convert.ToDateTime(End_OUT);

                        DateTime EmpdateIN_Check = Convert.ToDateTime(dt_LogDays.Rows[intRow]["TimeIN"]);
                        DateTime EmpdateOUT_Check = Convert.ToDateTime(dt_LogDays.Rows[intRow]["TimeOUT"]);

                        if (EmpdateIN_Check >= ShiftdateStartIN_Check && EmpdateIN_Check <= ShiftdateEndIN_Check)
                        {
                            Employee_Shift_Name_DB = Shift_Ds.Rows[K]["ShiftDesc"].ToString();
                            Shift_Check_blb_Check1 = true;
                           
                        }

                        //Check the Shift with In Time Grace Time 15 minutes

                        if (!Late_IN_Check && !Early_OUT_Check)
                        {
                            if (EmpdateIN_Check >= ShiftdateEndIN_Check && EmpdateIN_Check <= ShiftdateStartOUT_Check)
                            {
                                Late_IN_Check = true;
                            }
                            if (EmpdateOUT_Check >= ShiftdateEndIN_Check && EmpdateOUT_Check <= ShiftdateStartOUT_Check)
                            {
                                Early_OUT_Check = true;
                            }

                            if(Late_IN_Check || Early_OUT_Check)
                            {
                                break;
                            }
                        }

                    }
                    if (Early_OUT_Check || Late_IN_Check)
                    {
                        string Plantname = "";
                        string PlantINTime = "";
                        string PlantOUTTime = "";
                        string InTimeDiff = "";
                        string OUTTimeDiff = "";
                        bool plant_Check = false;
                        string[] tblname = { "logtime_bar_plant", "logtime_powder_plant", "logtime_tsp_plant" };
                        string[] tblplant = { "BAR", "POWDER", "TSP" };
                        int inc = 0;

                      

                    CHeCK:
                        if (Employee_Shift_Name_DB == "SHIFT3")
                        {
                            SSQL = "";
                            SSQL = "Select * from " + tblname[inc] + " where compCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " and MachineID='" + dt_LogDays.Rows[intRow]["MachineID_Encrypt"].ToString() + "'";
                            SSQL = SSQL + " and DATE_FORMAT(Time,'%Y-%m-%d %H:%i') BETWEEN DATE_FORMAT('" + Convert.ToDateTime(FromDate).ToString("yyyy-MM-dd") + " 21:00','%Y-%m-%d %H:%i') AND  DATE_FORMAT('" + Convert.ToDateTime(FromDate).AddDays(0).ToString("yyyy-MM-dd") + " 23:00','%Y-%m-%d %H:%i')  order by Time Asc LIMIT 1";
                            DataTable dt_INplant = new DataTable();
                            dt_INplant = objdata.RptEmployeeMultipleDetails(SSQL);

                            if (dt_INplant.Rows.Count != 0)
                            {
                                Plantname = tblplant[inc];
                                PlantINTime = Convert.ToDateTime(dt_INplant.Rows[0]["Time"]).ToString("hh:mm tt");

                                SSQL = "";
                                SSQL = "Select * from " + tblname[inc] + " where compCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " and MachineID='" + dt_LogDays.Rows[intRow]["MachineID_Encrypt"].ToString() + "'";
                                SSQL = SSQL + " and DATE_FORMAT(Time,'%Y-%m-%d %H:%i') BETWEEN DATE_FORMAT('" + Convert.ToDateTime(FromDate).ToString("yyyy-MM-dd") + " 21:00','%Y-%m-%d %H:%i') AND  DATE_FORMAT('" + Convert.ToDateTime(FromDate).AddDays(1).ToString("yyyy-MM-dd") + " 14:00','%Y-%m-%d %H:%i') order by Time DESC LIMIT 1";
                                DataTable dt_OUTplant = new DataTable();
                                dt_OUTplant = objdata.RptEmployeeMultipleDetails(SSQL);
                                if (dt_OUTplant.Rows.Count > 0)
                                {
                                    PlantOUTTime = Convert.ToDateTime(dt_OUTplant.Rows[0]["Time"]).ToString("hh:mm tt");
                                }
                                plant_Check = true;
                            }
                        }
                        else if(Employee_Shift_Name_DB == "SHIFT2")
                        {
                            SSQL = "";
                            SSQL = "Select * from " + tblname[inc] + " where compCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " and MachineID='" + dt_LogDays.Rows[intRow]["MachineID_Encrypt"].ToString() + "'";
                            SSQL = SSQL + " and DATE_FORMAT(Time,'%Y-%m-%d %H:%i') BETWEEN DATE_FORMAT('" + Convert.ToDateTime(FromDate).ToString("yyyy-MM-dd") + " 13:00','%Y-%m-%d %H:%i') AND  DATE_FORMAT('" + Convert.ToDateTime(FromDate).AddDays(1).ToString("yyyy-MM-dd") + " 15:00','%Y-%m-%d %H:%i')  order by Time Asc LIMIT 1";
                            DataTable dt_INplant = new DataTable();
                            dt_INplant = objdata.RptEmployeeMultipleDetails(SSQL);

                            if (dt_INplant.Rows.Count != 0)
                            {
                                Plantname = tblplant[inc];
                                PlantINTime = Convert.ToDateTime(dt_INplant.Rows[0]["Time"]).ToString("hh:mm tt");

                                SSQL = "";
                                SSQL = "Select * from " + tblname[inc] + " where compCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " and MachineID='" + dt_LogDays.Rows[intRow]["MachineID_Encrypt"].ToString() + "'";
                                SSQL = SSQL + " and DATE_FORMAT(Time,'%Y-%m-%d %H:%i') BETWEEN DATE_FORMAT('" + Convert.ToDateTime(FromDate).ToString("yyyy-MM-dd") + " 13:00','%Y-%m-%d %H:%i') AND  DATE_FORMAT('" + Convert.ToDateTime(FromDate).AddDays(1).ToString("yyyy-MM-dd") + " 07:00','%Y-%m-%d %H:%i') order by Time DESC LIMIT 1";
                                DataTable dt_OUTplant = new DataTable();
                                dt_OUTplant = objdata.RptEmployeeMultipleDetails(SSQL);
                                if (dt_OUTplant.Rows.Count > 0)
                                {
                                    PlantOUTTime = Convert.ToDateTime(dt_OUTplant.Rows[0]["Time"]).ToString("hh:mm tt");
                                   
                                }
                                plant_Check = true;
                            }
                        }
                        else if (Employee_Shift_Name_DB == "SHIFT1")
                        {
                            SSQL = "";
                            SSQL = "Select * from " + tblname[inc] + " where compCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " and MachineID='" + dt_LogDays.Rows[intRow]["MachineID_Encrypt"].ToString() + "'";
                            SSQL = SSQL + " and DATE_FORMAT(Time,'%Y-%m-%d %H:%i') BETWEEN DATE_FORMAT('" + Convert.ToDateTime(FromDate).ToString("yyyy-MM-dd") + " 05:00','%Y-%m-%d %H:%i') AND  DATE_FORMAT('" + Convert.ToDateTime(FromDate).AddDays(0).ToString("yyyy-MM-dd") + " 10:00','%Y-%m-%d %H:%i')  order by Time Asc LIMIT 1";
                            DataTable dt_INplant = new DataTable();
                            dt_INplant = objdata.RptEmployeeMultipleDetails(SSQL);

                            if (dt_INplant.Rows.Count != 0)
                            {
                                Plantname = tblplant[inc];
                                PlantINTime = Convert.ToDateTime(dt_INplant.Rows[0]["Time"]).ToString("hh:mm tt");

                                SSQL = "";
                                SSQL = "Select * from " + tblname[inc] + " where compCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " and MachineID='" + dt_LogDays.Rows[intRow]["MachineID_Encrypt"].ToString() + "'";
                                SSQL = SSQL + " and DATE_FORMAT(Time,'%Y-%m-%d %H:%i') BETWEEN DATE_FORMAT('" + Convert.ToDateTime(FromDate).ToString("yyyy-MM-dd") + " 05:00','%Y-%m-%d %H:%i') AND  DATE_FORMAT('" + Convert.ToDateTime(FromDate).AddDays(0).ToString("yyyy-MM-dd") + " 23:00','%Y-%m-%d %H:%i') order by Time DESC LIMIT 1";
                                DataTable dt_OUTplant = new DataTable();
                                dt_OUTplant = objdata.RptEmployeeMultipleDetails(SSQL);
                                if (dt_OUTplant.Rows.Count > 0)
                                {
                                    PlantOUTTime = Convert.ToDateTime(dt_OUTplant.Rows[0]["Time"]).ToString("hh:mm tt");
                                }
                                plant_Check = true;
                            }
                        }
                        else if (Employee_Shift_Name_DB == "SHIFT4")
                        {
                            SSQL = "";
                            SSQL = "Select * from " + tblname[inc] + " where compCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " and MachineID='" + dt_LogDays.Rows[intRow]["MachineID_Encrypt"].ToString() + "'";
                            SSQL = SSQL + " and DATE_FORMAT(Time,'%Y-%m-%d %H:%i') BETWEEN DATE_FORMAT('" + Convert.ToDateTime(FromDate).ToString("yyyy-MM-dd") + " 08:00','%Y-%m-%d %H:%i') AND  DATE_FORMAT('" + Convert.ToDateTime(FromDate).AddDays(0).ToString("yyyy-MM-dd") + " 10:00','%Y-%m-%d %H:%i')  order by Time Asc LIMIT 1";
                            DataTable dt_INplant = new DataTable();
                            dt_INplant = objdata.RptEmployeeMultipleDetails(SSQL);

                            if (dt_INplant.Rows.Count != 0)
                            {
                                Plantname = tblplant[inc];
                                PlantINTime = Convert.ToDateTime(dt_INplant.Rows[0]["Time"]).ToString("hh:mm tt");

                                SSQL = "";
                                SSQL = "Select * from " + tblname[inc] + " where compCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " and MachineID='" + dt_LogDays.Rows[intRow]["MachineID_Encrypt"].ToString() + "'";
                                SSQL = SSQL + " and DATE_FORMAT(Time,'%Y-%m-%d %H:%i') BETWEEN DATE_FORMAT('" + Convert.ToDateTime(FromDate).ToString("yyyy-MM-dd") + " 08:00','%Y-%m-%d %H:%i') AND  DATE_FORMAT('" + Convert.ToDateTime(FromDate).AddDays(0).ToString("yyyy-MM-dd") + " 23:00','%Y-%m-%d %H:%i') order by Time DESC LIMIT 1";
                                DataTable dt_OUTplant = new DataTable();
                                dt_OUTplant = objdata.RptEmployeeMultipleDetails(SSQL);
                                if (dt_OUTplant.Rows.Count > 0)
                                {
                                    PlantOUTTime = Convert.ToDateTime(dt_OUTplant.Rows[0]["Time"]).ToString("hh:mm tt");
                                }
                                plant_Check = true;
                            }
                        }
                        else if (Employee_Shift_Name_DB == "SHIFT5")
                        {
                            SSQL = "";
                            SSQL = "Select * from " + tblname[inc] + " where compCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " and MachineID='" + dt_LogDays.Rows[intRow]["MachineID_Encrypt"].ToString() + "'";
                            SSQL = SSQL + " and DATE_FORMAT(Time,'%Y-%m-%d %H:%i') BETWEEN DATE_FORMAT('" + Convert.ToDateTime(FromDate).ToString("yyyy-MM-dd") + " 17:00','%Y-%m-%d %H:%i') AND  DATE_FORMAT('" + Convert.ToDateTime(FromDate).AddDays(0).ToString("yyyy-MM-dd") + " 19:00','%Y-%m-%d %H:%i')  order by Time Asc LIMIT 1";
                            DataTable dt_INplant = new DataTable();
                            dt_INplant = objdata.RptEmployeeMultipleDetails(SSQL);

                            if (dt_INplant.Rows.Count != 0)
                            {
                                Plantname = tblplant[inc];
                                PlantINTime = Convert.ToDateTime(dt_INplant.Rows[0]["Time"]).ToString("hh:mm tt");

                                SSQL = "";
                                SSQL = "Select * from " + tblname[inc] + " where compCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " and MachineID='" + dt_LogDays.Rows[intRow]["MachineID_Encrypt"].ToString() + "'";
                                SSQL = SSQL + " and DATE_FORMAT(Time,'%Y-%m-%d %H:%i') BETWEEN DATE_FORMAT('" + Convert.ToDateTime(FromDate).ToString("yyyy-MM-dd") + " 17:00','%Y-%m-%d %H:%i') AND  DATE_FORMAT('" + Convert.ToDateTime(FromDate).AddDays(1).ToString("yyyy-MM-dd") + " 07:00','%Y-%m-%d %H:%i') order by Time DESC LIMIT 1";
                                DataTable dt_OUTplant = new DataTable();
                                dt_OUTplant = objdata.RptEmployeeMultipleDetails(SSQL);
                                if (dt_OUTplant.Rows.Count > 0)
                                {
                                    PlantOUTTime = Convert.ToDateTime(dt_OUTplant.Rows[0]["Time"]).ToString("hh:mm tt");
                                    
                                   
                                }
                                plant_Check = true;
                            }
                        }


                        if (!plant_Check && inc != 2)
                        {
                            inc  = inc + 1;
                            goto CHeCK;
                        }

                        //if (!plant_Check)
                        //{
                        //    inc = +1;
                        //    goto CHeCK;
                        //}

                        if (PlantINTime != "")
                        {
                            TimeSpan ts = new TimeSpan();
                            ts = Convert.ToDateTime(PlantINTime).Subtract(Convert.ToDateTime(dt_LogDays.Rows[intRow]["TimeIN"].ToString()));
                            InTimeDiff = ts.Hours.ToString("00") + ":" + ts.Minutes.ToString("00");

                        }
                        else
                        {
                            InTimeDiff = "";
                        }
                        if (PlantOUTTime != "")
                        {
                            TimeSpan ts = new TimeSpan();
                            ts = Convert.ToDateTime(dt_LogDays.Rows[intRow]["TimeOUT"].ToString()).Subtract(Convert.ToDateTime(PlantOUTTime));
                            OUTTimeDiff = ts.Hours.ToString("00") + ":" + ts.Minutes.ToString("00");
                        }
                        else
                        {
                            OUTTimeDiff = "";
                        }

                        AutoDataTable.NewRow();
                        AutoDataTable.Rows.Add();

                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["Sno"] = AutoDataTable.Rows.Count ;
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["TokenNo"] = dt_LogDays.Rows[intRow]["TokenNo"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["EmpName"] = dt_LogDays.Rows[intRow]["EmpName"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["Plant"] = Plantname; /* dt_LogDays.Rows[intRow]["Plant"].ToString();*/
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["InTime"] = dt_LogDays.Rows[intRow]["TimeIN"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["PlantInTime"] = PlantINTime; /*dt_LogDays.Rows[intRow]["PlantInTime"].ToString();*/
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["InTimeDiff"] = InTimeDiff;
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["LateIn"] = Late_IN_Check ? "1" : "0";
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["PlantOutTime"] = PlantOUTTime;
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["OutTime"] = dt_LogDays.Rows[intRow]["TimeOUT"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["OutTimeDiff"] = OUTTimeDiff;
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["EarlyOut"] = Early_OUT_Check ? "1" : "0";
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["Company"] = dt.Rows[0]["CompName"].ToString().ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["Address"] = dt.Rows[0]["Add1"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["FromDate"] = DateTime.Parse(FromDate).ToString("dd/MM/yyyy");
                    }




                    //if (EmpdateIN_Check >= ShiftdateStartIN_Check && EmpdateIN_Check <= ShiftdateEndIN_Check)
                    //{
                    //    //Employee_Shift_Name_DB = Shift_Ds.Rows[K]["ShiftDesc"].ToString();
                    //    //// Shift_Check_blb_Check = true;
                    //    //break;



                    //}





                    //if (Shift_Check_blb_Check == false)
                    //{
                    //    Employee_Shift_Name_DB = "No Shift";
                    //}
                }
            }
        }
        ////Plant InTime
        //SSQL = "SELECT existingCode AS TokenNo,FIRSTname AS EmpName,'"+PlantName+"' as Plant,TIME(lt.TimeIn) AS InTime,TIME(lp.time) AS PlantInTime,TIMEDIFF(TIME(lp.time),TIME(lt.TimeIn)) AS InTimeDiff,";
        //SSQL = SSQL + " CASE WHEN TIMEDIFF(TIME(lp.time),TIME(lt.TimeIn))>'00:10:00' THEN '1' ELSE '0' END AS LateIn FROM logtime_in lt ";
        //SSQL = SSQL + " INNER JOIN(SELECT * FROM "+Plant+" ORDER BY TIME ASC) lp ON lt.Machineid=lp.machineid INNER JOIN";
        //SSQL = SSQL + " employee_mst em ON em.MachineID_Encrypt=lt.Machineid WHERE lt.TimeIn BETWEEN '" + FromDate+"' AND DATE_SUB(NOW(),INTERVAL 1 DAY)";
        //dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

        ////Plant OutTime

        //SSQL = "SELECT TIME(lt.TimeOut) AS OutTime,TIME(lp.time) AS PlantOutTime,TIMEDIFF(TIME(lt.TimeOut),TIME(lp.time)) AS OutTimeDiff,";
        //SSQL = SSQL + " CASE WHEN TIMEDIFF(TIME(lt.TimeOut),TIME(lp.time))>'00:10:00' THEN '1' ELSE '0' END AS EarlyOut FROM logtime_out lt";
        //SSQL = SSQL + " INNER JOIN(SELECT * FROM " + Plant + " ORDER BY TIME DESC) lp ON lt.Machineid=lp.machineid INNER JOIN";
        //SSQL = SSQL + " employee_mst em ON em.MachineID_Encrypt=lt.Machineid  WHERE lt.TimeOut BETWEEN '" + FromDate + "' AND DATE_SUB(NOW(),INTERVAL 1 DAY)";
        //dt2 = objdata.RptEmployeeMultipleDetails(SSQL);


        //string name = dt.Rows[0]["CompName"].ToString();
        //string Addres = dt.Rows[0]["Add1"].ToString();
        //int sno = 1;
        //if (dt1.Rows.Count > 0)
        //{

        //    for (int iRow = 0; iRow < dt1.Rows.Count; iRow++)
        //    {

        //        AutoDataTable.NewRow();
        //        AutoDataTable.Rows.Add();


        //        AutoDataTable.Rows[iRow]["Sno"] = sno;
        //        AutoDataTable.Rows[iRow]["CompanyName"] = SessionCcode;
        //        AutoDataTable.Rows[iRow]["LocationName"] = SessionLcode;
        //        AutoDataTable.Rows[iRow]["TokenNo"] = dt1.Rows[iRow]["TokenNo"].ToString();
        //        AutoDataTable.Rows[iRow]["EmpName"] = dt1.Rows[iRow]["EmpName"].ToString();
        //        AutoDataTable.Rows[iRow]["Plant"] = dt1.Rows[iRow]["Plant"].ToString();
        //        AutoDataTable.Rows[iRow]["InTime"] = dt1.Rows[iRow]["InTime"].ToString();
        //        AutoDataTable.Rows[iRow]["PlantInTime"] = dt1.Rows[iRow]["PlantInTime"].ToString();
        //        AutoDataTable.Rows[iRow]["InTimeDiff"] = dt1.Rows[iRow]["InTimeDiff"].ToString();
        //        AutoDataTable.Rows[iRow]["LateIn"] = dt1.Rows[iRow]["LateIn"].ToString();
        //        AutoDataTable.Rows[iRow]["Company"] = name.ToString();
        //        AutoDataTable.Rows[iRow]["Address"] = Addres.ToString();
        //        AutoDataTable.Rows[iRow]["FromDate"] = DateTime.Parse(FromDate).ToString("dd/MM/yyyy");



        //        sno += 1;

        //    }
        //    for (int i = 0; i < dt2.Rows.Count; i++)
        //    {
        //        AutoDataTable.Rows[i]["PlantOutTime"] = dt2.Rows[i]["PlantOutTime"].ToString();
        //        AutoDataTable.Rows[i]["OutTime"] = dt2.Rows[i]["OutTime"].ToString();
        //        AutoDataTable.Rows[i]["OutTimeDiff"] = dt2.Rows[i]["OutTimeDiff"].ToString();
        //        AutoDataTable.Rows[i]["EarlyOut"] = dt2.Rows[i]["EarlyOut"].ToString();
        //    }
        //}
    }
    public void GetLateCommersTable1(String Plant, String PlantName)
    {
        //SSQL = "SELECT * FROM logtime_in WHERE compCode='" + SessionCcode.ToString() + "' and loccode='" + SessionLcode.ToString() + "'";

        //mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
        SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        //Plant InTime
        SSQL = "SELECT existingCode AS TokenNo,FIRSTname AS EmpName,'" + PlantName + "' as Plant,TIME(lt.TimeIn) AS InTime,TIME(lp.time) AS PlantInTime,TIMEDIFF(TIME(lp.time),TIME(lt.TimeIn)) AS InTimeDiff,";
        SSQL = SSQL + " CASE WHEN TIMEDIFF(TIME(lp.time),TIME(lt.TimeIn))>'00:10:00' THEN '1' ELSE '0' END AS LateIn FROM logtime_in lt ";
        SSQL = SSQL + " INNER JOIN(SELECT * FROM " + Plant + " ORDER BY TIME ASC) lp ON lt.Machineid=lp.machineid INNER JOIN";
        SSQL = SSQL + " employee_mst em ON em.MachineID_Encrypt=lt.machineid WHERE lt.TimeIn BETWEEN '" + FromDate + "' AND DATE_SUB(NOW(),INTERVAL 1 DAY)";
        dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

        //Plant OutTime

        SSQL = "SELECT TIME(lt.TimeOut) AS OutTime,TIME(lp.time) AS PlantOutTime,TIMEDIFF(TIME(lt.TimeOut),TIME(lp.time)) AS OutTimeDiff,";
        SSQL = SSQL + " CASE WHEN TIMEDIFF(TIME(lt.TimeOut),TIME(lp.time))>'00:10:00' THEN '1' ELSE '0' END AS EarlyOut FROM logtime_out lt";
        SSQL = SSQL + " INNER JOIN(SELECT * FROM " + Plant + " ORDER BY TIME DESC) lp ON lt.Machineid=lp.machineid INNER JOIN";
        SSQL = SSQL + " employee_mst em ON em.MachineID_Encrypt=lt.Machineid WHERE lt.TimeOut BETWEEN '" + FromDate + "' AND DATE_SUB(NOW(),INTERVAL 1 DAY)";
        dt2 = objdata.RptEmployeeMultipleDetails(SSQL);


        string name = dt.Rows[0]["CompName"].ToString();
        string Addres = dt.Rows[0]["Add1"].ToString();
        int sno = 1;
        if (dt1.Rows.Count > 0)
        {
            int Count = AutoDataTable.Rows.Count;
            int Tot = dt1.Rows.Count + Count;
            int Tot1 = dt2.Rows.Count + Count;
            int k = 0;
            for (int iRow = Count; iRow < Tot; iRow++)
            {

                AutoDataTable.NewRow();
                AutoDataTable.Rows.Add();


                AutoDataTable.Rows[iRow]["Sno"] = iRow + 1;
                AutoDataTable.Rows[iRow]["CompanyName"] = SessionCcode;
                AutoDataTable.Rows[iRow]["LocationName"] = SessionLcode;
                AutoDataTable.Rows[iRow]["TokenNo"] = dt1.Rows[k]["TokenNo"].ToString();
                AutoDataTable.Rows[iRow]["EmpName"] = dt1.Rows[k]["EmpName"].ToString();
                AutoDataTable.Rows[iRow]["Plant"] = dt1.Rows[k]["Plant"].ToString();
                AutoDataTable.Rows[iRow]["InTime"] = dt1.Rows[k]["InTime"].ToString();
                AutoDataTable.Rows[iRow]["PlantInTime"] = dt1.Rows[k]["PlantInTime"].ToString();
                AutoDataTable.Rows[iRow]["InTimeDiff"] = dt1.Rows[k]["InTimeDiff"].ToString();
                AutoDataTable.Rows[iRow]["LateIn"] = dt1.Rows[k]["LateIn"].ToString();
                AutoDataTable.Rows[iRow]["Company"] = name.ToString();
                AutoDataTable.Rows[iRow]["Address"] = Addres.ToString();
                k += 1;

            }
            k = 0;
            for (int i = Count; i < Tot1; i++)
            {
                AutoDataTable.Rows[i]["PlantOutTime"] = dt2.Rows[k]["PlantOutTime"].ToString();
                AutoDataTable.Rows[i]["OutTime"] = dt2.Rows[k]["OutTime"].ToString();
                AutoDataTable.Rows[i]["OutTimeDiff"] = dt2.Rows[k]["OutTimeDiff"].ToString();
                AutoDataTable.Rows[i]["EarlyOut"] = dt2.Rows[k]["EarlyOut"].ToString();
                k += 1;
            }
        }
    }

    public void GetLateCommersTable2(String Plant, String PlantName)
    {
        //SSQL = "SELECT * FROM logtime_in WHERE compCode='" + SessionCcode.ToString() + "' and loccode='" + SessionLcode.ToString() + "'";

        //mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
        SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        //Plant InTime
        SSQL = "SELECT existingCode AS TokenNo,FIRSTname AS EmpName,'" + PlantName + "' as Plant,TIME(lt.TimeIn) AS InTime,TIME(lp.time) AS PlantInTime,TIMEDIFF(TIME(lp.time),TIME(lt.TimeIn)) AS InTimeDiff,";
        SSQL = SSQL + " CASE WHEN TIMEDIFF(TIME(lp.time),TIME(lt.TimeIn))>'00:10:00' THEN '1' ELSE '0' END AS LateIn FROM logtime_in lt ";
        SSQL = SSQL + " INNER JOIN(SELECT * FROM " + Plant + " ORDER BY TIME ASC) lp ON lt.Machineid=lp.machineid INNER JOIN";
        SSQL = SSQL + " employee_mst em ON em.MachineID_Encrypt=lt.Machineid WHERE lt.TimeIn BETWEEN '" + FromDate + "' AND DATE_SUB(NOW(),INTERVAL 1 DAY)";
        dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

        //Plant OutTime

        SSQL = "SELECT TIME(lt.TimeOut) AS OutTime,TIME(lp.time) AS PlantOutTime,TIMEDIFF(TIME(lt.TimeOut),TIME(lp.time)) AS OutTimeDiff,";
        SSQL = SSQL + " CASE WHEN TIMEDIFF(TIME(lt.TimeOut),TIME(lp.time))>'00:10:00' THEN '1' ELSE '0' END AS EarlyOut FROM logtime_out lt";
        SSQL = SSQL + " INNER JOIN(SELECT * FROM " + Plant + " ORDER BY TIME DESC) lp ON lt.Machineid=lp.machineid INNER JOIN";
        SSQL = SSQL + " employee_mst em ON em.MachineID_Encrypt=lt.Machineid WHERE lt.TimeOut BETWEEN '" + FromDate + "' AND DATE_SUB(NOW(),INTERVAL 1 DAY)";
        dt2 = objdata.RptEmployeeMultipleDetails(SSQL);


        string name = dt.Rows[0]["CompName"].ToString();
        string Addres = dt.Rows[0]["Add1"].ToString();
        int sno = 1;
        if (dt1.Rows.Count > 0)
        {
            int Count = AutoDataTable.Rows.Count;
            int Tot = dt1.Rows.Count + Count;
            int Tot1 = dt2.Rows.Count + Count;
            int k = 0;
            for (int iRow = Count; iRow < Tot; iRow++)
            {

                AutoDataTable.NewRow();
                AutoDataTable.Rows.Add();


                AutoDataTable.Rows[iRow]["Sno"] = iRow + 1;
                AutoDataTable.Rows[iRow]["CompanyName"] = SessionCcode;
                AutoDataTable.Rows[iRow]["LocationName"] = SessionLcode;
                AutoDataTable.Rows[iRow]["TokenNo"] = dt1.Rows[k]["TokenNo"].ToString();
                AutoDataTable.Rows[iRow]["EmpName"] = dt1.Rows[k]["EmpName"].ToString();
                AutoDataTable.Rows[iRow]["Plant"] = dt1.Rows[k]["Plant"].ToString();
                AutoDataTable.Rows[iRow]["InTime"] = dt1.Rows[k]["InTime"].ToString();
                AutoDataTable.Rows[iRow]["PlantInTime"] = dt1.Rows[k]["PlantInTime"].ToString();
                AutoDataTable.Rows[iRow]["InTimeDiff"] = dt1.Rows[k]["InTimeDiff"].ToString();
                AutoDataTable.Rows[iRow]["LateIn"] = dt1.Rows[k]["LateIn"].ToString();
                AutoDataTable.Rows[iRow]["Company"] = name.ToString();
                AutoDataTable.Rows[iRow]["Address"] = Addres.ToString();



                k += 1;

            }
            k = 0;
            for (int i = Count; i < Tot1; i++)
            {
                AutoDataTable.Rows[i]["PlantOutTime"] = dt2.Rows[k]["PlantOutTime"].ToString();
                AutoDataTable.Rows[i]["OutTime"] = dt2.Rows[k]["OutTime"].ToString();
                AutoDataTable.Rows[i]["OutTimeDiff"] = dt2.Rows[k]["OutTimeDiff"].ToString();
                AutoDataTable.Rows[i]["EarlyOut"] = dt2.Rows[k]["EarlyOut"].ToString();
            }
        }
    }
    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }
    private void MailReport()
    {
        //Mail.CC = "padmanaban";
        Mail.Subject = "Daily Employee Plant Wise Late Commers And Early Out Report";
        Mail.Body = "Dear Sir, Please find the Attachement for (" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + ") Employee Plant Wise Late Commers And Early Out";
        // Mail.CC=new 
        // string FileName = Path.GetFileName(fuAttachment.PostedFile.FileName);
        string Server_Path = Server.MapPath("~");
        string AttachfileName_Miss = "";
        //  string Invoice_No_Str = WitSalesOrdNo.Replace("/", "_");
        AttachfileName_Miss = Server_Path + "/Daily_Report/Daily_LateIn_EarlyOut " + Convert.ToDateTime(Date).ToString("dd_MM_yyyy_HHmmss") + ".pdf";
        if (File.Exists(AttachfileName_Miss))
        {
            Mail.Attachments.Add(new Attachment(AttachfileName_Miss));
        }

        Mail.IsBodyHtml = false;
        SmtpClient smtp = new SmtpClient();
        smtp.Host = "smtp.gmail.com";
        smtp.EnableSsl = true;
        NetworkCredential NetworkCred = new NetworkCredential("nldvnaga.hr@gmail.com", "Altius2005");
        smtp.UseDefaultCredentials = false;
        smtp.Credentials = NetworkCred;
        smtp.Port = 587;
        smtp.Send(Mail);
        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('Email sent.');", true);
        //File.Delete(Server_Path + "/Daily_Report/Daily_Attendance_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy") + ".pdf");
        //File.Delete(Server_Path + "/Daily_Report/Daily_Attendance_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy") + ".pdf");
        //File.Delete(Server_Path + "/Daily_Report/SalaryConsolidate_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy") + ".pdf");
        //File.Delete(Server_Path + "/Daily_Report/Daily_Improper_Punch_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy") + ".pdf");
    }
}