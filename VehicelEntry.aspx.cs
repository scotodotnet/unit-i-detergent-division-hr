﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class VehicelEntry : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Vehicle Master";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            Load_Shift();
            Load_Contractor();
            //Initial_Data_Referesh();
            //Load_Canteen();

        }

        //Load_Data();
        // txtDate.Text = DateTime.Today.ToShortDateString();
       // Load_Data();
        // txttime.Text = DateTime.Now.ToString("hh:mm tt");
    }
    //private void Load_Data()
    //{
    //    SSQL = "Select CanteenName,Shift,Convert(varchar,Dte,103) as Dte,CONVERT(varchar(15),time,22) as time,ItemName,Amount,Auto_ID from MstTeaSnacks where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
    //    DataTable dt = new DataTable();
    //    dt = objdata.RptEmployeeMultipleDetails(SSQL);
    //    Repeater1.DataSource = dt;
    //    Repeater1.DataBind();
    //}
    //private void Load_Canteen()
    //{
    //    SSQL = "";
    //    SSQL = "Select * from MstCanteen where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
    //    ddlVehicleType.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
    //    ddlCanteenName.DataTextField = "CanteenName";
    //    ddlCanteenName.DataValueField = "CanID";
    //    ddlCanteenName.DataBind();
    //    ddlCanteenName.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    //}
    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("ExistingCode", typeof(string)));
        dt.Columns.Add(new DataColumn("EmpName", typeof(string)));
        dt.Columns.Add(new DataColumn("VehicleType", typeof(string)));
        dt.Columns.Add(new DataColumn("Route", typeof(string)));
        dt.Columns.Add(new DataColumn("BusStop", typeof(string)));
        dt.Columns.Add(new DataColumn("BusFare", typeof(string)));
        dt.Columns.Add(new DataColumn("Date_Str", typeof(string)));

        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSource;
    }
    private void Load_Shift()
    {
        SSQL = "";
        SSQL = "Select * from Shift_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        ddlShift.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlShift.DataTextField = "ShiftDesc";
        ddlShift.DataValueField = "ShiftDesc";
        ddlShift.DataBind();
        ddlShift.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }
    private void Load_Contractor()
    {
        SSQL = "";
        SSQL = "Select distinct Contract from Employee_mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and deptname!='SECURITY'";
        ddlContractor.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlContractor.DataTextField = "Contract";
        ddlContractor.DataValueField = "Contract";
        ddlContractor.DataBind();
        ddlContractor.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }
    
    private void Load_Route()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlRoute.Items.Clear();
        query = "Select Distinct RouteName from RouteMst where Type='" + ddlVehicleType.SelectedItem.Text + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlRoute.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["RouteName"] = "-Select-";
        dr["RouteName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlRoute.DataTextField = "RouteName";
        ddlRoute.DataValueField = "RouteName";
        ddlRoute.DataBind();
    }
    private void Load_Route1()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlRoute.Items.Clear();
        query = "Select Distinct BusStop from MstBusStop";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlRoute.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["BusStop"] = "-Select-";
        dr["BusStop"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlRoute.DataTextField = "BusStop";
        ddlRoute.DataValueField = "BusStop";
        ddlRoute.DataBind();
    }
    protected void ddlVehicleType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(ddlVehicleType.SelectedItem.Text=="Company")
        { 
            Load_Route();
        }
        else
        {
            Load_Route1();
        }
    }
    protected void btnDeleteEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        SSQL = "Delete from MstTeaSnacks where CCode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and Auto_ID='" + e.CommandName.ToString() + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
       // Load_Data();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SaveMode = "";
        bool ErrFlag = false;
      

        if (!ErrFlag)
        {
           

            foreach (GridViewRow gvsal in GVModule.Rows)
            {
                SaveMode = "Insert";

                CheckBox ChkSelect_chk = (CheckBox)gvsal.FindControl("chkSelect");
                string AddCheck = "0";
                string VehicleType = "";
                string TokenNo = "";
                string EmpName = "";
                string Dept = "";
                string Route = "";
                string BusStop = "";
                string BUsFare = "0";
                //string VehicleType = "";
                

                if (ChkSelect_chk.Checked == true) { AddCheck = "1"; }


                TokenNo = gvsal.Cells[0].Text.ToString();
                EmpName = gvsal.Cells[1].Text.ToString();
                Dept = gvsal.Cells[2].Text.ToString();
                VehicleType = gvsal.Cells[3].Text.ToString();
                Route = gvsal.Cells[4].Text.ToString();
                BusStop = gvsal.Cells[5].Text.ToString();
                BUsFare = gvsal.Cells[6].Text.ToString();
                //TokenNo = gvsal.Cells[1].Text.ToString();


                //if (ChkSelect_chk.Checked == true || ChkModify.Checked == true || ChkDelete.Checked == true || ChkView.Checked == true || ChkApprove.Checked == true || ChkPrint.Checked == true)

                if (ChkSelect_chk.Checked == true)
                {

                    //Insert Items
                    SSQL = "insert into Emp_Vehicle_Entry(ExistingCode,EmpName,CCode,LCode,Dept,Vehicletype,Route,BusStop,BusFare,Date_Str)values";
                    SSQL = SSQL + "('" + TokenNo + "','" + EmpName + "','" + SessionCcode.ToString() + "','" + SessionLcode.ToString() + "','" + Dept + "','" +VehicleType+ "','" + Route + "','" + BusStop + "','" + BUsFare + "','"+ Convert.ToDateTime(txtDate.Text).ToString("dd/MM/yyyy") + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                   // Load_Data();
                }
            }

            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Details Not Saved Properly...');", true);
            }

        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        ddlShift.ClearSelection();
        //ddlCanteenName.ClearSelection();
        //txtSancksAmount.Text = "0.00";
        //txtTeaAmount.Text = "0.00";
    }
    protected void btnView_Click(object sender, EventArgs e)
    {
        if (ddlVehicleType.SelectedItem.Text == "Company")
        {
            DataTable DT = new DataTable();
            SSQL = "SELECT existingcode,Firstname,deptname,designation,vehicles_type,BusRoute,'NOT AVAIL' as BusStop,'NOT AVAIL' as BusFare FROM employee_mst where Compcode='" + SessionCcode.ToString() + "' and LocCode='" + SessionLcode.ToString() + "' and vehicles_type='Company' and BusRoute='"+ddlRoute.SelectedItem.Text+"'";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT.Rows.Count != 0)
            {
                GVModule.DataSource = DT;
                GVModule.DataBind();
            }
            else
            {

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Found!!');", true);
            }

            if (GVModule.Rows.Count != 0)
            {
                GVPanel.Visible = true;
                chkAll.Visible = true;
                btnSave.Visible = true;
                btnClear.Visible = true;
            }
            else
            {
                GVPanel.Visible = false;
                chkAll.Visible = false;
                btnSave.Visible = false;
                btnClear.Visible = false;
            }
        }
        else if(ddlVehicleType.SelectedItem.Text == "Private" || ddlVehicleType.SelectedItem.Text == "Bike" || ddlVehicleType.SelectedItem.Text == "Others")
        {
            DataTable DT = new DataTable();
            SSQL = "SELECT existingcode,Firstname,deptname,designation,vehicles_type,'NOT AVAIL' as BusRoute,BusStop,BusFare FROM employee_mst where Compcode='" + SessionCcode.ToString() + "' and LocCode='" + SessionLcode.ToString() + "' and (vehicles_type='Bike' or vehicles_type='Private' or vehicles_type='Others') and BusStop='" + ddlRoute.SelectedItem.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            GVModule.DataSource = DT;
            GVModule.DataBind();

            if (GVModule.Rows.Count != 0)
            {
                GVPanel.Visible = true;
                chkAll.Visible = true;
                btnSave.Visible = true;
                btnClear.Visible = true;
            }
            else
            {
                GVPanel.Visible = false;
                chkAll.Visible = false;
                btnSave.Visible = false;
                btnClear.Visible = false;
            }
        }
        else if(ddlShift.SelectedItem.Text!="-Select-")
        {
            DataTable DT = new DataTable();
            SSQL = "SELECT existingcode,Firstname,deptname,designation,vehicles_type, BusRoute,BusStop,BusFare FROM employee_mst where Compcode='" + SessionCcode.ToString() + "' and LocCode='" + SessionLcode.ToString() + "' and Shift_Name='" + ddlShift.SelectedItem.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            GVModule.DataSource = DT;
            GVModule.DataBind();

            if (GVModule.Rows.Count != 0)
            {
                GVPanel.Visible = true;
                chkAll.Visible = true;
                btnSave.Visible = true;
                btnClear.Visible = true;
            }
            else
            {
                GVPanel.Visible = false;
                chkAll.Visible = false;
                btnSave.Visible = false;
                btnClear.Visible = false;
            }
        }
        else if (ddlContractor.SelectedItem.Text != "-Select-")
        {
            DataTable DT = new DataTable();
            SSQL = "SELECT existingcode,Firstname,deptname,designation,vehicles_type, BusRoute,BusStop,BusFare FROM employee_mst where Compcode='" + SessionCcode.ToString() + "' and LocCode='" + SessionLcode.ToString() + "' and Contract='" + ddlContractor.SelectedItem.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            GVModule.DataSource = DT;
            GVModule.DataBind();

            if (GVModule.Rows.Count != 0)
            {
                GVPanel.Visible = true;
                chkAll.Visible = true;
                btnSave.Visible = true;
                btnClear.Visible = true;
            }
            else
            {
                GVPanel.Visible = false;
                chkAll.Visible = false;
                btnSave.Visible = false;
                btnClear.Visible = false;
            }
        }
    }
    protected void chkAll_CheckedChanged(object sender, EventArgs e)
    {
        foreach (GridViewRow gvsal in GVModule.Rows)
        {
            //User Rights Update in Grid
            if (chkAll.Checked == true)
            {
                ((CheckBox)gvsal.FindControl("chkSelect")).Checked = true;

            }
            else
            {
                ((CheckBox)gvsal.FindControl("chkSelect")).Checked = false;

            }
        }
    }
}