﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstCanteen : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Canteen Master";
            load_Shift();
        }

        Load_Data_Canteen();
        Load_data_Tea();
    }

    private void Load_data_Tea()
    {
        SSQL = "";
        SSQL = "Select * from mstTea where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        DataTable dt_tea = new DataTable();
        dt_tea = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater2.DataSource = dt_tea;
        Repeater2.DataBind();
    }
    private void load_Shift()
    {
        SSQL = "";
        SSQL = "Select * from Shift_mst where Compcode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        ddlShift.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlShift.DataTextField = "ShiftDesc";
        ddlShift.DataValueField = "ShiftDesc";
        ddlShift.DataBind();
        ddlShift.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    private void Load_Data_Canteen()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstCanteen where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (ddlShift.SelectedValue == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Shift');", true);
            ErrFlag = true;
            return;
        }
        if (txtAmount.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Canteen Amount');", true);
            ErrFlag = true;
            return;
        }
        if (!ErrFlag)
        {
            if (btnSave.Text == "Save")
            {
                SSQL = "";
                SSQL = "Select * from MstCanteen Where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ShiftName='" + ddlShift.SelectedValue + "'";
                DataTable dt_Check = new DataTable();
                dt_Check = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_Check.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('The Data is Already Present!!!');", true);
                    return;
                }
            }
            SSQL = "";
            SSQL = "Delete from MstCanteen where Auto_ID='" + txtAutoID.Value + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "";
            SSQL = "insert into mstCanteen(Ccode,Lcode,Shiftname,Amount)";
            SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "','" + ddlShift.SelectedValue + "','" + txtAmount.Text + "')";
            objdata.RptEmployeeMultipleDetails(SSQL);

            if (btnSave.Text == "Save")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Data Saved Successfully!');", true);
            }
            if (btnSave.Text == "Update")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Data Updated Successfully!');", true);
            }
            Load_Data_Canteen();
            btnClear_Click(sender, e);

        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtAmount.Text = "";
        txtAutoID.Value = "";
        ddlShift.ClearSelection();
        btnSave.Text = "Save";

    }

    protected void btnEditEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from mstCanteen where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Auto_ID='" + e.CommandName.ToString() + "'";
        DataTable dt_get = new DataTable();
        dt_get = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt_get.Rows.Count > 0)
        {
            txtAmount.Text = dt_get.Rows[0]["Amount"].ToString();
            ddlShift.SelectedValue = dt_get.Rows[0]["ShiftName"].ToString();
            txtAutoID.Value = dt_get.Rows[0]["Auto_ID"].ToString();
            btnSave.Text = "Update";
        }
        Load_Data_Canteen();
    }

    protected void btnDeleteEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "delete from mstCanteen where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Auto_ID='" + e.CommandName.ToString() + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
        Load_Data_Canteen();

    }

    protected void btnTeaSave_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;

        if (txtTeaAmount.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Tea Amount');", true);
            ErrFlag = true;
            return;
        }
        if (!ErrFlag)
        {
            SSQL = "";
            SSQL = "Delete from mstTea where ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "";
            SSQL = "insert into mstTea(Ccode,Lcode,Amount)";
            SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "','" + txtTeaAmount.Text + "')";
            objdata.RptEmployeeMultipleDetails(SSQL);

            btnTeaClear_Click(sender, e);
            Load_data_Tea();
        }
    }

    protected void btnTeaClear_Click(object sender, EventArgs e)
    {
        txtTeaAmount.Text = "0";
        btnTeaSave.Text = "Save";
    }

    protected void btnEditEnquiry_Grid_Tea_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from msttea where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Auto_ID='" + e.CommandName.ToString() + "'";
        DataTable dt_get = new DataTable();
        dt_get = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt_get.Rows.Count > 0)
        {
            txtTeaAmount.Text = dt_get.Rows[0]["Amount"].ToString();
            btnTeaSave.Text = "Update";
        }
        Load_data_Tea();
    }

    protected void btnDeleteEnquiry_Grid_Tea_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "delete from msttea where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Auto_ID='" + e.CommandName.ToString() + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
        Load_data_Tea();
    }
}