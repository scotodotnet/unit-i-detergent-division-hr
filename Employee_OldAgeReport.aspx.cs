﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;

public partial class Employee_OldAgeReport : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Date = "";
    string SSQL;
    //string mvarUserType = "IF User";
    string mvarUserType;
    DataTable mDataSet = new DataTable();
    DataTable dt = new DataTable();
    BALDataAccess objdata = new BALDataAccess();
    DataTable AutoDataTable = new DataTable();
    string iEmpDet = "";
    //char[] delimiters = new char[] { "-",">" };

    string[] delimiters = { "-", ">" };

    string mUnitName = "";
    string strPhotoPath = "";
    FileStream fs;
    BinaryReader br;
    DataRow drow;
    string Empcode;
    DataSet ds = new DataSet();
    string Division;
    string FInancialYear;
    string FromDate = "";
    string ToDate = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report- Employee Old Age Report";

            }

            Date = Convert.ToDateTime(DateTime.Now.AddDays(-1)).ToString();
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();

            SessionUserType = Session["Isadmin"].ToString();

            Division = Request.QueryString["Division"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            if (SessionUserType == "2")
            {
                GetOldAgeTable();

            }
            else
            {
                GetOldAgeTable();

            }


            ds.Tables.Add(AutoDataTable);
            if (AutoDataTable.Rows.Count > 0)
            {
                ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("crystal/EmployeeOldAge.rpt"));

                report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                //if (Division != "-Select-")
                //{
                //    report.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division:" + "'" + "'" + Division + "'";
                //}
                //else
                //{
                report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate + "'";
                report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate + "'";
                //}
                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = report;
                //   report.Dispose();
            }
            else
            {
                Response.Write("<script>alert('No Records Found!!!')</Script>");
            }
        }
    }
    public void GetOldAgeTable()
    {
        AutoDataTable.Columns.Add("Sno");
        AutoDataTable.Columns.Add("CompanyName");
        AutoDataTable.Columns.Add("LocationName");
        AutoDataTable.Columns.Add("TokenNo");
        AutoDataTable.Columns.Add("EmpName");
        AutoDataTable.Columns.Add("DeptName");
        AutoDataTable.Columns.Add("Designation");
        AutoDataTable.Columns.Add("Contractor");
        AutoDataTable.Columns.Add("Age");
        AutoDataTable.Columns.Add("Company");
        AutoDataTable.Columns.Add("Address");
        AutoDataTable.Columns.Add("Date");

        //SSQL = "";
        //SSQL = "SELECT LD.ExistingCode,LD.FirstName,EM.EmployeeMobile,EM.Deptname,EM.Designation,EM.Contract FROM logtime_days LD INNER JOIN employee_mst EM ON EM.MachineID=LD.MachineID WHERE LD.Present='0.0'";
        //SSQL = SSQL + " AND LD.Attn_Date BETWEEN DATE_FORMAT('" + Convert.ToDateTime(FromDate).ToString("yyyy-MM-dd") + "','%Y-%m-%d') AND ";
        //SSQL = SSQL + " DATE_FORMAT('" + Convert.ToDateTime(ToDate).ToString("yyyy-MM-dd") + "','%Y-%m-%d') GROUP BY LD.ExistingCode,LD.FirstName HAVING COUNT(LD.Present) > 2";

        SSQL = "";
        SSQL = "SELECT ExistingCode,FirstName,Deptname,Designation,Contract,Age FROM employee_mst WHERE Age>=58 AND DOJ";
        SSQL = SSQL + " BETWEEN DATE_FORMAT('" + Convert.ToDateTime(FromDate).ToString("yyyy-MM-dd") + "','%Y-%m-%d') AND DATE_FORMAT ('" + Convert.ToDateTime(ToDate).ToString("yyyy-MM-dd") + "','%Y-%m-%d')  ";
       // SSQL = SSQL + " DATE_FORMAT('" + Convert.ToDateTime(ToDate).ToString("yyyy-MM-dd") + "','%Y-%m-%d') GROUP BY LD.ExistingCode,LD.FirstName HAVING COUNT(LD.Present) > 2";

        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
        SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        string name = dt.Rows[0]["CompName"].ToString();
        string Addres = dt.Rows[0]["Add1"].ToString();
        int sno = 1;
        if (mDataSet.Rows.Count > 0)
        {

            for (int iRow = 0; iRow < mDataSet.Rows.Count; iRow++)
            {

                AutoDataTable.NewRow();
                AutoDataTable.Rows.Add();


                AutoDataTable.Rows[iRow]["Sno"] = sno;
                AutoDataTable.Rows[iRow]["CompanyName"] = SessionCcode;
                AutoDataTable.Rows[iRow]["LocationName"] = SessionLcode;
                AutoDataTable.Rows[iRow]["TokenNo"] = mDataSet.Rows[iRow]["ExistingCode"].ToString();
                AutoDataTable.Rows[iRow]["EmpName"] = mDataSet.Rows[iRow]["FirstName"].ToString();
                AutoDataTable.Rows[iRow]["DeptName"] = mDataSet.Rows[iRow]["DeptName"].ToString();
                AutoDataTable.Rows[iRow]["Designation"] = mDataSet.Rows[iRow]["Designation"].ToString();
                AutoDataTable.Rows[iRow]["Contractor"] = mDataSet.Rows[iRow]["Contract"].ToString();
                AutoDataTable.Rows[iRow]["Age"] = mDataSet.Rows[iRow]["Age"].ToString();
                AutoDataTable.Rows[iRow]["Company"] = name.ToString();
                AutoDataTable.Rows[iRow]["Address"] = Addres.ToString();
                AutoDataTable.Rows[iRow]["Date"] = DateTime.Parse(FromDate).ToString("dd/MM/yyyy");

                sno += 1;

            }
        }

    }
    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }
}