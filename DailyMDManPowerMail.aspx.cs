﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;

public partial class DailyMDManPowerMail : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Date = "";
    string SSQL;
    //string mvarUserType = "IF User";
    string mvarUserType;
    DataTable mDataSet = new DataTable();
    DataTable dt = new DataTable();
    DataTable dt1 = new DataTable();
    DataTable dt2 = new DataTable();
    DataTable dt3 = new DataTable();
    BALDataAccess objdata = new BALDataAccess();
    DataTable AutoDataTable = new DataTable();
    DataTable AutoDataTable1 = new DataTable();
    DataTable AutoDataTable2 = new DataTable();
    string iEmpDet = "";
    //char[] delimiters = new char[] { "-",">" };

    string[] delimiters = { "-", ">" };

    string mUnitName = "";
    string strPhotoPath = "";
    FileStream fs;
    BinaryReader br;
    DataRow drow;
    string Empcode;
    DataSet ds = new DataSet();
    string Division;
    string FInancialYear;
    string FromDate = "";
    string BCount = "";
    string PCount = "";
    string TCount = "";

    MailMessage Mail = new MailMessage("SCOTO <nldvnaga.hr@gmail.com>", "jayalalitha@nagamills.com,prabhup@nagamills.com,sasikumar@nagamills.com,munusamyk@nagamills.com,thirumalaisamy@nagamills.com,renganathanm@nagamills.com,padmanabhanscoto@gmail.com");
    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Shift Man Power Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("SalaryProcess"));
                //li.Attributes.Add("class", "droplink active open");
            }

            Date = Convert.ToDateTime(DateTime.Now.AddDays(-1)).ToString();
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            //SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            // SessionUserType = Session["Isadmin"].ToString();

            //Division = Request.QueryString["Division"].ToString();
            //FInancialYear = DateTime.Now.Year.ToString();
            //FInancialYear = FInancialYear + "-01" + "-01";
            //FromDate = Request.QueryString["FromDate"].ToString();
            //FromDate = DateTime.Parse(FromDate).ToString("yyyy/MM/dd");
            // Date = Convert.ToDateTime(DateTime.Now.AddDays(-1)).ToString();
           // Date = Convert.ToDateTime("02/07/2021").ToString();
            GetShiftManPowerTable1("logtime_bar_plant");
            GetShiftManPowerTable2("logtime_powder_plant");
            GetShiftManPowerTable3("logtime_tsp_plant");


           


            ds.Tables.Add(AutoDataTable);
            ds.Tables.Add(AutoDataTable1);
            ds.Tables.Add(AutoDataTable2);
            AutoDataTable.Merge(AutoDataTable1);
            AutoDataTable.Merge(AutoDataTable2);
            if (AutoDataTable.Rows.Count > 0)
            {

                ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("crystal/Shift_Manpower_Report.rpt"));
                report.DataDefinition.FormulaFields["BCount"].Text = "'" + BCount + "'";
                report.DataDefinition.FormulaFields["PCount"].Text = "'" + PCount + "'";
                report.DataDefinition.FormulaFields["TCount"].Text = "'" + TCount + "'";
                report.Database.Tables[0].SetDataSource(AutoDataTable);
                //report.Database.Tables[0].SetDataSource(ds.Tables[1]);
                //report.Database.Tables[0].SetDataSource(ds.Tables[2]);

                //if (Division != "-Select-")
                //{
                //    report.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division:" + "'" + "'" + Division + "'";
                //}
                //else
                //{
                //    report.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division:" + "'";
                ////}
                //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                //CrystalReportViewer1.ReportSource = report;
                string Server_Path = Server.MapPath("~");
                string AttachfileName_Miss = "";
                //  string Invoice_No_Str = WitSalesOrdNo.Replace("/", "_");
                AttachfileName_Miss = Server_Path + "/Daily_Report/ShiftManPower_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy_HHmmss") + ".pdf";

                if (File.Exists(AttachfileName_Miss))
                {
                    File.Delete(AttachfileName_Miss);
                }

                report.ExportToDisk(ExportFormatType.PortableDocFormat, AttachfileName_Miss);
                report.Close();

            }
            else
            {
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Data Found..!');", true);
                Response.Write("<script>alert('No Records Found!!!')</Script>");

            }
        }
      MailReport();
    }
    public void GetShiftManPowerTable1(String Plant)
    {
        AutoDataTable.Columns.Add("Sno");
        AutoDataTable.Columns.Add("CompanyName");
        AutoDataTable.Columns.Add("LocationName");
        AutoDataTable.Columns.Add("Shift");
        AutoDataTable.Columns.Add("Plant");
        AutoDataTable.Columns.Add("EmployeeCount");
        AutoDataTable.Columns.Add("Male");
        AutoDataTable.Columns.Add("Female");
        AutoDataTable.Columns.Add("Company");
        AutoDataTable.Columns.Add("Address");
        AutoDataTable.Columns.Add("FromDate");
        AutoDataTable.Columns.Add("BCount");
        AutoDataTable.Columns.Add("Actual");
        AutoDataTable.Columns.Add("Manhrstobe");

        //Standard Count
        DataTable Cnt = new DataTable();
        DataTable dt_hrs = new DataTable();
        DataTable dt_Manhrs = new DataTable();
        //SSQL = "SELECT std AS Total,Manhrs AS Manhrs,shift as Shift_name FROM MstStdManhrs ";
        //SSQL = SSQL + " WHERE Plant='Bar' and CCode='" + SessionCcode.ToString() + "' and LCode='" + SessionLcode.ToString() + "'";
        //Cnt = objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "SELECT COUNT(DISTINCT(lp.machineid)) AS Total,em.shift_Name FROM employee_mst em ";
        SSQL = SSQL + " INNER JOIN logtime_Bar_plant lp ON lp.machineid=em.machineID_Encrypt";
        SSQL = SSQL + " WHERE DATE_FORMAT(lp.TIME,'%Y-%m-%d')=DATE_FORMAT('" + Convert.ToDateTime(Date).AddDays(0).ToString("yyyy-MM-dd") + "','%Y-%m-%d') and Em.compCode='" + SessionCcode.ToString() + "' and Em.loccode='" + SessionLcode.ToString() + "' GROUP BY em.shift_name";
        Cnt = objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        string name = dt.Rows[0]["CompName"].ToString();
        string Addres = dt.Rows[0]["Add1"].ToString();
        //int sno = 1;
        if (Cnt.Rows.Count > 0)
        {


            for (int iRow = 0; iRow < Cnt.Rows.Count; iRow++)
            {
                //Actual Count
                SSQL = "SELECT COUNT(DISTINCT(lp.machineid)) AS Total,em.shift_Name,'Bar Plant' AS Plant FROM employee_mst em ";
                SSQL = SSQL + " INNER JOIN logtime_bar_plant lp ON lp.machineid=em.machineID_Encrypt";
                SSQL = SSQL + " WHERE DATE_FORMAT(lp.TIME,'%Y-%m-%d')='" + Convert.ToDateTime(Date).ToString("yyyy-MM-dd") + "' and em.compCode='" + SessionCcode.ToString() + "' and em.loccode='" + SessionLcode.ToString() + "' And em.shift_Name='" + Cnt.Rows[iRow]["shift_name"].ToString() + "'";
                dt3 = objdata.RptEmployeeMultipleDetails(SSQL);

                //Actual Hours
                SSQL = "SELECT SUM(DISTINCT(ld.Total_hrs)) AS Hrs FROM employee_mst em ";
                SSQL = SSQL + " INNER JOIN logtime_bar_plant lp ON lp.machineid=em.machineID_Encrypt INNER JOIN logtime_days Ld ON Ld.Machineid=em.machineid AND lp.machineid=em.machineID_Encrypt";
                SSQL = SSQL + " WHERE DATE_FORMAT(lp.TIME,'%Y-%m-%d')='" + Convert.ToDateTime(Date).ToString("yyyy-MM-dd") + "' and em.compCode='" + SessionCcode.ToString() + "' and em.loccode='" + SessionLcode.ToString() + "' And em.shift_Name='" + Cnt.Rows[iRow]["shift_name"].ToString() + "'";
                dt_hrs = objdata.RptEmployeeMultipleDetails(SSQL);

                AutoDataTable.NewRow();
                AutoDataTable.Rows.Add();


                //AutoDataTable.Rows[iRow]["Sno"] = sno;
                AutoDataTable.Rows[iRow]["CompanyName"] = SessionCcode;
                AutoDataTable.Rows[iRow]["LocationName"] = SessionLcode;
                AutoDataTable.Rows[iRow]["Shift"] = Cnt.Rows[iRow]["shift_name"].ToString();
                AutoDataTable.Rows[iRow]["Plant"] = dt3.Rows[0]["Plant"].ToString();
                AutoDataTable.Rows[iRow]["EmployeeCount"] = dt3.Rows[0]["Total"].ToString();
                AutoDataTable.Rows[iRow]["Address"] = Addres.ToString();
                AutoDataTable.Rows[iRow]["Company"] = name.ToString();
                AutoDataTable.Rows[iRow]["BCount"] = Cnt.Rows[iRow]["Total"].ToString();
                AutoDataTable.Rows[iRow]["FromDate"] = DateTime.Parse(Date).ToString("dd/MM/yyyy");
                AutoDataTable.Rows[iRow]["Actual"] = dt_hrs.Rows[0]["Hrs"].ToString();
                AutoDataTable.Rows[iRow]["Manhrstobe"] = Cnt.Rows[iRow]["Total"].ToString();
                BCount = Cnt.Rows[0]["Total"].ToString();
                //Check Male

                SSQL = "SELECT COUNT(DISTINCT(lp.machineid)) AS Male FROM employee_mst em ";
                SSQL = SSQL + " INNER JOIN logtime_bar_plant lp ON lp.machineid=em.machineID_Encrypt WHERE DATE_FORMAT(lp.TIME,'%Y-%m-%d')='" + Convert.ToDateTime(Date).ToString("yyyy-MM-dd") + "' AND em.gender='Male' and em.compCode='" + SessionCcode.ToString() + "' and em.loccode='" + SessionLcode.ToString() + "' And em.shift_Name='" + Cnt.Rows[iRow]["shift_name"].ToString() + "'";
                dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
                AutoDataTable.Rows[iRow]["Male"] = dt1.Rows[0]["Male"].ToString();
                //Check Female

                SSQL = "SELECT COUNT(DISTINCT(lp.machineid)) AS Female FROM employee_mst em ";
                SSQL = SSQL + " INNER JOIN logtime_bar_plant lp ON lp.machineid=em.machineID_Encrypt WHERE DATE_FORMAT(lp.TIME,'%Y-%m-%d')='" + Convert.ToDateTime(Date).ToString("yyyy-MM-dd") + "' AND em.gender='Female' and em.compCode='" + SessionCcode.ToString() + "' and em.loccode='" + SessionLcode.ToString() + "' And em.shift_Name='" + Cnt.Rows[iRow]["shift_name"].ToString() + "'";
                dt2 = objdata.RptEmployeeMultipleDetails(SSQL);
                AutoDataTable.Rows[iRow]["Female"] = dt2.Rows[0]["Female"].ToString();






                // sno += 1;

            }
        }
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Data Found..!');", true);
        //}

    }
    public void GetShiftManPowerTable2(String Plant)
    {
        AutoDataTable1.Columns.Add("Sno");
        AutoDataTable1.Columns.Add("CompanyName");
        AutoDataTable1.Columns.Add("LocationName");
        AutoDataTable1.Columns.Add("Shift");
        AutoDataTable1.Columns.Add("Plant");
        AutoDataTable1.Columns.Add("EmployeeCount");
        AutoDataTable1.Columns.Add("Male");
        AutoDataTable1.Columns.Add("Female");
        AutoDataTable1.Columns.Add("Company");
        AutoDataTable1.Columns.Add("Address");
        AutoDataTable1.Columns.Add("FromDate");
        AutoDataTable1.Columns.Add("BCount");
        AutoDataTable1.Columns.Add("Actual");
        AutoDataTable1.Columns.Add("Manhrstobe");

        DataTable Cnt = new DataTable();
        DataTable dt_hrs = new DataTable();
        SSQL = "SELECT COUNT(DISTINCT(lp.machineid)) AS Total,em.shift_Name FROM employee_mst em ";
        SSQL = SSQL + " INNER JOIN logtime_Powder_plant lp ON lp.machineid=em.machineID_Encrypt";
        SSQL = SSQL + " WHERE DATE_FORMAT(lp.TIME,'%Y-%m-%d')=DATE_FORMAT('" + Convert.ToDateTime(Date).AddDays(0).ToString("yyyy-MM-dd") + "','%Y-%m-%d') and Em.compCode='" + SessionCcode.ToString() + "' and Em.loccode='" + SessionLcode.ToString() + "' GROUP BY em.shift_name";
        Cnt = objdata.RptEmployeeMultipleDetails(SSQL);
        //SSQL = "SELECT std AS Total,Manhrs AS Manhrs,shift as Shift_name FROM MstStdManhrs ";
        //SSQL = SSQL + " WHERE Plant='Powder' and CCode='" + SessionCcode.ToString() + "' and LCode='" + SessionLcode.ToString() + "'";
        //Cnt = objdata.RptEmployeeMultipleDetails(SSQL);


        SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        string name = dt.Rows[0]["CompName"].ToString();
        string Addres = dt.Rows[0]["Add1"].ToString();
        // int sno = 1;
        if (Cnt.Rows.Count > 0)
        {


            for (int iRow = 0; iRow < Cnt.Rows.Count; iRow++)
            {

                SSQL = "SELECT COUNT(DISTINCT(lp.machineid)) AS Total,em.shift_Name,'Powder Plant' AS Plant FROM employee_mst em ";
                SSQL = SSQL + " INNER JOIN logtime_Powder_plant lp ON lp.machineid=em.machineID_Encrypt";
                SSQL = SSQL + " WHERE DATE_FORMAT(lp.TIME,'%Y-%m-%d')='" + Convert.ToDateTime(Date).ToString("yyyy-MM-dd") + "' and em.compCode='" + SessionCcode.ToString() + "' and em.loccode='" + SessionLcode.ToString() + "' And em.shift_Name='" + Cnt.Rows[iRow]["shift_name"].ToString() + "'";
                dt3 = objdata.RptEmployeeMultipleDetails(SSQL);

                //Actual Hours
                SSQL = "SELECT SUM(DISTINCT(ld.Total_hrs)) AS Hrs FROM employee_mst em ";
                SSQL = SSQL + " INNER JOIN logtime_Powder_plant lp ON lp.machineid=em.machineID_Encrypt INNER JOIN logtime_days Ld ON Ld.Machineid=em.machineid AND lp.machineid=em.machineID_Encrypt";
                SSQL = SSQL + " WHERE DATE_FORMAT(lp.TIME,'%Y-%m-%d')='" + Convert.ToDateTime(Date).ToString("yyyy-MM-dd") + "' and em.compCode='" + SessionCcode.ToString() + "' and em.loccode='" + SessionLcode.ToString() + "' And em.shift_Name='" + Cnt.Rows[iRow]["shift_name"].ToString() + "'";
                dt_hrs = objdata.RptEmployeeMultipleDetails(SSQL);

                AutoDataTable1.NewRow();
                AutoDataTable1.Rows.Add();


                // AutoDataTable1.Rows[iRow]["Sno"] = sno;
                AutoDataTable1.Rows[iRow]["CompanyName"] = SessionCcode;
                AutoDataTable1.Rows[iRow]["LocationName"] = SessionLcode;
                AutoDataTable1.Rows[iRow]["Shift"] = Cnt.Rows[iRow]["shift_name"].ToString();
                AutoDataTable1.Rows[iRow]["Plant"] = dt3.Rows[0]["Plant"].ToString();
                AutoDataTable1.Rows[iRow]["EmployeeCount"] = dt3.Rows[0]["Total"].ToString();
                AutoDataTable1.Rows[iRow]["Address"] = Addres.ToString();
                AutoDataTable1.Rows[iRow]["Company"] = name.ToString();
                AutoDataTable1.Rows[iRow]["BCount"] = Cnt.Rows[iRow]["Total"].ToString();
                AutoDataTable1.Rows[iRow]["FromDate"] = DateTime.Parse(Date).ToString("dd/MM/yyyy");
                AutoDataTable1.Rows[iRow]["Actual"] = dt_hrs.Rows[0]["Hrs"].ToString();
                AutoDataTable1.Rows[iRow]["Manhrstobe"] = Cnt.Rows[iRow]["Total"].ToString();
                BCount = Cnt.Rows[0]["Total"].ToString();
                //Check Male

                SSQL = "SELECT COUNT(DISTINCT(lp.machineid)) AS Male FROM employee_mst em ";
                SSQL = SSQL + " INNER JOIN logtime_Powder_plant lp ON lp.machineid=em.machineID_Encrypt WHERE DATE_FORMAT(lp.TIME,'%Y-%m-%d')='" + Convert.ToDateTime(Date).ToString("yyyy-MM-dd") + "' AND em.gender='Male' and em.compCode='" + SessionCcode.ToString() + "' and em.loccode='" + SessionLcode.ToString() + "' And em.shift_Name='" + Cnt.Rows[iRow]["shift_name"].ToString() + "'";
                dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
                AutoDataTable1.Rows[iRow]["Male"] = dt1.Rows[0]["Male"].ToString();
                //Check Female

                SSQL = "SELECT COUNT(DISTINCT(lp.machineid)) AS Female FROM employee_mst em ";
                SSQL = SSQL + " INNER JOIN logtime_Powder_plant lp ON lp.machineid=em.machineID_Encrypt WHERE DATE_FORMAT(lp.TIME,'%Y-%m-%d')='" + Convert.ToDateTime(Date).ToString("yyyy-MM-dd") + "' AND em.gender='Female' and em.compCode='" + SessionCcode.ToString() + "' and em.loccode='" + SessionLcode.ToString() + "' And em.shift_Name='" + Cnt.Rows[iRow]["shift_name"].ToString() + "'";
                dt2 = objdata.RptEmployeeMultipleDetails(SSQL);
                AutoDataTable1.Rows[iRow]["Female"] = dt2.Rows[0]["Female"].ToString();

                // sno += 1;

            }
        }
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Data Found..!');", true);
        //}

    }
    public void GetShiftManPowerTable3(String Plant)
    {
        AutoDataTable2.Columns.Add("Sno");
        AutoDataTable2.Columns.Add("CompanyName");
        AutoDataTable2.Columns.Add("LocationName");
        AutoDataTable2.Columns.Add("Shift");
        AutoDataTable2.Columns.Add("Plant");
        AutoDataTable2.Columns.Add("EmployeeCount");
        AutoDataTable2.Columns.Add("Male");
        AutoDataTable2.Columns.Add("Female");
        AutoDataTable2.Columns.Add("Company");
        AutoDataTable2.Columns.Add("Address");
        AutoDataTable2.Columns.Add("FromDate");
        AutoDataTable2.Columns.Add("BCount");
        AutoDataTable2.Columns.Add("Actual");
        AutoDataTable2.Columns.Add("Manhrstobe");

        DataTable Cnt = new DataTable();
        DataTable dt_hrs = new DataTable();
        SSQL = "SELECT COUNT(DISTINCT(lp.machineid)) AS Total,em.shift_Name FROM employee_mst em ";
        SSQL = SSQL + " INNER JOIN logtime_tsp_plant lp ON lp.machineid=em.machineID_Encrypt";
        SSQL = SSQL + " WHERE DATE_FORMAT(lp.TIME,'%Y-%m-%d')=DATE_FORMAT('" + Convert.ToDateTime(Date).AddDays(0).ToString("yyyy-MM-dd") + "','%Y-%m-%d') and Em.compCode='" + SessionCcode.ToString() + "' and Em.loccode='" + SessionLcode.ToString() + "' GROUP BY em.shift_name";
        Cnt = objdata.RptEmployeeMultipleDetails(SSQL);
        SSQL = "SELECT std AS Total,Manhrs AS Manhrs,shift as Shift_name FROM MstStdManhrs ";
        SSQL = SSQL + " WHERE Plant='TSP' and CCode='" + SessionCcode.ToString() + "' and LCode='" + SessionLcode.ToString() + "'";
        Cnt = objdata.RptEmployeeMultipleDetails(SSQL);


        SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        string name = dt.Rows[0]["CompName"].ToString();
        string Addres = dt.Rows[0]["Add1"].ToString();
        //int sno = 1;
        if (Cnt.Rows.Count > 0)
        {


            for (int iRow = 0; iRow < Cnt.Rows.Count; iRow++)
            {

                SSQL = "SELECT COUNT(DISTINCT(lp.machineid)) AS Total,em.shift_Name,'TSP Plant' AS Plant FROM employee_mst em ";
                SSQL = SSQL + " INNER JOIN logtime_tsp_plant lp ON lp.machineid=em.machineID_Encrypt";
                SSQL = SSQL + " WHERE DATE_FORMAT(lp.TIME,'%Y-%m-%d')='" + Convert.ToDateTime(Date).ToString("yyyy-MM-dd") + "' and em.compCode='" + SessionCcode.ToString() + "' and em.loccode='" + SessionLcode.ToString() + "' And em.shift_Name='" + Cnt.Rows[iRow]["shift_name"].ToString() + "'";
                dt3 = objdata.RptEmployeeMultipleDetails(SSQL);

                //Actual Hours
                SSQL = "SELECT SUM(DISTINCT(ld.Total_hrs)) AS Hrs FROM employee_mst em ";
                SSQL = SSQL + " INNER JOIN logtime_tsp_plant lp ON lp.machineid=em.machineID_Encrypt INNER JOIN logtime_days Ld ON Ld.Machineid=em.machineid AND lp.machineid=em.machineID_Encrypt";
                SSQL = SSQL + " WHERE DATE_FORMAT(lp.TIME,'%Y-%m-%d')='" + Convert.ToDateTime(Date).ToString("yyyy-MM-dd") + "' and em.compCode='" + SessionCcode.ToString() + "' and em.loccode='" + SessionLcode.ToString() + "' And em.shift_Name='" + Cnt.Rows[iRow]["shift_name"].ToString() + "'";
                dt_hrs = objdata.RptEmployeeMultipleDetails(SSQL);

                AutoDataTable2.NewRow();
                AutoDataTable2.Rows.Add();


                // AutoDataTable2.Rows[iRow]["Sno"] = sno;
                AutoDataTable2.Rows[iRow]["CompanyName"] = SessionCcode;
                AutoDataTable2.Rows[iRow]["LocationName"] = SessionLcode;
                AutoDataTable2.Rows[iRow]["Shift"] = Cnt.Rows[iRow]["shift_name"].ToString();
                AutoDataTable2.Rows[iRow]["Plant"] = dt3.Rows[0]["Plant"].ToString();
                AutoDataTable2.Rows[iRow]["EmployeeCount"] = dt3.Rows[0]["Total"].ToString();
                AutoDataTable2.Rows[iRow]["Address"] = Addres.ToString();
                AutoDataTable2.Rows[iRow]["Company"] = name.ToString();
                AutoDataTable2.Rows[iRow]["BCount"] = Cnt.Rows[iRow]["Total"].ToString();
                AutoDataTable2.Rows[iRow]["FromDate"] = DateTime.Parse(Date).ToString("dd/MM/yyyy");
                AutoDataTable2.Rows[iRow]["Actual"] = dt_hrs.Rows[0]["Hrs"].ToString();
                AutoDataTable2.Rows[iRow]["Manhrstobe"] = Cnt.Rows[iRow]["Total"].ToString();
                BCount = Cnt.Rows[0]["Total"].ToString();
                //Check Male

                SSQL = "SELECT COUNT(DISTINCT(lp.machineid)) AS Male FROM employee_mst em ";
                SSQL = SSQL + " INNER JOIN logtime_tsp_plant lp ON lp.machineid=em.machineID_Encrypt WHERE DATE_FORMAT(lp.TIME,'%Y-%m-%d')='" + Convert.ToDateTime(Date).ToString("yyyy-MM-dd") + "' AND em.gender='Male' and em.compCode='" + SessionCcode.ToString() + "' and em.loccode='" + SessionLcode.ToString() + "' And em.shift_Name='" + Cnt.Rows[iRow]["shift_name"].ToString() + "'";
                dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
                AutoDataTable2.Rows[iRow]["Male"] = dt1.Rows[0]["Male"].ToString();
                //Check Female

                SSQL = "SELECT COUNT(DISTINCT(lp.machineid)) AS Female FROM employee_mst em ";
                SSQL = SSQL + " INNER JOIN logtime_tsp_plant lp ON lp.machineid=em.machineID_Encrypt WHERE DATE_FORMAT(lp.TIME,'%Y-%m-%d')='" + Convert.ToDateTime(Date).ToString("yyyy-MM-dd") + "' AND em.gender='Female' and em.compCode='" + SessionCcode.ToString() + "' and em.loccode='" + SessionLcode.ToString() + "' And em.shift_Name='" + Cnt.Rows[iRow]["shift_name"].ToString() + "'";
                dt2 = objdata.RptEmployeeMultipleDetails(SSQL);
                AutoDataTable2.Rows[iRow]["Female"] = dt2.Rows[0]["Female"].ToString();

                // sno += 1;

            }
        }
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Data Found..!');", true);
        //}


    }
    private void MailReport()
    {
        //Mail.CC = "padmanaban";
        Mail.Subject = "Daily Attendance Report";
        Mail.Body = "Dear Sir, Please find the Attachement for (" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + ") Attendance Report";
        // Mail.CC=new 
        // string FileName = Path.GetFileName(fuAttachment.PostedFile.FileName);
        string Server_Path = Server.MapPath("~");
        string AttachfileName_Miss = "";
        //  string Invoice_No_Str = WitSalesOrdNo.Replace("/", "_");
        AttachfileName_Miss = Server_Path + "/Daily_Report/ShiftManPower_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy_HHmmss") + ".pdf";
        if (File.Exists(AttachfileName_Miss))
        {
            Mail.Attachments.Add(new Attachment(AttachfileName_Miss));
        }
      
        Mail.IsBodyHtml = false;
        SmtpClient smtp = new SmtpClient();
        smtp.Host = "smtp.gmail.com";
        smtp.EnableSsl = true;
        NetworkCredential NetworkCred = new NetworkCredential("nldvnaga.hr@gmail.com", "Altius2005");
        smtp.UseDefaultCredentials = false;
        smtp.Credentials = NetworkCred;
        smtp.Port = 587;
        smtp.Send(Mail);
        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('Email sent.');", true);
        //File.Delete(Server_Path + "/Daily_Report/Daily_Attendance_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy") + ".pdf");
        //File.Delete(Server_Path + "/Daily_Report/Daily_Attendance_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy") + ".pdf");
        //File.Delete(Server_Path + "/Daily_Report/SalaryConsolidate_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy") + ".pdf");
        //File.Delete(Server_Path + "/Daily_Report/Daily_Improper_Punch_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy") + ".pdf");
    }
}