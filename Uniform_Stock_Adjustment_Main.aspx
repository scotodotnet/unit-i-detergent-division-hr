﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="Uniform_Stock_Adjustment_Main.aspx.cs" Inherits="Uniform_Stock_Adjustment_Main" Title="Uniform Stock Adjustment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>

 <script type="text/javascript">
     $(document).ready(function() {
         $('#example').dataTable();
      
     });
	</script>
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>

<asp:UpdatePanel ID="UpdatePanel5" runat="server">
 <ContentTemplate>
<!-- begin #content -->
<div id="content" class="content">
 <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        
        <li><a href="javascript:;">Uniform</a></li>
        <li class="active">Stock Adjustment</li>
    </ol>
    <!-- end breadcrumb -->
     <!-- begin page-header -->
    <h1 class="page-header">Stock Adjustment</h1>
    <!-- end page-header -->
    
    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <div>
                <!-- begin panel -->
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                        <h4 class="panel-title">Stock Adjustment</h4>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3 col-sm-3">
                                <asp:LinkButton ID="lbtnAdd" runat="server" class="btn btn-success" 
                                    onclick="lbtnAdd_Click">Add New</asp:LinkButton>
                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-3"></div><br />
                        </div>
                        <div class="row">
                       
                               <div class="form-group">
                               <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
					                    <HeaderTemplate>
                                            <table id="example" class="display table">
                                                <thead>
                                                    <tr>
                                                       <th>TransID</th>
                                                       <th>TransDate</th>
                                                       <th>Remarks</th>
                                                       <th>Add Qty</th>
                                                       <th>Minus Qty</th>
                                                       <th>Mode</th>
                                                      
                                                    </tr>
                                                </thead>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Eval("TransID")%></td>
                                                <td><%# Eval("TransDate")%></td>
                                                <td><%# Eval("Remarks")%></td>
                                                <td><%# Eval("Qty_Total")%></td>
                                                <td><%# Eval("Minus_Qty_Total")%></td>
                                                 
                                                <td>
                                                    <asp:LinkButton ID="btnEditIssueEntry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                        Text="" OnCommand="GridEditEntryClick" CommandArgument="Edit" CommandName='<%# Eval("TransID")%>'>
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="btnApprove" class="btn btn-primary btn-sm fa fa-thumbs-up"  runat="server" 
                                                        Text="" OnCommand="GridApproveEntryClick" CommandArgument="Approve" CommandName='<%# Eval("TransID")%>'
                                                        OnClientClick="return confirm('Are you sure you want to approve this Inward details?');">
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="btnDelete" class="btn btn-danger btn-sm fa fa-trash"  runat="server" 
                                                        Text="" OnCommand="GridDeleteEntryClick" CommandArgument="Approve" CommandName='<%# Eval("TransID")%>' 
                                                        OnClientClick="return confirm('Are you sure you want to delete this Inward details?');">
                                                    </asp:LinkButton>
                                               
                                               
                                               </td>
                                                    
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate></table></FooterTemplate>                                
					                </asp:Repeater>
                               </div>
                         
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
</div>
</ContentTemplate>
</asp:UpdatePanel>




</asp:Content>



