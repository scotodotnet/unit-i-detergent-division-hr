﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstPermission : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Permission Master";
            Load_Data();
        }

    }

    private void Load_Data()
    {
        SSQL = "";
        SSQL = "Select * from permissionmst where CCode='" + SessionCcode + "' and LCode='" + SessionLcode + "' ";
        DataTable dt_get = new DataTable();
        dt_get = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt_get.Rows.Count > 0)
        {
            txtDays.Text = dt_get.Rows[0]["Days"].ToString();
        }
        else
        {
            txtDays.Text = "0";
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (txtDays.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Days!!!');", true);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            SSQL = "Delete from permissionmst where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);
            SSQL = "Insert into permissionmst(CCode,Lcode,Days) values('" + SessionCcode + "','" + SessionLcode + "','" + txtDays.Text + "')";
            objdata.RptEmployeeMultipleDetails(SSQL);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Permission Added Successfully!!!');", true);
            Load_Data();
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtDays.Text = "";
    }
}