﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class RO_Expenses_Entry_Main : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR | RO ON DUTY EXPENSES";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Menu_Commision"));
            //li.Attributes.Add("class", "has-sub active open");
        }
        ondutyDetails();
    }

    public void ondutyDetails()
    {

        DataTable dtDisplay = new DataTable();
        string Query = "Select * from RO_OnDuty_Expenses where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
        dtDisplay = objdata.RptEmployeeMultipleDetails(Query);
        Repeater1.DataSource = dtDisplay;
        Repeater1.DataBind();

    }
    protected void GridEditEntryClick(object sender, CommandEventArgs e)
    {
        DataTable DT = new DataTable();

        string Query = "Select * from RO_OnDuty_Expenses where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and TransID='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(Query);

        if (DT.Rows.Count != 0)
        {
            Session["RO_TransID"] = e.CommandName.ToString();
            Response.Redirect("RO_Expenses_Entry.aspx");
        }
    }

    protected void lbtnAdd_Click(object sender, EventArgs e)
    {
        Session.Remove("RO_TransID");
        Response.Redirect("RO_Expenses_Entry.aspx");
    }

    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        DataTable DT = new DataTable();

        string Query = "Select * from RO_OnDuty_Expenses where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and TransID='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(Query);

        if (DT.Rows.Count != 0)
        {
            Query = "Delete from RO_OnDuty_Expenses where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and TransID='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(Query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('RO ON Duty Details get Deleted..');", true);
        }
        ondutyDetails();
    }
}
