﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;


public partial class EmpVehicleReport : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Division;
    string Status;
    double Final_Count;
    string TokenNo;

    double Present_Count;
    double Absent_Count;
    decimal Fixed_Work_Days;
    decimal Month_WH_Count;
    double Present_WH_Count;
    double NFH_Days_Count;
    double NFH_Days_Present_Count;

    BALDataAccess objdata = new BALDataAccess();

    string SSQL = "";
    string Adolescent_Shift;
    DataSet ds = new DataSet();
    DataSet ds1 = new DataSet();
    DataSet ds2 = new DataSet();

    ReportDocument report = new ReportDocument();
    DataTable AutoDTable = new DataTable();
    DataTable DataCell = new DataTable();
   
    DataTable mEmployeeDT = new DataTable();
    DataTable MEmployeeDS = new DataTable();
    DateTime date1;
    DateTime Date2 = new DateTime();
    int intK;
    string FromDate;
    string ToDate;
    string Date_Value_Str;
    string Date_value_str1;
    string[] Time_Minus_Value_Check;
    string WagesType;
    double leaveCount;

    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report- Muster Report Between Dates";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("ManualEntry"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
           
            SessionUserType = Session["Isadmin"].ToString();
             Division = Request.QueryString["Division"].ToString();
           
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            string SessionUserType_1 = SessionUserType;
            if (SessionUserType == "2")
            {
                Vehicle_Between_Dates();
            }
            else if (SessionUserType_1 == SessionUserType)
            {
                Vehicle_Between_Dates();
            }
        }
    }
    protected void Vehicle_Between_Dates()
    {
        DataTable dt = new DataTable();
        SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);


        string name = dt.Rows[0]["CompName"].ToString();
        string Addres = dt.Rows[0]["Add1"].ToString();
        DataCell.Columns.Add("CompanyName");
        DataCell.Columns.Add("LocationName");
        DataCell.Columns.Add("SNo");
        DataCell.Columns.Add("Route");
        DataCell.Columns.Add("VehicleType");
        DataCell.Columns.Add("Total");
        DataCell.Columns.Add("FromDate");
        DataCell.Columns.Add("FDate");
        DataCell.Columns.Add("TDate");
        DataCell.Columns.Add("Date");

        DataTable mLocalDS = new DataTable();
        DataTable maingateDS = new DataTable();


       

        SSQL = "SELECT Route,COUNT(*) Total,Vehicletype,date_str FROM Emp_Vehicle_Entry WHERE date_str between '" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyyy") + "' and '" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyyy") + "'";
        SSQL = SSQL + " And Ccode='" + SessionCcode + "'";
        SSQL = SSQL + " And LCode='" + SessionLcode + "' GROUP BY route,date_str ORDER BY date_str ASC";
        mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);

       
        int ssno = 1;
        for (int iRow = 0; iRow < mLocalDS.Rows.Count; iRow++)
        {

            DataCell.NewRow();
            DataCell.Rows.Add();

            DataCell.Rows[iRow]["CompanyName"] = name;
            DataCell.Rows[iRow]["LocationName"] = Addres;

            DataCell.Rows[iRow]["SNo"] = ssno;
            DataCell.Rows[iRow]["Route"] = mLocalDS.Rows[iRow]["Route"].ToString();
            DataCell.Rows[iRow]["VehicleType"] = mLocalDS.Rows[iRow]["Vehicletype"].ToString();
            DataCell.Rows[iRow]["Total"] = mLocalDS.Rows[iRow]["Total"].ToString();
            DataCell.Rows[iRow]["FromDate"] = mLocalDS.Rows[iRow]["date_str"].ToString();
            DataCell.Rows[iRow]["Date"] = FromDate;
            DataCell.Rows[iRow]["FDate"] = FromDate;
            DataCell.Rows[iRow]["TDate"] = ToDate;
          


            ssno = ssno + 1;

        }

             ds.Tables.Add(DataCell);
           

            report.Load(Server.MapPath("crystal/EmployeeVehicleReport.rpt"));
            report.SetDataSource(ds.Tables[0]);
           

            // report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
       
    }
    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }

}