﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Text;
using System.Security.Cryptography;
using System.IO;

public partial class MstTimeDelete : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();


        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Time Delete";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");


            Load_Data_EmpDet();
            Initial_Data_Referesh();
            Initial_Data_Referesh1();
        }
       
        Load_OLD_data();
        Load_OLD_data1();
    }

    private void Load_Data_EmpDet()
    {
        string query = "";
        DataTable DT = new DataTable();

        //Repeater1.DataSource = DT;
        //Repeater1.DataBind();

        txtMachineID.Items.Clear();
        query = "Select Cast(EmpNo as char(10)) as EmpNo from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And IsActive='Yes'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        txtMachineID.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["EmpNo"] = "-Select-";
        dr["EmpNo"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtMachineID.DataTextField = "EmpNo";
        txtMachineID.DataValueField = "EmpNo";
        txtMachineID.DataBind();
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("MachineID", typeof(string)));
        dt.Columns.Add(new DataColumn("TimeIN", typeof(string)));
        
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        
        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSource;
    }

    private void Initial_Data_Referesh1()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("MachineID", typeof(string)));
        dt.Columns.Add(new DataColumn("TimeOUT", typeof(string)));

       
        Repeater2.DataSource = dt;
        Repeater2.DataBind();

        ViewState["ItemTable1"] = Repeater2.DataSource;

        //dt = Repeater1.DataSource;
    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
       
    }

    private void Load_OLD_data1()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable1"];

        Repeater2.DataSource = dt;
        Repeater2.DataBind();
    }

    protected void btnTimeOUTView_Click(object sender, EventArgs e)
    {
        LogTimeOUTFunction();
    }

    protected void btnTimeINView_Click(object sender, EventArgs e)
    {
        LogTimeINFunction();
    }

    public void LogTimeINFunction()
    {
        DataTable AutoDataTable = new DataTable();
        bool ErrFlag = false;
        if (txtMachineID.SelectedItem.Text=="-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Your MachineID.');", true);

            ErrFlag = true;
        }
        else
        {
            DataTable dtLogTimeIN = new DataTable();
            string s = txtMachineID.SelectedItem.Text;
            //string[] delimiters = new string[] { "-->" };
            //string[] items = s.Split(delimiters, StringSplitOptions.None);
            //ss = items[0].Trim();
            //ss1 = items[1].Trim();

            string Machine_ID_Encrypt = UTF8Encryption(txtMachineID.SelectedItem.Text.Trim());
            DateTime AtteDate;
            string attdate = txtTimeINDate.Text;
            AtteDate = Convert.ToDateTime(attdate.ToString());

            AutoDataTable.Columns.Add("MachineID");
            AutoDataTable.Columns.Add("TimeIN");

            SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Encrypt + "'";
            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And TimeIN >='" + AtteDate.AddDays(0).ToString("yyyy/MM/dd") + " " + "00:01" + "'";
            SSQL = SSQL + " And TimeIN <='" + AtteDate.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:59" + "'";
            SSQL = SSQL + " Order by TimeIN ASC";

            dtLogTimeIN = objdata.RptEmployeeMultipleDetails(SSQL);

            if (dtLogTimeIN.Rows.Count <= 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Found In IN Time.');", true);

                Repeater1.DataSource = AutoDataTable;
                Repeater1.DataBind();
                ViewState["ItemTable"] = AutoDataTable;
                ErrFlag = true;
            }
            else
            {
                Int32 DSVAL = 0;
                for (int i = 0; i < dtLogTimeIN.Rows.Count; i++)
                {
                    AutoDataTable.NewRow();
                    AutoDataTable.Rows.Add();
                    AutoDataTable.Rows[DSVAL][0] = txtMachineID.SelectedItem.Text; ;
                    AutoDataTable.Rows[DSVAL][1] = dtLogTimeIN.Rows[i]["TimeIN"].ToString();

                    DSVAL += 1;
                }
                Repeater1.DataSource = AutoDataTable;
                Repeater1.DataBind();
                ViewState["ItemTable"] = AutoDataTable;
            }
        }
    }

    public void LogTimeOUTFunction()
    {
        DataTable AutoDataTableNew = new DataTable();
        bool ErrFlag = false;
        if (txtMachineID.SelectedItem.Text == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Your MachineID.');", true);
            ErrFlag = true;
        }
        else
        {

            DataTable dtLogTimeIN = new DataTable();
            //string s = ddlTicketNo.SelectedItem.Text;
            //string[] delimiters = new string[] { "-->" };
            //string[] items = s.Split(delimiters, StringSplitOptions.None);
            //ss = items[0].Trim();
            //ss1 = items[1].Trim();

            string Machine_ID_Encrypt = UTF8Encryption(txtMachineID.SelectedItem.Text.Trim());

            string attdate = txtTimeOUTDate.Text;
            DateTime AtteDate = Convert.ToDateTime(attdate.ToString());

            AutoDataTableNew.Columns.Add("MachineID");
           
            AutoDataTableNew.Columns.Add("TimeOUT");
            DataTable dtLogTimeOUT = new DataTable();

            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Encrypt + "'";
            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And TimeOUT >='" + AtteDate.AddDays(0).ToString("yyyy/MM/dd") + " " + "00:01" + "'";
            SSQL = SSQL + " And TimeOUT <='" + AtteDate.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:59" + "'";
            SSQL = SSQL + " Order by TimeOUT ASC";

            dtLogTimeOUT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (dtLogTimeOUT.Rows.Count <= 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Found In OUT Time.');", true);
                Repeater2.DataSource = AutoDataTableNew;
                Repeater2.DataBind();
                ViewState["ItemTable1"] = AutoDataTableNew;
                ErrFlag = true;
            }
            else
            {
                Int32 DSVAL = 0;
                for (int i = 0; i < dtLogTimeOUT.Rows.Count; i++)
                {
                    AutoDataTableNew.NewRow();
                    AutoDataTableNew.Rows.Add();
                    AutoDataTableNew.Rows[DSVAL][0] = txtMachineID.SelectedItem.Text;
                    AutoDataTableNew.Rows[DSVAL][1] = dtLogTimeOUT.Rows[i]["TimeOUT"].ToString();

                    DSVAL += 1;
                }
                Repeater2.DataSource = AutoDataTableNew;
                Repeater2.DataBind();
                ViewState["ItemTable1"] = AutoDataTableNew;
            }

        }

    }

    protected void txtMachineID_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query;
        DataTable DT = new DataTable();

        query = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And EmpNo='" + txtMachineID.SelectedItem.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            txtEmpName.Text = DT.Rows[0]["FirstName"].ToString();
            txtDepartment.Text = DT.Rows[0]["DeptName"].ToString();
        }
        else
        {
            txtMachineID.SelectedValue = "-Select-";
            txtEmpName.Text = "";
            txtDepartment.Text = "";
        }
    }

    private static string UTF8Encryption_OLD(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }

    public string UTF8Encryption(string mvarPlanText)
    {
        string cipherText = "";
        try
        {
            string passPhrase = "Altius";
            string saltValue = "info@altius.co.in";
            string hashAlgorithm = "SHA1";
            string initVector = "@1B2c3D4e5F6g7H8";
            int passwordIterations = 2;
            int keySize = 256;
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(mvarPlanText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);
            byte[] keyBytes = password.GetBytes(keySize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            cipherText = Convert.ToBase64String(cipherTextBytes);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

        return cipherText;
    }

    private static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = string.Empty;
        UTF8Encoding encodepwd = new UTF8Encoding();
        Decoder Decode = encodepwd.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        decryptpwd = new String(decoded_char);
        return decryptpwd;
    }

    protected void GridTimeINClick(object sender, CommandEventArgs e)
    {
        DataTable DT = new DataTable();

        SSQL = "Select TimeIN from LogTime_IN where MachineID='" + UTF8Encryption(e.CommandName.ToString().Trim()) + "'";
        SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And TimeIN ='" + Convert.ToDateTime(e.CommandArgument).ToString("yyyy/MM/dd HH:mm:ss") + "'";
        SSQL = SSQL + " Order by TimeIN ASC";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT.Rows.Count != 0)
        {
            SSQL = "Delete from LogTime_IN where MachineID='" + UTF8Encryption(e.CommandName.ToString().Trim()) + "'";
            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And TimeIN ='" + Convert.ToDateTime(e.CommandArgument).ToString("yyyy/MM/dd HH:mm:ss") + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('TimeIN Deleted Successfully.');", true);

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('TimeIN Not found.');", true);
        }
        LogTimeINFunction();
    }

    protected void GridTimeOUTClick(object sender, CommandEventArgs e)
    {
        DataTable DT = new DataTable();

      
        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(e.CommandName.ToString().Trim()) + "'";
        SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And TimeOUT ='" + Convert.ToDateTime(e.CommandArgument).ToString("yyyy/MM/dd HH:mm:ss") + "'";
        SSQL = SSQL + " Order by TimeOUT ASC";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT.Rows.Count != 0)
        {
            SSQL = "Delete from LogTime_OUT where MachineID='" + UTF8Encryption(e.CommandName.ToString().Trim()) + "'";
            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And TimeOUT ='" + Convert.ToDateTime(e.CommandArgument).ToString("yyyy/MM/dd HH:mm:ss") + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('TimeOUT Deleted Successfully.');", true);

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('TimeOUT Not found.');", true);
        }
        LogTimeOUTFunction();
    }
}
