﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;

public partial class MigrantReport : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Date = "";
    string SSQL;
    //string mvarUserType = "IF User";
    string mvarUserType;
    DataTable mDataSet = new DataTable();
    DataTable dt = new DataTable();
    BALDataAccess objdata = new BALDataAccess();
    DataTable AutoDataTable = new DataTable();
    string iEmpDet = "";
    //char[] delimiters = new char[] { "-",">" };

    string[] delimiters = { "-", ">" };

    string mUnitName = "";
    string strPhotoPath = "";
    FileStream fs;
    BinaryReader br;
    DataRow drow;
    string Empcode;
    DataSet ds = new DataSet();
    string Division;
    string FInancialYear;
    string FromDate;
    string ToDate;
    string Year;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Migrant Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("SalaryProcess"));
                //li.Attributes.Add("class", "droplink active open");
            }

            Date = Convert.ToDateTime(DateTime.Now.AddDays(-1)).ToString();
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            //FromDate = "2021/03/01";
            //ToDate = "2021/03/31";

            //Year = Request.QueryString["Year"].ToString();
            FromDate = DateTime.Parse(FromDate).ToString("yyyy/MM/dd");
            ToDate = DateTime.Parse(ToDate).ToString("yyyy/MM/dd");

            if (SessionUserType == "2")
            {
                GetMigrantTable();

            }
            else
            {
                GetMigrantTable();

            }


            ds.Tables.Add(AutoDataTable);
            if (AutoDataTable.Rows.Count > 0)
            {
                ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("crystal/MigrantReport.rpt"));

                report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                //if (Division != "-Select-")
                //{
                //    report.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division:" + "'" + "'" + Division + "'";
                //}
                //else
                //{
                //    report.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division:" + "'";
                //}
                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = report;
            }
            else
            {
                Response.Write("<script>alert('No Records Found!!!')</Script>");
            }

        }
    }
    public void GetMigrantTable()
    {
        AutoDataTable.Columns.Add("Sno");
        AutoDataTable.Columns.Add("CompanyName");
        AutoDataTable.Columns.Add("LocationName");
        AutoDataTable.Columns.Add("EmployeeName");
        AutoDataTable.Columns.Add("ExistingCode");
        AutoDataTable.Columns.Add("Designation");
        AutoDataTable.Columns.Add("Plant");
        //AutoDataTable.Columns.Add("LocationName");
        AutoDataTable.Columns.Add("Gender");
        AutoDataTable.Columns.Add("DOJ");
        AutoDataTable.Columns.Add("DOB");
        AutoDataTable.Columns.Add("State");
        AutoDataTable.Columns.Add("Mobile");
        AutoDataTable.Columns.Add("Aadhar");
        AutoDataTable.Columns.Add("Contract");
        AutoDataTable.Columns.Add("Company");
        AutoDataTable.Columns.Add("Address");
        AutoDataTable.Columns.Add("FromDate");
        AutoDataTable.Columns.Add("ToDate");

        SSQL = "SELECT Firstname,ExistingCode,Designation,Plant,Gender,DOJ,BirthDate,StateName,EmployeeMobile,Adhar_No,Contract FROM employee_mst  ";
        SSQL = SSQL + " WHERE DOJ Between '" + FromDate + "' AND '" + ToDate + "' And compCode='" + SessionCcode.ToString() + "' and loccode='" + SessionLcode.ToString() + "' AND StateName!='TAMILNADU' AND deptname!='SECURITY'";
        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
        SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        string name = dt.Rows[0]["CompName"].ToString();
        string Addres = dt.Rows[0]["Add1"].ToString();
        int sno = 1;
        if (mDataSet.Rows.Count > 0)
        {

            for (int iRow = 0; iRow < mDataSet.Rows.Count; iRow++)
            {

                AutoDataTable.NewRow();
                AutoDataTable.Rows.Add();


                AutoDataTable.Rows[iRow]["Sno"] = sno;
                AutoDataTable.Rows[iRow]["CompanyName"] = SessionCcode;
                AutoDataTable.Rows[iRow]["LocationName"] = SessionLcode;
                AutoDataTable.Rows[iRow]["EmployeeName"] = mDataSet.Rows[iRow]["Firstname"].ToString();
                AutoDataTable.Rows[iRow]["ExistingCode"] = mDataSet.Rows[iRow]["ExistingCode"].ToString();
                AutoDataTable.Rows[iRow]["Designation"] = mDataSet.Rows[iRow]["Designation"].ToString();
                AutoDataTable.Rows[iRow]["Plant"] = mDataSet.Rows[iRow]["Plant"].ToString();
                AutoDataTable.Rows[iRow]["Gender"] = mDataSet.Rows[iRow]["Gender"].ToString();
                AutoDataTable.Rows[iRow]["DOJ"] = Convert.ToDateTime(mDataSet.Rows[iRow]["DOJ"]).ToString("dd/MM/yyyy");
                AutoDataTable.Rows[iRow]["DOB"] = Convert.ToDateTime(mDataSet.Rows[iRow]["BirthDate"]).ToString("dd/MM/yyyy");
                AutoDataTable.Rows[iRow]["State"] = mDataSet.Rows[iRow]["StateName"].ToString();
                AutoDataTable.Rows[iRow]["Mobile"] = mDataSet.Rows[iRow]["EmployeeMobile"].ToString();
                AutoDataTable.Rows[iRow]["Aadhar"] = mDataSet.Rows[iRow]["Adhar_No"].ToString();
                AutoDataTable.Rows[iRow]["Contract"] = mDataSet.Rows[iRow]["Contract"].ToString();
                AutoDataTable.Rows[iRow]["Company"] = name.ToString();
                AutoDataTable.Rows[iRow]["Address"] = Addres.ToString();
                AutoDataTable.Rows[iRow]["FromDate"] = DateTime.Parse(FromDate).ToString("dd/MM/yyyy");
                AutoDataTable.Rows[iRow]["ToDate"] = DateTime.Parse(ToDate).ToString("dd/MM/yyyy");


                sno += 1;

            }
        }



    }
    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }
}