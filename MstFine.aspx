﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="MstFine.aspx.cs" Inherits="MstFine" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Master</a></li>
				<li class="active">Fine Master</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Fine Master</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Fine Master</h4>
                        </div>
                        <div class="panel-body">
                       
                      <div class="row">
                       <div class="col-md-3">
								<div class="form-group">
								  <label>Improper Days</label>
								  <asp:TextBox runat="server" ID="txtImproperDays" class="form-control" MaxLength="10"></asp:TextBox>
								  <asp:RequiredFieldValidator ControlToValidate="txtImproperDays" Display="Dynamic"   ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                                 <div class="col-md-3">
								<div class="form-group">
								  <label>Improper Amount</label>
								  <asp:TextBox runat="server" ID="txtImproperAmt" class="form-control" MaxLength="10"></asp:TextBox>
								  <asp:RequiredFieldValidator ControlToValidate="txtImproperAmt" Display="Dynamic"   ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                               
                               <div class="col-md-3">
								<div class="form-group">
								  <label>LateIn Days</label>
								  <asp:TextBox runat="server" ID="txtLateINDays" class="form-control" MaxLength="10"></asp:TextBox>
								  <asp:RequiredFieldValidator ControlToValidate="txtLateINDays" Display="Dynamic"   ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                                 <div class="col-md-3">
								<div class="form-group">
								  <label>LateIn Amount</label>
								  <asp:TextBox runat="server" ID="txtLateINAmt" class="form-control" MaxLength="10"></asp:TextBox>
								  <asp:RequiredFieldValidator ControlToValidate="txtLateINAmt" Display="Dynamic"   ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                      </div>
                      
                       <div class="row">
                     
                    
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-3">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                         ValidationGroup="Validate_Field" onclick="btnSave_Click"   />
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" onclick="btnClear_Click" 
                                         />
								 </div>
                               </div>
                              <!-- end col-4 -->
                             
                         </div>
                        <!-- end row -->
                        
                    
                        
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
   
<!-- end #content -->




</asp:Content>

