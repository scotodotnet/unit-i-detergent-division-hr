﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstStdManhours : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Standard Manhours Master";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            load_Shift();
        }
       Load_Data();
    }
    private void Load_Data()
    {
        SSQL = "";
        SSQL = "Select * from MstStdManhrs where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        DataTable dt_ = new DataTable();
        dt_ = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataSource = dt_;
        Repeater1.DataBind();
    }
    private void load_Shift()
    {
        SSQL = "";
        SSQL = "Select * from Shift_mst where Compcode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        ddlShift.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlShift.DataTextField = "ShiftDesc";
        ddlShift.DataValueField = "ShiftDesc";
        ddlShift.DataBind();
        ddlShift.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }
    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstStdManhrs where id='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            ddlShift.SelectedItem.Text = DT.Rows[0]["Shift"].ToString();
            ddlPlant.SelectedItem.Text = DT.Rows[0]["Plant"].ToString();
            txtStd.Text = DT.Rows[0]["Std"].ToString();
            txtManhrs.Text = DT.Rows[0]["Manhrs"].ToString();
            txtdate.Text = DT.Rows[0]["Date"].ToString();
            txtid.Value = DT.Rows[0]["id"].ToString();



            btnSave.Text = "Update";
        }
        else
        {
            ddlShift.SelectedItem.Text = "";
            txtStd.Text = "";
            txtManhrs.Text = "";
           // chkDefault.Checked = false;
        }

    }

    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstStdManhrs where id='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            query = "Delete from MstStdManhrs where id='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Record Deleted Successfully');", true);

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Bank Not Found');", true);
        }
        load_Shift();
        Load_Data();
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SaveMode = "Insert";
        string query = "";
        DataTable DT = new DataTable();
        string Def_Chk = "";
        bool ErrFlag = false;

       

       
        if (!ErrFlag)
        {
            query = "select * from MstStdManhrs where id='" + txtid.Value + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);

            if (DT.Rows.Count > 0)
            {
                SaveMode = "Update";
                query = "delete from MstStdManhrs where id='" + txtid.Value + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }
            else
            {
                SaveMode = "Insert";
            }

            query = "Insert into MstStdManhrs (Shift,Std,Manhrs,Date,Ccode,Lcode,Plant)";
            query = query + "values('" + ddlShift.SelectedItem.Text + "','" + txtStd.Text + "','" + txtManhrs.Text + "','" + txtdate.Text + "','" + SessionCcode + "','" + SessionLcode + "','" + ddlPlant.SelectedItem.Text + "')";
            objdata.RptEmployeeMultipleDetails(query);

            if (SaveMode == "Update")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Updated Successfully...!');", true);
            }
            else if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Saved Successfully...!');", true);
            }
            Clear_All_Field();
        }
        load_Shift();
        Load_Data();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        ddlShift.SelectedItem.Text = "";
        ddlPlant.SelectedItem.Text = "";
        txtStd.Text = "";
        txtManhrs.Text = "";
        //chkDefault.Checked = false;
        btnSave.Text = "Save";
        txtdate.Text = "";
        txtid.Value = "";
    }
}