﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.Globalization;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
public partial class SpinningIncentiveShiftWise : System.Web.UI.Page
{
    string WagesType = "";
    string FromDate = "";
    string ToDate = "";
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string ss = "";
    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();


    DateTime date1 = new DateTime();
    DateTime Date2 = new DateTime();
    string Date_Value_Str;
    string Date_value_str1;
    string SSQL = "";

    DataSet ds = new DataSet();

    string Division = "";
    DataTable AutoDTable = new DataTable();
    DataTable DataCell = new DataTable();
    DataTable dt = new DataTable();

    System.Web.UI.WebControls.DataGrid grid =
                         new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-Spinning Incentive Shift Wise";
               
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
           
            SessionUserType = Session["Isadmin"].ToString();
            Division = Request.QueryString["Division"].ToString();
            string TempWages = Request.QueryString["Wages"].ToString();
            WagesType = TempWages.Replace("_", "&");
         
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();


            AdminSpinningDays();

        }
    }
    public void AdminSpinningDays()
    {
        //DataCell.Columns.Add("CompanyName");
        //DataCell.Columns.Add("Location");
        //DataCell.Columns.Add("MonthName");
        //DataCell.Columns.Add("Get_Year");
        //DataCell.Columns.Add("Wages_Type");
        DataCell.Columns.Add("SNo");
        DataCell.Columns.Add("ExistingCode");
        DataCell.Columns.Add("EmpName");
        DataCell.Columns.Add("Department");
        DataCell.Columns.Add("Designation");
        DataCell.Columns.Add("DOJ");
        //if (WagesType == "REGULAR")
        //{
        //    DataCell.Columns.Add("Shif Type");
        //}
        DataCell.Columns.Add("PerDay_Amt");
        DataCell.Columns.Add("Shift I");
        DataCell.Columns.Add("Shift II");
        DataCell.Columns.Add("Shift III");
       
        DataCell.Columns.Add("Total");

        date1 = Convert.ToDateTime(FromDate);
        string dat = ToDate;
        Date2 = Convert.ToDateTime(dat);

        DateTime Fdayy = Convert.ToDateTime(date1.AddDays(0).ToShortDateString());
        DateTime TDay = Convert.ToDateTime(Date2.AddDays(0).ToShortDateString());
        Date_Value_Str = Fdayy.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture);
        Date_value_str1 = TDay.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture);
        DataTable mDataSet = new DataTable();

        string Month = Fdayy.ToString("MMMM");

            decimal shiftcount1 = 0;
            decimal shiftcount2 = 0;
            decimal shiftcount3 = 0;
            DataTable Empdet = new DataTable();
            DataTable dt1 = new DataTable();
            DataTable DT = new DataTable();

                 SSQL=" Select ExistingCode,EmpNo,FirstName,DeptName,Designation, DOJ,";
          //if (WagesType.ToUpper() == "REGULAR".ToUpper())
          //        {
          //            SSQL = SSQL + " Shift_Rotation_Type,";
          //        }

                 SSQL = SSQL + "COALESCE(SHIFT1,0)as SHIFT1,COALESCE(SHIFT2,0) as SHIFT2 ,COALESCE(SHIFT3,0) as SHIFT3 ";
                 SSQL = SSQL + " from (Select EM.ExistingCode,EM.EmpNo,EM.FirstName,EM.DeptName,EM.Designation,";
                 SSQL = SSQL + " Convert(varchar,EM.DOJ,105) as DOJ,";
                  //if (WagesType.ToUpper() == "REGULAR".ToUpper())
                  //{
                  //    SSQL = SSQL + " EM.Shift_Rotation_Type,";
                  //}
                   SSQL = SSQL + " LT.Shift,sum(convert(decimal(18,2),Present)) as PresentDays";
                  SSQL = SSQL + " from LogTime_Days LT inner join Employee_Mst EM ";
                   SSQL = SSQL + " on LT.ExistingCode=EM.ExistingCode";

                   SSQL = SSQL + " Where LT.CompCode='" + SessionCcode + "' And LT.LocCode='" + SessionLcode + "' and LT.Wages='" + WagesType + "'";
                  SSQL = SSQL + " And EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "' and EM.Wages='"+WagesType +"'";
                  SSQL = SSQL + " And CONVERT(DATETIME,LT.Attn_Date_Str, 103)>=CONVERT(DATETIME,'"+FromDate+"',103)";
                  SSQL = SSQL + " And CONVERT(DATETIME,LT.Attn_Date_Str, 103)<=CONVERT(DATETIME,'"+ToDate+"',103)";
                  if (Division != "-Select-")
                  {
                      SSQL = SSQL + " And EM.Division = '" + Division + "'";
                  }
                  SSQL = SSQL + " group by EM.ExistingCode,EM.EmpNo,EM.FirstName,EM.DeptName,EM.Designation,EM.DOJ,LT.Shift";
                   //if (WagesType.ToUpper() == "REGULAR".ToUpper())
                   //{
                   //    SSQL = SSQL + " ,EM.Shift_Rotation_Type";
                   //}
                   SSQL = SSQL + ")src pivot(";
                   SSQL = SSQL + "max(PresentDays) for Shift in ([SHIFT1], [SHIFT2], [SHIFT3])) as MaxBookingDays ";

            Empdet = objdata.RptEmployeeMultipleDetails(SSQL);

           

            if(Empdet.Rows.Count !=0)
            {
                for (int k = 0; k < Empdet.Rows.Count; k++)
                {
                    decimal Spinning_Incentive_Days = 0;
                    decimal shift1 = 0;
                    decimal shift2 = 0;
                    decimal shift3 = 0;
                    shift1 = Convert.ToDecimal(Empdet.Rows[k]["SHIFT1"].ToString());
                    shift2 = Convert.ToDecimal(Empdet.Rows[k]["SHIFT2"].ToString());
                    shift3 = Convert.ToDecimal(Empdet.Rows[k]["SHIFT3"].ToString());
                    if (WagesType.ToUpper() == "REGULAR".ToUpper())
                    {
                        
                        //Regular Condition Checking
                        string Minimum_Days = "0";
                        SSQL = "Select * from Department_Inc_Mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Wages='REGULAR'";
                        DataTable DT_SH = new DataTable();
                        DT_SH = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (DT_SH.Rows.Count != 0) { Minimum_Days = DT_SH.Rows[0]["Min_Days"].ToString(); }
                        if (Convert.ToDecimal(shift3.ToString()) != 0)
                        {
                            if (Convert.ToDecimal(Minimum_Days) <= Convert.ToDecimal(shift3.ToString()))
                            {
                                Spinning_Incentive_Days = shift2 + shift3;
                            }
                            else
                            {
                                //Date of Joining Check
                                string query_doj = "";
                                bool DOJ_Check_True_or_False = false;
                                DataTable DT_Q_DOJ = new DataTable();
                                query_doj = "Select Convert(varchar(20),DOJ,103) as DOJ from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ExistingCode='" + Empdet.Rows[k]["ExistingCode"].ToString() + "'";
                                query_doj = query_doj + " And DATENAME(MM,DOJ)=DATENAME(MM,CONVERT(DATETIME,'" + FromDate + "',103)) And YEAR(DOJ)=Year(CONVERT(DATETIME,'" + FromDate + "',103))";
                                DT_Q_DOJ = objdata.RptEmployeeMultipleDetails(query_doj);
                                if (DT_Q_DOJ.Rows.Count != 0)
                                {
                                    string DOJ_Date_Str_Checking = DT_Q_DOJ.Rows[0]["DOJ"].ToString();
                                    string[] DOJ_Sshift_Inc = DOJ_Date_Str_Checking.Split('/');
                                    if (DOJ_Sshift_Inc[0].ToString() == "01")
                                    {
                                        DOJ_Check_True_or_False = false;
                                    }
                                    else
                                    {
                                        DOJ_Check_True_or_False = true;
                                    }
                                }
                                else
                                {
                                    DOJ_Check_True_or_False = false;
                                }
                                if (DOJ_Check_True_or_False == true)
                                {
                                    if (Convert.ToDecimal(shift3.ToString()) != 0)
                                    {
                                        Spinning_Incentive_Days = shift2 + shift3;
                                    }
                                    else
                                    {
                                        Spinning_Incentive_Days = 0;
                                    }
                                }
                                else
                                {
                                    Spinning_Incentive_Days = 0;
                                }
                            }
                        }
                        else
                        {
                            Spinning_Incentive_Days = 0;
                        }
                    }
                    else if (WagesType.ToUpper() == "HOSTEL".ToUpper())
                    {
                        Spinning_Incentive_Days = shift1 + shift2 + shift3;
                    }

                    //perDay Salary

                    SSQL = "Select * from Incentive_Mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Wages='" + WagesType  + "'";
                    SSQL = SSQL + " and DeptName='" + Empdet.Rows[k]["DeptName"].ToString() + "' and Designation='" + Empdet.Rows[k]["Designation"].ToString() + "'";
                    DT = objdata.RptEmployeeMultipleDetails(SSQL);


                    DataCell.NewRow();
                    DataCell.Rows.Add();
                    DataCell.Rows[k]["SNo"] = k + 1;
                    DataCell.Rows[k]["ExistingCode"] = Empdet.Rows[k]["ExistingCode"];
                    DataCell.Rows[k]["EmpName"] = Empdet.Rows[k]["FirstName"];
                    DataCell.Rows[k]["Department"] = Empdet.Rows[k]["DeptName"];
                    DataCell.Rows[k]["Designation"] = Empdet.Rows[k]["Designation"];
                    DataCell.Rows[k]["DOJ"] = Empdet.Rows[k]["DOJ"];
                    //if (WagesType == "REGULAR")
                    //{
                    //    DataCell.Rows[k]["Shif Type"] = shiftType;
                       
                    //}
                    DataCell.Rows[k]["Shift I"] = Empdet.Rows[k]["SHIFT1"];
                    DataCell.Rows[k]["Shift II"] = Empdet.Rows[k]["SHIFT2"];
                    DataCell.Rows[k]["Shift III"] = Empdet.Rows[k]["SHIFT3"];
                    if (DT.Rows.Count!= 0)
                    {
                        DataCell.Rows[k]["PerDay_Amt"] = DT.Rows[0]["IncAmt"];
                    }
                    else
                    {
                        DataCell.Rows[k]["PerDay_Amt"] = "0";
                    }
                    DataCell.Rows[k]["Total"] = Spinning_Incentive_Days;
         
           
            }
                 SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            string name = dt.Rows[0]["CompName"].ToString();
                grid.DataSource = DataCell;
                grid.DataBind();
                string attachment = "attachment;filename=SPINNING INCENTIVE SHIFT WISE.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                grid.HeaderStyle.Font.Bold = true;
                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                grid.RenderControl(htextw);
                Response.Write("<table>");

                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='8'>");
                Response.Write("<a style=\"font-weight:bold\">" + name + "</a>");


                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='8'>");
                Response.Write("<a style=\"font-weight:bold\">" + SessionLcode  + "</a>");


                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='8'>");
                Response.Write("<a style=\"font-weight:bold\">" + WagesType + "-  SPINNING INCENTIVE SHIFT WISE - " + Month + "-" + Fdayy.Year + "</a>");


                Response.Write("</td>");
                Response.Write("</tr>");


                Response.Write("</table>");


                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();
        }

     
        else
        {
            lblReport.Text = "No Records Matched..";
        }
    }
}
