﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Documentprocess : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;

    string SSQL;
    BALDataAccess objdata = new BALDataAccess();
    String CurrentYear1;
    static int CurrentYear;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Fine Report";

                Load_Location();
              
            }
        }
    }
    public void Load_Location()
    {
        string SSQL = "";
        DataTable dtempty = new DataTable();
        ddlunit.DataSource = dtempty;
        ddlunit.DataBind();
        DataTable dt = new DataTable();
        SSQL = "Select LocCode as LCode from Location_Mst where CompCode='" + SessionCcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlunit.DataSource = dt;
        ddlunit.DataTextField = "LCode";
        ddlunit.DataValueField = "LCode";
        ddlunit.DataBind();
    }

    protected void btnReport_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        SSQL = "Select EmpNo,Adhar_No,Voter_ID,DL_No,Passport_No,Ration_Card,PanCard,Smart_Card ,Other_Card_No ";
        SSQL=SSQL + " from Employee_Mst where LocCode='"+ddlunit.SelectedItem.Text +"' ";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count != 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string doctype = "";
                string docnumber = "";
                string adharcode="Adhar Card";
                string votercode="Voter Card";
                string rationcode="Ration Card";
                string pancode="Pan Card";
                string DrivingCard="Driving Licence";
                string Smartcard="Smart Card";
                string Bankpass="Bank Pass Book";
                string passport="Passport";
                 string others="Others";
               
                for(int k=0;k<9;k++)
                {

                    if (k == 0)
                    {
                        doctype = "Adhar Card";
                        docnumber = dt.Rows[i]["Adhar_No"].ToString();
                    }
                    else if (k == 1)
                    {
                        doctype = "Voter Card";
                        docnumber = dt.Rows[i]["Voter_ID"].ToString();
                    }
                    else if (k == 2)
                    {
                        doctype = "Ration Card";
                        docnumber = dt.Rows[i]["Ration_Card"].ToString();
                    }
                    else if (k == 3)
                    {
                        doctype = "Pan Card";
                        docnumber = dt.Rows[i]["PanCard"].ToString();
                    }
                    else if (k == 4)
                    {
                        doctype = "Driving Licence";
                        docnumber = dt.Rows[i]["DL_No"].ToString();
                    }
                    else if (k == 5)
                    {
                        doctype = "Smart Card";
                        docnumber = dt.Rows[i]["Smart_Card"].ToString();
                    }
                    else if (k == 6)
                    {
                        doctype = "Bank Pass Book";
                        docnumber = "";
                    }
                    else if (k == 7)
                    {
                        doctype = "Passport";
                        docnumber = dt.Rows[i]["Passport_No"].ToString();
                    }
                    else if (k == 8)
                    {
                        doctype = "Others";
                        docnumber = dt.Rows[i]["Other_Card_No"].ToString();
                    }

                    if (doctype != "" && docnumber != "")
                    {
                        SSQL = "Select * from Employee_Doc_Mst where CompCode='" + SessionCcode + "' and LocCode='" + ddlunit.SelectedItem.Text + "' and EmpNo='" + dt.Rows[i]["EmpNo"].ToString() + "'";
                        dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (dt1.Rows.Count != 0)
                        {
                            SSQL = "Delete From Employee_Doc_Mst where CompCode='" + SessionCcode + "' and LocCode='" + ddlunit.SelectedItem.Text + "' and EmpNo='" + dt.Rows[i]["EmpNo"].ToString() + "'";
                            objdata.RptEmployeeMultipleDetails(SSQL);
                        }

                        SSQL = " insert into Employee_Doc_Mst (CompCode,LocCode,EmpNo,DocType,DocNo,Created_By,Created_Date)";
                        SSQL = SSQL + " values('"+SessionCcode +"','"+ddlunit.SelectedItem.Text +"','"+dt.Rows[i]["EmpNo"].ToString()+"' ,";
                        SSQL = SSQL + " '" + doctype + "','" + docnumber + "','admin',CURDATE())";
                        objdata.RptEmployeeMultipleDetails(SSQL);
                    }

                }

            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('inserted Suceessfully..');", true);
        }
    }
}
