﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;


public partial class DailyMail : System.Web.UI.Page
{
    string ShiftType1 = "";
    string Date = "";
    string Status = "";

    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    // string SessionRights;
    string SessionUserType;
    string ss = "";
    ReportDocument report = new ReportDocument();
    ReportDocument report2 = new ReportDocument();
    ReportDocument report3 = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();

    string SSQL = "";
    string Adolescent_Shift;
    DataSet ds = new DataSet();

    string Division = "";
    DataTable AutoDTable = new DataTable();
    DataTable DataCell = new DataTable();
    DataTable dt = new DataTable();


    string Datestr = "";
    string Datestr1 = "";
    string ShiftType = "";
    DataTable mDataSet = new DataTable();

    DataTable mLocalDS_INTAB = new DataTable();
    DataTable mLocalDS_OUTTAB = new DataTable();
    string Date_Value_Str;
    string Time_IN_Str = "";
    string Time_Out_Str = "";
    int time_Check_dbl = 0;
    string Total_Time_get = "";

    DataRow dtRow;

    DataTable MLocal_Day = new DataTable();
    DataTable Payroll_DS = new DataTable();

    string Shift_Start_Time;
    string Shift_End_Time;
    string Employee_Time = "";
    DateTime ShiftdateStartIN = new DateTime();
    DateTime ShiftdateEndIN = new DateTime();
    DateTime EmpdateIN = new DateTime();
    string ColumnName = "";
    double Totol_Hours_Check = 0;
    double Totol_Hours_Check_1 = 8.2;
    string[] Time_Split_Str;
    string Time_IN_Get;
    string Time_Out_Update = "";
    long Random_No_Get;
    string[] Time_Minus_Value_Check;
    DataTable DataCells = new DataTable();

    MailMessage Mail = new MailMessage("SCOTO <nldvnaga.hr@gmail.com>", "sasikumar@nagamills.com,padmanabhanscoto@gmail.com");

    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            Page.Title = "Spay Module | MD Mailing Report";
        }

        Date = Convert.ToDateTime(DateTime.Now.AddDays(-1)).ToString();
        SessionCcode = "NAGA";
        SessionLcode = "UNIT I";

        GetAttdNotLeftFromPlant(DateTime.Now.ToString("yyyy-MM-dd"));

        //MailReport();
    }

    private void MailReport()
    {
        Mail.Subject = "Not Left From UNIT";
        Mail.Body = "Dear Sir, Please find the Attachement for (" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + ") NOT LEFT FROM UNIT Report";
        // Mail.CC=new 
        // string FileName = Path.GetFileName(fuAttachment.PostedFile.FileName);
        string Server_Path = Server.MapPath("~");
        string AttachfileName_Miss = "";
        //  string Invoice_No_Str = WitSalesOrdNo.Replace("/", "_");
        AttachfileName_Miss = Server_Path + "/Daily_Report/Notleft_fromunit" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy_HHmmss") + ".pdf";
        if (File.Exists(AttachfileName_Miss))
        {
            Mail.Attachments.Add(new Attachment(AttachfileName_Miss));
        }

        Mail.IsBodyHtml = false;
        SmtpClient smtp = new SmtpClient();
        smtp.Host = "smtp.gmail.com";
        smtp.EnableSsl = true;
        NetworkCredential NetworkCred = new NetworkCredential("nldvnaga.hr@gmail.com", "Altius2005");
        smtp.UseDefaultCredentials = false;
        smtp.Credentials = NetworkCred;
        smtp.Port = 587;
        smtp.Send(Mail);
        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('Email sent.');", true);
    }

    private void GetAttdNotLeftFromPlant(string Date)
    {
        DateTime CurrnDate = Convert.ToDateTime(Date);
        string str_CurrnDate = CurrnDate.ToString("yyyy/MM/dd");
        string str_CurrnDate1 = CurrnDate.AddDays(1).ToString("yyyy/MM/dd");
        //string str_CurrnDate = "2021/03/22";
        //string str_CurrnDate1 = "2021/03/22";
        string Count = "0";
        string plant_Name = "";
        string ShiftName = "";
        DataTable AutoDt = new DataTable();
        AutoDt.Columns.Add("Tkno");
        AutoDt.Columns.Add("Empname");
        AutoDt.Columns.Add("Department");
        AutoDt.Columns.Add("Designation");
        AutoDt.Columns.Add("Shift");
        AutoDt.Columns.Add("TIMEIN");
        AutoDt.Columns.Add("Plant");

        DataTable Shift_Ds1 = new DataTable();

        SSQL = "";
        SSQL = "Select * from Logtime_IN where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And DATE_FORMAT(TimeIN,'%Y/%m/%d %H:%i') >=DATE_FORMAT('" + Convert.ToDateTime(str_CurrnDate).ToString("yyyy/MM/dd") + " " + "11:00','%Y/%m/%d %H:%i') And DATE_FORMAT(TimeIN,'%Y/%m/%d %H:%i') <=DATE_FORMAT('" + Convert.ToDateTime(str_CurrnDate1).ToString("yyyy/MM/dd") + " " + "14:00','%Y/%m/%d %H:%i') Order by TimeIN ASC";
        DataTable dt_IN = new DataTable();
        dt_IN = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt_IN.Rows.Count > 0)
        {
            for (int i = 0; i < dt_IN.Rows.Count; i++)
            {
                bool OUT_Check = true;
                SSQL = "";
                SSQL = "select * from Employee_mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
                SSQL = SSQL + " and MachineID_Encrypt='" + dt_IN.Rows[i]["MachineID"] + "'";
                DataTable dt_Emp = new DataTable();
                dt_Emp = objdata.RptEmployeeMultipleDetails(SSQL);
                string Employee_Shift_Name_DB = "";

                if(dt_IN.Rows[i]["MachineID"].ToString() == ("7t1CRrIi3MKRP+XXZmNP4w==").ToString())
                {
                    string stop = "";
                }
                if (dt_Emp.Rows.Count > 0)
                {

                    SSQL = "Select * from Shift_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    if ((dt_Emp.Rows[0]["Wages"].ToString().ToUpper() == ("STAFF")) || (dt_Emp.Rows[0]["Wages"].ToString().ToUpper() == ("SECURITY").ToUpper()) || (dt_Emp.Rows[0]["Wages"].ToString().ToUpper() == ("FITTER & ELECTRICIANS").ToUpper()) || (dt_Emp.Rows[0]["Wages"].ToString().ToUpper() == ("DRIVERS").ToUpper()))
                    {
                        SSQL = SSQL + " And ShiftDesc = 'GENERAL'";
                    }
                    else
                    {
                        SSQL = SSQL + " And ShiftDesc <> 'GENERAL'";
                    }
                    SSQL = SSQL + " Order by ShiftDesc Asc";
                    Shift_Ds1 = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (Shift_Ds1.Rows.Count != 0)
                    {
                        SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'"; //And ShiftDesc like '%SHIFT%'";
                        if ((dt_Emp.Rows[0]["Wages"].ToString().ToUpper() == ("STAFF")) || (dt_Emp.Rows[0]["Wages"].ToString().ToUpper() == ("SECURITY").ToUpper()) || (dt_Emp.Rows[0]["Wages"].ToString().ToUpper() == ("FITTER & ELECTRICIANS").ToUpper()) || (dt_Emp.Rows[0]["Wages"].ToString().ToUpper() == ("DRIVERS").ToUpper()))
                        {
                            SSQL = SSQL + " And ShiftDesc = 'GENERAL'";
                        }
                        else
                        {
                            SSQL = SSQL + " And ShiftDesc like '%SHIFT%'";
                        }
                        Shift_Ds1 = objdata.RptEmployeeMultipleDetails(SSQL);
                        bool Shift_Check_blb = false, Shift_Check_blb_Check1 = false;
                        for (int K = 0; K < Shift_Ds1.Rows.Count; K++)
                        {

                            string Start_IN1 = Convert.ToDateTime(Date_Value_Str).AddDays(Convert.ToDouble(Shift_Ds1.Rows[K]["StartIN_Days"])).ToString("dd/MM/yyyy") + " " + Shift_Ds1.Rows[K]["StartIN"].ToString();
                            string End_In1 = Convert.ToDateTime(Date_Value_Str).AddDays(Convert.ToDouble(Shift_Ds1.Rows[K]["EndIN_Days"])).ToString("dd/MM/yyyy") + " " + Shift_Ds1.Rows[K]["EndIN"].ToString();

                            DateTime ShiftdateStartIN_Check1 = Convert.ToDateTime(Start_IN1);
                            DateTime ShiftdateEndIN_Check1 = Convert.ToDateTime(End_In1);
                            DateTime EmpdateIN_Check1 = Convert.ToDateTime(dt_IN.Rows[i]["TimeIN"]);
                            if (EmpdateIN_Check1 >= ShiftdateStartIN_Check1 && EmpdateIN_Check1 <= ShiftdateEndIN_Check1)
                            {
                                Employee_Shift_Name_DB = Shift_Ds1.Rows[K]["ShiftDesc"].ToString();
                                Shift_Check_blb_Check1 = true;
                                break;
                            }
                        }
                        if (Shift_Check_blb_Check1 == false)
                        {
                            Employee_Shift_Name_DB = "No Shift";
                        }
                    }
                    else
                    {
                        Employee_Shift_Name_DB = "No Shift";
                    }

                    plant_Name = "";
                    bool Check = false;
                    SSQL = "";
                    SSQL = "Select * from logtime_bar_plant where MachineID='" + dt_IN.Rows[i]["MachineID"].ToString() + "'";
                    SSQL = SSQL + " And DATE_FORMAT(Time,'%Y/%m/%d') =DATE_FORMAT('" + Convert.ToDateTime(str_CurrnDate).ToString("yyyy/MM/dd") + "','%Y/%m/%d')  Order by Time ASC LIMIT 1";
                    DataTable dt_Bar_plant = new DataTable();
                    dt_Bar_plant= objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dt_Bar_plant.Rows.Count > 0)
                    {
                        //if (dt_Bar_plant.Rows.Count % 2 == 1)
                        //{
                        plant_Name = "BAR";
                        Check = true;
                        //}
                    }

                    SSQL = "";
                    SSQL = "Select * from logtime_tsp_plant where MachineID='" + dt_IN.Rows[i]["MachineID"].ToString() + "'";
                    SSQL = SSQL + " And DATE_FORMAT(Time,'%Y/%m/%d') =DATE_FORMAT('" + Convert.ToDateTime(str_CurrnDate).ToString("yyyy/MM/dd") + "','%Y/%m/%d')  Order by Time ASC LIMIT 1";
                    DataTable dt_TSP_plant = new DataTable();
                    dt_TSP_plant = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dt_TSP_plant.Rows.Count > 0)
                    {
                        //if (dt_Bar_plant.Rows.Count % 2 == 1)
                        //{
                        plant_Name = "TSP";
                        Check = true;
                        // }
                    }

                    SSQL = "";
                    SSQL = "Select * from logtime_powder_plant where MachineID='" + dt_IN.Rows[i]["MachineID"].ToString() + "'";
                    SSQL = SSQL + " And DATE_FORMAT(Time,'%Y/%m/%d') =DATE_FORMAT('" + Convert.ToDateTime(str_CurrnDate).ToString("yyyy/MM/dd") + "','%Y/%m/%d') Order by Time ASC LIMIT 1";
                    DataTable dt_Powder_plant = new DataTable();
                    dt_Powder_plant = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dt_Powder_plant.Rows.Count > 0)
                    {
                        //if (dt_Bar_plant.Rows.Count % 2 == 1)
                        //{
                        plant_Name = "POWDER";
                        Check = true;
                        //}
                    }
                    if (Employee_Shift_Name_DB != "No Shift")
                    {
                        string str_START = "";
                        string str_END = "";
                        int srt_days_START = 0;
                        int srt_days_END = 0;
                        if (Employee_Shift_Name_DB == "SHIFT1")
                        {
                            srt_days_START = 0;
                            str_START = "13:30";
                            str_END = "14:30";
                            srt_days_END = 0;
                        }
                        else if (Employee_Shift_Name_DB == "SHIFT2")
                        {
                            srt_days_START = 0;
                            str_START = "21:30";
                            str_END = "22:30";
                            srt_days_END = 0;
                        }
                        else if (Employee_Shift_Name_DB == "SHIFT3")
                        {
                            srt_days_START = 1;
                            str_START = "05:30";
                            str_END = "06:30";
                            srt_days_END = 1;
                        }
                        else if (Employee_Shift_Name_DB == "SHIFT4")
                        {
                            srt_days_START = 0;
                            str_START = "16:30";
                            str_END = "17:30";
                            srt_days_END = 0;
                        }

                        SSQL = "";
                        SSQL = "Select * from logtime_OUT where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " and MachineID='" + dt_IN.Rows[i]["MachineID"] + "'";
                        SSQL = SSQL + " And DATE_FORMAT(TimeIN,'%Y/%m/%d %H:%i') >=DATE_FORMAT('" + Convert.ToDateTime(str_CurrnDate).AddDays(srt_days_START).ToString("yyyy/MM/dd") + " " + str_START + "','%Y/%m/%d %H:%i') And DATE_FORMAT(TimeIN,'%Y/%m/%d %H:%i') <=DATE_FORMAT('" + Convert.ToDateTime(str_CurrnDate).AddDays(srt_days_END).ToString("yyyy/MM/dd") + " " + str_END + "','%Y/%m/%d %H:%i') Order by TimeIN ASC";

                        DataTable dt_OUT = new DataTable();
                        dt_OUT = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (dt_OUT.Rows.Count == 0)
                        {
                            OUT_Check = true;
                        }
                    }

                    if (!OUT_Check)
                    {

                        //AutoDt.Columns.Add("Tkno");
                        //AutoDt.Columns.Add("Empname");
                        //AutoDt.Columns.Add("Department");
                        //AutoDt.Columns.Add("Designation");
                        //AutoDt.Columns.Add("Shift");
                        //AutoDt.Columns.Add("TIMEIN");
                        //AutoDt.Columns.Add("Plant");

                        AutoDt.Rows.Add();
                        AutoDt.Rows[AutoDt.Rows.Count - 1]["Tkno"] = dt_Emp.Rows[0]["ExistingCode"].ToString();
                        AutoDt.Rows[AutoDt.Rows.Count - 1]["Empname"] = dt_Emp.Rows[0]["Firstname"].ToString();
                        AutoDt.Rows[AutoDt.Rows.Count - 1]["Department"] = dt_Emp.Rows[0]["Deptname"].ToString();
                        AutoDt.Rows[AutoDt.Rows.Count - 1]["Designation"] = dt_Emp.Rows[0]["Designation"].ToString();
                        AutoDt.Rows[AutoDt.Rows.Count - 1]["Shift"] = Employee_Shift_Name_DB;
                        AutoDt.Rows[AutoDt.Rows.Count - 1]["TIMEIN"] = Convert.ToDateTime(dt_IN.Rows[i]["TimeIN"]).ToString("hh:mm tt");
                        AutoDt.Rows[AutoDt.Rows.Count - 1]["Plant"] = plant_Name;
                        AutoDt.AcceptChanges();



                    }
                }
            }
        }
        if (AutoDt.Rows.Count > 0)
        {

            string CName = "";
            string LocName = "";
            SSQL = "";
            SSQL = "Select * from Naga_rights.AdminRights where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            DataTable Company_Dt = new DataTable();
            Company_Dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (Company_Dt.Rows.Count > 0)
            {
                CName = Company_Dt.Rows[0]["Cname"].ToString().ToUpper();
                LocName = Company_Dt.Rows[0]["Lcode"].ToString().ToUpper() + " - " + Company_Dt.Rows[0]["Location"].ToString().ToUpper();
            }
                
            report.Load(Server.MapPath("crystal/NotLeftReport.rpt"));

            report.Database.Tables[0].SetDataSource(AutoDt);
            report.DataDefinition.FormulaFields["CompName"].Text = "'" + CName + "'";
            report.DataDefinition.FormulaFields["Address"].Text = "'" + LocName + "'";
            report.DataDefinition.FormulaFields["Date"].Text = "'" + LocName + "'";
            string Server_Path = Server.MapPath("~");
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report2;
            string AttachfileName_Miss = "";
            //AttachfileName_Miss = Server_Path + "/Daily_Report/Notleft_fromunit_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy_HHmmss") + ".pdf";

            //if (File.Exists(AttachfileName_Miss))
            //{
            //    File.Delete(AttachfileName_Miss);
            //}

            //report.ExportToDisk(ExportFormatType.PortableDocFormat, AttachfileName_Miss);
            //report.Close();
            //report.Dispose();
        }
        else
        {

        }
    }
}