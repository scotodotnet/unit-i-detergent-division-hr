﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

public partial class DayAttendanceMissShift : System.Web.UI.Page
{
    string ShiftType1 = "";
    string Date = "";
    string Status = "";

    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string ss = "";
    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
   
    string SSQL = "";
    string Adolescent_Shift;
    DataSet ds = new DataSet();

    string Division = "";
    
    string Emp_Wages_Type = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Day Attendance Day Wise";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            Status = Request.QueryString["Status"].ToString();
            //ShiftType1 = Request.QueryString["ShiftType1"].ToString();
            Date = Request.QueryString["FromDate"].ToString();
            Division = Request.QueryString["Division"].ToString();
            Emp_Wages_Type = Request.QueryString["Wages"].ToString();

            Get_Report();
            
        }
    }

    private void Get_Report()
    {
        SSQL = "";
        SSQL = "Select EM.MachineID as MachineCode,LD.ExistingCode as ExCode,LD.DeptName,LD.FirstName as Name,LD.Match_Shift as Shift,LD.TimeIN,LD.TimeOUT,";
        SSQL = SSQL + " (CASE when COALESCE(LD.LateIN+LD.LateOUT,0)>0 then 'H' end) as Status from Employee_mst EM inner join Logtime_Days LD on LD.MachineID=EM.MachineID";
        SSQL = SSQL + " Where LD.CompCode='"+SessionCcode+"' and LD.LocCode='"+SessionLcode+"' and EM.CompCode='"+SessionCcode+"'";
        SSQL = SSQL + " and EM.LocCode='" + SessionLcode + "' and LD.Attn_Date_Str='" + Date + "'";
        if (Emp_Wages_Type != "-Select-")
        {
            SSQL = SSQL + " and LD.Wages='" + Emp_Wages_Type + "'";
        }
        SSQL = SSQL + " and (LD.LateIN+LD.LateOUT)>0";
        DataTable dt_Atten = new DataTable();
        dt_Atten = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt_Atten.Rows.Count > 0)
        {
            string Cname = "";
            string Address = "";

            SSQL = "";
            SSQL = "Select Cname,CONCAT_WS('-', Address1, Address2) as Address, Pincode from " + Session["SessionEpay"] + ".AdminRights where CCode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            DataTable Comp_Dt = new DataTable();
            Comp_Dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (Comp_Dt.Rows.Count > 0)
            {
                Cname = Comp_Dt.Rows[0]["Cname"].ToString();
                Address = Comp_Dt.Rows[0]["Address"].ToString();
            }

            report.Load(Server.MapPath("crystal/Day_Attendance_MissShift.rpt"));

            report.Database.Tables[0].SetDataSource(dt_Atten);
            report.DataDefinition.FormulaFields["ReportDate"].Text = "'" + Date + "'";

            report.DataDefinition.FormulaFields["Company"].Text = "'" + Cname + "'";
            report.DataDefinition.FormulaFields["Address"].Text = "'" + Address + "'";

            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page,this.GetType(),"alert","alert('No Record Found')",true);
        }
    }
}