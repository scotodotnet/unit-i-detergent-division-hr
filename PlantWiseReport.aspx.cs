﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

public partial class PlantWiseReport : System.Web.UI.Page
{
    string ShiftType1 = "";
    string Date = "";
    string Status = "";
    string FromDate = "";
    string ToDate = "";

    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string ss = "";
    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();



    string SSQL = "";
    string Adolescent_Shift;
    DataSet ds = new DataSet();
    DataSet ds1 = new DataSet();
    DataSet ds2 = new DataSet();

    string Division = "";
    DataTable AutoDTable = new DataTable();
    DataTable DataCell = new DataTable();
    DataTable DataCell1 = new DataTable();
    DataTable DataCell2 = new DataTable();
    DataTable dt = new DataTable();


    string Datestr = "";
    string Datestr1 = "";
    string ShiftType = "";
    DataTable mDataSet = new DataTable();

   

    DataRow dtRow;

  
    string Emp_Wages_Type = "";
   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Day Attendance Day Wise";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            //Status = Request.QueryString["Status"].ToString();
            //ShiftType1 = Request.QueryString["ShiftType1"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
           // ToDate = Request.QueryString["ToDate"].ToString();
            Division = Request.QueryString["Division"].ToString();
           // Emp_Wages_Type = Request.QueryString["Wages"].ToString();

            //if (SessionUserType == "1")
            //{
            //    GetAttdDayWise_Change();
            //}
            if (SessionUserType == "2")
            {
                NonAdminGetAttdDayWise_Change();
            }
            else
            {
          
                GetAttdDayWise_Change();
            }
        }

    }
    public void NonAdminGetAttdDayWise_Change()
    {
        SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);


        string name = dt.Rows[0]["CompName"].ToString();
        string Addres = dt.Rows[0]["Add1"].ToString();

        string TableName = "";

        if (Status == "Pending")
        {
            TableName = "Employee_Mst_New_Emp";
        }

        else
        {
            TableName = "Employee_Mst";
        }

       

        //POWDER PLANT
        DataCell.Columns.Add("CompanyName");
        DataCell.Columns.Add("LocationName");
        DataCell.Columns.Add("ShiftDate");
        DataCell.Columns.Add("SNo");
        DataCell.Columns.Add("Dept");
        DataCell.Columns.Add("Type");
        DataCell.Columns.Add("Shift");
        DataCell.Columns.Add("Category");
        DataCell.Columns.Add("SubCategory");
        DataCell.Columns.Add("EmpCode");
        DataCell.Columns.Add("ExCode");
        DataCell.Columns.Add("Name");
        DataCell.Columns.Add("TimeIN");
        DataCell.Columns.Add("TimeOUT");

   
        DataCell.Columns.Add("Wages");
        DataCell.Columns.Add("MachineID");

        //BAR PLANT
        DataCell1.Columns.Add("CompanyName");
        DataCell1.Columns.Add("LocationName");
        DataCell1.Columns.Add("ShiftDate");
        DataCell1.Columns.Add("SNo");
        DataCell1.Columns.Add("Dept");
        DataCell1.Columns.Add("Type");
        DataCell1.Columns.Add("Shift");
        DataCell1.Columns.Add("Category");
        DataCell1.Columns.Add("SubCategory");
        DataCell1.Columns.Add("EmpCode");
        DataCell1.Columns.Add("ExCode");
        DataCell1.Columns.Add("Name");
        DataCell1.Columns.Add("TimeIN");
        DataCell1.Columns.Add("TimeOUT");


        DataCell1.Columns.Add("Wages");
        DataCell1.Columns.Add("MachineID");

        //TSP PLANT
        DataCell2.Columns.Add("CompanyName");
        DataCell2.Columns.Add("LocationName");
        DataCell2.Columns.Add("ShiftDate");
        DataCell2.Columns.Add("SNo");
        DataCell2.Columns.Add("Dept");
        DataCell2.Columns.Add("Type");
        DataCell2.Columns.Add("Shift");
        DataCell2.Columns.Add("Category");
        DataCell2.Columns.Add("SubCategory");
        DataCell2.Columns.Add("EmpCode");
        DataCell2.Columns.Add("ExCode");
        DataCell2.Columns.Add("Name");
        DataCell2.Columns.Add("TimeIN");
        DataCell2.Columns.Add("TimeOUT");


        DataCell2.Columns.Add("Wages");
        DataCell2.Columns.Add("MachineID");



        DataTable mLocalDS = new DataTable();

      
     
        // POWDER PLANT

        SSQL = "select * from logtime_powder_plant A inner Join " + TableName + " B on A.MachineId=B.MachineID_Encrypt where DATE_FORMAT(TIME,'%Y-%m-%d') between '" + Convert.ToDateTime(FromDate).ToString("yyyy-MM-dd") + "' and '" + Convert.ToDateTime(FromDate).ToString("yyyy-MM-dd") + "'";
        SSQL = SSQL + " And B.Compcode='" + SessionCcode + "'";
        SSQL = SSQL + " And B.LocCode='" + SessionLcode + "'";


        //if (Emp_Wages_Type != "-Select-")
        //{
        //    SSQL = SSQL + " And B.Wages='" + Emp_Wages_Type + "'";
        //}


        mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);

        int ssno = 1;
        for (int iRow = 0; iRow < mLocalDS.Rows.Count; iRow++)
        {

            DataCell.NewRow();
            DataCell.Rows.Add();

            DataCell.Rows[iRow]["CompanyName"] = name;
            DataCell.Rows[iRow]["LocationName"] = Addres;
           
            DataCell.Rows[iRow]["SNo"] = ssno;
            DataCell.Rows[iRow]["Dept"] = mLocalDS.Rows[iRow]["DeptName"].ToString();
            DataCell.Rows[iRow]["Type"] = mLocalDS.Rows[iRow]["TypeName"].ToString();
            DataCell.Rows[iRow]["Shift"] = mLocalDS.Rows[iRow]["Shift_Name"].ToString();
            DataCell.Rows[iRow]["Category"] = mLocalDS.Rows[iRow]["CatName"].ToString();
            DataCell.Rows[iRow]["SubCategory"] = mLocalDS.Rows[iRow]["SubCatName"].ToString();
            DataCell.Rows[iRow]["EmpCode"] = mLocalDS.Rows[iRow]["MachineID1"].ToString();
            DataCell.Rows[iRow]["ExCode"] = mLocalDS.Rows[iRow]["ExistingCode"].ToString();
            DataCell.Rows[iRow]["Name"] = mLocalDS.Rows[iRow]["FirstName"].ToString();
            DataCell.Rows[iRow]["TimeIN"] = mLocalDS.Rows[iRow]["Time"].ToString();
            DataCell.Rows[iRow]["TimeOUT"] = mLocalDS.Rows[iRow]["Time"].ToString();
            DataCell.Rows[iRow]["Wages"] = mLocalDS.Rows[iRow]["Contract"].ToString();
            DataCell.Rows[iRow]["MachineID"] = mLocalDS.Rows[iRow]["MachineID1"].ToString();
         


            ssno = ssno + 1;
        }
      
        // BAR PLANT

        SSQL = "select * from logtime_Bar_plant A inner Join " + TableName + " B on A.MachineId=B.MachineID_Encrypt where DATE_FORMAT(TIME,'%Y-%m-%d') between '" + Convert.ToDateTime(FromDate).ToString("yyyy-MM-dd") + "' and '" + Convert.ToDateTime(FromDate).ToString("yyyy-MM-dd") + "'";
        SSQL = SSQL + " And B.Compcode='" + SessionCcode + "'";
        SSQL = SSQL + " And B.LocCode='" + SessionLcode + "'";


        //if (Emp_Wages_Type != "-Select-")
        //{
        //    SSQL = SSQL + " And B.Wages='" + Emp_Wages_Type + "'";
        //}

        mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);

        int ssno1 = 1;
        for (int iRow = 0; iRow < mLocalDS.Rows.Count; iRow++)
        {

            DataCell1.NewRow();
            DataCell1.Rows.Add();

            DataCell1.Rows[iRow]["CompanyName"] = name;
            DataCell1.Rows[iRow]["LocationName"] = Addres;

            DataCell1.Rows[iRow]["SNo"] = ssno;
            DataCell1.Rows[iRow]["Dept"] = mLocalDS.Rows[iRow]["DeptName"].ToString();
            DataCell1.Rows[iRow]["Type"] = mLocalDS.Rows[iRow]["TypeName"].ToString();
            DataCell1.Rows[iRow]["Shift"] = mLocalDS.Rows[iRow]["Shift_Name"].ToString();
            DataCell1.Rows[iRow]["Category"] = mLocalDS.Rows[iRow]["CatName"].ToString();
            DataCell1.Rows[iRow]["SubCategory"] = mLocalDS.Rows[iRow]["SubCatName"].ToString();
            DataCell1.Rows[iRow]["EmpCode"] = mLocalDS.Rows[iRow]["MachineID1"].ToString();
            DataCell1.Rows[iRow]["ExCode"] = mLocalDS.Rows[iRow]["ExistingCode"].ToString();
            DataCell1.Rows[iRow]["Name"] = mLocalDS.Rows[iRow]["FirstName"].ToString();
            DataCell1.Rows[iRow]["TimeIN"] = mLocalDS.Rows[iRow]["Time"].ToString();
            DataCell1.Rows[iRow]["TimeOUT"] = mLocalDS.Rows[iRow]["Time"].ToString();
            DataCell1.Rows[iRow]["Wages"] = mLocalDS.Rows[iRow]["Contract"].ToString();
            DataCell1.Rows[iRow]["MachineID"] = mLocalDS.Rows[iRow]["MachineID1"].ToString();



            ssno1 = ssno1 + 1;
        }
        // TSP PLANT

        SSQL = "select * from logtime_tsp_plant A inner Join " + TableName + " B on A.MachineId=B.MachineID_Encrypt where DATE_FORMAT(TIME,'%Y-%m-%d') between '" + Convert.ToDateTime(FromDate).ToString("yyyy-MM-dd") + "' and '" + Convert.ToDateTime(FromDate).ToString("yyyy-MM-dd") + "'";
        SSQL = SSQL + " And B.Compcode='" + SessionCcode + "'";
        SSQL = SSQL + " And B.LocCode='" + SessionLcode + "'";


        //if (Emp_Wages_Type != "-Select-")
        //{
        //    SSQL = SSQL + " And B.Wages='" + Emp_Wages_Type + "'";
        //}


        mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);

        int ssno2 = 1;
        for (int iRow = 0; iRow < mLocalDS.Rows.Count; iRow++)
        {

            DataCell2.NewRow();
            DataCell2.Rows.Add();

            DataCell2.Rows[iRow]["CompanyName"] = name;
            DataCell2.Rows[iRow]["LocationName"] = Addres;

            DataCell2.Rows[iRow]["SNo"] = ssno;
            DataCell2.Rows[iRow]["Dept"] = mLocalDS.Rows[iRow]["DeptName"].ToString();
            DataCell2.Rows[iRow]["Type"] = mLocalDS.Rows[iRow]["TypeName"].ToString();
            DataCell2.Rows[iRow]["Shift"] = mLocalDS.Rows[iRow]["Shift_Name"].ToString();
            DataCell2.Rows[iRow]["Category"] = mLocalDS.Rows[iRow]["CatName"].ToString();
            DataCell2.Rows[iRow]["SubCategory"] = mLocalDS.Rows[iRow]["SubCatName"].ToString();
            DataCell2.Rows[iRow]["EmpCode"] = mLocalDS.Rows[iRow]["MachineID1"].ToString();
            DataCell2.Rows[iRow]["ExCode"] = mLocalDS.Rows[iRow]["ExistingCode"].ToString();
            DataCell2.Rows[iRow]["Name"] = mLocalDS.Rows[iRow]["FirstName"].ToString();
            DataCell2.Rows[iRow]["TimeIN"] = mLocalDS.Rows[iRow]["Time"].ToString();
            DataCell2.Rows[iRow]["TimeOUT"] = mLocalDS.Rows[iRow]["Time"].ToString();
            DataCell2.Rows[iRow]["Wages"] = mLocalDS.Rows[iRow]["Contract"].ToString();
            DataCell2.Rows[iRow]["MachineID"] = mLocalDS.Rows[iRow]["MachineID1"].ToString();



            ssno2 = ssno2 + 1;
        }
        ds.Tables.Add(DataCell);
        ds1.Tables.Add(DataCell1);
        ds2.Tables.Add(DataCell2);
        //ReportDocument report = new ReportDocument();

        report.Load(Server.MapPath("crystal/PlantWiseReport.rpt"));
        report.Subreports[0].SetDataSource(ds.Tables[0]);
        report.Subreports[1].SetDataSource(ds1.Tables[0]);
        report.Subreports[2].SetDataSource(ds2.Tables[0]);

       // report.Database.Tables[0].SetDataSource(ds.Tables[0]);
        report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
        CrystalReportViewer1.ReportSource = report;


    }

    public void GetAttdDayWise_Change()
    {
        SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);


        string name = dt.Rows[0]["CompName"].ToString();
        string Addres = dt.Rows[0]["Add1"].ToString();

        string TableName = "";

        if (Status == "Pending")
        {
            TableName = "Employee_Mst_New_Emp";
        }

        else
        {
            TableName = "Employee_Mst";
        }



        //POWDER PLANT
        DataCell.Columns.Add("CompanyName");
        DataCell.Columns.Add("LocationName");
        DataCell.Columns.Add("ShiftDate");
        DataCell.Columns.Add("SNo");
        DataCell.Columns.Add("Dept");
        DataCell.Columns.Add("Type");
        DataCell.Columns.Add("Shift");
        DataCell.Columns.Add("Category");
        DataCell.Columns.Add("SubCategory");
        DataCell.Columns.Add("EmpCode");
        DataCell.Columns.Add("ExCode");
        DataCell.Columns.Add("Name");
        DataCell.Columns.Add("TimeIN");
        DataCell.Columns.Add("TimeOUT");
        DataCell.Columns.Add("MainTotal");
        DataCell.Columns.Add("FromDate");
        DataCell.Columns.Add("ToDate");

        DataCell.Columns.Add("Wages");
        DataCell.Columns.Add("MachineID");

        //BAR PLANT
        DataCell1.Columns.Add("CompanyName");
        DataCell1.Columns.Add("LocationName");
        DataCell1.Columns.Add("ShiftDate");
        DataCell1.Columns.Add("SNo");
        DataCell1.Columns.Add("Dept");
        DataCell1.Columns.Add("Type");
        DataCell1.Columns.Add("Shift");
        DataCell1.Columns.Add("Category");
        DataCell1.Columns.Add("SubCategory");
        DataCell1.Columns.Add("EmpCode");
        DataCell1.Columns.Add("ExCode");
        DataCell1.Columns.Add("Name");
        DataCell1.Columns.Add("TimeIN");
        DataCell1.Columns.Add("TimeOUT");
        DataCell1.Columns.Add("MainTotal");
        DataCell1.Columns.Add("FromDate");
        DataCell1.Columns.Add("ToDate");
        DataCell1.Columns.Add("Wages");
        DataCell1.Columns.Add("MachineID");

        //TSP PLANT
        DataCell2.Columns.Add("CompanyName");
        DataCell2.Columns.Add("LocationName");
        DataCell2.Columns.Add("ShiftDate");
        DataCell2.Columns.Add("SNo");
        DataCell2.Columns.Add("Dept");
        DataCell2.Columns.Add("Type");
        DataCell2.Columns.Add("Shift");
        DataCell2.Columns.Add("Category");
        DataCell2.Columns.Add("SubCategory");
        DataCell2.Columns.Add("EmpCode");
        DataCell2.Columns.Add("ExCode");
        DataCell2.Columns.Add("Name");
        DataCell2.Columns.Add("TimeIN");
        DataCell2.Columns.Add("TimeOUT");
        DataCell2.Columns.Add("MainTotal");
        DataCell2.Columns.Add("FromDate");
        DataCell2.Columns.Add("ToDate");
        DataCell2.Columns.Add("Wages");
        DataCell2.Columns.Add("MachineID");

        DataTable mLocalDS = new DataTable();
        DataTable maingateDS = new DataTable();


        // MAIN GATE TOTAL

        SSQL = "SELECT COUNT(*) as Total FROM logtime_days WHERE  DATE_FORMAT(attn_date,'%Y-%m-%d') between '" + Convert.ToDateTime(FromDate).ToString("yyyy-MM-dd") + "' and '" + Convert.ToDateTime(FromDate).ToString("yyyy-MM-dd") + "'  AND Present!='0.0'";
        SSQL = SSQL + " And Compcode='" + SessionCcode + "'";
        SSQL = SSQL + " And LocCode='" + SessionLcode + "' ";
        maingateDS = objdata.RptEmployeeMultipleDetails(SSQL);

        // POWDER PLANT

        SSQL = "select B.MachineID,B.DeptName,B.TypeName,B.Shift_Name,B.CatName,B.SubCatName,B.ExistingCode,B.FirstName,A.Time,A.Time,B.Contract from logtime_powder_plant A inner Join " + TableName + " B on A.MachineId=B.MachineID_Encrypt where DATE_FORMAT(TIME,'%Y-%m-%d') between '" + Convert.ToDateTime(FromDate).ToString("yyyy-MM-dd") + "' and '" + Convert.ToDateTime(FromDate).ToString("yyyy-MM-dd") + "'";
        SSQL = SSQL + " And B.Compcode='" + SessionCcode + "'";
        SSQL = SSQL + " And B.LocCode='" + SessionLcode + "' ";


        //if (Emp_Wages_Type != "-Select-")
        //{
        //    SSQL = SSQL + " And B.Wages='" + Emp_Wages_Type + "'";
        //}


        mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);

        int ssno = 1;
        for (int iRow = 0; iRow < mLocalDS.Rows.Count; iRow++)
        {

            DataCell.NewRow();
            DataCell.Rows.Add();

            DataCell.Rows[iRow]["CompanyName"] = name;
            DataCell.Rows[iRow]["LocationName"] = Addres;

            DataCell.Rows[iRow]["SNo"] = ssno;
            DataCell.Rows[iRow]["Dept"] = mLocalDS.Rows[iRow]["DeptName"].ToString();
            DataCell.Rows[iRow]["Type"] = mLocalDS.Rows[iRow]["TypeName"].ToString();
            DataCell.Rows[iRow]["Shift"] = mLocalDS.Rows[iRow]["Shift_Name"].ToString();
            DataCell.Rows[iRow]["Category"] = mLocalDS.Rows[iRow]["CatName"].ToString();
            DataCell.Rows[iRow]["SubCategory"] = mLocalDS.Rows[iRow]["SubCatName"].ToString();
            DataCell.Rows[iRow]["EmpCode"] = mLocalDS.Rows[iRow]["MachineID"].ToString();
            DataCell.Rows[iRow]["ExCode"] = mLocalDS.Rows[iRow]["ExistingCode"].ToString();
            DataCell.Rows[iRow]["Name"] = mLocalDS.Rows[iRow]["FirstName"].ToString();
            DataCell.Rows[iRow]["TimeIN"] = mLocalDS.Rows[iRow]["Time"].ToString();
            DataCell.Rows[iRow]["TimeOUT"] = mLocalDS.Rows[iRow]["Time"].ToString();
            DataCell.Rows[iRow]["Wages"] = mLocalDS.Rows[iRow]["Contract"].ToString();
            DataCell.Rows[iRow]["MachineID"] = mLocalDS.Rows[iRow]["MachineID"].ToString();
            DataCell.Rows[iRow]["MainTotal"] = maingateDS.Rows[0]["Total"].ToString();
            DataCell.Rows[iRow]["FromDate"] = FromDate;
            DataCell.Rows[iRow]["ToDate"] = ToDate;


            ssno = ssno + 1;
        }

        // BAR PLANT

        SSQL = "select B.MachineID,B.DeptName,B.TypeName,B.Shift_Name,B.CatName,B.SubCatName,B.ExistingCode,B.FirstName,A.Time,A.Time,B.Contract from logtime_Bar_plant A inner Join " + TableName + " B on A.MachineId=B.MachineID_Encrypt where DATE_FORMAT(TIME,'%Y-%m-%d') between '" + Convert.ToDateTime(FromDate).ToString("yyyy-MM-dd") + "' and '" + Convert.ToDateTime(FromDate).ToString("yyyy-MM-dd") + "'";
        SSQL = SSQL + " And B.Compcode='" + SessionCcode + "'";
        SSQL = SSQL + " And B.LocCode='" + SessionLcode + "' ";


        //if (Emp_Wages_Type != "-Select-")
        //{
        //    SSQL = SSQL + " And B.Wages='" + Emp_Wages_Type + "'";
        //}

        mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);

        int ssno1 = 1;
        for (int iRow = 0; iRow < mLocalDS.Rows.Count; iRow++)
        {

            DataCell1.NewRow();
            DataCell1.Rows.Add();

            DataCell1.Rows[iRow]["CompanyName"] = name;
            DataCell1.Rows[iRow]["LocationName"] = Addres;

            DataCell1.Rows[iRow]["SNo"] = ssno;
            DataCell1.Rows[iRow]["Dept"] = mLocalDS.Rows[iRow]["DeptName"].ToString();
            DataCell1.Rows[iRow]["Type"] = mLocalDS.Rows[iRow]["TypeName"].ToString();
            DataCell1.Rows[iRow]["Shift"] = mLocalDS.Rows[iRow]["Shift_Name"].ToString();
            DataCell1.Rows[iRow]["Category"] = mLocalDS.Rows[iRow]["CatName"].ToString();
            DataCell1.Rows[iRow]["SubCategory"] = mLocalDS.Rows[iRow]["SubCatName"].ToString();
            DataCell1.Rows[iRow]["EmpCode"] = mLocalDS.Rows[iRow]["MachineID"].ToString();
            DataCell1.Rows[iRow]["ExCode"] = mLocalDS.Rows[iRow]["ExistingCode"].ToString();
            DataCell1.Rows[iRow]["Name"] = mLocalDS.Rows[iRow]["FirstName"].ToString();
            DataCell1.Rows[iRow]["TimeIN"] = mLocalDS.Rows[iRow]["Time"].ToString();
            DataCell1.Rows[iRow]["TimeOUT"] = mLocalDS.Rows[iRow]["Time"].ToString();
            DataCell1.Rows[iRow]["Wages"] = mLocalDS.Rows[iRow]["Contract"].ToString();
            DataCell1.Rows[iRow]["MachineID"] = mLocalDS.Rows[iRow]["MachineID"].ToString();
            DataCell1.Rows[iRow]["MainTotal"] = maingateDS.Rows[0]["Total"].ToString();
            DataCell1.Rows[iRow]["FromDate"] = FromDate;
            DataCell1.Rows[iRow]["ToDate"] = ToDate;

            ssno1 = ssno1 + 1;
        }
        // TSP PLANT

        SSQL = "select B.MachineID,B.DeptName,B.TypeName,B.Shift_Name,B.CatName,B.SubCatName,B.ExistingCode,B.FirstName,A.Time,A.Time,B.Contract from logtime_tsp_plant A inner Join " + TableName + " B on A.MachineId=B.MachineID_Encrypt where DATE_FORMAT(TIME,'%Y-%m-%d') between '" + Convert.ToDateTime(FromDate).ToString("yyyy-MM-dd") + "' and '" + Convert.ToDateTime(FromDate).ToString("yyyy-MM-dd") + "'";
        SSQL = SSQL + " And B.Compcode='" + SessionCcode + "'";
        SSQL = SSQL + " And B.LocCode='" + SessionLcode + "' ";


        //if (Emp_Wages_Type != "-Select-")
        //{
        //    SSQL = SSQL + " And B.Wages='" + Emp_Wages_Type + "'";
        //}


        mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);

        int ssno2 = 1;
        for (int iRow = 0; iRow < mLocalDS.Rows.Count; iRow++)
        {

            DataCell2.NewRow();
            DataCell2.Rows.Add();

            DataCell2.Rows[iRow]["CompanyName"] = name;
            DataCell2.Rows[iRow]["LocationName"] = Addres;

            DataCell2.Rows[iRow]["SNo"] = ssno;
            DataCell2.Rows[iRow]["Dept"] = mLocalDS.Rows[iRow]["DeptName"].ToString();
            DataCell2.Rows[iRow]["Type"] = mLocalDS.Rows[iRow]["TypeName"].ToString();
            DataCell2.Rows[iRow]["Shift"] = mLocalDS.Rows[iRow]["Shift_Name"].ToString();
            DataCell2.Rows[iRow]["Category"] = mLocalDS.Rows[iRow]["CatName"].ToString();
            DataCell2.Rows[iRow]["SubCategory"] = mLocalDS.Rows[iRow]["SubCatName"].ToString();
            DataCell2.Rows[iRow]["EmpCode"] = mLocalDS.Rows[iRow]["MachineID"].ToString();
            DataCell2.Rows[iRow]["ExCode"] = mLocalDS.Rows[iRow]["ExistingCode"].ToString();
            DataCell2.Rows[iRow]["Name"] = mLocalDS.Rows[iRow]["FirstName"].ToString();
            DataCell2.Rows[iRow]["TimeIN"] = mLocalDS.Rows[iRow]["Time"].ToString();
            DataCell2.Rows[iRow]["TimeOUT"] = mLocalDS.Rows[iRow]["Time"].ToString();
            DataCell2.Rows[iRow]["Wages"] = mLocalDS.Rows[iRow]["Contract"].ToString();
            DataCell2.Rows[iRow]["MachineID"] = mLocalDS.Rows[iRow]["MachineID"].ToString();
            DataCell2.Rows[iRow]["MainTotal"] = maingateDS.Rows[0]["Total"].ToString();
            DataCell2.Rows[iRow]["FromDate"] = FromDate;
            DataCell2.Rows[iRow]["ToDate"] = ToDate;

            ssno2 = ssno2 + 1;
        }
        ds.Tables.Add(DataCell);
        ds1.Tables.Add(DataCell1);
        ds2.Tables.Add(DataCell2);
        //ReportDocument report = new ReportDocument();

        report.Load(Server.MapPath("crystal/PlantWiseReport.rpt"));
        report.Subreports[0].SetDataSource(ds.Tables[0]);
        report.Subreports[1].SetDataSource(ds1.Tables[0]);
        report.Subreports[2].SetDataSource(ds2.Tables[0]);

        // report.Database.Tables[0].SetDataSource(ds.Tables[0]);
        report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
        CrystalReportViewer1.ReportSource = report;
        

    }
    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }
    private static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = "0";
        BALDataAccess objdata_new = new BALDataAccess();

        string query = "Select * from Employee_Mst where MachineID_Encrypt='" + encryptpwd + "'";
        DataTable DT = new DataTable();
        DT = objdata_new.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            decryptpwd = DT.Rows[0]["MachineID"].ToString();
        }
        else
        {
            query = "Select * from Employee_Mst_New_Emp where MachineID_Encrypt='" + encryptpwd + "'";
            DT = objdata_new.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                decryptpwd = DT.Rows[0]["MachineID"].ToString();
            }
            else
            {
                decryptpwd = "0";
            }
        }

      
        return decryptpwd;
    }
}