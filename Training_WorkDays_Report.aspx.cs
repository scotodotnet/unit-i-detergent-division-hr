﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Data.OleDb;
using Payroll;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Training_WorkDays_Report : System.Web.UI.Page
{
    string SessionAdmin;
    string DepartmentCode;
    string Name_Upload;
    string Name_Upload1;
    BALDataAccess objdata = new BALDataAccess();
    AttenanceClass objatt = new AttenanceClass();
    string SessionCcode;
    string SessionLcode;
    string WagesType;
    string SessionUserType;
    string SessionEpay;
    string Query = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();        
        if (!IsPostBack)
        {
            ddlCategory_SelectedIndexChanged(sender,e);
        }
    }

    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        string StaffLabour = "";

        if (ddlCategory.SelectedValue == "Staff")
        {
            StaffLabour = "1";
        }
        else if (ddlCategory.SelectedValue == "Labour")
        {
            StaffLabour = "2";
        }
        else
        {
            StaffLabour = "0";
        }


        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlEmployeeType.Items.Clear();
        query = "select EmpType from MstEmployeeType where EmpCategory='" + StaffLabour + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlEmployeeType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpType";
        ddlEmployeeType.DataBind();
    }

    protected void btnReport_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string Employee_Type = "";
            if (txtWorkDays_Date.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Date...');", true);
                ErrFlag = true;
            }

            if (!ErrFlag)
            {
                //if (ddldept.SelectedValue == "0")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Department Properly');", true);
                //    ErrFlag = true;
                //}
                if (!ErrFlag)
                {
                    Employee_Type = "";
                    if (ddlEmployeeType.SelectedValue != "-Select-")
                    {
                        Employee_Type = ddlEmployeeType.SelectedItem.Text;
                    }

                    ResponseHelper.Redirect("ViewReport_Trainee_Days.aspx?Workdays_Date=" + txtWorkDays_Date.Text + "&Employee_Type=" + Employee_Type + "&RptName=" + "Trainee_Work_Days_Report", "_blank", "");

                }
            }
        }
        catch (Exception ex)
        {
        }
    }
}
