﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.IO;

public partial class EmployeeDetNew : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Employee Details";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            Initial_Data_Referesh();

            Load_Department();
            Load_Designation();
            Load_WagesType();
            Load_Bank();
            Load_Unit();
            Load_WorkingUnit();
            Load_Taluk();
            Load_District();
            Load_State();
            Load_Recruitment();
            Load_PFCode();
            Load_ESICode();
            Load_Grade();
            Load_Division();
            Load_Route();
            Load_BusNo();
            rbtnSalaryThrough_SelectedIndexChanged(sender, e);
            RdbPFEligible_SelectedIndexChanged(sender, e);
            RdbESIEligible_SelectedIndexChanged(sender, e);
            txtRecruitThrg_SelectedIndexChanged(sender, e);
            ddlWorkingUnit.SelectedValue = SessionLcode;
            ddlSalaryUnit.SelectedValue = SessionLcode;

            if (Session["MachineID"] != null)
            {
                txtMachineID.Text = Session["MachineID"].ToString();
                txtMachineID.Enabled = false;
                btnSearch_Click(sender, e);
            }

        }
        Load_OLD_data();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        DataTable DT_Check = new DataTable();

        SSQL = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And EmpNo='" + txtMachineID.Text + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);


        if (DT_Check.Rows.Count != 0)
        {
            txtExistingCode.Text = DT_Check.Rows[0]["ExistingCode"].ToString();
            txtTokenID.Text = DT_Check.Rows[0]["EmpNo"].ToString();
            txtFirstName.Text = DT_Check.Rows[0]["FirstName"].ToString();
            txtLastName.Text = DT_Check.Rows[0]["LastName"].ToString();
            ddlCategory.SelectedValue = DT_Check.Rows[0]["CatName"].ToString();
            ddlSubCategory.SelectedValue = DT_Check.Rows[0]["SubCatName"].ToString();
            ddlShift.SelectedValue = DT_Check.Rows[0]["ShiftType"].ToString();
            txtDOB.Text = Convert.ToDateTime(DT_Check.Rows[0]["BirthDate"]).ToString("dd/MM/yyyy");
            txtAge.Text = DT_Check.Rows[0]["Age"].ToString();
            ddlGender.SelectedValue = DT_Check.Rows[0]["Gender"].ToString().ToUpper();
            txtDOJ.Text = Convert.ToDateTime(DT_Check.Rows[0]["DOJ"]).ToString("dd/MM/yyyy");
            ddlDepartment.SelectedValue = DT_Check.Rows[0]["DeptCode"].ToString();
            Load_Designation();
            ddlDesignation.SelectedValue = DT_Check.Rows[0]["Designation"].ToString();
            txtQulification.Text = DT_Check.Rows[0]["Qualification"].ToString();
            txtEmpMobileNo.Text = DT_Check.Rows[0]["EmployeeMobile"].ToString();
            ddlOTEligible.SelectedValue = DT_Check.Rows[0]["OTEligible"].ToString();
            ddlWagesType.SelectedValue = DT_Check.Rows[0]["Wages"].ToString();
            ddlEmpLevel.SelectedValue = DT_Check.Rows[0]["EmpLevel"].ToString();
            RdbPFEligible.SelectedValue = DT_Check.Rows[0]["Eligible_PF"].ToString();
            txtPFNo.Text = DT_Check.Rows[0]["PFNo"].ToString();
            txtPFDate.Text = DT_Check.Rows[0]["PFDOJ"].ToString();
            RdbESIEligible.SelectedValue = DT_Check.Rows[0]["Eligible_ESI"].ToString();
            txtESINo.Text = DT_Check.Rows[0]["ESINo"].ToString();
            txtESIDate.Text = DT_Check.Rows[0]["ESIDOJ"].ToString();
            txtUAN.Text = DT_Check.Rows[0]["UAN"].ToString();
            ddlPFCode.SelectedValue = DT_Check.Rows[0]["PF_Code"].ToString();
            ddlESICode.SelectedValue = DT_Check.Rows[0]["ESICode"].ToString();
            txtHostelRoom.Text = DT_Check.Rows[0]["RoomNo"].ToString();
            ddlVehicleType.SelectedValue = DT_Check.Rows[0]["Vehicles_Type"].ToString();
            Load_Route();
            txtVillage.SelectedValue = DT_Check.Rows[0]["BusRoute"].ToString();
            Load_BusNo();
            txtBusNo.SelectedValue = DT_Check.Rows[0]["BusNo"].ToString();
            if (DT_Check.Rows[0]["IsActive"].ToString() == "Yes")
            {
                dbtnActive.SelectedValue = "1";
            }
            else
            {
                dbtnActive.SelectedValue = "2";
            }
            txtReliveDate.Text = DT_Check.Rows[0]["DOR"].ToString();
            txtReason.Text = DT_Check.Rows[0]["Reason"].ToString();
            txt480Days.Text = DT_Check.Rows[0]["Emp_Permn_Date"].ToString();
            txtCertificate.Text = DT_Check.Rows[0]["Certificate"].ToString();
            ddlWeekOff.SelectedValue = DT_Check.Rows[0]["WeekOff"].ToString();
            rbtnSalaryThrough.SelectedValue = DT_Check.Rows[0]["Salary_Through"].ToString();
            ddlBankName.SelectedValue = DT_Check.Rows[0]["BankName"].ToString();
            txtIFSC.Text = DT_Check.Rows[0]["IFSC_Code"].ToString();
            txtBranch.Text = DT_Check.Rows[0]["BranchCode"].ToString();
            txtAccNo.Text = DT_Check.Rows[0]["AccountNo"].ToString();
            txtBasic.Text = DT_Check.Rows[0]["BaseSalary"].ToString();
            txtVPF.Text = DT_Check.Rows[0]["VPF"].ToString();
            txtAllowance1.Text = DT_Check.Rows[0]["Alllowance1"].ToString();
            txtAllowance2.Text = DT_Check.Rows[0]["Alllowance2"].ToString();
            txtDeduction1.Text = DT_Check.Rows[0]["Deduction1"].ToString();
            txtDeduction2.Text = DT_Check.Rows[0]["Deduction2"].ToString();
            txtOTSal.Text = DT_Check.Rows[0]["OT_Salary"].ToString();
            ddlMartialStatus.SelectedValue = DT_Check.Rows[0]["MaritalStatus"].ToString();
            txtNationality.Text = DT_Check.Rows[0]["Nationality"].ToString();
            txtReligion.Text = DT_Check.Rows[0]["Religion"].ToString();
            txtHeight.Text = DT_Check.Rows[0]["Height"].ToString();
            txtWeight.Text = DT_Check.Rows[0]["Weight"].ToString();
            txtStdWorkingHrs.Text = DT_Check.Rows[0]["StdWrkHrs"].ToString();
            if (DT_Check.Rows[0]["IsActive"].ToString() == "Yes")
            {
                rbtnPhysically.SelectedValue = "1";
            }
            else
            {
                rbtnPhysically.SelectedValue = "2";
            }
            txtPhyReason.Text = DT_Check.Rows[0]["Handicapped_Reason"].ToString();
            ddlBloodGrp.SelectedValue = DT_Check.Rows[0]["BloodGroup"].ToString();
            txtRecruitThrg.SelectedValue = DT_Check.Rows[0]["RecuritmentThro"].ToString();
            ddlUnit.SelectedValue = DT_Check.Rows[0]["RecuritmentUnit"].ToString();
            txtRecruitmentName.SelectedValue = DT_Check.Rows[0]["RecuriterName"].ToString();
            txtExistingEmpNo.Text = DT_Check.Rows[0]["ExistingEmpNo"].ToString();
            txtExistingEmpName.Text = DT_Check.Rows[0]["ExistingEmpName"].ToString();
            txtRecruitMobile.Text = DT_Check.Rows[0]["RecutersMob"].ToString();

            txtRefParentsName.Text = DT_Check.Rows[0]["RefParentsName"].ToString();
            txtRefMobileNo.Text = DT_Check.Rows[0]["RefParentsMobile"].ToString();

            txtRecruitThrg_SelectedIndexChanged(sender, e);

            ddlGrade.SelectedValue = DT_Check.Rows[0]["Grade"].ToString();
            ddlDivision.SelectedValue = DT_Check.Rows[0]["Division"].ToString();

            txtLeaveFrom.Text = DT_Check.Rows[0]["LeaveFrom"].ToString();
            txtLeaveTo.Text = DT_Check.Rows[0]["LeaveTo"].ToString();

            txtNominee.Text = DT_Check.Rows[0]["Nominee"].ToString();
            txtFatherName.Text = DT_Check.Rows[0]["FamilyDetails"].ToString();
            txtParentMob1.Text = DT_Check.Rows[0]["ParentsPhone"].ToString();
            txtMotherName.Text = DT_Check.Rows[0]["MotnerName"].ToString();
            txtParentMob2.Text = DT_Check.Rows[0]["parentsMobile"].ToString();
            txtGuardianName.Text = DT_Check.Rows[0]["GuardianName"].ToString();
            txtGuardianMobile.Text = DT_Check.Rows[0]["GuardianMobile"].ToString();
            txtPermAddr.Text = DT_Check.Rows[0]["Address1"].ToString();
            txtPermTaluk.SelectedValue = DT_Check.Rows[0]["Taluk_Perm"].ToString();
            txtPermDist.SelectedValue = DT_Check.Rows[0]["Permanent_Dist"].ToString();
            ddlState.SelectedValue = DT_Check.Rows[0]["StateName"].ToString();
            if (DT_Check.Rows[0]["OtherState"].ToString() == "Yes")
            {
                chkOtherState.Checked = true;
            }
            else
            {
                chkOtherState.Checked = false;
            }
            txtTempTaluk.SelectedValue = DT_Check.Rows[0]["Taluk_Present"].ToString();
            txtTempDist.SelectedValue = DT_Check.Rows[0]["Present_Dist"].ToString();
            txtTempAddr.Text = DT_Check.Rows[0]["Address2"].ToString();
            if (DT_Check.Rows[0]["SamepresentAddress"].ToString() == "1")
            {
                chkSame.Checked = true;
            }
            else
            {
                chkSame.Checked = false;
            }
            txtIdenMark1.Text = DT_Check.Rows[0]["IDMark1"].ToString();
            txtIdenMark2.Text = DT_Check.Rows[0]["IDMark2"].ToString();
        }
        else
        {
            Clear_All_Field();
        }

    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("DocType", typeof(string)));
        dt.Columns.Add(new DataColumn("DocNo", typeof(string)));
        dt.Columns.Add(new DataColumn("Doc_Img", typeof(string)));


        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSource;
    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    private void Load_PFCode()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlPFCode.Items.Clear();
        query = "Select *from Location_Mst where LocCode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlPFCode.DataSource = dtdsupp;
        //DataRow dr = dtdsupp.NewRow();
        //dr["DeptCode"] = "0";
        //dr["DeptName"] = "-Select-";
        //dtdsupp.Rows.InsertAt(dr, 0);
        ddlPFCode.DataTextField = "PF_Code";
        ddlPFCode.DataValueField = "PF_Code";
        ddlPFCode.DataBind();
    }

    private void Load_ESICode()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlESICode.Items.Clear();
        query = "Select *from ESICode_Mst where LocCode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlESICode.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ESICode"] = "0";
        dr["ESICode"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlESICode.DataTextField = "ESICode";
        ddlESICode.DataValueField = "ESICode";
        ddlESICode.DataBind();
    }

    private void Load_Grade()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlGrade.Items.Clear();
        query = "Select *from MstGrade";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlGrade.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["GradeName"] = "-Select-";
        dr["GradeName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlGrade.DataTextField = "GradeName";
        ddlGrade.DataValueField = "GradeName";
        ddlGrade.DataBind();
    }


    private void Load_Department()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlDepartment.Items.Clear();
        query = "Select *from Department_Mst";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlDepartment.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["DeptCode"] = "0";
        dr["DeptName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlDepartment.DataTextField = "DeptName";
        ddlDepartment.DataValueField = "DeptCode";
        ddlDepartment.DataBind();
    }

    private void Load_Designation()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlDesignation.Items.Clear();
        query = "Select *from Designation_Mst where DeptName='" + ddlDepartment.SelectedItem.Text + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlDesignation.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["DesignName"] = "-Select-";
        dr["DesignName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlDesignation.DataTextField = "DesignName";
        ddlDesignation.DataValueField = "DesignName";
        ddlDesignation.DataBind();
    }

    private void Load_Route()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtVillage.Items.Clear();
        query = "Select Distinct RouteName from RouteMst where Type='" + ddlVehicleType.SelectedItem.Text + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtVillage.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["RouteName"] = "-Select-";
        dr["RouteName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtVillage.DataTextField = "RouteName";
        txtVillage.DataValueField = "RouteName";
        txtVillage.DataBind();
    }

    private void Load_BusNo()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtBusNo.Items.Clear();
        query = "Select Distinct BusNo from RouteMst where Type='" + ddlVehicleType.SelectedItem.Text + "' And RouteName='" + txtVillage.SelectedItem.Text + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtBusNo.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["BusNo"] = "-Select-";
        dr["BusNo"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtBusNo.DataTextField = "BusNo";
        txtBusNo.DataValueField = "BusNo";
        txtBusNo.DataBind();
    }

    private void Load_WagesType()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlWagesType.Items.Clear();
        query = "Select *from MstEmployeeType";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlWagesType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlWagesType.DataTextField = "EmpType";
        ddlWagesType.DataValueField = "EmpType";
        ddlWagesType.DataBind();
    }

    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Designation();
    }

    private void Load_Bank()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        DataTable DT = new DataTable();
        ddlBankName.Items.Clear();
        query = "Select *from MstBank";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlBankName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["BankName"] = "-Select-";
        dr["BankName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlBankName.DataTextField = "BankName";
        ddlBankName.DataValueField = "BankName";
        ddlBankName.DataBind();

        query = "Select *from MstBank where Default_Bank='Yes'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            ddlBankName.SelectedValue = DT.Rows[0]["BankName"].ToString();
            txtIFSC.Text = DT.Rows[0]["IFSCCode"].ToString();
            txtBranch.Text = DT.Rows[0]["Branch"].ToString();
        }
        else
        {
            ddlBankName.SelectedValue = "-Select-";
            txtIFSC.Text = "";
            txtBranch.Text = "";
        }
    }

    private void Load_Unit()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        DataTable DT = new DataTable();
        ddlUnit.Items.Clear();
        query = "Select LocCode from Location_Mst where CompCode='" + SessionCcode + "'";
        //query = query + " And LocCode!='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlUnit.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["LocCode"] = "-Select-";
        dr["LocCode"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlUnit.DataTextField = "LocCode";
        ddlUnit.DataValueField = "LocCode";
        ddlUnit.DataBind();

    }

    private void Load_Recruitment()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        DataTable DT = new DataTable();
        txtRecruitmentName.Items.Clear();
        if (txtRecruitThrg.SelectedItem.Text == "Agent")
        {
            query = "Select AgentName as ROName from MstAgent";
        }
        else
        {
            query = "Select ROName from MstRecruitOfficer";
        }
        //query = query + " And LocCode!='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtRecruitmentName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ROName"] = "-Select-";
        dr["ROName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtRecruitmentName.DataTextField = "ROName";
        txtRecruitmentName.DataValueField = "ROName";
        txtRecruitmentName.DataBind();

    }

    private void Load_WorkingUnit()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        DataTable DT = new DataTable();
        ddlWorkingUnit.Items.Clear();
        query = "Select LocCode from Location_Mst where CompCode='" + SessionCcode + "'";
        //query = query + " And LocCode!='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlWorkingUnit.DataSource = dtdsupp;
        ddlWorkingUnit.DataTextField = "LocCode";
        ddlWorkingUnit.DataValueField = "LocCode";
        ddlWorkingUnit.DataBind();


        ddlSalaryUnit.Items.Clear();
        ddlSalaryUnit.DataSource = dtdsupp;
        ddlSalaryUnit.DataTextField = "LocCode";
        ddlSalaryUnit.DataValueField = "LocCode";
        ddlSalaryUnit.DataBind();
    }

    private void Load_Taluk()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        DataTable DT = new DataTable();
        txtPermTaluk.Items.Clear();
        query = "Select *from MstTaluk";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtPermTaluk.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["Taluk"] = "-Select-";
        dr["Taluk"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtPermTaluk.DataTextField = "Taluk";
        txtPermTaluk.DataValueField = "Taluk";
        txtPermTaluk.DataBind();

        txtTempTaluk.Items.Clear();
        txtTempTaluk.DataSource = dtdsupp;
        txtTempTaluk.DataTextField = "Taluk";
        txtTempTaluk.DataValueField = "Taluk";
        txtTempTaluk.DataBind();
    }

    private void Load_District()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        DataTable DT = new DataTable();
        txtPermDist.Items.Clear();
        query = "Select *from MstDistrict";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtPermDist.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["District"] = "-Select-";
        dr["District"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtPermDist.DataTextField = "District";
        txtPermDist.DataValueField = "District";
        txtPermDist.DataBind();

        txtTempDist.Items.Clear();
        txtTempDist.DataSource = dtdsupp;
        txtTempDist.DataTextField = "District";
        txtTempDist.DataValueField = "District";
        txtTempDist.DataBind();
    }
    private void Load_State()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        DataTable DT = new DataTable();
        ddlState.Items.Clear();
        query = "Select *from MstState";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlState.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["State"] = "-Select-";
        dr["State"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlState.DataTextField = "State";
        ddlState.DataValueField = "State";
        ddlState.DataBind();

    }

    private void Load_Division()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        DataTable DT = new DataTable();
        ddlDivision.Items.Clear();
        query = "Select *from Division_Master where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlDivision.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["Division"] = "-Select-";
        dr["Division"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlDivision.DataTextField = "Division";
        ddlDivision.DataValueField = "Division";
        ddlDivision.DataBind();

    }

    protected void RdbPFEligible_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (RdbPFEligible.SelectedValue == "1")
        {
            txtPFNo.Enabled = true;
            txtPFDate.Enabled = true;
        }
        else
        {
            txtPFNo.Enabled = false;
            txtPFDate.Enabled = false;
        }
    }
    protected void RdbESIEligible_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (RdbESIEligible.SelectedValue == "1")
        {
            txtESINo.Enabled = true;
            txtESIDate.Enabled = true;
        }
        else
        {
            txtESINo.Enabled = false;
            txtESIDate.Enabled = false;
        }
    }
    protected void rbtnSalaryThrough_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (rbtnSalaryThrough.SelectedValue == "1")
        {
            ddlBankName.Enabled = false;
            txtAccNo.Enabled = false;
        }
        else
        {
            ddlBankName.Enabled = true; ;
            txtAccNo.Enabled = true;
        }
    }
    public void AgeCalc()
    {
        try
        {
            if (txtDOB.Text != "")
            {
                int dt = System.DateTime.Now.Year;
                string date1Day = this.txtDOB.Text.Remove(2);
                string date1Month = this.txtDOB.Text.Substring(3, 2);
                string date1Year = this.txtDOB.Text.Substring(6);
                int date2year = Convert.ToInt32(date1Year.ToString());
                int datediff = dt - date2year;

                if (datediff >= 0)
                {
                    string age = Convert.ToString(datediff);
                    txtAge.Text = age.ToString();

                }
                else
                {
                    bool ErrFlag = false;
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Give Valid Date');", true);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Give Valid Date');", true);

                    ErrFlag = true;
                }
            }
            else
            {
                txtAge.Text = "";
            }
        }
        catch (Exception ex)
        {
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Give Valid Date');", true);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Give Valid Date');", true);

        }

    }
    protected void txtDOB_TextChanged(object sender, EventArgs e)
    {
        AgeCalc();
    }
    protected void txtRecruitThrg_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlUnit.SelectedValue = "-Select-";
        txtRecruitMobile.Text = "";
        if (txtRecruitThrg.SelectedItem.Text == "Recruitment Officer")
        {
            lblRecruit.Visible = true;
            lblAgent.Visible = false;
            txtRecruitmentName.Enabled = true;
            txtRecruitMobile.Enabled = true;
            ddlUnit.Enabled = true;
            txtExistingEmpNo.Enabled = false;
            txtExistingEmpName.Enabled = false;
            txtRefParentsName.Enabled = false;
            txtRefMobileNo.Enabled = false;
        }
        else if (txtRecruitThrg.SelectedItem.Text == "Agent")
        {
            lblAgent.Visible = true;
            lblRecruit.Visible = false;
            txtRecruitmentName.Enabled = true;
            txtRecruitMobile.Enabled = true;
            ddlUnit.Enabled = true;
            txtExistingEmpNo.Enabled = false;
            txtExistingEmpName.Enabled = false;
            txtRefParentsName.Enabled = false;
            txtRefMobileNo.Enabled = false;
        }
        else if (txtRecruitThrg.SelectedItem.Text == "Existing Employee")
        {
            lblAgent.Visible = false;
            lblRecruit.Visible = true;
            txtRecruitmentName.Enabled = false;
            txtRecruitMobile.Enabled = false;
            ddlUnit.Enabled = true;
            txtExistingEmpNo.Enabled = true;
            txtExistingEmpName.Enabled = true;
            txtRefParentsName.Enabled = false;
            txtRefMobileNo.Enabled = false;
        }
        else if (txtRecruitThrg.SelectedItem.Text == "Parents")
        {
            lblAgent.Visible = false;
            lblRecruit.Visible = true;
            txtRecruitmentName.Enabled = false;
            txtRecruitMobile.Enabled = false;
            ddlUnit.Enabled = true;
            txtExistingEmpNo.Enabled = false;
            txtExistingEmpName.Enabled = false;
            txtRefParentsName.Enabled = true;
            txtRefMobileNo.Enabled = true;
        }
        else
        {
            lblRecruit.Visible = true;
            lblAgent.Visible = false;
            txtRecruitmentName.Enabled = false;
            txtRecruitMobile.Enabled = false;
            ddlUnit.Enabled = false;
            txtExistingEmpNo.Enabled = false;
            txtExistingEmpName.Enabled = false;
            txtRefParentsName.Enabled = false;
            txtRefMobileNo.Enabled = false;
        }
        Load_Recruitment();
    }

    protected void btnEmpSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;
        string Employee_Save_Table = "";

        if (RdbPFEligible.SelectedValue == "1")
        {
            if (txtPFNo.Text == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the PF No..!');", true);
            }
            if (txtPFDate.Text == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the PF Date..!');", true);
            }
        }

        if (RdbESIEligible.SelectedValue == "1")
        {
            if (txtESINo.Text == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the ESI No..!');", true);
            }
            if (txtESIDate.Text == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the ESI Date..!');", true);
            }
        }

        if (rbtnSalaryThrough.SelectedValue == "2")
        {
            if (ddlBankName.SelectedValue == "-Select-")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the BankName..!');", true);
            }

            if (txtAccNo.Text == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Account Number..!');", true);
            }
        }

        if (!ErrFlag)
        {
            SSQL = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And EmpNo='" + txtMachineID.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT_Check.Rows.Count != 0)
            {
                Employee_Save_Table = "Employee_Mst";
            }
            else
            {
                Employee_Save_Table = "Employee_Mst_New_Emp";
            }
        }

        if (!ErrFlag)
        {
            //Check Token No
            SSQL = "";
            SSQL = "Select * from " + Employee_Save_Table + " Where CompCode='" + SessionCcode + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And MachineID != '" + txtMachineID.Text + "' And ExistingCode = '" + txtExistingCode.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT_Check.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This TOKEN NO Already Assign to Another Person...');", true);
                txtExistingCode.Focus();
            }
        }

        if (!ErrFlag)
        {
            if (rbtnSalaryThrough.SelectedValue == "2")
            {
                //Check Bank Account No
                SSQL = "";
                SSQL = "Select * from " + Employee_Save_Table + " Where CompCode='" + SessionCcode + "'";
                SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And MachineID != '" + txtMachineID.Text + "' And ExistingCode != '" + txtExistingCode.Text + "'";
                SSQL = SSQL + " And AccountNo='" + txtAccNo.Text + "'";
                DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT_Check.Rows.Count != 0)
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This ACCOUNT NO Already Assign to Another Person...');", true);
                    txtAccNo.Focus();
                }
            }
        }
        if (!ErrFlag)
        {
            if (RdbPFEligible.SelectedValue == "1")
            {
                //Check PF NO
                SSQL = "";
                SSQL = "Select * from " + Employee_Save_Table + " Where CompCode='" + SessionCcode + "'";
                SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And MachineID != '" + txtMachineID.Text + "' And ExistingCode != '" + txtExistingCode.Text + "'";
                SSQL = SSQL + " And PFNo='" + txtPFNo.Text + "'";
                DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT_Check.Rows.Count != 0)
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This PF NO Already Assign to Another Person...');", true);
                    txtPFNo.Focus();
                }
            }
        }

        if (!ErrFlag)
        {
            if (RdbESIEligible.SelectedValue == "1")
            {
                //Check ESI NO
                SSQL = "";
                SSQL = "Select * from " + Employee_Save_Table + " Where CompCode='" + SessionCcode + "'";
                SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And MachineID != '" + txtMachineID.Text + "' And ExistingCode != '" + txtExistingCode.Text + "'";
                SSQL = SSQL + " And ESINo='" + txtESINo.Text + "'";
                DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT_Check.Rows.Count != 0)
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This ESI NO Already Assign to Another Person...');", true);
                    txtPFNo.Focus();
                }
            }
        }

        if (!ErrFlag)
        {
            //Machine ID Encrypt
            string StrMachine_ID_Encrypt = "";
            StrMachine_ID_Encrypt = UTF8Encryption(txtMachineID.Text);

            //check State 

            string Other_State = "";
            if (chkOtherState.Checked == true)
            {
                Other_State = "Yes";
            }
            else
            {
                Other_State = "No";
            }
            string Same_ASPermanent = "";
            if (chkSame.Checked == true)
            {
                Same_ASPermanent = "1";
            }
            else
            {
                Same_ASPermanent = "0";
            }


            SSQL = "";
            SSQL = "Delete from " + Employee_Save_Table + " Where CompCode='" + SessionCcode + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And EmpNo='" + txtMachineID.Text + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "";
            SSQL = "Delete from Employee_Doc_Mst Where CompCode='" + SessionCcode + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And EmpNo='" + txtMachineID.Text + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);


            SSQL = "";
            SSQL = "Insert into " + Employee_Save_Table + " (CompCode,LocCode,TypeName,EmpPrefix,EmpNo";
            SSQL = SSQL + ",ExistingCode,MachineID,MachineID_Encrypt,MachineID_Encrypt_New,FirstName,LastName,MiddleInitial";
            SSQL = SSQL + ",Gender,BirthDate,Age,MaritalStatus,ShiftType,CatName,SubCatName";
            SSQL = SSQL + " ,DOJ,DeptCode,DeptName,Designation,Qualification,EmployeeMobile";
            SSQL = SSQL + ",OTEligible,Wages,Eligible_PF,PFNo,PFDOJ,Eligible_ESI,ESINo,ESIDOJ";
            SSQL = SSQL + ",UAN,PF_Code,ESICode,RoomNo,Vehicles_Type,BusRoute,BusNo,IsActive,EmpStatus";
            SSQL = SSQL + ",DOR,Reason,Emp_Permn_Date,Certificate,WeekOff,Salary_Through";
            SSQL = SSQL + ",BankName,IFSC_Code,BranchCode,AccountNo";
            SSQL = SSQL + ",BaseSalary,VPF,Alllowance1,Alllowance2,Deduction1,Deduction2";
            SSQL = SSQL + ",OT_Salary,Nationality,Religion,Height,Weight,StdWrkHrs,Calculate_Work_Hours";
            SSQL = SSQL + ",Handicapped,Handicapped_Reason,BloodGroup,RecuritmentThro";
            SSQL = SSQL + ",RecuritmentUnit,RecuriterName,RecutersMob,ExistingEmpNo,ExistingEmpName";
            SSQL = SSQL + ",WorkingUnit,SalaryUnit,Grade,Division,Nominee,FamilyDetails,ParentsPhone";
            SSQL = SSQL + ",MotnerName,parentsMobile,GuardianName,GuardianMobile";
            SSQL = SSQL + ",Address1,Taluk_Perm,Permanent_Dist,StateName,OtherState";
            SSQL = SSQL + ",SamepresentAddress,Taluk_Present,Present_Dist,Address2";
            SSQL = SSQL + ",IDMark1,IDMark2,RefParentsName,RefParentsMobile,LeaveFrom,LeaveTo,EmpLevel";
            SSQL = SSQL + ",Training_Status";
            SSQL = SSQL + ",Created_By,Created_Date";
            SSQL = SSQL + ") Values ( ";
            SSQL = SSQL + "'" + SessionCcode + "','" + SessionLcode + "','REGULAR','A',";
            SSQL = SSQL + "'" + txtTokenID.Text + "','" + txtExistingCode.Text + "','" + txtMachineID.Text + "','" + StrMachine_ID_Encrypt + "','" + StrMachine_ID_Encrypt + "',";
            SSQL = SSQL + "'" + txtFirstName.Text + "','" + txtLastName.Text + "','" + txtLastName.Text + "',";
            SSQL = SSQL + "'" + ddlGender.SelectedItem.Text + "','" + Convert.ToDateTime(txtDOB.Text).ToString("yyyy/MM/dd") + "',";
            SSQL = SSQL + "'" + txtAge.Text + "','" + ddlMartialStatus.SelectedItem.Text + "','" + ddlShift.SelectedItem.Text + "',";
            SSQL = SSQL + "'" + ddlCategory.SelectedItem.Text + "','" + ddlSubCategory.SelectedItem.Text + "',";
            SSQL = SSQL + "'" + Convert.ToDateTime(txtDOJ.Text).ToString("yyyy/MM/dd") + "','" + ddlDepartment.SelectedValue + "','" + ddlDepartment.SelectedItem.Text + "','" + ddlDesignation.SelectedItem.Text + "',";
            SSQL = SSQL + "'" + txtQulification.Text + "','+91-" + txtEmpMobileNo.Text + "','" + ddlOTEligible.SelectedItem.Text + "',";
            SSQL = SSQL + "'" + ddlWagesType.SelectedItem.Text + "','" + RdbPFEligible.SelectedValue + "','" + txtPFNo.Text + "',";
            SSQL = SSQL + "'" + txtPFDate.Text + "','" + RdbESIEligible.SelectedValue + "','" + txtESINo.Text + "','" + txtESIDate.Text + "',";
            SSQL = SSQL + "'" + txtUAN.Text + "','" + ddlPFCode.SelectedItem.Text + "','" + ddlESICode.SelectedItem.Text + "',";
            SSQL = SSQL + "'" + txtHostelRoom.Text + "','" + ddlVehicleType.SelectedItem.Text + "','" + txtVillage.SelectedItem.Text + "',";
            SSQL = SSQL + "'" + txtBusNo.SelectedItem.Text + "','" + dbtnActive.SelectedItem.Text + "','ON ROLL','" + txtReliveDate.Text + "',";
            SSQL = SSQL + "'" + txtReason.Text + "','" + txt480Days.Text + "','" + txtCertificate.Text + "','" + ddlWeekOff.SelectedItem.Text + "',";
            SSQL = SSQL + "'" + rbtnSalaryThrough.SelectedValue + "',";
            if (rbtnSalaryThrough.SelectedValue == "1")
            {
                SSQL = SSQL + "'','','','',";
            }
            else
            {
                SSQL = SSQL + "'" + ddlBankName.SelectedItem.Text + "','" + txtIFSC.Text + "','" + txtBranch.Text + "','" + txtAccNo.Text + "',";
            }
            SSQL = SSQL + "'" + txtBasic.Text + "','" + txtVPF.Text + "','" + txtAllowance1.Text + "','" + txtAllowance2.Text + "',";
            SSQL = SSQL + "'" + txtDeduction1.Text + "','" + txtDeduction2.Text + "','" + txtOTSal.Text + "','" + txtNationality.Text + "',";
            SSQL = SSQL + "'" + txtReligion.Text + "','" + txtHeight.Text + "','" + txtWeight.Text + "','" + txtStdWorkingHrs.Text + "','" + txtStdWorkingHrs.Text + "',";
            SSQL = SSQL + "'" + rbtnPhysically.SelectedItem.Text + "','" + txtPhyReason.Text + "','" + ddlBloodGrp.SelectedItem.Text + "',";
            SSQL = SSQL + "'" + txtRecruitThrg.SelectedItem.Text + "','" + ddlUnit.SelectedItem.Text + "','" + txtRecruitmentName.SelectedItem.Text + "',";
            SSQL = SSQL + "'" + txtRecruitMobile.Text + "','" + txtExistingEmpNo.Text + "','" + txtExistingEmpName.Text + "',";
            SSQL = SSQL + "'" + ddlWorkingUnit.SelectedItem.Text + "','" + ddlSalaryUnit.SelectedItem.Text + "','" + ddlGrade.SelectedItem.Text + "',";
            SSQL = SSQL + "'" + ddlDivision.SelectedItem.Text + "','" + txtNominee.Text + "','" + txtFatherName.Text + "','" + txtParentMob1.Text + "',";
            SSQL = SSQL + "'" + txtMotherName.Text + "','" + txtParentMob2.Text + "','" + txtGuardianName.Text + "','" + txtGuardianMobile.Text + "',";
            SSQL = SSQL + "'" + txtPermAddr.Text + "','" + txtPermTaluk.SelectedItem.Text + "','" + txtPermDist.SelectedItem.Text + "',";
            SSQL = SSQL + "'" + ddlState.SelectedItem.Text + "','" + Other_State + "','" + Same_ASPermanent + "',";
            SSQL = SSQL + "'" + txtTempTaluk.SelectedItem.Text + "','" + txtTempDist.SelectedItem.Text + "','" + txtTempAddr.Text + "',";
            SSQL = SSQL + "'" + txtIdenMark1.Text + "','" + txtIdenMark2.Text + "','" + txtRefParentsName.Text + "','" + txtRefMobileNo.Text + "',";
            SSQL = SSQL + "'" + txtLeaveFrom.Text + "','" + txtLeaveTo.Text + "','" + ddlEmpLevel.SelectedItem.Text + "',";
            if (ddlEmpLevel.SelectedItem.Text == "Trainee")
            {
                SSQL = SSQL + "'1',";
            }
            else if (ddlEmpLevel.SelectedItem.Text == "Semi-Exp")
            {
                SSQL = SSQL + "'2',";
            }
            else if (ddlEmpLevel.SelectedItem.Text == "Experienced")
            {
                SSQL = SSQL + "'3',";
            }
            SSQL = SSQL + "'" + SessionUserName + "',CURDATE()";
            SSQL = SSQL + ")";
            objdata.RptEmployeeMultipleDetails(SSQL);




            //Employee Level

            if (Employee_Save_Table == "Employee_Mst")
            {
                if (ddlEmpLevel.SelectedItem.Text == "Semi-Exp")
                {
                    DataTable DT_Level = new DataTable();

                    SSQL = "Select *from Training_Level_Change where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " And EmpNo='" + txtMachineID.Text + "'";
                    DT_Level = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (DT_Level.Rows.Count == 0)
                    {
                        query = "insert into Training_Level_Change(Ccode,Lcode,EmpNo,MachineID,ExistingCode,";
                        query = query + "Training_Level,Level_Date)values('" + SessionCcode + "','" + SessionLcode + "',";
                        query = query + "'" + txtMachineID.Text + "','" + txtMachineID.Text + "',";
                        query = query + "'" + txtExistingCode.Text + "','Semi-Exp',convert(varchar,CURDATE(),103))";
                        objdata.RptEmployeeMultipleDetails(query);
                    }
                }
            }



            //Emp Status Update
            string Emp_Status = "";
            if (Employee_Save_Table == "Employee_Mst_New_Emp")
            {
                Emp_Status = "Pending";
            }
            else
            {
                Emp_Status = "Completed";
            }

            DataTable EmpStatus = new DataTable();
            SSQL = "Select * from Employee_Mst_Status where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And MachineID='" + txtMachineID.Text + "'";
            EmpStatus = objdata.RptEmployeeMultipleDetails(SSQL);
            if (EmpStatus.Rows.Count != 0)
            {
                //Update
                SSQL = "Update Employee_Mst_Status set Token_No='" + txtExistingCode.Text + "',Emp_Status='" + Emp_Status + "',Cancel_Reson='' where";
                SSQL = SSQL + " CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + txtMachineID.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else
            {
                //Insert
                SSQL = "Insert Into Employee_Mst_Status(CompCode,LocCode,Token_No,MachineID,Emp_Status,Cancel_Reson)";
                SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + txtExistingCode.Text + "','" + txtMachineID.Text + "','" + Emp_Status + "','')";
                objdata.RptEmployeeMultipleDetails(SSQL);
                //InsertDeleteUpdate_Lunch_Server(SSQL)
            }

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                query = "Insert Into Employee_Doc_Mst(CompCode,LocCode,EmpNo,DocType,DocNo,";
                query = query + "Created_By,Created_Date) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + txtMachineID.Text + "',";
                query = query + " '" + dt.Rows[i]["DocType"].ToString() + "','" + dt.Rows[i]["DocNo"].ToString() + "',";
                query = query + " '" + SessionUserName + "',CURDATE())";
                objdata.RptEmployeeMultipleDetails(query);
            }

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Employee Details Saved Successfully..!');", true);
            Session.Remove("MachineID");
            Response.Redirect("Employee_Main.aspx");
        }
    }

    protected void txtMachineID_TextChanged(object sender, EventArgs e)
    {
        DataTable DT = new DataTable();

        SSQL = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And EmpNo='" + txtMachineID.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT.Rows.Count == 0)
        {
            SSQL = "Select * from Employee_Mst_New_Emp where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And EmpNo='" + txtMachineID.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count != 0)
            {
                txtTokenID.Text = "";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Already Employee No Exists!');", true);
            }
            else
            {
                txtTokenID.Text = txtMachineID.Text;
            }

        }
        else
        {
            txtTokenID.Text = "";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Already Employee No Exists!');", true);
        }
    }

    protected void chkSame_CheckedChanged(object sender, EventArgs e)
    {
        if (chkSame.Checked == true)
        {
            txtTempAddr.Text = txtPermAddr.Text;
        }
        else
        {
            txtTempAddr.Text = "";
        }
    }

    private static string UTF8Encryption_OLD(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }

    public string UTF8Encryption(string mvarPlanText)
    {
        string cipherText = "";
        try
        {
            string passPhrase = "Altius";
            string saltValue = "info@altius.co.in";
            string hashAlgorithm = "SHA1";
            string initVector = "@1B2c3D4e5F6g7H8";
            int passwordIterations = 2;
            int keySize = 256;
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(mvarPlanText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);
            byte[] keyBytes = password.GetBytes(keySize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            cipherText = Convert.ToBase64String(cipherTextBytes);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

        return cipherText;
    }

    private static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = string.Empty;
        UTF8Encoding encodepwd = new UTF8Encoding();
        Decoder Decode = encodepwd.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        decryptpwd = new String(decoded_char);
        return decryptpwd;
    }

    protected void btnEmpClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        txtMachineID.Text = ""; txtExistingCode.Text = "";
        txtTokenID.Text = ""; ddlCategory.SelectedValue = "0";
        ddlSubCategory.SelectedValue = "0"; ddlShift.SelectedValue = "0";
        txtFirstName.Text = ""; txtLastName.Text = ""; txtDOB.Text = ""; txtAge.Text = "";
        ddlGender.SelectedValue = "0"; txtDOJ.Text = ""; ddlDepartment.SelectedValue = "0";
        ddlDesignation.SelectedValue = "-Select-"; txtQulification.Text = ""; txtEmpMobileNo.Text = "";
        ddlOTEligible.SelectedValue = "-Select-"; ddlWagesType.SelectedValue = "-Select-"; ddlEmpLevel.SelectedValue = "-Select-";
        RdbPFEligible.SelectedValue = "2"; chkExment.Checked = false; txtPFNo.Text = ""; txtPFDate.Text = "";
        RdbESIEligible.SelectedValue = "2"; txtESINo.Text = ""; txtESIDate.Text = "";
        txtUAN.Text = ""; txtHostelRoom.Text = ""; ddlVehicleType.SelectedValue = "-Select-";
        txtVillage.SelectedValue = "-Select-"; txtBusNo.SelectedValue = "-Select-"; dbtnActive.SelectedValue = "1";
        txtReliveDate.Text = ""; txtReason.Text = ""; txt480Days.Text = "";
        txtCertificate.Text = ""; ddlWeekOff.SelectedValue = "-Select-";
        rbtnSalaryThrough.SelectedValue = "1"; txtAccNo.Text = "";
        txtBasic.Text = "0.0"; txtVPF.Text = "0.0"; txtAllowance1.Text = "0.0";
        txtAllowance2.Text = "0.0"; txtDeduction1.Text = "0.0"; txtDeduction2.Text = "0.0";
        txtOTSal.Text = "0.0"; ddlMartialStatus.SelectedValue = "0"; txtNationality.Text = "INDIAN";
        txtReligion.Text = ""; txtHeight.Text = ""; txtWeight.Text = ""; txtStdWorkingHrs.Text = "";
        rbtnPhysically.SelectedValue = "2"; txtPhyReason.Text = ""; ddlBloodGrp.SelectedValue = "0";
        txtRecruitThrg.SelectedValue = "-Select-"; txtRecruitMobile.Text = ""; txtExistingEmpNo.Text = "";
        txtExistingEmpName.Text = ""; ddlUnit.SelectedValue = "-Select-"; ddlWorkingUnit.SelectedValue = SessionLcode;
        ddlSalaryUnit.SelectedValue = SessionLcode; ddlGrade.SelectedValue = "-Select-"; ddlDivision.SelectedValue = "-Select-";
        txtNominee.Text = ""; txtFatherName.Text = ""; txtParentMob1.Text = ""; txtMotherName.Text = ""; txtParentMob2.Text = "";
        txtGuardianName.Text = ""; txtGuardianMobile.Text = ""; txtPermAddr.Text = ""; txtPermDist.SelectedValue = "-Select-";
        txtPermTaluk.SelectedValue = "-Select-"; ddlState.SelectedValue = "-Select-"; chkOtherState.Checked = false;
        txtTempTaluk.SelectedValue = "-Select-"; txtTempDist.SelectedValue = "-Select-"; txtTempAddr.Text = "";
        chkSame.Checked = false; txtIdenMark1.Text = ""; txtIdenMark2.Text = "";
        ddlDocType.SelectedValue = "-Select-"; txtDocNo.Text = "";

        Load_ESICode();
        Load_PFCode();
        Load_Bank();
        Load_Recruitment();
        Load_Department();
        Load_Designation();
        lblRecruit.Visible = true; lblAgent.Visible = false; txtRecruitmentName.Enabled = false;
        txtRecruitMobile.Enabled = false; ddlUnit.Enabled = false; txtExistingEmpNo.Enabled = false;
        txtExistingEmpName.Enabled = false;


        ddlDocType.SelectedValue = "0";
        txtDocNo.Text = "";
        Initial_Data_Referesh(); ;
        Load_OLD_data();
        txtReason.Enabled = false; txtPhyReason.Enabled = false;
        txtMachineID.Enabled = true;
        Session.Remove("MachineID");
        btnEmpSave.Text = "Save";
    }

    protected void txtRecruitmentName_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        if (txtRecruitThrg.SelectedItem.Text == "Agent")
        {
            query = "Select * from MstAgent";
            dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        }
        else if (txtRecruitThrg.SelectedItem.Text == "Recruitment Officer")
        {
            query = "Select * from MstRecruitOfficer";
            dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        }
        if (dtdsupp.Rows.Count != 0)
        {
            if (txtRecruitThrg.SelectedItem.Text == "Recruitment Officer")
            {
                txtRecruitmentName.SelectedValue = dtdsupp.Rows[0]["ROName"].ToString();
                ddlUnit.SelectedValue = dtdsupp.Rows[0]["Unit"].ToString();
                txtRecruitMobile.Text = dtdsupp.Rows[0]["Mobile"].ToString();
            }
            else if (txtRecruitThrg.SelectedItem.Text == "Agent")
            {
                txtRecruitmentName.SelectedValue = dtdsupp.Rows[0]["AgentName"].ToString();
                ddlUnit.SelectedValue = "-Select-";
                txtRecruitMobile.Text = dtdsupp.Rows[0]["Mobile"].ToString();
            }
        }
        else
        {
            txtRecruitmentName.SelectedValue = "-Select-";
            ddlUnit.SelectedValue = "-Select-";
            txtRecruitMobile.Text = "";
        }
    }

    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        string qry = "";

        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["DocNo"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();

    }

    protected void btnDocAdd_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string query = "";

        if (ddlDocType.SelectedItem.Text == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Document Type...');", true);
        }

        if (txtDocNo.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Document No.');", true);
        }

        if (ddlDocType.SelectedItem.Text == "Adhar Card")
        {
            if ((txtDocNo.Text).Length != 12 || (txtDocNo.Text).Length > 12)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check Your ADHAR NO...Only 12 digit Allowed for ADHAR NO.');", true);
            }
        }
        if (ddlDocType.SelectedItem.Text == "Voter Card")
        {
            if ((txtDocNo.Text).Length != 10 || (txtDocNo.Text).Length > 10)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check Your VOTER ID NO...Only 10 digit Allowed for VOTER ID NO.');", true);
            }
        }

        if (ddlDocType.SelectedItem.Text == "Ration Card")
        {
            if ((txtDocNo.Text).Length != 12 || (txtDocNo.Text).Length > 12)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check Your RATION CARD NO...Only 12 digit Allowed for RATION CARD NO.');", true);
            }
        }

        if (ddlDocType.SelectedItem.Text == "Pan Card")
        {
            if ((txtDocNo.Text).Length != 10 || (txtDocNo.Text).Length > 10)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check Your PAN CARD NO...Only 10 digit Allowed for PAN CARD NO.');", true);
            }
        }

        if (ddlDocType.SelectedItem.Text == "Driving Licence")
        {
            if ((txtDocNo.Text).Length != 16 || (txtDocNo.Text).Length > 16)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check Your Driving Licence No...Only 16 digit Allowed for Driving Licence No.');", true);
            }
        }
        if (ddlDocType.SelectedItem.Text == "Smart Card")
        {
            if ((txtDocNo.Text).Length != 19 || (txtDocNo.Text).Length > 19)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check Your SMART CARD NO...Only 19 digit Allowed for SMART CARD NO.');", true);
            }
        }

        if (!ErrFlag)
        {
            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if ((dt.Rows[i]["DocType"].ToString().ToUpper() == ddlDocType.SelectedItem.Text.ToUpper()))
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Document Already Added..');", true);
                    }
                }



                if (!ErrFlag)
                {

                    dr = dt.NewRow();
                    dr["DocType"] = ddlDocType.SelectedItem.Text;
                    dr["DocNo"] = txtDocNo.Text;

                    string Shade_Name = ddlDocType.SelectedItem.Text + "_" + txtMachineID.Text;//txtShadeName.SelectedItem.Text + "-" + txtShadeNo.Text;

                    string FileName = Path.GetFileName(filUpload.PostedFile.FileName);
                    if (filUpload.HasFile)
                    {
                        string Exten = Path.GetExtension(filUpload.PostedFile.FileName);
                        if (Exten == ".jpg")
                        {
                            //filUpload.SaveAs(Server.MapPath("~/Images/" + Shade_Name + Exten));
                            filUpload.SaveAs("D:/Images/" + Shade_Name.Trim() + Exten);
                        }
                    }

                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();

                    //Totalsum();


                    ddlDocType.SelectedValue = "0";

                    txtDocNo.Text = "";

                }
            }
            else
            {
                dr = dt.NewRow();
                dr["DocType"] = ddlDocType.SelectedItem.Text;
                dr["DocNo"] = txtDocNo.Text;


                string Shade_Name = ddlDocType.SelectedItem.Text + "_" + txtMachineID.Text;//txtShadeName.SelectedItem.Text + "-" + txtShadeNo.Text;

                string FileName = Path.GetFileName(filUpload.PostedFile.FileName);
                if (filUpload.HasFile)
                {
                    string Exten = Path.GetExtension(filUpload.PostedFile.FileName);
                    if (Exten == ".jpg")
                    {
                        //filUpload.SaveAs(Server.MapPath("~/Images/" + Shade_Name + Exten));
                        filUpload.SaveAs("D:/Images/" + Shade_Name.Trim() + Exten);
                    }
                }


                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();

                //Totalsum();


                ddlDocType.SelectedValue = "0";

                txtDocNo.Text = "";

            }
        }
    }
    protected void rbtnPhysically_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rbtnPhysically.SelectedValue == "1")
        {
            txtPhyReason.Enabled = true;
        }
        else
        {
            txtPhyReason.Enabled = false;
        }
    }
    protected void dbtnActive_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (dbtnActive.SelectedValue == "2")
        {
            txtReason.Enabled = true;
        }
        else
        {
            txtReason.Enabled = false;
        }
    }
    protected void ddlVehicleType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Route();
    }
    protected void txtVillage_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_BusNo();
    }
}
